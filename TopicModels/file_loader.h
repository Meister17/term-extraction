// Copyright 2013 Michael Nokel
#ifndef TOPICMODELS_FILE_LOADER_H_
#define TOPICMODELS_FILE_LOADER_H_

#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "./auxiliary.h"
#include "./similar_ngrams_finder.h"

/**
  @brief Class for parsing and loading input files

  This class should be used for parsing and loading input and vocabulary files.
*/
class FileLoader {
 public:
  /**
    Initializes object
    @param seed_number seeding number for generating random numbers
  */
  explicit FileLoader(int seed_number)
      : SEED_NUMBER_(seed_number) {
  }

  /**
    Parses and loads file with vocabulary
    @param filename file with vocabulary
    @param[out] vocabulary vector where words will be placed
    @throw std::runtime_error in case of any occurred error
  */
  void ParseVocabularyFile(const std::string& filename,
                           std::vector<std::string>* vocabulary);

  /**
    Parses file containing sets of similar words
    @param filename file containing sets of similar words
    @param vocabulary vector containing vocabulary
    @throw std::runtime_error in case of any occurred error
  */
  void ParseSimilarSetsFile(const std::string& filename,
                            const std::vector<std::string>& vocabulary);

  /**
    Parses and loads file with input data
    @param filename file with input data
    @param vocabulary already parsed vocabulary
    @param[out] source_data vector containing hash map of words with their
    frequencies in the documents
    @throw std::runtime_error in case of any occurred error
  */
  void ParseInputFile(
      const std::string& filename,
      const std::vector<std::string>& vocabulary,
      std::vector<std::unordered_map<size_t, size_t>>* source_data) const;

  /**
    Splits data to train and test parts
    @param vocabulary vector containing source data vocabulary
    @param source_data vector containing source data
    @param test_part part of the text collection to be used in the test
    @param[out] train_vocabulary vector where new vocabulary with words only in
    the train part will be stored
    @param[out] train_data vector where train data will be stored on which
    P(w|t) will be estimated
    @param[out] test_train_data vector where part of the test data will be
    placed on which P(t|d) will be estimated
    @param[out] test_data vector where part of the test data will be placed on
    which perplexity will be further calculated
  */
  void SplitToTrainAndTest(
      const std::vector<std::string>& vocabulary,
      const std::vector<std::unordered_map<size_t, size_t>>& source_data,
      double test_part,
      std::vector<std::string>* train_vocabulary,
      std::vector<std::unordered_map<size_t, size_t>>* train_data,
      std::vector<std::unordered_map<size_t, size_t>>* test_train_data,
      std::vector<std::unordered_map<size_t, size_t>>* test_data)
      const noexcept;

  /**
    Prints words within each revealed topic in the descending order of their
    probabilities P(w|t)
    @param filename file where words within each revealed topic will be printed
    @param vocabulary vector containing words
    @param phi vector containing P(w|t) probabilities
    @param top_words_number number of top words to print for each topic
    @throw std::runtime_error in case of any occurred error
  */
  void PrintTopicWords(const std::string& filename,
                       const std::vector<std::string>& vocabulary,
                       const std::vector<std::vector<double>>& phi,
                       int top_words_number) const;

  /**
    Checks whether given ngrams are similar or not
    @param first_ngram first ngram to check
    @param second_ngram second ngram to check
    @return true if given ngrams are similar
  */
  bool AreSimilarNGrams(size_t first_ngram, size_t second_ngram) const noexcept;

 private:
  /** Seeding number for generating random numbers */
  const int SEED_NUMBER_;

  /** Default frequency to insert (equals to one) */
  const size_t DEFAULT_FREQUENCY_ = 1;

  /** Object used for finding similar ngrams */
  SimilarNGramsFinder similar_ngrams_finder_;

 private:
  /**
    Stores new document into the vector of documents
    @param tokens vector containing parsed tokens word:number_occurrences
    @param ngrams hash set containing ngrams from similar words
    @param[out] source_data vector with words in documents
    @throw std::runtime_error in case of any occurred error
  */
  void StoreNewDocument(
      const std::vector<std::string>& tokens,
      const std::unordered_set<size_t>& ngrams,
      std::vector<std::unordered_map<size_t, size_t>>* source_data) const;

  /**
    Appends given vector to the forming vector of test data
    @param words_in_document vector containing words in document
    @param start_index index of the first word to add
    @param end_index index of the last word (+1) to add
    @param[out] test_data forming vector of test data where given words should
    be appended
  */
  void AppendToTestData(
      const std::vector<size_t>& words_in_document,
      size_t start_index,
      size_t end_index,
      std::vector<std::unordered_map<size_t, size_t>>* test_data)
      const noexcept;
};

#endif  // TOPICMODELS_FILE_LOADER_H_
