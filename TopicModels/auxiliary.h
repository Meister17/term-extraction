// Copyright 2013 Michael Nokel
#ifndef TOPICMODELS_AUXILIARY_H_
#define TOPICMODELS_AUXILIARY_H_

#include <algorithm>
#include <vector>


/**
  @brief Enum class containing all possible topic model algorithms

  Currently it supports only PLSA and LDA topic models, while NO_TOPIC_MODEL
  indicates error
*/
enum class TopicModelAlgorithm {
  NO_TOPIC_MODEL,
  PLSA,
  LDA
};

/**
  @brief Enum class containing all possible evaluation measures

  Currently it supports only Perplexity, TC-PMI, TC_PMI_NSIM, while
  NO_MEASURE indicates error
*/
enum class EvaluationMeasure {
  NO_MEASURE,
  PERPLEXITY,
  TC_PMI,
  TC_PMI_NSIM
};

/**
  @brief Structure representing bigram

  This structure contains indices of the first and second unigrams forming
  bigram
*/
struct Bigram {
  Bigram(size_t first_word, size_t second_word)
      : first_unigram(first_word),
        second_unigram(second_word) {
  }

  /** First unigram in the bigram */
  size_t first_unigram;

  /** Second unigram in the bigram */
  size_t second_unigram;

  /**
    Comparison operator for organizing a set
    @param other other bigram to compare with
    @return true if current bigram is less than given another one
  */
  bool operator<(const Bigram& other) const noexcept {
    if (first_unigram == other.first_unigram) {
      return second_unigram < other.second_unigram;
    }
    return first_unigram < other.first_unigram;
  }
};

#endif  // TOPICMODELS_AUXILIARY_H_
