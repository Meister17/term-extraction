// Copyright 2013 Michael Nokel
#include <iostream>
#include <stdexcept>
#include "./program_options_parser.h"
#include "./topic_model_analyzer.h"

using std::cerr;
using std::exception;

/**
  Main function that starts the whole program
  @param argc number of command line arguments
  @param argv command line arguments
  @return code of success
*/
int main(int argc, char** argv) {
  try {
    ProgramOptionsParser program_options_parser;
    program_options_parser.Parse(argc, argv);
    TopicModelAnalyzer topic_model_analyzer(
        program_options_parser.logging_directory_name(),
        program_options_parser.seed_number(),
        program_options_parser.test_part(),
        program_options_parser.topics_number(),
        program_options_parser.top_words_tc_number());
    topic_model_analyzer.Init(program_options_parser.input_filename(),
                              program_options_parser.vocabulary_filename(),
                              program_options_parser.similar_sets_filename());
    topic_model_analyzer.EstimateModel(
        program_options_parser.algorithm(),
        program_options_parser.algorithm_options(),
        program_options_parser.evaluation_measure(),
        program_options_parser.inverted_index_filename());
    topic_model_analyzer.PrintResults(
        program_options_parser.top_words_number(),
        program_options_parser.topic_words_filename());
  } catch (const exception& except) {
    cerr << "Fatal error occurred: " << except.what() << "\n";
    return 1;
  }
  return 0;
}
