// Copyright 2013 Michael Nokel
#ifndef TOPICMODELS_TOPIC_MODEL_ANALYZER_H_
#define TOPICMODELS_TOPIC_MODEL_ANALYZER_H_

#include <boost/thread.hpp>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>
#include "./auxiliary.h"
#include "./file_loader.h"
#include "./topic_models/topic_model.h"

/**
  Main class for revealing and evaluating topics in text collection.

  This class should be used to parse input files, reveal and evaluate topics,
  and print words within each topic.
*/
class TopicModelAnalyzer {
 public:
  /**
    Initializes object
    @param logging_directory_name directory for logging information
    @param seed_number seeding number for generating random numbers
    @param test_part part of text collection to use for test
    @param topics_number desired number of topics
    @param top_words_tc_number number of top words to consider in Topic
    Coherence measures
  */
  TopicModelAnalyzer(const std::string& logging_directory_name,
                     int seed_number,
                     double test_part,
                     int topics_number,
                     int top_words_tc_number)
      : LOGGING_DIRECTORY_NAME_(logging_directory_name),
        SEED_NUMBER_(seed_number),
        TEST_PART_(test_part),
        TOPICS_NUMBER_(topics_number),
        TOP_WORDS_TC_NUMBER_(top_words_tc_number),
        file_loader_(seed_number) {
  }

  /**
    Parses files with input data and vocabulary
    @param input_filename file with input data
    @param vocabulary_filename file with vocabulary
    @param similar_sets_filename file with sets of similar words
    @throw std::runtime_error in case of any occurred error
  */
  void Init(const std::string& input_filename,
            const std::string& vocabulary_filename,
            const std::string& similar_sets_filename);

 protected:
  /** Directory for logging information */
  const std::string LOGGING_DIRECTORY_NAME_ = "";

  /** Number for seeding random numbers */
  const int SEED_NUMBER_ = 239;

  /** Part of text collection to use as a test */
  const double TEST_PART_ = 0.1;

  /** Desired number of topics to reveal */
  const int TOPICS_NUMBER_ = 100;

  /** Number of top words to consider in Topic Coherence measures */
  const int TOP_WORDS_TC_NUMBER_ = 10;

  /** Number of threads to use in the thread pool */
  const int NUMBER_THREADS_ = 2;//boost::thread::hardware_concurrency();

  /** Object for loading input files */
  FileLoader file_loader_;

  /** Vocabulary */
  std::vector<std::string> vocabulary_;

  /** Vector containing words and their frequencies within each document */
  std::vector<std::unordered_map<size_t, size_t>> source_data_;

  /** Pointerfor particular topic model algorithm (for training on the subset of
      the whole collection) */
  std::shared_ptr<TopicModel> perplexity_topic_model_;

  /** Pointer for particular topic model algorithm (for training on the whole
      collection) */
  std::shared_ptr<TopicModel> train_set_topic_model_;

 public:
  /**
    Estimates and evaluates topic model
    @param algorithm topic model algorithm to use for revealing topics
    @param algorithm_options vector containing options for topic model algorithm
    @param evaluation_measures vector containing evaluation measures to compute
    @param inverted_index_filename file containing inverted index for the given
    text collection from Wikipedia
  */
  void EstimateModel(const TopicModelAlgorithm& algorithm,
                     const std::vector<std::string>& algorithm_options,
                     const std::vector<EvaluationMeasure>& evaluation_measures,
                     const std::string& inverted_index_filename);

  /**
    Prints results of revealing topics in the text collection
    @param top_words_number number of top words to print for each topic
    @param topic_words_filename file where words within each topic will be
    printed
    @throw std::runtime_error in case of any occurred error
  */
  void PrintResults(int top_words_number,
                    const std::string& topic_words_filename) const;

 private:
  /**
    Initializes topic model algorithm
    @param algorithm topic model algorithm to use for revealing topics
    @param algorithm_options vector containing options for topic model algorithm
    @param compute_perplexity flag indicating whether to compute perplexity or
    not
    @param compute_train_set_measures flag indicating whether to compute train
    set measures or not
  */
  void InitializeAlgorithm(const TopicModelAlgorithm& algorithm,
                           const std::vector<std::string>& algorithm_options,
                           bool compute_perplexity,
                           bool compute_train_set_measures);
  /**
    Calculates Perplexity on the test set
    @param store_new_vocabulary flag indicating whether new train vocabulary
    should be stored
  */
  void CalculatePerplexity(bool store_new_vocabulary) noexcept;

  /**
    Calculates given evaluation measures on the whole train set
    @param evaluation_measures vector containing evaluation measures to compute
    @param inverted_index_filename file containing inverted index of the text
    collection based on Wikipedia
  */
  void CalculateTrainSetMeasures(
      const std::vector<EvaluationMeasure>& evaluation_measures,
      const std::string& inverted_index_filename) const noexcept;
};

#endif  // TOPICMODELS_TOPIC_MODEL_ANALYZER_H_
