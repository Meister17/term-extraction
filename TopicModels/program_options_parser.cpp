// Copyright 2013 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>
#include "./auxiliary.h"
#include "./program_options_parser.h"

using boost::program_options::notify;
using boost::program_options::options_description;
using boost::program_options::parse_command_line;
using boost::program_options::parse_config_file;
using boost::program_options::store;
using boost::program_options::value;
using boost::program_options::variables_map;
using boost::algorithm::to_lower;
using boost::filesystem::create_directories;
using boost::filesystem::path;
using boost::is_any_of;
using boost::split;
using std::cout;
using std::runtime_error;
using std::string;
using std::vector;

void ProgramOptionsParser::Parse(int argc, char** argv) {
  variables_map program_options_storage;
  string configuration_filename = "";
  string algorithm_string = "";
  string algorithm_options_string = "";
  string evaluation_measure_string = "";
  options_description program_options_description("Options for program:");
  program_options_description.add_options()
      ("help,h", value<bool>(&help_message_printed_)->implicit_value(true),
          "Print help message and exit")
      ("config_filename,c", value<string>(&configuration_filename),
          "Configuration file")
      ("seed_number", value<int>(&seed_number_), "Seeding number")
      ("input_filename", value<string>(&input_filename_), "Input file")
      ("vocabulary_filename", value<string>(&vocabulary_filename_),
          "Vocabulary file")
      ("similar_sets_filename", value<string>(&similar_sets_filename_),
          "File containing sets of similar words")
      ("inverted_index_filename",
          value<string>(&inverted_index_filename_),
          "File containing pre-computed inverted index")
      ("topics_number", value<int>(&topics_number_), "Number of topics")
      ("algorithm", value<string>(&algorithm_string), "Topic model algorithm")
      ("algorithm_options", value<string>(&algorithm_options_string),
          "Options for topic model algorithm")
      ("evaluation_measure", value<string>(&evaluation_measure_string),
          "Evaluation measure to use")
      ("test_part", value<double>(&test_part_),
          "Part of the collection to use as test")
      ("top_words_number", value<int>(&top_words_number_),
          "Number of top words to print for each topic")
      ("top_words_tc_number", value<int>(&top_words_tc_number_),
          "Number of top words to consider in TCP measures")
      ("topic_words_filename", value<string>(&topic_words_filename_),
          "File for printing words for each topic")
      ("logging_directory_name", value<string>(&logging_directory_name_),
          "Directory for logging");
  store(parse_command_line(argc, argv, program_options_description),
        program_options_storage);
  notify(program_options_storage);
  if (!configuration_filename.empty()) {
    store(parse_config_file<char>(configuration_filename.data(),
                                  program_options_description),
          program_options_storage);
    notify(program_options_storage);
  }
  if (help_message_printed_) {
    cout << program_options_description << "\n";
  }
  algorithm_ = ParseTopicModelAlgorithm(algorithm_string);
  evaluation_measures_ = ParseEvaluationMeasures(evaluation_measure_string);
  if (!algorithm_options_string.empty()) {
    algorithm_options_ = ParseAlgorithmOptions(algorithm_options_string);
  }
  if (top_words_number_ <= 0 || top_words_tc_number_ <= 0 ||
      topics_number_ <= 0 || test_part_ <= 0 || test_part_ >= 1 ||
      evaluation_measures_.empty()) {
    throw runtime_error("Wrong program options have been specified");
  }
  CorrectDirectoryName(&logging_directory_name_);
}

TopicModelAlgorithm ProgramOptionsParser::ParseTopicModelAlgorithm(
    const string& algorithm_string) const {
  string algorithm = algorithm_string;
  to_lower(algorithm);
  if (algorithm == "plsa" || algorithm == "plsi") {
    return TopicModelAlgorithm::PLSA;
  } else if (algorithm == "lda") {
    return TopicModelAlgorithm::LDA;
  }
  throw runtime_error("Wrong topic model");
}

vector<EvaluationMeasure> ProgramOptionsParser::ParseEvaluationMeasures(
    const string& measure_string) const {
  vector<EvaluationMeasure> evaluation_measures;
  string measures = measure_string;
  to_lower(measures);
  vector<string> measures_vector;
  split(measures_vector, measures, is_any_of(","));
  for (const auto& measure : measures_vector) {
    if (measure == "perplexity") {
      evaluation_measures.push_back(EvaluationMeasure::PERPLEXITY);
    } else if (measure == "tc-pmi") {
      evaluation_measures.push_back(EvaluationMeasure::TC_PMI);
    } else if (measure == "tc-pmi-nsim") {
      evaluation_measures.push_back(EvaluationMeasure::TC_PMI_NSIM);
    } else {
      throw runtime_error("Wrong evaluation measure");
    }
  }
  return evaluation_measures;
}

vector<string> ProgramOptionsParser::ParseAlgorithmOptions(
    const string& options_string) const noexcept {
  string options = options_string;
  to_lower(options);
  vector<string> algorithm_options;
  split(algorithm_options, options, is_any_of(","));
  return algorithm_options;
}

void ProgramOptionsParser::CorrectDirectoryName(string* directory_name)
    const {
  path path_separator("/");
  string path_separator_string = path_separator.make_preferred().native();
  if (directory_name->back() != path_separator_string[0]) {
    directory_name->push_back(path_separator_string[0]);
  }
  create_directories(*directory_name);
}
