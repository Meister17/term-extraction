// Copyright 2014 Michael Nokel
#ifndef TOPICMODELS_SIMILAR_NGRAMS_FINDER_H_
#define TOPICMODELS_SIMILAR_NGRAMS_FINDER_H_

#include <string>
#include <vector>


class SimilarNGramsFinder {
 public:
  /**
    Parses file containing sets with similar words
    @param filename file containing sets with similar words
    @param vocabulary vector containing vocabulary
    @throw std::runtime_error in case of any occurred error
  */
  void ParseSimilarSetsFile(const std::string& filename,
                            const std::vector<std::string>& vocabulary);

 private:
  /** Vector containing sets of similar words */
  std::vector<std::vector<size_t>> similar_sets_;

 public:
  /**
    Checks whether given ngrams are similar
    @param first_ngram first ngram to check
    @param second_ngram second ngram to check
    @return true if given ngrams are similar
  */
  bool AreSimilarNGrams(size_t first_ngram, size_t second_ngram) const noexcept;

  /**
    Returns iterator to the beginning of the vector containing similar sets
    @return iterator to the beginning of the vector containing similar sets
  */
  std::vector<std::vector<size_t>>::const_iterator GetStartIterator()
      const noexcept {
    return similar_sets_.begin();
  }

  /**
    Returns iterator to the end of the vector containing similar sets
    @return iterator to the end of the vector containing similar sets
  */
  std::vector<std::vector<size_t>>::const_iterator GetEndIterator()
      const noexcept {
    return similar_sets_.end();
  }
};

#endif  // TOPICMODELS_SIMILAR_NGRAMS_FINDER_H_
