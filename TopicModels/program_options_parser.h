// Copyright 2013 Michael Nokel
#ifndef TOPICMODELS_PROGRAM_OPTIONS_PARSER_H_
#define TOPICMODELS_PROGRAM_OPTIONS_PARSER_H_

#include <string>
#include <vector>
#include "./auxiliary.h"

/**
  @brief Class for working with program options

  This class should be used for parsing program options from command line and/or
  from configuration file. After what one can access each necessary program
  option.
*/
class ProgramOptionsParser {
 public:
  /**
    Parses program options from command line and/or from configuration file
    @param argc number of command line arguments (from main function)
    @param argv command line arguments (from main function)
    @throw std::exception in case of any occurred error
  */
  void Parse(int argc, char** argv);

 private:
  /** Flag indicating whether help message has been printed or not */
  bool help_message_printed_ = false;

  /** Seed number for generating random numbers */
  int seed_number_ = 239;

  /** File containing input data for topic model algorithm */
  std::string input_filename_ = "";

  /** File containing vocabulary for topic model algorithm */
  std::string vocabulary_filename_ = "";

  /** File containing sets of similar n-grams */
  std::string similar_sets_filename_ = "";

  /** File containing pre-computed inverted index (via Wikipedia) */
  std::string inverted_index_filename_ = "";

  /** Desired number of topics to reveal */
  int topics_number_ = 100;

  /** Topic model algorithm to perform */
  TopicModelAlgorithm algorithm_ = TopicModelAlgorithm::NO_TOPIC_MODEL;

  /** Options of topic model algorithm */
  std::vector<std::string> algorithm_options_;

  /** Evaluation measures to compute */
  std::vector<EvaluationMeasure> evaluation_measures_;

  /** Part of collection to use as a test */
  double test_part_ = 0.1;

  /** Number of top words to print */
  int top_words_number_ = 5000;

  /** Number of top words to consider in Topic Coherence measures */
  int top_words_tc_number_ = 10;

  /** File for printing words that form each topic */
  std::string topic_words_filename_ = "";

  /** Directory for logging */
  std::string logging_directory_name_ = "";

 public:
  bool help_message_printed() const noexcept {
    return help_message_printed_;
  }

  int seed_number() const noexcept {
    return seed_number_;
  }

  std::string input_filename() const noexcept {
    return input_filename_;
  }

  std::string vocabulary_filename() const noexcept {
    return vocabulary_filename_;
  }

  std::string similar_sets_filename() const noexcept {
    return similar_sets_filename_;
  }

  std::string inverted_index_filename() const noexcept {
    return inverted_index_filename_;
  }

  int topics_number() const noexcept {
    return topics_number_;
  }

  TopicModelAlgorithm algorithm() const noexcept {
    return algorithm_;
  }

  std::vector<std::string> algorithm_options() const noexcept {
    return algorithm_options_;
  }

  std::vector<EvaluationMeasure> evaluation_measure() const noexcept {
    return evaluation_measures_;
  }

  double test_part() const noexcept {
    return test_part_;
  }

  int top_words_number() const noexcept {
    return top_words_number_;
  }

  int top_words_tc_number() const noexcept {
    return top_words_tc_number_;
  }

  std::string topic_words_filename() const noexcept {
    return topic_words_filename_;
  }

  std::string logging_directory_name() const noexcept {
    return logging_directory_name_;
  }

 private:
  /**
    Parse topic model algorithm from its string representation
    @param algorithm_string string representation of topic model algorithm
    @return enum value that corresponds to given topic model algorithm
    @throw std::runtime_error in case of wrong topic model algorithm
  */
  TopicModelAlgorithm ParseTopicModelAlgorithm(
      const std::string& algorithm_string) const;

  /**
    Parse evaluaton measures from their string representations
    @param measure_string string representation of evaluation measures
    @return vector containing enum values corresponding to given evaluation
    measures
    @throw std::runtime_error in case of wrong evaluation measures
  */
  std::vector<EvaluationMeasure> ParseEvaluationMeasures(
      const std::string& measure_string) const;

  /**
    Parses topic model algorithm options from their string representation
    @param algorithm_options string representation of topic model algorithm
    options
    @return vector containing topic model algorithm options
  */
  std::vector<std::string> ParseAlgorithmOptions(
      const std::string& algorithm_options) const noexcept;

  /**
    Corrects and creates given directory
    @param[out] directory_name name of directory to correct and create
    @throw std::exception in case of any occurred error
  */
  void CorrectDirectoryName(std::string* directory_name) const;
};

#endif  // TOPICMODELS_PROGRAM_OPTIONS_PARSER_H_
