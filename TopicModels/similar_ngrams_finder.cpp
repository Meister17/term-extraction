// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <cctype>
#include <iostream>
#include <fstream>
#include <functional>
#include <stdexcept>
#include <string>
#include <vector>
#include "./similar_ngrams_finder.h"

using boost::is_any_of;
using boost::split;
using std::cout;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::lower_bound;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::sort;
using std::string;
using std::vector;

void SimilarNGramsFinder::ParseSimilarSetsFile(
    const string& filename,
    const vector<string>& vocabulary) {
  cout << "Parsing file with similar words\n";
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse file with similar sets");
  }
  while (!file.eof()) {
    string line;
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (line.empty()) {
      continue;
    }
    vector<string> tokens;
    split(tokens, line, is_any_of("\t"));
    vector<size_t> similar_words;
    for (const auto& token : tokens) {
      auto iterator = lower_bound(vocabulary.begin(), vocabulary.end(), token);
      if (iterator == vocabulary.end() || *iterator != token) {
        cout << token << "\n";
        throw runtime_error("Failed to parse file with similar sets");
      }
      similar_words.push_back(iterator - vocabulary.begin());
    }
    sort(similar_words.begin(), similar_words.end());
    similar_sets_.push_back(similar_words);
  }
  file.close();
  if (similar_sets_.empty()) {
    throw runtime_error("Failed to parse file with similar sets");
  }
  std::cerr << similar_sets_.size() << " similar sets loaded\n";
}

bool SimilarNGramsFinder::AreSimilarNGrams(size_t first_ngram,
                                           size_t second_ngram) const noexcept {
  for (const auto& similar_set : similar_sets_) {
    auto first_iterator = lower_bound(similar_set.begin(),
                                      similar_set.end(),
                                      first_ngram);
    auto second_iterator = lower_bound(similar_set.begin(),
                                       similar_set.end(),
                                       second_ngram);
    if (first_iterator != similar_set.end() && *first_iterator == first_ngram &&
        second_iterator != similar_set.end() &&
        *second_iterator == second_ngram) {
      return true;
    }
  }
  return false;
}
