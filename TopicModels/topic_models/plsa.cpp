// Copyright 2013 Michael Nokel
#include <fstream>
#include <random>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>
#include "./plsa.h"

using std::ofstream;
using std::mt19937;
using std::runtime_error;
using std::stoi;
using std::stod;
using std::string;
using std::unordered_map;
using std::vector;

void PLSA::ParseAlgorithmOptions(const vector<string>& algorithm_options) {
  if (algorithm_options.size() != 4) {
    throw runtime_error("Failed to parse algorithm options");
  }
  alpha_ = stod(algorithm_options[0]);
  beta_ = stod(algorithm_options[1]);
  maximum_iterations_number_ = stoi(algorithm_options[2]);
  tolerance_ = stod(algorithm_options[3]);
  ofstream log_file(LOGGING_FILENAME_.data());
  if (!log_file) {
    throw runtime_error("Failed to log information for PLSA");
  }
  log_file.close();
}

void PLSA::EstimateModel(
    const vector<unordered_map<size_t, size_t>>& source_data,
    size_t vocabulary_size,
    size_t topics_number) {
  ofstream log_file(LOGGING_FILENAME_.data(), ofstream::out | ofstream::app);
  if (!log_file) {
    throw runtime_error("Failed to log information for PLSA");
  }
  log_file << "Starting PLSA\n";
  mt19937 generator(SEED_NUMBER_);
  InitializeProbabilities(source_data.size(),
                          vocabulary_size,
                          topics_number,
                          &generator);
  vector<vector<double>> nwt(vocabulary_size,
                             vector<double>(topics_number, 0.0));
  vector<vector<double>> ntd(topics_number,
                             vector<double>(source_data.size(), 0.0));
  vector<double> nt(topics_number, 0.0);
  vector<double> nd(source_data.size(), 0.0);
  for (size_t document = 0; document < source_data.size(); ++document) {
    for (const auto& word : source_data[document]) {
      nd[document] += word.second;
    }
  }
  double phi_norm = CalculateNorm(phi_);
  double theta_norm = CalculateNorm(theta_);
  double delta_phi_norm = phi_norm;
  double delta_theta_norm = theta_norm;
  for (int iteration = 1; iteration <= maximum_iterations_number_ &&
       (delta_phi_norm >= tolerance_ || delta_theta_norm >= tolerance_);
       ++iteration) {
    log_file << "Iteration:" << iteration << "; delta phi:" << delta_phi_norm <<
        "; delta theta:" << delta_theta_norm << "\n";
    log_file.flush();
    nwt.assign(nwt.size(), vector<double>(topics_number, 0.0));
    ntd.assign(ntd.size(), vector<double>(source_data.size(), 0.0));
    nt.assign(nt.size(), 0.0);
    for (size_t document = 0; document < source_data.size(); ++document) {
      for (const auto& element : source_data[document]) {
        size_t word = element.first;
        double ndw = element.second;
        double denominator = 0.0;
        for (size_t topic = 0; topic < topics_number; ++topic) {
          denominator += phi_[word][topic] * theta_[topic][document];
        }
        for (size_t topic = 0; topic < topics_number; ++topic) {
          double value = phi_[word][topic] * theta_[topic][document];
          if (value > 0.0) {
            value *= ndw / denominator;
            nwt[word][topic] += value;
            ntd[topic][document] += value;
            nt[topic] += value;
          }
        }
      }
    }
    for (size_t topic = 0; topic < topics_number; ++topic) {
      for (size_t word = 0; word < vocabulary_size; ++word) {
        phi_[word][topic] = (nwt[word][topic] + beta_) /
            (nt[topic] + beta_ * vocabulary_size);
      }
      for (size_t document = 0; document < source_data.size(); ++document) {
        theta_[topic][document] = (ntd[topic][document] + alpha_) /
            (nd[document] + alpha_ * topics_number);
      }
    }
    double prev_norm = phi_norm;
    phi_norm = CalculateNorm(phi_);
    delta_phi_norm = fabs(phi_norm - prev_norm);
    prev_norm = theta_norm;
    theta_norm = CalculateNorm(theta_);
    delta_theta_norm = fabs(theta_norm - prev_norm);
  }
  log_file.close();
}

void PLSA::EstimateModelOnTestTrainPart(
    const vector<unordered_map<size_t, size_t>>& source_data,
    size_t topics_number) {
  ofstream log_file(LOGGING_FILENAME_.data(), ofstream::out | ofstream::app);
  if (!log_file) {
    throw runtime_error("Failed to log information for PLSA");
  }
  log_file << "Starting PLSA on test part\n";
  mt19937 generator(SEED_NUMBER_);
  theta_ = vector<vector<double>>(topics_number,
                                  vector<double>(source_data.size(), 0.0));
  InitializeMatrix(&theta_, &generator);
  vector<vector<double>> ntd(theta_.size(),
                             vector<double>(source_data.size(), 0.0));
  vector<double> nd(source_data.size(), 0.0);
  for (size_t document = 0; document < source_data.size(); ++document) {
    for (const auto& word : source_data[document]) {
      nd[document] += word.second;
    }
  }
  double theta_norm = CalculateNorm(theta_);
  double delta_norm = theta_norm;
  for (int iteration = 1; iteration <= maximum_iterations_number_ &&
       delta_norm >= tolerance_; ++iteration) {
    log_file << "Iteration: " << iteration << "; delta theta:" << delta_norm <<
        "\n";
    log_file.flush();
    ntd.assign(ntd.size(), vector<double>(source_data.size(), 0.0));
    for (size_t document = 0; document < source_data.size(); ++document) {
      for (const auto& element : source_data[document]) {
        size_t word = element.first;
        double ndw = element.second;
        double denominator = 0.0;
        for (size_t topic = 0; topic < topics_number; ++topic) {
          denominator += phi_[word][topic] * theta_[topic][document];
        }
        for (size_t topic = 0; topic < topics_number; ++topic) {
          ntd[topic][document] += ndw * phi_[word][topic] *
              theta_[topic][document] / denominator;
        }
      }
    }
    for (size_t topic = 0; topic < topics_number; ++topic) {
      for (size_t document = 0; document < source_data.size(); ++document) {
        theta_[topic][document] = ntd[topic][document] / nd[document];
      }
    }
    double prev_norm = theta_norm;
    theta_norm = CalculateNorm(theta_);
    delta_norm = fabs(theta_norm - prev_norm);
  }
  log_file.close();
}

double PLSA::GetWordProbabilityInDocument(size_t word, size_t document)
    const noexcept {
  double probability = 0.0;
  for (size_t topic = 0; topic < theta_.size(); ++topic) {
    probability += phi_[word][topic] * theta_[topic][document];
  }
  return probability;
}
