// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <functional>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "./probabilities_loader.h"
#include "../auxiliary.h"

using boost::is_any_of;
using boost::split;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::max;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::set_intersection;
using std::string;
using std::unordered_map;
using std::unordered_set;
using std::vector;

void ProbabilitiesLoader::LoadFile(const string& filename,
                                   const unordered_set<size_t>& top_words,
                                   const vector<string>& vocabulary) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to load file with probabilities");
  }
  unordered_map<string, size_t> top_words_map;
  unordered_set<size_t> loaded_words;
  for (auto word : top_words) {
    top_words_map[vocabulary[word]] = word;
  }
  size_t maximum_document_frequency = 0;
  file >> maximum_document_frequency;
  unordered_map<size_t, vector<size_t>> documents_ngrams;
  string line;
  while (!file.eof()) {
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (line.empty()) {
      continue;
    }
    string word = line.substr(0, line.find_first_of("\t"));
    auto found_iterator = top_words_map.find(word);
    if (found_iterator != top_words_map.end()) {
      size_t ngram_index = found_iterator->second;
      loaded_words.insert(ngram_index);
      vector<string> tokens;
      split(tokens, line, is_any_of("\t"));
      unigram_probabilities_[ngram_index] =
          static_cast<double>(tokens.size() - 1) /
          static_cast<double>(maximum_document_frequency);
      documents_ngrams[ngram_index] = vector<size_t>();
      for (size_t index = 1; index < tokens.size(); ++index) {
        documents_ngrams[ngram_index].push_back(atoi(tokens[index].data()));
      }
    }
  }
  if (loaded_words.size() != top_words.size()) {
    for (auto word : top_words) {
      if (loaded_words.find(word) == loaded_words.end()) {
        std::cerr << vocabulary[word] << "\n";
      }
    }
    throw runtime_error("New words found, regenerate inverted index");
  }
  vector<size_t> intersection_vector(maximum_document_frequency);
  for (auto first_iterator = documents_ngrams.begin(); first_iterator !=
       documents_ngrams.end(); ++first_iterator) {
    auto second_iterator = first_iterator;
    for (++second_iterator; second_iterator != documents_ngrams.end();
         ++second_iterator) {
      size_t cooccurrence_frequency =
          set_intersection(first_iterator->second.begin(),
                           first_iterator->second.end(),
                           second_iterator->second.begin(),
                           second_iterator->second.end(),
                           intersection_vector.begin()) -
          intersection_vector.begin();
      double probability = static_cast<double>(cooccurrence_frequency) /
          static_cast<double>(maximum_document_frequency);
      bigram_probabilities_[Bigram(first_iterator->first,
                                   second_iterator->first)] = probability;
      bigram_probabilities_[Bigram(second_iterator->first,
                                   first_iterator->first)] = probability;
    }
  }
  file.close();
}
