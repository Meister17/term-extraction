// Copyright 2013 Michael Nokel
#ifndef TOPICMODELS_TOPIC_MODELS_LDA_H_
#define TOPICMODELS_TOPIC_MODELS_LDA_H_

#include <string>
#include <unordered_map>
#include <vector>
#include "./topic_model.h"

/**
  @brief Class implementing LDA topic model

  This class contains implementation for LDA topic model.
*/
class LDA : public TopicModel {
 public:
  /**
    Initializes object
    @param logging_filename file for logging information
    @param seed_number seeding number for generating random numbers
    @param top_words_tc_number number of top words to consider in Topic
    Coherence measures
  */
  LDA(const std::string& logging_filename,
      int seed_number,
      int top_words_tc_number)
      : TopicModel(logging_filename, seed_number, top_words_tc_number) {
  }

  /**
    Parses options for LDA topic model
    @param algorithm_options vector containing options for LDA topic model
    @throw std::runtime_error in case of any wrong algorithm option
  */
  void ParseAlgorithmOptions(const std::vector<std::string>& algorithm_options)
      override;

  /**
    Estimates LDA topic model by revealing topics in the given text collection
    @param source_data vector containing word-topic estimation for each document
    @param vocabulary_size size of vocabulary in the collection
    @param topics_number desired number of topics
  */
  void EstimateModel(
      const std::vector<std::unordered_map<size_t, size_t>>& source_data,
      size_t vocabulary_size,
      size_t topics_number) override;

  /**
    Estimates P(t|d) probabilities on the given test set
    @param source_data vector containing words within each document (test set)
    @param topics_number desired number of topics
  */
  void EstimateModelOnTestTrainPart(
      const std::vector<std::unordered_map<size_t, size_t>>& source_data,
      size_t topics_number) override;

 private:
  /** Alpha hyperparameter for LDA topic model */
  double alpha_ = 0.5;

  /** Beta hyperparameter for LDA topic model */
  double beta_ = 0.01;

  /** Matrix containing word-topic estimations */
  std::vector<std::vector<double>> nwt_;

  /** Vector containing topic estimations */
  std::vector<double> nt_;

 private:
  /**
    Calculates word probability in the given document
    @param word word to calculate probability for
    @param document document to calculate probability in
    @return word probability in the given document
  */
  double GetWordProbabilityInDocument(size_t word, size_t document)
                                      const noexcept override;
};

#endif  // TOPICMODELS_TOPIC_MODELS_LDA_H_
