// Copyright 2013 Michael Nokel
#ifndef TOPICMODELS_TOPIC_MODELS_TOPIC_MODEL_H_
#define TOPICMODELS_TOPIC_MODELS_TOPIC_MODEL_H_

#include <map>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>
#include "../auxiliary.h"

class FileLoader;

/**
  @brief Abstract class for all topic model algorithms

  This class should be derived by any particular realization of topic model,
  i.e. by LDA, PLSA, etc.
*/
class TopicModel {
 public:
  /**
    Initializes object
    @param logging_filename file for logging information
    @param seed_number seed number for generating random numbers
    @param top_words_tc_number number of top words to consider for Topic
    Coherence measures
  */
  TopicModel(const std::string& logging_filename,
             int seed_number,
             int top_words_tc_number)
      : SEED_NUMBER_(seed_number),
        TOP_WORDS_TC_NUMBER_(top_words_tc_number),
        LOGGING_FILENAME_(logging_filename) {
  }

  virtual ~TopicModel() {
  }

 protected:
  /** Seed number for generating random numbers */
  const size_t SEED_NUMBER_ = 239;

  /** Number of top words to consider for Topic Coherence measures */
  const size_t TOP_WORDS_TC_NUMBER_ = 10;

  /** File for logging information */
  const std::string LOGGING_FILENAME_ = "";

  /** Maximum number of iterations to perform */
  int maximum_iterations_number_ = 1000;

  /** Tolerance to achieve between successive iterations */
  double tolerance_ = 1e-9;

  /** Vector containing P(w|t) probabilities */
  std::vector<std::vector<double>> phi_;

  /** Vector containing P(t|d) probabilities */
  std::vector<std::vector<double>> theta_;

 public:
  std::vector<std::vector<double>> phi() const noexcept {
    return phi_;
  }

  /**
    Parses options for topic model algorithm
    @param algorithm_options vector containing options for topic model algorithm
    @throw std::runtime_error in case of any occurred error
  */
  virtual void ParseAlgorithmOptions(
      const std::vector<std::string>& algorithm_options) = 0;

  /**
    Estimates model by revealing topics in the given text collection
    @param source_data vector containing words with their frequencies within
    each document
    @param vocabulary_size size of vocabulary in the collection
    @param topics_number desired number of topics
  */
  virtual void EstimateModel(
      const std::vector<std::unordered_map<size_t, size_t>>& source_data,
      size_t vocabulary_size,
      size_t topics_number) = 0;

  /**
    Estimates P(t|d) probabilities on the given test set
    @param source_data vector containing words with their frequencies within
    each document (train part of the test set)
    @param topics_number desired number of topics
  */
  virtual void EstimateModelOnTestTrainPart(
      const std::vector<std::unordered_map<size_t, size_t>>& source_data,
      size_t topics_number) = 0;

  /**
    Calculates Perplexity on the test set
    @param source_data vector containing words with their frequencies within
    each document (test set)
    @return Perplexity
  */
  virtual void CalculatePerplexity(
      const std::vector<std::unordered_map<size_t, size_t>>& source_data)
      const noexcept;

  /**
    Calculates TC-PMI on the resulted topics
    @param file_loader object for determining similar words
    @param vocabulary vocabulary containing all words
    @param use_morphology flag indicating whether morphology should be used or
    not
    @param inverted_index_filename
    @return TC-PMI
  */
  virtual void CalculateTCPMI(
      const FileLoader& file_loader,
      const std::vector<std::string>& vocabulary,
      bool use_morphology,
      const std::string& inverted_index_filename) const noexcept;

 protected:
  /**
    Initializes matrices P(w|t), P(t|d) and P(w|t) for each document (from the
    uniform distribution)
    @param documents_number number of documents in the collection
    @param words_number number of words in the collection
    @param topics_number number of topics in the collection
    @param[out] generator generator of random numbers
  */
  virtual void InitializeProbabilities(size_t documents_number,
                                       size_t words_number,
                                       size_t topics_number,
                                       std::mt19937* generator) noexcept;

  /**
    Initializes matrix with random numbers generated from the given uniform
    distribution
    @param[out] generator of random numbers
    @param[out] matrix matrix to initialize
  */
  virtual void InitializeMatrix(std::vector<std::vector<double>>* matrix,
                                std::mt19937* generator) const noexcept;

  /**
    Calculates Euclidean norm of the given matrix
    @param matrix matrix to calculate norm for
    @return calculated Euclidean norm of the given matrix
  */
  virtual double CalculateNorm(const std::vector<std::vector<double>>& matrix)
      const noexcept;

  /**
    Calculates word probability in the given document
    @param word word to calculate probability for
    @param document document to calculate probability in
    @return word probability in the given document
  */
  virtual double GetWordProbabilityInDocument(size_t word, size_t document)
                                              const noexcept = 0;

 private:
  /**
    Forms vector containing top words for given topic
    @param topic topic for which top words should be returned
    @param file_loader object for determining similar words
    @param use_morphology flag indicating whether morphology should be used or
    not
    @return vector where top words for given topic will be stored
  */
  virtual std::vector<size_t> GetTopWordsForTopic(size_t topic,
                                                  const FileLoader& file_loader,
                                                  bool use_morphology)
                                                  const noexcept;

  /**
    Computes variant of TC-PMI measure
    @param top_words top words from each topic
    @param vocabulary vocabulary containing all words
    @param filename file containing pre-computed single word and word
    co-occurrence probabilities
    @param use_morphology flag indicating whether morphology should be used or
    not
  */
  virtual void ComputeTCPMIVariant(
      const std::vector<std::vector<size_t>>& top_words,
      const std::vector<std::string>& vocabulary,
      const std::string& filename,
      bool use_morphology) const;
};

#endif  // TOPICMODELS_TOPIC_MODELS_TOPIC_MODEL_H_
