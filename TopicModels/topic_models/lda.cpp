// Copyright 2013 Michael Nokel
#include <fstream>
#include <random>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>
#include "./lda.h"

using std::discrete_distribution;
using std::mt19937;
using std::ofstream;
using std::random_device;
using std::runtime_error;
using std::stoi;
using std::stod;
using std::string;
using std::unordered_map;
using std::vector;

void LDA::ParseAlgorithmOptions(const vector<string>& algorithm_options) {
  if (algorithm_options.size() != 4) {
    throw runtime_error("Failed to parse algorithm options");
  }
  alpha_ = stod(algorithm_options[0]);
  beta_ = stod(algorithm_options[1]);
  maximum_iterations_number_ = stoi(algorithm_options[2]);
  tolerance_ = stod(algorithm_options[3]);
  ofstream log_file(LOGGING_FILENAME_.data());
  if (!log_file) {
    throw runtime_error("Failed to log information for LDA");
  }
  log_file.close();
}

void LDA::EstimateModel(
    const vector<unordered_map<size_t, size_t>>& source_data,
    size_t vocabulary_size,
    size_t topics_number) {
  ofstream log_file(LOGGING_FILENAME_.data(), std::ios_base::app);
  if (!log_file) {
    throw runtime_error("Failed to log information for LDA");
  }
  log_file << "Starting LDA\n";
  mt19937 generator(SEED_NUMBER_);
  InitializeProbabilities(source_data.size(),
                          vocabulary_size,
                          topics_number,
                          &generator);
  nwt_ = vector<vector<double>>(vocabulary_size,
                                vector<double>(topics_number, beta_));
  vector<vector<double>> ntd(topics_number,
                             vector<double>(source_data.size(), alpha_));
  nt_ = vector<double>(topics_number, beta_ * vocabulary_size);
  vector<double> nd(source_data.size(), alpha_ * theta_.size());
  for (size_t document = 0; document < source_data.size(); ++document) {
    for (const auto& word : source_data[document]) {
      nd[document] += word.second;
    }
  }
  double phi_norm = CalculateNorm(phi_);
  double theta_norm = CalculateNorm(theta_);
  double delta_phi_norm = phi_norm;
  double delta_theta_norm = theta_norm;
  vector<unordered_map<size_t, size_t>> tdw(source_data.size(),
                                            unordered_map<size_t, size_t>());
  for (int iteration = 1; iteration <= maximum_iterations_number_ &&
       (delta_phi_norm >= tolerance_ || delta_theta_norm >= tolerance_);
       ++iteration) {
    log_file << "Iteration: " << iteration << "; delta phi:" << delta_phi_norm
        << "; delta theta:" << delta_theta_norm << "\n";
    for (size_t document = 0; document < source_data.size(); ++document) {
      for (const auto& element : source_data[document]) {
        size_t word = element.first;
        for (size_t number = 0; number < element.second; ++number) {
          if (iteration >= 2) {
            --nwt_[word][tdw[document][word]];
            --ntd[tdw[document][word]][document];
            --nt_[tdw[document][word]];
          }
          vector<double> ptdw(topics_number);
          for (size_t topic = 0; topic < topics_number; ++topic) {
            ptdw[topic] = nwt_[word][topic] / nt_[topic] * ntd[topic][document];
          }
          discrete_distribution<> distribution(ptdw.begin(), ptdw.end());
          tdw[document][word] = distribution(generator);
          ++nwt_[word][tdw[document][word]];
          ++ntd[tdw[document][word]][document];
          ++nt_[tdw[document][word]];
        }
      }
    }
    for (size_t topic = 0; topic < topics_number; ++topic) {
      for (size_t word = 0; word < vocabulary_size; ++word) {
        phi_[word][topic] = nwt_[word][topic] / nt_[topic];
      }
      for (size_t document = 0; document < source_data.size(); ++document) {
        theta_[topic][document] = ntd[topic][document] / nd[document];
      }
    }
    double prev_norm = phi_norm;
    phi_norm = CalculateNorm(phi_);
    delta_phi_norm = fabs(phi_norm - prev_norm);
    prev_norm = theta_norm;
    theta_norm = CalculateNorm(theta_);
    delta_theta_norm = fabs(theta_norm - prev_norm);
  }
  log_file.close();
}

void LDA::EstimateModelOnTestTrainPart(
    const vector<unordered_map<size_t, size_t>>& source_data,
    size_t topics_number) {
  ofstream log_file(LOGGING_FILENAME_.data(), std::ios_base::app);
  if (!log_file) {
    throw runtime_error("Failed to log information for LDA");
  }
  log_file << "Starting LDA on test part\n";
  mt19937 generator(SEED_NUMBER_);
  theta_ = vector<vector<double>>(topics_number,
                                  vector<double>(source_data.size(), 0.0));
  InitializeMatrix(&theta_, &generator);
  vector<vector<double>> ntd(topics_number,
                             vector<double>(source_data.size(), alpha_));
  vector<double> nd(source_data.size(), alpha_);
  for (size_t document = 0; document < source_data.size(); ++document) {
    for (const auto& word : source_data[document]) {
      nd[document] += word.second;
    }
  }
  double theta_norm = CalculateNorm(theta_);
  double delta_norm = theta_norm;
  vector<unordered_map<int, int>> tdw(source_data.size(),
                                      unordered_map<int, int>());
  for (int iteration = 1; iteration <= maximum_iterations_number_ &&
       delta_norm >= tolerance_; ++iteration) {
    log_file << "Iteration: " << iteration << "; delta theta:" << delta_norm <<
        "\n";
    for (size_t document = 0; document < source_data.size(); ++document) {
      for (const auto& element : source_data[document]) {
        size_t word = element.first;
        if (iteration >= 2) {
          --ntd[tdw[document][word]][document];
        }
        vector<double> ptdw(topics_number);
        for (size_t topic = 0; topic < topics_number; ++topic) {
          ptdw[topic] = nwt_[word][topic] / nt_[topic] * ntd[topic][document];
        }
        discrete_distribution<> distribution(ptdw.begin(), ptdw.end());
        tdw[document][word] = distribution(generator);
        ++ntd[tdw[document][word]][document];
      }
    }
    for (size_t topic = 0; topic < topics_number; ++topic) {
      for (size_t document = 0; document < source_data.size(); ++document) {
        theta_[topic][document] = ntd[topic][document] / nd[document];
      }
    }
    double prev_norm = theta_norm;
    theta_norm = CalculateNorm(theta_);
    delta_norm = fabs(theta_norm - prev_norm);
  }
  log_file.close();
}

double LDA::GetWordProbabilityInDocument(size_t word, size_t document)
    const noexcept {
  double probabilitty = 0.0;
  for (size_t topic = 0; topic < theta_.size(); ++topic) {
    probabilitty += phi_[word][topic] * theta_[topic][document];
  }
  return probabilitty;
}
