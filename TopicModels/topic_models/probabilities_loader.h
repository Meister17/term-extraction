// Copyright 2014 Michael Nokel
#ifndef TOPICMODELS_PROBABILITIES_LOADERS_PROBABILITIES_LOADER_H_
#define TOPICMODELS_PROBABILITIES_LOADERS_PROBABILITIES_LOADER_H_

#include <map>
#include <stdexcept>
#include <string>
#include <unordered_set>
#include <vector>
#include "../auxiliary.h"

/**
  @brief Class for working with files containing pre-computed single word and
  word co-occurrence probabilities

  This class should be used for parsing file containing pre-computed single-word
  and word co-occurrence probabilities, after what one can obtain desired
  probabilities
*/
class ProbabilitiesLoader {
 public:
  /**
    Parses file containing pre-computed single word and word co-occurrence
    probabilities
    @param filename file containing pre-compured single word and word
    co-occurrence probabilities
    @param top_words set containing top words from each topic
    @param vocabulary vocabulary containing all words
    @throw std::runtime_error in case of any occurred error
  */
  void LoadFile(const std::string& filename,
                const std::unordered_set<size_t>& top_words,
                const std::vector<std::string>& vocabulary);

 protected:
  /** Map containing unigram probabilities */
  std::map<size_t, double> unigram_probabilities_;

  /** Map containing bigram probabilities */
  std::map<Bigram, double> bigram_probabilities_;

 public:
  /**
    Returns probability of the given unigram
    @param unigram unigram to get probability for
    @return probability of the given unigram
  */
  double GetUnigramProbability(size_t unigram) const noexcept {
    auto iterator = unigram_probabilities_.find(unigram);
    if (iterator == unigram_probabilities_.end()) {
      return 0;
    }
    return iterator->second;
  }

  /**
    Returns probability of the given bigram
    @param first_unigram first unigram forming bigram
    @param second_unigram second unigram forming bigram
    @return probability of the given bigram
  */
  double GetBigramProbability(size_t first_unigram, size_t second_unigram)
      const noexcept {
    auto iterator = bigram_probabilities_.find(Bigram(first_unigram,
                                                      second_unigram));
    if (iterator == bigram_probabilities_.end()) {
      return 0;
    }
    return iterator->second;
  }
};

#endif  // TOPICMODELS_PROBABILITIES_LOADERS_PROBABILITIES_LOADER_H_
