// Copyright 2013 Michael Nokel
#include <cmath>
#include <cstdlib>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <random>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>
#include "./topic_model.h"
#include "../auxiliary.h"
#include "../file_loader.h"
#include "./probabilities_loader.h"

using std::accumulate;
using std::cout;
using std::map;
using std::max;
using std::min;
using std::mt19937;
using std::numeric_limits;
using std::ofstream;
using std::pair;
using std::runtime_error;
using std::string;
using std::uniform_real_distribution;
using std::unique_ptr;
using std::unordered_map;
using std::unordered_set;
using std::vector;

void TopicModel::CalculatePerplexity(
    const vector<unordered_map<size_t, size_t>>& source_data) const noexcept {
  cout << "Calculating Perplexity\n";
  double nominator = 0.0;
  double denominator = 0.0;
  for (size_t document = 0; document < source_data.size(); ++document) {
    for (const auto& element : source_data[document]) {
      double pwd = max(GetWordProbabilityInDocument(element.first, document),
                       numeric_limits<double>::min());
      nominator += element.second * log(pwd);
      denominator += element.second;
    }
  }
  double perplexity = exp(-nominator / denominator);
  cout << "Perplexity: " << perplexity << "\n";
}

void TopicModel::CalculateTCPMI(
    const FileLoader& file_loader,
    const vector<string>& vocabulary,
    bool use_morphology,
    const string& inverted_index_filename) const noexcept {
  if (use_morphology) {
    cout << "Calculating TC-PMI-nSIM\n";
  } else {
    cout << "Calculating TC-PMI\n";
  }
  vector<vector<size_t>> top_words;
  for (size_t topic = 0; topic < phi_.front().size(); ++topic) {
    top_words.push_back(
        GetTopWordsForTopic(topic, file_loader, use_morphology));
  }
  ComputeTCPMIVariant(top_words,
                      vocabulary,
                      inverted_index_filename,
                      use_morphology);
}

vector<size_t> TopicModel::GetTopWordsForTopic(
    size_t topic,
    const FileLoader& file_loader,
    bool use_morphology) const noexcept {
  vector<size_t> top_words;
  map<double, vector<size_t>> words_in_topic;
  for (size_t word = 0; word < phi_.size(); ++word) {
    auto result = words_in_topic.insert({phi_[word][topic], vector<size_t>()});
    result.first->second.push_back(word);
  }
  for (auto iterator = words_in_topic.rbegin(); top_words.size() <
       TOP_WORDS_TC_NUMBER_ && iterator != words_in_topic.rend();
       ++iterator) {
    for (const auto& word : iterator->second) {
      bool new_word = true;
      if (use_morphology) {
        for (const auto& top_word : top_words) {
          if (file_loader.AreSimilarNGrams(word, top_word)) {
            new_word = false;
            break;
          }
        }
      }
      if (new_word) {
        top_words.push_back(word);
      }
    }
  }
  return top_words;
}

void TopicModel::ComputeTCPMIVariant(const vector<vector<size_t>>& top_words,
                                     const vector<string>& vocabulary,
                                     const string& filename,
                                     bool use_morphology) const {
  vector<double> tcpmi(phi_.front().size(), 0.0);
  unordered_set<size_t> top_words_set;
  for (const auto& topic : top_words) {
    for (auto word : topic) {
      top_words_set.insert(word);
    }
  }
  ProbabilitiesLoader probabilities_loader;
  probabilities_loader.LoadFile(filename, top_words_set, vocabulary);
  for (size_t topic = 0; topic < top_words.size(); ++topic) {
    for (size_t first_index = 0; first_index < top_words[topic].size();
         ++first_index) {
      double first_probability = probabilities_loader.GetUnigramProbability(
          top_words[topic][first_index]);
      for (size_t second_index = first_index + 1; second_index <
           top_words[topic].size(); ++second_index) {
        double bigram_probability = probabilities_loader.GetBigramProbability(
            top_words[topic][first_index],
            top_words[topic][second_index]);
        double second_probability = probabilities_loader.GetUnigramProbability(
            top_words[topic][second_index]);
	      if (bigram_probability > 0) {
          tcpmi[topic] += log(bigram_probability) - log(first_probability) -
              log(second_probability);
	      }
      }
    }
  }
  double tc_pmi = accumulate(tcpmi.begin(), tcpmi.end(), 0.0) / tcpmi.size();
  if (use_morphology) {
    cout << "Topic Coherence - PMI with morphology: " << tc_pmi << "\n";
  } else {
    cout << "Topic Coherence: " << tc_pmi << "\n";
  }
}

void TopicModel::InitializeProbabilities(size_t documents_number,
                                         size_t words_number,
                                         size_t topics_number,
                                         mt19937* generator) noexcept {
  phi_ = vector<vector<double>>(words_number,
                                vector<double>(topics_number, 0.0));
  theta_ = vector<vector<double>>(topics_number,
                                  vector<double>(documents_number, 0.0));
  InitializeMatrix(&phi_, generator);
  InitializeMatrix(&theta_, generator);
}

void TopicModel::InitializeMatrix(vector<vector<double>>* matrix,
                                  mt19937* generator) const noexcept {
  uniform_real_distribution<> distribution(numeric_limits<double>::min(), 1);
  for (auto& row : *matrix) {
    for (auto& element : row) {
      element = distribution(*generator);
    }
  }
}

double TopicModel::CalculateNorm(const vector<vector<double>>& matrix)
    const noexcept {
  double norm = 0.0;
  for (const auto& row : matrix) {
    for (const auto& element : row) {
      norm += element * element;
    }
  }
  return sqrt(norm);
}
