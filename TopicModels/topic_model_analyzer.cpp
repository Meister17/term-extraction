// Copyright 2013 Michael Nokel
#include <boost/threadpool.hpp>
#include <algorithm>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>
#include "./topic_model_analyzer.h"
#include "./topic_models/lda.h"
#include "./topic_models/plsa.h"
#include "./topic_models/topic_model.h"

using boost::threadpool::pool;
using std::accumulate;
using std::cout;
using std::find;
using std::numeric_limits;
using std::pair;
using std::runtime_error;
using std::string;
using std::unique_ptr;
using std::unordered_map;
using std::vector;

void TopicModelAnalyzer::Init(const string& input_filename,
                              const string& vocabulary_filename,
                              const string& similar_sets_filename) {
  file_loader_.ParseVocabularyFile(vocabulary_filename, &vocabulary_);
  if (!similar_sets_filename.empty()) {
    file_loader_.ParseSimilarSetsFile(similar_sets_filename, vocabulary_);
  }
  file_loader_.ParseInputFile(input_filename, vocabulary_, &source_data_);
}

void TopicModelAnalyzer::EstimateModel(
    const TopicModelAlgorithm& algorithm,
    const vector<string>& algorithm_options,
    const vector<EvaluationMeasure>& evaluation_measures,
    const string& inverted_index_filename) {
  if (evaluation_measures.empty()) {
    cout << "Nothing to compute\n";
  } else {
    cout << "Starting topic model algorithm\n";
    pool thread_pool(NUMBER_THREADS_);
    bool compute_perplexity =
        find(evaluation_measures.begin(),
             evaluation_measures.end(),
             EvaluationMeasure::PERPLEXITY) != evaluation_measures.end();
    bool compute_train_set_measures = !compute_perplexity ||
        evaluation_measures.size() > 1;
    InitializeAlgorithm(algorithm,
                        algorithm_options,
                        compute_perplexity,
                        compute_train_set_measures);
    if (compute_perplexity) {
      thread_pool.schedule(boost::bind(&TopicModelAnalyzer::CalculatePerplexity,
                                       this,
                                       evaluation_measures.size() == 1));
    }
    if (compute_train_set_measures) {
      thread_pool.schedule(boost::bind(
          &TopicModelAnalyzer::CalculateTrainSetMeasures,
          this,
          evaluation_measures,
          inverted_index_filename));
    }
  }
}

void TopicModelAnalyzer::InitializeAlgorithm(
    const TopicModelAlgorithm& algorithm,
    const vector<string>& algorithm_options,
    bool compute_perplexity,
    bool compute_train_set_measures) {
  string train_set_filename = LOGGING_DIRECTORY_NAME_ + "train_set.log";
  string perplexity_filename = LOGGING_DIRECTORY_NAME_ + "perplexity.log";
  switch (algorithm) {
    case TopicModelAlgorithm::PLSA:
      if (compute_train_set_measures) {
        train_set_topic_model_.reset(
            new PLSA(train_set_filename, SEED_NUMBER_, TOP_WORDS_TC_NUMBER_));
        train_set_topic_model_->ParseAlgorithmOptions(algorithm_options);
      }
      if (compute_perplexity) {
        perplexity_topic_model_.reset(
            new PLSA(perplexity_filename, SEED_NUMBER_, TOP_WORDS_TC_NUMBER_));
        perplexity_topic_model_->ParseAlgorithmOptions(algorithm_options);
      }
      break;
    case TopicModelAlgorithm::LDA:
      if (compute_train_set_measures) {
        train_set_topic_model_.reset(
            new LDA(train_set_filename, SEED_NUMBER_, TOP_WORDS_TC_NUMBER_));
        train_set_topic_model_->ParseAlgorithmOptions(algorithm_options);
      }
      if (compute_perplexity) {
        perplexity_topic_model_.reset(
            new LDA(perplexity_filename, SEED_NUMBER_, TOP_WORDS_TC_NUMBER_));
        perplexity_topic_model_->ParseAlgorithmOptions(algorithm_options);
      }
      break;
    case TopicModelAlgorithm::NO_TOPIC_MODEL: default:
      throw runtime_error("Wrong algorithm has been specified");
  }
}

void TopicModelAnalyzer::CalculatePerplexity(bool store_new_vocabulary)
    noexcept {
  vector<string> train_vocabulary;
  vector<unordered_map<size_t, size_t>> train_data;
  vector<unordered_map<size_t, size_t>> test_train_data;
  vector<unordered_map<size_t, size_t>> test_data;
  file_loader_.SplitToTrainAndTest(vocabulary_,
                                   source_data_,
                                   TEST_PART_,
                                   &train_vocabulary,
                                   &train_data,
                                   &test_train_data,
                                   &test_data);
  if (store_new_vocabulary) {
    // only we are writing -> no race condition
    vocabulary_ = train_vocabulary;
    source_data_ = train_data;
  }
  perplexity_topic_model_->EstimateModel(train_data,
                                         train_vocabulary.size(),
                                         TOPICS_NUMBER_);
  perplexity_topic_model_->EstimateModelOnTestTrainPart(test_train_data,
                                                        TOPICS_NUMBER_);
  perplexity_topic_model_->CalculatePerplexity(test_data);
}

void TopicModelAnalyzer::CalculateTrainSetMeasures(
    const vector<EvaluationMeasure>& evaluation_measures,
    const string& inverted_index_filename) const noexcept {
  train_set_topic_model_->EstimateModel(source_data_,
                                        vocabulary_.size(),
                                        TOPICS_NUMBER_);
  for (const auto& measure : evaluation_measures) {
    switch (measure) {
      case EvaluationMeasure::TC_PMI:
        train_set_topic_model_->CalculateTCPMI(file_loader_,
                                               vocabulary_,
                                               false,
                                               inverted_index_filename);
        break;
      case EvaluationMeasure::TC_PMI_NSIM:
        train_set_topic_model_->CalculateTCPMI(file_loader_,
                                               vocabulary_,
                                               true,
                                               inverted_index_filename);
        break;
      case EvaluationMeasure::PERPLEXITY:
        break;
      case EvaluationMeasure::NO_MEASURE: default:
        throw runtime_error("Wrong evaluation measure to compute");
    }
  }
}

void TopicModelAnalyzer::PrintResults(int top_words_number,
                                      const string& topic_words_filename)
                                      const {
  cout << "Printing results\n";
  vector<vector<double>> phi;
  if (train_set_topic_model_.get() == nullptr) {
    phi = perplexity_topic_model_->phi();
  } else {
    phi = train_set_topic_model_->phi();
  }
  file_loader_.PrintTopicWords(topic_words_filename,
                               vocabulary_,
                               phi,
                               top_words_number);
}
