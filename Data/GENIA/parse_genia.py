#!/usr/bin/python
import optparse
import os
import xml.sax


class GeniaCorpusHandler(xml.sax.ContentHandler):
    def __init__(self, text_directory, terms_file):
        xml.sax.ContentHandler.__init__(self)
        self.file = None
        self.terms_set = set()
        self.current_terms = []
        self.reading_title = False
        self.terms_file = terms_file
        self.text_directory = text_directory

    def startElement(self, name, attrs):
        if name == 'cons':
            self.current_terms.append('')
        elif name == 'bibliomisc':
            assert not self.reading_title
            self.reading_title = True
            if self.file is not None:
                self.file.close()

    def endElement(self, name):
        if name == 'cons':
            self.terms_set.add(self.current_terms[-1].strip())
            self.current_terms = self.current_terms[:-1]
        elif name == 'bibliomisc':
            self.reading_title = False

    def characters(self, content):
        for index in xrange(len(self.current_terms)):
            self.current_terms[index] += content
        if self.reading_title:
            self.file = open(os.path.join(self.text_directory,
                                          content.replace(':', '_')), 'w')
        elif self.file is not None:
            self.file.write(content)

    def endDocument(self):
        with open(self.terms_file, 'w') as output:
            for term in self.terms_set:
                output.write(term + '\n')
        self.file.close()


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage='Usage: %prog <genia_file> <text_directory> <terms_file>')
    options, args = parser.parse_args()
    if len(args) != 3:
        parser.error('Incorrect usage')
    genia_file = args[0]
    text_directory = args[1]
    terms_file = args[2]
    if not os.path.exists(text_directory):
        os.makedirs(text_directory)
    with open(genia_file, 'r') as input:
        xml.sax.parse(input, GeniaCorpusHandler(text_directory, terms_file))
