#!/usr/bin/python
import fnmatch
import nltk
import optparse
import os


def clean_xml_and_html(source_directory, files_mask, destination_directory):
    if not os.path.exists(destination_directory):
        os.makedirs(destination_directory)
    for root, dirnames, filenames in os.walk(source_directory):
        for index, filename in enumerate(fnmatch.filter(filenames, files_mask)):
            print 'Processing #%d file:%s' % (index, filename)
            with open(os.path.join(source_directory, filename), 'r') as input:
                with open(os.path.join(destination_directory, filename), 'w') as output:
                    output.write(nltk.clean_html(''.join(input.readlines())))


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage='Usage: %prog [options] <source_directory> <destination_directory>')
    parser.add_option('-m', '--mask', dest='files_mask', type=str,
                      help='Mask of files to be processed')
    options, args = parser.parse_args()
    if len(args) != 2:
        parser.error('Incorrect usage')

    source_directory = args[0]
    destination_directory = args[1]
    clean_xml_and_html(source_directory, options.files_mask, destination_directory)
