#!/usr/bin/python
import os
from optparse import OptionParser
import shutil
import string


def process_directory(input_directory, output_directory):
    if os.path.exists(output_directory):
        shutil.rmtree(output_directory)
    os.makedirs(output_directory)
    file_number = 0
    for filename in os.listdir(input_directory):
        short_filename, file_extension = os.path.splitext(filename)
        if file_extension == '.sgm':
            with open(os.path.join(input_directory, filename), 'r') as input:
                print 'Parsing file %s: %d documents' % (filename, file_number)
                inside_text = False
                output = None
                for index_line, line in enumerate(input):
                    try:
                        position = line.index('<REUTERS')
                        output = open(os.path.join(output_directory, str(file_number) + '.txt'), 'w')
                    except ValueError:
                        pass
                    try:
                        position = line.index('</REUTERS>')
                        output.close()
                        file_number += 1
                    except ValueError:
                        pass
                    try:
                        position = line.index('<TEXT')
                        position = line.index('"UNPROC"')
                        inside_text = True
                        continue
                    except ValueError:
                        pass
                    try:
                        position = line.index('</TEXT>')
                        inside_text = False
                    except ValueError:
                        pass
                    try:
                        first_index = line.index('<TITLE>')
                        try:
                            second_index = line.index('</TITLE>')
                        except ValueError:
                            second_index = len(line)
                        output.write(filter(lambda x: x in string.printable, line[first_index + len('<TITLE>'):second_index]))
                        output.write('\n')
                    except ValueError:
                        pass
                    try:
                        position = line.index('<BODY>')
                        inside_text = True
                        output.write(filter(lambda x: x in string.printable, line[position + len('<BODY>'):len(line)]))
                        continue
                    except ValueError:
                        pass
                    try:
                        position = line.index('</BODY>')
                        inside_text = False
                        output.write(filter(lambda x: x in string.printable, line[0:position]))
                        output.write('\n')
                    except ValueError:
                        pass
                    if inside_text:
                        output.write(filter(lambda x: x in string.printable, line))

if __name__ == '__main__':
    parser = OptionParser(usage='Usage: %prog <input_directory> <output_directory>')
    options, args = parser.parse_args()
    if len(args) != 2:
        parser.error('Incorrect usage')

    input_directory = args[0]
    output_directory = args[1]
    process_directory(input_directory, output_directory)
