#!/usr/bin/env python
import optparse
from whoosh.lang.morph_en import variations


def get_prefixes(input_vocabulary_filename, output_prefixes_filename,
                 output_vocabulary_filename):
  words = {}
  with open(input_vocabulary_filename, 'r') as input:
    for line in input:
      tokens = line.strip().split('\t')
      tokens[-1] = '-'
      words[tokens[0]] = tokens[1:]
  words_keys = set(words.keys())
  prefixes = set()
  for word in words:
    cognates = words_keys.intersection(set(variations(word)))
    if len(cognates) > 1:
      prefix = None
      cutted = False
      for cognate in cognates:
        words[cognate][-1] = '+'
        if prefix is None or len(cognate) < len(prefix):
          prefix = cognate
        elif len(cognate) == len(prefix):
          cutted = True
          candidate = ""
          for index in range(0, len(cognate)):
            if cognate[index] == prefix[index]:
              candidate += cognate[index]
          prefix = candidate
      if len(prefix) > 1:
        if cutted:
          prefixes.add(prefix)
        else:
          prefixes.add(prefix[:-1])
  with open(output_prefixes_filename, 'w') as output:
    for prefix in sorted(prefixes):
      output.write(prefix + "\n")
  with open(output_vocabulary_filename, 'w') as output:
    for word in sorted(words):
      output.write(word + '\t' + '\t'.join(words[word]) + '\n')


if __name__ == '__main__':
  parser = optparse.OptionParser(
      usage='Usage: %prog <input_vocabulary_file> <output_prefixes_file> ' +
            '<output_vocabulary_file>')
  options, args = parser.parse_args()
  if len(args) != 3:
    parser.error('Incorrect usage')

  input_vocabulary_file = args[0]
  output_prefixes_file = args[1]
  output_vocabulary_file = args[2]
  get_prefixes(input_vocabulary_file, output_prefixes_file,
               output_vocabulary_file)