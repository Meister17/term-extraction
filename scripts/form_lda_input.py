#!/usr/bin/env python
# -*- coding: utf-8 -*-
import codecs
import optparse
import os

SKIP_INTERVAL = 100


class LDAInputCreator(object):
  def __init__(self, file_extension, stopwords_file, minimum_frequency):
    self.file_extension = file_extension
    self.minimum_frequency = minimum_frequency
    self.vocabulary = {}
    self.documents = []
    self.stopwords = set()
    with codecs.open(stopwords_file, 'r', 'utf-8') as input:
      for line in input:
        self.stopwords.add(line.strip())
    self.pos_of_words = {}

  def parse_directory(self, input_directory):
    word_frequencies = {}
    whole_vocabulary = set()
    for index, filename in enumerate(os.listdir(input_directory)):
      if filename.endswith(self.file_extension):
        input_filename = os.path.join(input_directory, filename)
        if index % SKIP_INTERVAL == 0:
          print 'Parsing file #%d: %s' % (index, filename)
        self.documents.append({})
        with codecs.open(input_filename, 'r', 'cp1251') as input:
          input.readline()
          same_words = set()
          for line in input:
            tokens = line.strip().split()
            if (tokens[4] == u'ЛЕ' and tokens[-1] != '??' and
                tokens[-2] not in self.stopwords and
                (tokens[-3] == '+' or tokens[-3] == '-')):
              whole_vocabulary.add(tokens[-2])
              pos = set()
              for char_index in range(0, len(tokens[-1]), 2):
                if tokens[-1][char_index] in u'абвгдежзи':
                  pos.add('N')
                elif tokens[-1][char_index] == u'й':
                  pos.add('A')
                elif tokens[-1][char_index] in u'клмнопрстуфхц':
                  pos.add('V')
                elif (tokens[-1][char_index : char_index + 2] == u'яа' or
                      tokens[-1][char_index : char_index + 2] == u'яб' or
                      tokens[-1][char_index : char_index + 2] == u'яи'):
                  pos.add('D')
              if tokens[-2] in self.pos_of_words:
                self.pos_of_words[tokens[-2]] = (
                    self.pos_of_words[tokens[-2]].union(pos))
              else:
                self.pos_of_words[tokens[-2]] = pos
              if line[0] != line.strip()[0]:
                same_words.add(tokens[-2])
              else:
                for word in same_words:
                  if word not in self.documents[-1]:
                    self.documents[-1][word] = 1
                  else:
                    self.documents[-1][word] += 1
                same_words.clear()
                same_words.add(tokens[-2])
          for word in same_words:
            if word not in self.documents[-1]:
              self.documents[-1][word] = 1
            else:
              self.documents[-1][word] += 1
        for word, frequency in self.documents[-1].iteritems():
          if word not in word_frequencies:
            word_frequencies[word] = frequency
          else:
            word_frequencies[word] += frequency
    result_vocabulary = set()
    for word in whole_vocabulary:
      if (word_frequencies[word] >= self.minimum_frequency and
          len(self.pos_of_words[word]) > 0):
        result_vocabulary.add(word)
    for index, word in enumerate(sorted(result_vocabulary)):
      self.vocabulary[word] = index

  def print_lda_vocabulary(self, lda_vocabulary_file):
    print 'Printing vocabulary for LDA'
    with codecs.open(lda_vocabulary_file, 'w', 'utf-8') as output:
      for word in sorted(self.vocabulary.keys()):
        output.write(word)
        for pos in self.pos_of_words[word]:
          output.write('\t' + pos)
        output.write('\n')

  def print_lda_input(self, lda_input_file):
    print 'Printing input file for LDA'
    with codecs.open(lda_input_file, 'w', 'utf-8') as output:
      for document in self.documents:
        output.write(str(len(document)))
        for word, frequency in document.iteritems():
          if word in self.vocabulary:
            word_index = self.vocabulary[word]
            output.write(' ' + str(word_index) + ':' + str(frequency))
        output.write('\n')


if __name__ == '__main__':
  parser = optparse.OptionParser(
      usage='Usage: %prog [options] <input_dir> <lda_input> <lda_vocabulary>')
  parser.add_option('-e', '--file_extension', dest='file_extension', type=str,
                    help='Extension of files for processing')
  parser.add_option('-s', '--stopwords_file', dest='stopwords_file', type=str,
                    help='File with stop words')
  parser.add_option('-f', '--minimum_frequency', dest='minimum_frequency',
                    type=int, default=5, help='Minimum frequency of the word')
  options, args = parser.parse_args()
  if len(args) != 3:
    parser.error('Incorrect usage')

  input_directory = args[0]
  lda_input_file = args[1]
  lda_vocabulary_file = args[2]
  lda_input_creator = LDAInputCreator(options.file_extension,
                                      options.stopwords_file,
                                      options.minimum_frequency)
  lda_input_creator.parse_directory(input_directory)
  lda_input_creator.print_lda_vocabulary(lda_vocabulary_file)
  lda_input_creator.print_lda_input(lda_input_file)