#!/usr/bin/env python
import operator
import optparse
import os


def load_freq_file(filename, threshold):
  terms = {}
  with open(filename, 'r') as input:
    for index, line in enumerate(input):
      if index == threshold:
        break
      tokens = line.strip().split('\t')
      terms[tokens[0]] = tokens[2]
  return terms


def load_file(filename, terms):
  features = {}
  with open(filename, 'r') as input:
    for line in input:
      tokens = line.strip().split('\t')
      if len(tokens) != 3:
        continue
      if tokens[0] in terms:
        features[tokens[0]] = float(tokens[1])
  return features


def load_files(single_word_filename, two_word_filename, terms):
  features = load_file(single_word_filename, terms)
  features.update(load_file(two_word_filename, terms))
  return features


def print_result(features, terms, filename):
  with open(filename, 'w') as output:
    for term, value in sorted(features.iteritems(), key=operator.itemgetter(1), reverse=True):
      output.write(term + '\t' + str(value) + '\t' + terms[term] + '\n')


if __name__ == '__main__':
  parser = optparse.OptionParser(
      usage='Usage: %prog [options] <single_word_dir> <two_word_dir> ' +
            '<single_word_freq_file> <two_word_freq_file> <output_dir>')
  parser.add_option('-t', '--threshold', dest='threshold', type=int, default=5000,
                    help='Threshold for terms')
  options, args = parser.parse_args()
  if len(args) != 5:
    parser.error('Incorrect usage')

  single_word_dir = args[0]
  two_word_dir = args[1]
  single_word_freq_file = args[2]
  two_word_freq_file = args[3]
  output_dir = args[4]
  if not os.path.exists(output_dir):
    os.makedirs(output_dir)
  terms = load_freq_file(single_word_freq_file, options.threshold)
  terms.update(load_freq_file(two_word_freq_file, options.threshold))
  for filename in os.listdir(single_word_dir):
    if '_lda' not in filename and os.path.isfile(os.path.join(single_word_dir, filename)):
      features = load_files(os.path.join(single_word_dir, filename),
                            os.path.join(two_word_dir, filename), terms)
      print_result(features, terms, os.path.join(output_dir, filename))