#!/usr/bin/env python
import optparse


def get_better_variants(variants_file, single_words_file, output_file):
  word_frequencies = {}
  with open(single_words_file, 'r') as input:
    for line in input:
      tokens = line.strip().split('\t')
      if len(tokens) == 2:
        word_frequencies[tokens[0]] = int(tokens[1])
  with open(output_file, 'w') as output:
    with open(variants_file, 'r') as input:
      for line in input:
        tokens = line.strip().split('\t')
        best_token = ''
        max_frequency = 0
        for token in tokens:
          if token in word_frequencies and word_frequencies[token] > max_frequency:
            best_token = token
            max_frequency = word_frequencies[token]
        if best_token != '' and max_frequency > 0:
          output.write(best_token + '\t' + line)


if __name__ == '__main__':
  parser = optparse.OptionParser(
      usage='Usage: %prog <variants_file> <single_words_file> <output_file>')
  options, args = parser.parse_args()
  if len(args) != 3:
    parser.error('Incorrect usage')

  variants_file = args[0]
  single_words_file = args[1]
  output_file = args[2]
  get_better_variants(variants_file, single_words_file, output_file)