#!/usr/bin/python
import optparse
import os
import shutil

def divide_collection(source_directory, train_directory, test_directory, percent):
    if not os.path.exists(train_directory):
        os.makedirs(train_directory)
    else:
        shutil.rmtree(train_directory)
        os.mkdir(train_directory)
    if not os.path.exists(test_directory):
        os.makedirs(test_directory)
    else:
        shutil.rmtree(test_directory)
        os.mkdir(test_directory)
    number_files = 0
    for filename in os.listdir(source_directory):
        number_files += 1
    number_files = (int)(number_files * percent / 100.0)
    for index, filename in enumerate(os.listdir(source_directory)):
        if index < number_files:
            shutil.copy(os.path.join(source_directory, filename),
                        os.path.join(test_directory, filename))
        else:
            shutil.copy(os.path.join(source_directory, filename),
                        os.path.join(train_directory, filename))


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage='Usage: %prog [options] <source_dir> <train_dir> <test_dir>')
    parser.add_option('-p', '--percent', type=int, default=10,
                      help='Percent of collection to remain as test')
    options, args = parser.parse_args()
    if len(args) != 3:
        parser.error('Incorrect usage')

    source_directory = args[0]
    train_directory = args[1]
    test_directory = args[2]
    divide_collection(source_directory, train_directory, test_directory,
                      options.percent)