#!/usr/bin/python
# coding: utf-8
import optparse

RUSSIAN_LETTERS = u'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя '


def convert_contrast_file(input_file, output_file):
    words = {}
    total_number_words = 0
    with open(input_file, 'r') as input:
        line = input.readline().strip().split('\t')
        total_number_words = int(line[1])
        for line in input:
            line = line.strip().split('\t')
            is_russian = True
            for letter in line[0].decode('utf-8'):
                if letter not in RUSSIAN_LETTERS:
                    is_russian = False
                    break
            if is_russian:
                words[line[0]] = line[1]
            else:
                total_number_words -= int(line[1])
    with open(output_file, 'w') as output:
        output.write(str(total_number_words) + '\n')
        for word in sorted(words.iterkeys()):
            output.write(word + '\t' + str(words[word]) + '\n')


if __name__ == '__main__':
    parser = optparse.OptionParser(usage='Usage: %prog <input_file> <output_file>')
    options, args = parser.parse_args()
    if len(args) != 2:
        parser.error('Incorrect usage')

    input_file = args[0]
    output_file = args[1]
    convert_contrast_file(input_file, output_file)