#!/usr/bin/python
import json
import math
import optparse
import time
import urllib


TOTAL_NUMBER_WEB_PAGES = 25270000000000


def get_google_count(word):
  query = urllib.urlencode({'q': '"' + word + '"'})
  #url = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&%s' % query
  url = "http://boss.yahooapis.com/ysearch/web/v1/"
  response = urllib.urlopen(url)
  results = response.read()
  results = json.loads(results)
  data = results['responseData']
  time.sleep(20)
  return float(data['cursor']['estimatedResultCount'])


def compute_normalized_google_distance(first_frequency,
                                      second_frequency,
                                      together_frequency):
  max_count = max(math.log(first_frequency), math.log(second_frequency));
  min_count = min(math.log(first_frequency), math.log(second_frequency));
  return ((max_count - math.log(together_frequency)) /
          (math.log(TOTAL_NUMBER_WEB_PAGES) - min_count))


def calculate_normalized_google_distance(input_file, output_file):
  with open(input_file, 'r') as input:
    with open(output_file, 'w') as output:
      for line in input:
        line = line.strip()
        if len(line) == 0:
          continue
        words = line.split(' ')
        first_frequency = get_google_count(words[0])
        print first_frequency
        second_frequency = get_google_count(words[1])
        print second_frequency
        together_frequency = get_google_count(line)
        print together_frequency
        ngd = compute_normalized_google_distance(first_frequency,
                                                 second_frequency,
                                                 together_frequency)
        output.write(line + ' ' + str(ngd) + '\n')


if __name__ == "__main__":
  parser = optparse.OptionParser(usage='Usage: %prog <input_file> <output_file>')
  options, args = parser.parse_args()
  if len(args) != 2:
    parser.error('Incorrect usage')

  input_file = args[0]
  output_file = args[1]
  calculate_normalized_google_distance(input_file, output_file)