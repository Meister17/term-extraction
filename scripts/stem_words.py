#!/usr/bin/env python
from nltk.stem import SnowballStemmer, PorterStemmer, LancasterStemmer
from stemming import porter2, lovins, paicehusk
import optparse
import os


class Porter2Stemmer(object):
  def stem(self, word):
    return porter2.stem(word)


class LovinsStemmer(object):
  def stem(self, word):
    try:
      return lovins.stem(word)
    except IndexError:
      return word


class PaiceHuskStemmer(object):
  def stem(self, word):
    try:
      return paicehusk.stem(word)
    except ValueError:
      return word


class MyStemmer(object):
  def __init__(self, language, similar_letters_number, prefixes_file):
    self.language = language
    self.similar_letters_number = similar_letters_number
    self.prefixes = set()
    if prefixes_file is not None and os.path.exists(prefixes_file):
      with open(prefixes_file, 'r') as input:
        for line in input:
          self.prefixes.add(unicode(line.strip(), 'utf-8', errors='ignore'))

  def stem(self, word):
    if len(word) <= self.similar_letters_number:
      return word
    longest_prefix = self._get_longest_prefix(word)
    if len(longest_prefix) == 0 or len(word) < len(longest_prefix) + 1:
      return word[0:self.similar_letters_number]
    return word[0:len(longest_prefix) + 1]

  def _get_longest_prefix(self, word):
    longest_prefix = ''
    for prefix in self.prefixes:
      if word.startswith(prefix) and len(prefix) > len(longest_prefix):
        longest_prefix = prefix
    return longest_prefix


class NoStemmer(object):
  def stem(self, word):
    return word


def init_stemmer(language, stemmer, similar_letters_number=None,
                 prefixes_file=None):
  if stemmer == "my":
    if language == "russian":
      return MyStemmer(language, similar_letters_number, prefixes_file)
    elif language == "english":
      return MyStemmer(language, similar_letters_number, prefixes_file)
    else:
      raise Exception('Wrong language has been specified for my stemmer')
  elif stemmer == "no":
    if language == "russian" or language == "english":
      return NoStemmer()
    else:
      raise Exception('Wrong language has been specified for no stemmer')
  elif language == "english":
    if stemmer == "porter":
      return PorterStemmer()
    elif stemmer == "lancaster":
      return LancasterStemmer()
    elif stemmer == "porter2":
      return Porter2Stemmer()
    elif stemmer == "lovins":
      return LovinsStemmer()
    elif stemmer == "paicehusk":
      return PaiceHuskStemmer()
    else:
      raise Exception("Wrong stemmer for english language has been specified")
  elif stemmer == "snowball":
    return SnowballStemmer(language)
  else:
    raise Exception("Wrong stemmer for russian language has been specified")


def stem_ngram(ngram, stem_algorithm):
  stem_words = []
  for word in ngram.split():
    if word == 'of':
      continue
    stem_words.append(stem_algorithm.stem(word))
  return ' '.join(stem_words)


def create_similar_sets(input_file, output_file, language, stemmer,
                        similar_letters_number=None, prefixes_file=None):
  stem_algorithm = init_stemmer(language, stemmer, similar_letters_number,
                                prefixes_file)
  stem_dict = {}
  with open(input_file, 'r') as input:
    for line in input:
      tokens = line.strip().split('\t')
      if tokens[-1] == "+":
        good_token = False
        for token in tokens:
          if token == "N" or token == "A" or token == "B":
            good_token = True
        if good_token:
          stemmed_ngram = stem_ngram(unicode(tokens[0], 'utf-8', errors='ignore'),
                                     stem_algorithm)
          for stemmed_word in stemmed_ngram.split():
            if stemmed_word not in stem_dict:
              stem_dict[stemmed_word] = set()
            stem_dict[stemmed_word].add(tokens[0])
  with open(output_file, 'w') as output:
    for stemmed_word, similar_set in sorted(stem_dict.iteritems()):
      if len(similar_set) > 1 and len(stemmed_word) > 0:
        output.write('\t'.join(sorted(similar_set)) + '\t\n')


if __name__ == '__main__':
  parser = optparse.OptionParser(usage='Usage: %prog <input_file> <output_file>')
  parser.add_option('-l', '--language', dest='language', type=str,
                    default='english', help='Language of the text corpus')
  parser.add_option('-s', '--stemmer', dest='stemmer', type=str, default='my',
                    help='Stemmer to use')
  parser.add_option('-p', '--prefixes_file', dest='prefixes_file', type=str,
                    help='Prefixes file for MyStemmer')
  parser.add_option('-n', '--similar_letters_number',
                    dest='similar_letters_number', type=int,
                    help='Number of similar letters to consider words similar')
  options, args = parser.parse_args()
  if len(args) != 2:
    parser.error('Incorrect usage')

  input_file = args[0]
  output_file = args[1]
  create_similar_sets(input_file, output_file, options.language.lower(),
                      options.stemmer.lower(), options.similar_letters_number,
                      options.prefixes_file)
