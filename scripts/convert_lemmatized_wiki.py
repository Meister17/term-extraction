#!/usr/bin/env python
# -*- coding: utf-8 -*-
import multiprocessing as mp
import optparse
import os


def process_english_file(input_filename, output_filename):
  with open(output_filename, 'w') as output:
    with open(input_filename, 'r') as input:
      for line in input:
        line = line.lower()
        tokens = line.strip().split('\t')
        if len(tokens) != 3 or '<doc ' in tokens[0]:
          continue
        elif tokens[0] == '</doc>':
          output.write(line)
        elif not tokens[0].isalnum():
          continue
        else:
          output.write(line)


def process_russian_file(input_filename, output_filename):
  with open(output_filename, 'w') as output:
    with open(input_filename, 'r') as input:
      end_article = ""
      for line in input:
        line = line.decode('cp1251').encode('utf-8')
        tokens = line.strip().split()
        if len(tokens) <= 4:
          continue
        category = unicode(tokens[4], 'utf-8', errors='ignore')
        if category == u"ЦК" or u"ЛЕ" in category:
          if tokens[-2] == "doc" and end_article == "</":
            end_article = "</doc"
          else:
            output.write(line)
        elif tokens[0] == "<" and len(end_article) == 0:
          end_article = "<"
        elif tokens[0] == "/" and end_article == "<":
          end_article = "</"
        elif tokens[0] == ">" and end_article == "</doc":
          output.write("</doc>                    0 -1 0 ЗПР\n")
          end_article = ""
        else:
          continue


def convert_lemmatized_wiki(input_directory, output_directory, language):
  if not os.path.exists(output_directory):
    os.makedirs(output_directory)
  pool = mp.Pool(mp.cpu_count())
  for dirname in os.listdir(input_directory):
    current_input_directory = os.path.join(input_directory, dirname)
    current_output_directory = os.path.join(output_directory, dirname)
    if not os.path.exists(current_output_directory):
      os.makedirs(current_output_directory)
    for filename in os.listdir(current_input_directory):
      if language == "russian":
        pool.apply_async(process_russian_file, (os.path.join(current_input_directory, filename),
                                                os.path.join(current_output_directory, filename)))
      elif language == "english":
        pool.apply_async(process_english_file, (os.path.join(current_input_directory, filename),
                                                os.path.join(current_output_directory, filename)))
      else:
        raise Exception('Wrong language has been specified')
  pool.close()
  pool.join()


if __name__ == '__main__':
  parser = optparse.OptionParser(usage='Usage: %prog <input_directory> <output_directory> <language>')
  options, args = parser.parse_args()
  if len(args) != 3:
    parser.error('Incorrect usage')

  input_directory = args[0]
  output_directory = args[1]
  language = args[2]
  convert_lemmatized_wiki(input_directory, output_directory, language)