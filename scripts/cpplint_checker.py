#!/usr/bin/env python
import optparse
import os
import subprocess


def check_directory(input_directory):
  error_occurred = False
  for root, directoryies, files in os.walk(input_directory):
    for filename in files:
      if filename.endswith('.h') or filename.endswith('.cpp'):
        if subprocess.call('./cpplint.py ' + os.path.join(root, filename), shell=True) != 0:
          error_occurred = True
  if error_occurred:
    print 'Error occurred, see log'
  else:
    print 'No errors!'


if __name__ == '__main__':
  parser = optparse.OptionParser(usage='Usage: %prog <input_directory>')
  options, args = parser.parse_args()
  if len(args) != 1:
    parser.error('Incorrect usage')

  input_directory = args[0]
  check_directory(input_directory)