#!/usr/bin/python
import optparse


def create_banking_thesaurus(input_file, output_file):
    with open(input_file, 'r') as input:
        with open(output_file, 'w') as output:
            for line in input:
                line.replace('----', '')
                line = line.strip().split('\t')
                output.write(line[1] + '\n')


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage='Usage: %prog <input_file> <output_file>')
    options, args = parser.parse_args()
    if len(args) != 2:
        parser.error('Incorrect usage')

    input_file = args[0]
    output_file = args[1]
    create_banking_thesaurus(input_file, output_file)
