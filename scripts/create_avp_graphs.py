#!/usr/bin/python
import matplotlib.pyplot as plt
import optparse
import os

SINGLE_WORD_FEATURES = ['tfidf', 'cvalue', 'weirdness', 'maximum_ts_plsa', 'gradient_boosting']
TWO_WORD_FEATURES = ['tfidf', 'term_score_plsa', 'mutual_information', 'modified_gravity_count', 'gradient_boosting']
MODELS = ['unified_rus', 'unified_en', 'summed_up_rus', 'summed_up_en']


def compute_avps(filename):
    thresholds = []
    avps = []
    true_positive = 0.0
    avp = 0.0
    with open(filename, 'r') as input:
        for index, line in enumerate(input):
            line = line.strip().split('\t')
            if len(line) != 3:
                continue
            if line[2] == '+':
                true_positive += 1.0
                avp += true_positive / (index + 1)
            thresholds.append(index)
            avps.append(avp / true_positive * 100 if true_positive > 0 else 0.0)
    return thresholds, avps


def create_avp_graphs(source_directory, output_filename, candidates_type,
                      file_format):
    plt.xlabel('Number of the most frequent candidates')
    plt.ylabel('AvP, %')
    for filename in os.listdir(source_directory):
        feature_index = -1
        if candidates_type == 'singleword':
            feature_index = SINGLE_WORD_FEATURES.index(os.path.splitext(filename)[0])
        elif candidates_type == 'twoword':
            feature_index = TWO_WORD_FEATURES.index(os.path.splitext(filename)[0])
        elif candidates_type == 'models':
            feature_index = MODELS.index(os.path.splitext(filename)[0])
        else:
            raise Exception('Wrong type of candidates')
        line_type = ''
        if feature_index == 0:
            line_type = 'b-.'
        elif feature_index == 1:
            line_type = 'k:'
        elif feature_index == 2:
            line_type = 'm-'
        elif feature_index == 3 or feature_index == 5:
            line_type = 'g--'
        elif feature_index == 4:
            line_type = 'r-'
        else:
            raise Exception('Wrong feature')
        thresholds, avps = compute_avps(os.path.join(source_directory, filename))
        plt.plot(thresholds, avps, line_type, label=os.path.splitext(filename)[0])
    plt.ylim((15, 100))
    plt.legend(loc=1)
    plt.savefig(output_filename, format=file_format)


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage='Usage: %prog [options] <candidates_type> <source_directory> <graph_filename>')
    parser.add_option('-f', '--format', dest='format', type=str, default='png',
                      help='Format of file for printing graphs')
    options, args = parser.parse_args()
    if len(args) != 3:
        parser.error('Incorrect usage')

    candidates_type = args[0]
    source_directory = args[1]
    graph_filename = args[2]
    create_avp_graphs(source_directory, graph_filename, candidates_type,
                      options.format)
