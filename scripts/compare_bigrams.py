#!/usr/bin/env python3
from argparse import ArgumentParser


def load_bigrams(filename, number_bigrams):
    bigrams = set()
    with open(filename, 'r') as file_input:
        for index, line in enumerate(file_input):
            if index == number_bigrams:
                break
            bigrams.add(line.strip().split('\t')[0])
    return bigrams


def compare_bigram_lists(first_filename, second_filename, number_bigrams):
    first_bigrams = load_bigrams(first_filename, number_bigrams)
    second_bigrams = load_bigrams(second_filename, number_bigrams)
    print('Same' if first_bigrams == second_bigrams else 'Different')
    


if __name__ == '__main__':
    parser = ArgumentParser(description='Bigram lists comparator')
    parser.add_argument('first_file')
    parser.add_argument('second_file')
    parser.add_argument('-n', '--number_bigrams', type=int, default=1000)
    args = parser.parse_args()
    compare_bigram_lists(args.first_file, args.second_file, args.number_bigrams)
