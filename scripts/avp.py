#!/usr/bin/python
import optparse


def calculateAvP(input_file, thesaurus_file, output_file):
    terms = set()
    with open(thesaurus_file, 'r') as input:
        for line in input:
            terms.add(line.strip())
    avp = 0.0
    true_positive = 0.0
    with open(output_file, 'w') as output:
        with open(input_file, 'r') as input:
            for index, line in enumerate(input):
                candidates = line.strip().split(' ')
                if (len(candidates) <= 2 or
                        (len(candidates) == 3 and candidates[1] == 'of')):
                    if line.strip() in terms:
                        output.write(line.strip() + '\t+\n')
                        true_positive += 1
                        avp += true_positive / (index + 1)
                    else:
                        output.write(line.strip() + '\t-\n')
        if true_positive > 0:
            output.write('Average Precision: ' + str(avp / true_positive) + '\n')
        else:
            output.write('Average Precision: 1\n')


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage='Usage: %prog <input_file> <thesaurus_file> <output_file>')
    options, args = parser.parse_args()
    if len(args) != 3:
        parser.error('Incorrect usage')
    input_file = args[0]
    thesaurus_file = args[1]
    output_file = args[2]
    calculateAvP(input_file, thesaurus_file, output_file)
