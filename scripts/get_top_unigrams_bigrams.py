#!/usr/bin/env python
import operator
import optparse


def load_vocabulary(filename):
  vocabulary = []
  with open(filename, 'r') as input:
    for line in input:
      tokens = line.strip().split('\t')
      vocabulary.append(tokens[0])
  return vocabulary


def print_vocabulary(input_filename, output_filename, ngrams=None):
  with open(output_filename, 'w') as output:
    with open(input_filename, 'r') as input:
      for line in input:
        tokens = line.strip().split('\t')
        if ngrams is None or tokens[0] in ngrams:
          output.write(line)


def form_reindex(vocabulary, ngrams):
  reindex = {}
  for index, item in enumerate(vocabulary):
    if item in ngrams:
      reindex[index] = len(reindex)
  return reindex


def load_input(filename, reindex):
  data = []
  with open(filename, 'r') as input:
    for line in input:
      data.append({})
      tokens = line.strip().split()
      for token in tokens:
        parts = token.strip().split(':')
        item_index = int(parts[0])
        if item_index in reindex:
          data[-1][item_index] = int(parts[1])
  return data


def create_input(output_filename, unigram_input, reindex):
  with open(output_filename, 'w') as output:
    for document in unigram_input:
      for unigram, frequency in document.iteritems():
        output.write(str(reindex[unigram]) + ':' + str(frequency) + '\t')
      output.write('\n')


def create_new_input(unigram_voc_filename, unigram_input_filename,
                     unigram_list_filename, bigram_list_filename,
                     result_voc_filename, result_input_filename,
                     result_bigram_list_file):
  unigram_vocabulary = load_vocabulary(unigram_voc_filename)
  unigrams = set(load_vocabulary(unigram_list_filename))
  print_vocabulary(unigram_voc_filename, result_voc_filename, unigrams)
  print_vocabulary(bigram_list_filename, result_bigram_list_file)
  unigram_reindex = form_reindex(unigram_vocabulary, unigrams)
  unigram_input = load_input(unigram_input_filename, unigram_reindex)
  create_input(result_input_file, unigram_input, unigram_reindex)


if __name__ == '__main__':
  parser = optparse.OptionParser(
      usage='Usage: %prog [options] <unigram_voc_file> <unigram_input_file> ' +
            '<unigram_list_file> <bigram_list_file> <result_voc_file> ' +
            '<result_input_file> <result_bigram_list_file>')
  options, args = parser.parse_args()
  if len(args) != 7:
    parser.error('Incorrect usage')

  unigram_voc_file = args[0]
  unigram_input_file = args[1]
  unigram_list_file = args[2]
  bigram_list_file = args[3]
  result_voc_file = args[4]
  result_input_file = args[5]
  result_bigram_list_file = args[6]
  create_new_input(unigram_voc_file, unigram_input_file, unigram_list_file,
                   bigram_list_file, result_voc_file, result_input_file,
                   result_bigram_list_file)