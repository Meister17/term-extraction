#!/usr/bin/env python
import calculate_avp
import operator
import optparse
import os


def get_rangs(features):
  rangs = {}
  for index, (term, value) in enumerate(sorted(features.iteritems(),
                                               key=operator.itemgetter(1),
                                               reverse=True)):
    rangs[term] = index
  return rangs


def load_file(filename, terms, adding_terms):
  features = {}
  with open(filename, 'r') as input:
    for line in input:
      tokens = line.strip().split('\t')
      if len(tokens) != 3:
        continue
      if tokens[0] not in terms:
        if adding_terms:
          terms[tokens[0]] = tokens[2]
        else:
          raise Exception(tokens[0])
      features[tokens[0]] = float(tokens[1])
  return features


def load_files(single_word_filename, two_word_filename, terms):
  adding_terms = len(terms) == 0
  features = load_file(single_word_filename, terms, adding_terms)
  features.update(load_file(two_word_filename, terms, adding_terms))
  return get_rangs(features)


def get_next_rangs(rangs_to_choose, total_number):
  has_next_rangs = False
  good_rangs = False
  while not good_rangs:
    for index in range(len(rangs_to_choose) - 1, -1, -1):
      if rangs_to_choose[index] == total_number - 1:
        rangs_to_choose[index] = 0
      else:
        rangs_to_choose[index] += 1
        has_next_rangs = True
        break
    if len(set(rangs_to_choose)) == len(rangs_to_choose) or (len(set(rangs_to_choose)) == 1 and rangs_to_choose[0] == 0):
      good_rangs = True
      if len(set(rangs_to_choose)) == 1 and rangs_to_choose[0] == 0:
        has_next_rangs = False
  return has_next_rangs


def combine_files(rangs_to_choose, input_rangs, threshold):
  rangs = []
  for index in rangs_to_choose:
    rangs.append(input_rangs[index])
  result_rangs = {}
  for term, first_index in rangs[0].iteritems():
    best_index = first_index
    for second_index in range(1, len(rangs)):
      best_index = max(best_index, rangs[second_index][term])
    result_rangs[term] = best_index
  result_list = [None] * len(result_rangs)
  for index, (term, term_index) in enumerate(sorted(result_rangs.iteritems(),
                                                    key=operator.itemgetter(1))):
    result_list[index] = term
  thresholds, avps = calculate_avp.calculate_avp(terms, result_list)
  return avps[threshold], result_list


def print_output(output_file, result_list, terms):
  with open(output_file, 'w') as output:
    for index, term in enumerate(result_list):
      output.write(term + '\t' + str(index) + '\t' + terms[term] + '\n')


def print_best_result(rangs_to_choose, input_files, avp):
  result = ''
  for index in rangs_to_choose:
    result += input_files[index] + ' '
  print result + 'AvP: ' + str(avp)


if __name__ == '__main__':
  parser = optparse.OptionParser(
      usage='Usage: %prog [options] <single_word_dir> <two_word_dir> <output_file>')
  parser.add_option('-t', '--threshold', dest='threshold', type=int, default=5000,
                    help='Threshold for terms')
  parser.add_option('-n', '--number_files', dest='number_files', type=int,
                    default=5, help='Number of files to join')
  options, args = parser.parse_args()
  if len(args) != 3:
    parser.error('Incorrect usage')

  single_word_dir = args[0]
  two_word_dir = args[1]
  output_file = args[2]
  terms = {}
  input_rangs = []
  input_files = []
  for filename in os.listdir(single_word_dir):
    if '_lda.txt' not in filename and os.path.isfile(os.path.join(single_word_dir, filename)):
      input_rangs.append(load_files(os.path.join(single_word_dir, filename),
                                    os.path.join(two_word_dir, filename),
                                    terms))
      input_files.append(filename)
  rangs_to_choose = [0] * options.number_files
  for index in range(0, len(rangs_to_choose)):
    rangs_to_choose[index] = len(rangs_to_choose) - index - 1
  rangs_to_choose[-1] = -1
  best_avp = 0
  while get_next_rangs(rangs_to_choose, len(input_rangs)):
    avp, result_list = combine_files(rangs_to_choose, input_rangs, options.threshold)
    if avp > best_avp:
      print_best_result(rangs_to_choose, input_files, avp)
      best_avp = avp
      print_output(output_file, result_list, terms)
