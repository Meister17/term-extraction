#!/usr/bin/env python
import optparse


def check_bigrams(filename):
  bigrams = set()
  with open(filename, 'r') as input:
    for line in input:
      tokens = line.strip().split(' ')
      first = line.strip()
      element = tokens[0]
      tokens[0] = tokens[-1]
      tokens[-1] = element
      second = ' '.join(tokens)
      if first in bigrams or second in bigrams:
        print first
        print second
        print 'Wrong file'
      else:
        bigrams.add(first)


if __name__ == '__main__':
  parser = optparse.OptionParser(usage='Usage: %prog <filename>')
  options, args = parser.parse_args()
  if len(args) != 1:
    parser.error('Incorrect usage')

  filename = args[0]
  check_bigrams(filename)