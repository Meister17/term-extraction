#!/usr/bin/python
import math
import numpy as np
import optparse

def load_matrix(filename):
    number_rows = 0
    number_columns = 0
    with open(filename, 'r') as input:
        for line in input:
            number_rows += 1
            number_columns = len(line.strip().split())
    matrix = np.zeros((number_rows, number_columns))
    with open(filename, 'r') as input:
        for row, line in enumerate(input):
            words = line.strip().split()
            for column, word in enumerate(words):
                matrix[row, column] = float(word)
    return matrix


def calculate_perplexity(wt_prob_file, td_prob_file, wt_assign_file):
    wt = np.transpose(load_matrix(wt_prob_file))
    td = np.transpose(load_matrix(td_prob_file))
    numerator = 0.0
    denominator = 0.0
    with open(wt_assign_file, 'r') as input:
        for document, line in enumerate(input):
            words_map = {}
            words = line.strip().split()
            for element in words:
                tokens = element.split(':')
                word = int(tokens[0])
                if word not in words_map:
                    words_map[word] = 1
                else:
                    words_map[word] += 1
            for word, ndw in words_map.iteritems():
                numerator += ndw * math.log(np.dot(wt[word,:], td[:,document]))
                denominator += ndw
    return math.exp(-numerator / denominator)


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage='Usage: %prog <wt_prob_file> <td_prob_file> <wt_assign_file>')
    options, args = parser.parse_args()
    if len(args) != 3:
        parser.error('Incorrect usage')

    wt_prob_file = args[0]
    td_prob_file = args[1]
    wt_assign_file = args[2]
    perplexity = calculate_perplexity(wt_prob_file, td_prob_file,
                                      wt_assign_file)
    print 'Perplexity:', perplexity

