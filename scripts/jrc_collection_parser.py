#!/usr/bin/python
import lxml.etree
import os
import optparse
import re
import shutil
import xml.sax.saxutils


def process_directory(input_directory, output_directory):
    if os.path.exists(output_directory):
        shutil.rmtree(output_directory)
    os.makedirs(output_directory)
    file_number = 0
    for root, directories, files in os.walk(input_directory):
        for filename in files:
            print 'Parsing file #%d: %s' % (file_number, os.path.join(root, filename))
            if filename.endswith('.xml'):
                with open(os.path.join(output_directory, str(file_number) + '.txt'), 'w') as output:
                    tree = lxml.etree.parse(os.path.join(root, filename))
                    for p in tree.xpath('/TEI.2/text/body/div/p'):
                        text = xml.sax.saxutils.unescape(p.text).encode('utf-8')
                        text = re.sub(r'%lt%', r'<', text)
                        text = re.sub(r'%gt%', r'>', text)
                        text = re.sub(r'%quot%', r'"', text)
                        text = re.sub(r'%amp%', r'&', text)
                        output.write(text + '\n')
                file_number += 1


if __name__ == '__main__':
    parser = optparse.OptionParser(usage='Usage: %prog <input_directory> <output_directory>')
    options, args = parser.parse_args()
    if len(args) != 2:
        parser.error('Incorrect usage')

    input_directory = args[0]
    output_directory = args[1]
    process_directory(input_directory, output_directory)
