#!/usr/bin/env python
import optparse
import os


def reencode_dir(input_dirname, output_dirname, from_enc, to_enc):
  if not os.path.exists(output_dirname):
    os.makedirs(output_dirname)
    for filename in os.listdir(input_dirname):
      with open(os.path.join(output_dirname, filename) + '.txt', 'w') as output:
        with open(os.path.join(input_dirname, filename), 'r') as input:
          for line in input:
            output.write(line.decode(from_enc).encode(to_enc, errors='ignore'))


if __name__ == '__main__':
  parser = optparse.OptionParser(usage='Usage: %prog <input_dirname> <output_dirname> <from_enc> <to_enc>')
  options, args = parser.parse_args()
  if len(args) != 4:
    parser.error('Incorrect usage')

  input_dirname = args[0]
  output_dirname = args[1]
  from_enc = args[2]
  to_enc = args[3]
  reencode_dir(input_dirname, output_dirname, from_enc, to_enc)
