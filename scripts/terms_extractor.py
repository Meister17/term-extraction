#!/usr/bin/python
import copy
import pylab as pl
import numpy as np
from optparse import OptionParser
import os
from sklearn import ensemble
from sklearn.linear_model import LogisticRegression


def load_file(filename):
    print 'Loading file'
    features = []
    with open(filename, 'r') as input:
        line = input.readline().strip().split('\t')
        for index in range(1, len(line) - 1):
            features.append(line[index])
        number_columns = len(features)
        number_rows = len(list(input))
    words = [None] * number_rows
    data = np.zeros((number_rows, number_columns))
    labels = np.zeros(number_rows)
    with open(filename, 'r') as input:
        input.readline()
        for first_index, line in enumerate(input):
            line = line.strip().split('\t')
            words[first_index] = line[0]
            labels[first_index] = 1 if line[number_columns + 1] == 'term' else 0
            for second_index in range(0, number_columns):
                data[first_index, second_index] = float(line[second_index + 1])
    return np.array(features), words, data, labels


def train(data, labels, estimators, learning_rate, max_depth, subsample, random_state):
    model = ensemble.GradientBoostingClassifier(
        n_estimators=estimators,
        learning_rate=learning_rate,
        max_depth=max_depth,
        subsample=subsample,
        random_state=random_state,
        loss='deviance').fit(data, labels)
    #model = LogisticRegression(penalty="L1", tol=1e-6, C=500).fit(data, labels)
    return model


def plot_feature_importance(feature_importance, number_features, features, picture_name):
    feature_importance = 100.0 * (feature_importance / feature_importance.max())
    sorted_idx = np.argsort(feature_importance)[::-1]
    sorted_idx = sorted_idx[0:number_features][::-1]
    pos = np.arange(sorted_idx.shape[0]) + 0.5
    pl.figure(figsize=(20, 5))
    pl.grid(True)
    print feature_importance[sorted_idx]
    pl.barh(pos, feature_importance[sorted_idx], align='center')
    pl.yticks(pos, features[sorted_idx])
    pl.xlabel('Relative Importance')
    pl.savefig(picture_name)


def predict(model, data, start_index, candidates_list):
    predictions = model.predict_proba(data)[:, 1]
    for index in xrange(len(predictions)):
        candidates_list[start_index + index] = predictions[index]


def calculateAvP(predictions, labels):
    assert len(predictions) == len(labels)
    all_indices = range(len(predictions))
    indices = [index for (prediction, index) in sorted(zip(predictions, all_indices))][::-1]
    average_precision = 0.0
    true_positive = 0.0
    for index, sorted_index in enumerate(indices):
        if labels[sorted_index] == 1:
            true_positive += 1.0
            average_precision += true_positive / (index + 1)
    if true_positive > 0:
        return average_precision / true_positive
    else:
        return 1.0


def print_candidates(filename, words, candidates_list, labels, average_precision):
    print 'Printing candidates'
    all_indices = range(len(candidates_list))
    indices = [index for (prediction, index) in sorted(zip(candidates_list, all_indices))][::-1]
    with open(filename, 'w') as output:
        for index in indices:
            if labels[index] == 1:
                output.write(words[index] + '\t' + str(candidates_list[index]) + '\t+\n')
            else:
                output.write(words[index] + '\t' + str(candidates_list[index]) + '\t-\n')
        output.write('Average precision: ' + str(average_precision) + '\n')


def classify(features, words, data, labels, estimators, learning_rate,
             max_depth, subsample, random_state, picture_filename=None,
             number_features=None, filename=None):
    average_precision = 0.0
    predictions = np.zeros(len(data))
    feature_importance = np.zeros(len(features))
    for iteration in xrange(4):
        print 'Iteration', iteration
        start_index = iteration * len(data) / 4
        end_index = (iteration + 1) * len(data) / 4
        test_indices = range(start_index, end_index)
        train_indices = \
            [index for index in range(len(data)) if index < start_index or index >= end_index]
        model = train(
            data[train_indices, :],
            labels[train_indices],
            estimators,
            learning_rate,
            max_depth,
            subsample,
            random_state)
        if picture_filename is not None:
            feature_importance += model.feature_importances_
        predict(model, data[test_indices, :], start_index, predictions)
    average_precision = calculateAvP(predictions, labels)
    if filename is not None:
        print_candidates(filename, words, predictions, labels, average_precision)
    if picture_filename is not None:
        plot_feature_importance(feature_importance / 4, number_features,
                                features, picture_filename)
    return average_precision


def train_and_test(train_filename, test_filename, learning_rate, subsample,
                   random_state, output_filename):
    train_features, train_words, train_data, train_labels = load_file(train_filename)
    test_features, test_words, test_data, test_labels = load_file(test_filename)
    best_estimator = 0
    best_max_depth = 0
    best_average_precision = 0
    for estimator in range(100, 251, 10):
        for max_depth in range(3, 8):
            model = train(train_data, labels, estimator, learning_rate, max_depth,
                          subsample, random_state)
            predictions = np.zeros(len(test_data))
            predict(model, test_data, 0, predictions)
            average_precision = calculateAvP(predictions, test_labels)
            print '%d\t%d\t%g' % (estimator, max_depth, average_precision)
            if average_precision > best_average_precision:
                best_average_precision = average_precision
                best_estimator = estimator
                best_max_depth = max_depth
                print 'New maximum found', average_precision
    if output_filename is not None:
        print_candidates(output_filename, test_words, predictions, test_labels, best_average_precision)
    return best_average_precision


def find_best_features(features, words, data, labels, estimators, learning_rate, max_depth, subsample, random_state, filename):
    print 'Finding best features'
    best_indices = []
    best_average_precision = 0.0
    best_index = 0
    while best_index != -1:
        best_index = -1
        for first_index in xrange(data.shape[1]):
            try:
                best_indices.index(first_index)
                continue
            except ValueError:
                pass
            print 'Trying feature ', features[first_index]
            current_indices = copy.deepcopy(best_indices)
            current_indices.append(first_index)
            new_average_precision = \
                classify(features,
                         words,
                         data[:, current_indices],
                         labels,
                         estimators,
                         learning_rate,
                         max_depth,
                         subsample,
                         random_state)
            if new_average_precision >= best_average_precision:
                best_index = first_index
                best_average_precision = new_average_precision
                print 'Best feature: %s, AvP: %g' % (features[best_index], best_average_precision)
        if best_index != -1:
            best_indices.append(best_index)
            print 'Selected new best feature: ', features[best_index]
    return classify(features,
                    words,
                    data[:, best_indices],
                    labels,
                    estimators,
                    learning_rate,
                    max_depth,
                    subsample,
                    random_state,
                    filename=filename)

if __name__ == '__main__':
    parser = OptionParser(usage='Usage: %prog [options] <input_file> <output_file>')
    parser.add_option('-e', '--estimators', dest='estimators', type=int, default=100,
        help='Number of estimators for gradient boosting')
    parser.add_option('-l', '--learning_rate', dest='learning_rate', type=float, default=0.05,
        help='Learning rate for gradient boosting')
    parser.add_option('-d', '--max_depth', dest='max_depth', type=int, default=8,
        help='Maximum depth of trees in gradient boosting')
    parser.add_option('-r', '--random_state', dest='random_state', type=int, default=0,
        help='Random state for gradient boosting')
    parser.add_option('-s', '--subsample', dest='subsample', type=float, default=0.5,
        help='Subsample for gradient boosting')
    parser.add_option('-m', '--mode', dest='mode', type=str, default='classify_best',
        help='Mode of program: classification or finding best features')
    parser.add_option('-p', '--picture_filename', dest='picture_filename', type=str,
        help='File for plotting feature importance')
    parser.add_option('-n', '--number_features', dest='number_features', type=int,
        help='Number of important features')
    parser.add_option('-f', '--test_file', dest='test_file', type=str,
        help='File containing test data')
    parser.add_option('-a', '--algorithm', dest='algorithm', type=str,
        help='Machine learning algorithm')

    options, args = parser.parse_args()
    if len(args) != 2:
        parser.error('Incorrect usage')
    train_file = args[0]
    output_file = args[1]

    features, words, train_data, labels = load_file(train_file)
    if options.mode == 'classify':
        average_precision = classify(features, words, train_data, labels,
                                     options.estimators, options.learning_rate,
                                     options.max_depth, options.subsample,
                                     options.random_state,
                                     options.picture_filename,
                                     options.number_features, output_file)
    elif options.mode == 'find_features':
        average_precision = find_best_features(features, words, train_data,
                                               labels, options.estimators,
                                               options.learning_rate,
                                               options.max_depth,
                                               options.subsample,
                                               options.random_state,
                                               output_file)
    elif options.mode == 'classify_best':
        best_estimator = 0
        best_max_depth = 0
        best_average_precision = 0
        for estimator in range(100, 251, 10):
            for max_depth in range(3, 8):
                average_precision = classify(features, words, train_data,
                                             labels, estimator,
                                             options.learning_rate, max_depth,
                                             options.subsample,
                                             options.random_state,
                                             options.picture_filename,
                                             options.number_features)
                print '%d\t%d\t%g' % (estimator, max_depth, average_precision)
                if average_precision > best_average_precision:
                    best_average_precision = average_precision
                    best_estimator = estimator
                    best_max_depth = max_depth
                    print 'New maximum found', average_precision
    elif options.mode == 'train_and_test':
        average_precision = train_and_test(train_file, options.test_file,
                                           options.learning_rate,
                                           options.subsample,
                                           options.random_state,
                                           output_file)
        print 'AvP:', average_precision
    else:
        parser.error('Incorrect usage')
