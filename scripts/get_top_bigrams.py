#!/usr/bin/env python
import operator
import optparse


def get_top_bigrams(input_filename, output_filename):
  bigrams = {}
  with open(input_filename, 'r') as input:
    for line in input:
      tokens = line.strip().split('\t')
      bigrams[tokens[0]] = int(tokens[1])
  with open(output_filename, 'w') as output:
    for bigram, frequency in sorted(bigrams.iteritems(),
                                    key=operator.itemgetter(1),
                                    reverse=True):
      output.write(bigram + '\t' + str(frequency) + '\tB\t-\n')


if __name__ == '__main__':
  parser = optparse.OptionParser(usage='Usage: %prog <input_file> <output_file>')
  options, args = parser.parse_args()
  if len(args) != 2:
    parser.error('Incorrect usage')

  input_file = args[0]
  output_file = args[1]
  get_top_bigrams(input_file, output_file)