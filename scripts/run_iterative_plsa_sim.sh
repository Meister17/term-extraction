#!/usr/bin/env bash

set -x
set -e

if [ "$#" -ne 7 ]; then
    echo "Usage: $0 <input_directory> <output_directory> <language> <iterations> <topics_number> <stopwords_file> <inverted_index_file>"
    exit 1
fi

if [ ! -d $1 ]; then
    echo "Input directory does not exist"
    exit 1
fi

if [ ! -d $2 ]; then
    mkdir -p $2
fi

stemmer='snowball'
input_directory=$(realpath $1)
output_directory=$(realpath $2)
stopwords_file=$(realpath $6)

if [ $3 == 'english' ]; then
    stemmer='lancaster'
elif [ $3 != "russian" ]; then
    echo "Wrong language. Specify 'russian' or 'english'"
    exit 1
fi

positive_number="^[0-9]+$"
if ! [[ $4 =~ $positive_number ]]; then
    echo "Wrong number of iterations"
    exit 1
fi

if ! [[ $5 =~ $positive_number ]]; then
    echo "Wrong number of topics"
    exit 1
fi

if [ ! -f $6 ]; then
    echo "Wrong stopwords file"
    exit 1
fi

if [ ! -f $7 ]; then
    echo "Wrong inverted index of Wikipedia"
    exit 1
fi

bigrams_directory=${output_directory%%/}/Bigrams
if [ ! -d $bigrams_directory ]; then
    mkdir -p $bigrams_directory
fi

echo 'Compiling projects'
pushd . >> /dev/null
cd ../Experiments/LDAInputCreator
scons
cd ../BigramsAdder
scons
cd ../../TopicModels
scons
popd >> /dev/null

pushd . >> /dev/null
cd ../Experiments/LDAInputCreator
echo "Creating input for iterative PLSA-SIM algorithm"
./lda_input_creator --language=$3 --minimum_frequency=5 \
    --input_directory_name=$input_directory --stopwords_filename=$stopwords_file \
    --unigram_lda_input_filename=${output_directory%%/}/0_iteration_lda_input.txt \
    --unigram_lda_vocabulary_filename=${output_directory%%/}/0_iteration_lda_vocabulary.txt \
    --bigram_lda_input_filename=${output_directory%%/}/bigram_lda_input.txt \
    --bigram_lda_vocabulary_filename=${output_directory%%/}/bigram_lda_vocabulary.txt \
    --word_association_measures=modified_dice_coefficient \
    --word_association_directory_name=$bigrams_directory
popd >> /dev/null

pushd . >>/dev/null
cd ../TopicModels
echo 'Starting zero iteration'
echo 'Running PLSA-SIM algorithm'
./topic_model_analyzer --seed_number=239 \
    --input_filename=${output_directory%%/}/0_iteration_lda_input.txt \
    --vocabulary_filename=${output_directory%%/}/0_iteration_lda_vocabulary.txt \
    --inverted_index_filename=$7 --topics_number=$5 --algorithm=PLSA \
    --algorithm_options=0,0,100,1e-6 \
    --evaluation_measure=Perplexity,TC_PMI,TC_PMI_MORPHOLOGY \
    --test_part=0.1 --top_words_number=1000000 --top_words_tc_number=10 \
    --topic_words_filename=${output_directory%%/}/0_iteration_topic_words.txt \
    --logging_directory_name=$output_directory
popd >> /dev/null

for iteration in `seq 1 $4`;
do
    pushd . >> /dev/null
    echo 'Starting '$iteration' iteration'
    echo 'Incorporating similar words into PLSA-SIM algorithm'
    prev_iteration=$(($iteration - 1))
    ./form_lda_collocations.py \
        ${output_directory%%/}/${prev_iteration}_iteration_topic_words.txt \
        ${output_directory%%/}/bigram_lda_vocabulary.txt \
        ${output_directory%%/}/${prev_iteration}_iteration_lda_vocabulary.txt \
        $bigrams_directory/${iteration}_iteration_bigrams.txt \
        ${output_directory%%/}/lda_vocabulary_similar.txt -t 1000000
    cd ../Experiments/BigramsAdder
    echo 'Incorporating bigrams into PLSA-SIM algorithm'
    ./bigrams_adder --use_morphology=true \
        --unigram_vocabulary_filename=${output_directory%%/}/lda_vocabulary_similar.txt \
        --unigram_input_filename=${output_directory%%/}/${prev_iteration}_iteration_lda_input.txt \
        --bigram_vocabulary_filename=${output_directory%%/}/bigram_lda_vocabulary.txt \
        --bigram_input_filename=${output_directory%%/}/bigram_lda_input.txt \
        --bigram_list_filename=$bigrams_directory/${iteration}_iteration_bigrams.txt \
        --bigrams_number=100000 \
        --output_vocabulary_filename=${output_directory%%/}/${iteration}_iteration_lda_vocabulary.txt \
        --output_input_filename=${output_directory%%/}/${iteration}_iteration_lda_input.txt
    cd ../../scripts
    echo 'Creating similar words and bigrams'
    ./stem_words.py ${output_directory%%/}/${iteration}_iteration_lda_vocabulary.txt \
        ${output_directory%%/}/${iteration}_similar_sets.txt -l $3 -s $stemmer
    cd ../TopicModels
    echo 'Running PLSA-SIM algorithm'
        ./topic_model_analyzer --seed_number=239 \
        --input_filename=${output_directory%%/}/${iteration}_iteration_lda_input.txt \
        --vocabulary_filename=${output_directory%%/}/${iteration}_iteration_lda_vocabulary.txt \
        --inverted_index_filename=$7 \
        --similar_sets_filename=${output_directory%%/}/${iteration}_similar_sets.txt \
        --topics_number=$5 --algorithm=PLSA --algorithm_options=0,0,100,1e-6 \
        --evaluation_measure=Perplexity,TC_PMI,TC_PMI_MORPHOLOGY \
        --test_part=0.1 --top_words_number=1000000 --top_words_tc_number=10 \
        --topic_words_filename=${output_directory%%/}/${iteration}_iteration_topic_words.txt \
        --logging_directory_name=$output_directory
    popd >> /dev/null
done

echo 'Iterative PLSA-SIM algorithm successfully finished'
echo 'See results in '${2%%/}/$4'_iteration_topic_words.txt'
