#!/usr/bin/python
import numpy as np
import optparse
import os


def parse_files(source_directory, weight_names, file_extension):
    candidates = {}
    for filename in os.listdir(source_directory):
        if filename.endswith(file_extension):
            try:
                weight_index = weight_names.index(os.path.splitext(filename)[0])
            except ValueError:
                print 'Failed to find weight:', os.path.splitext(filename)[0]
                continue
            filename = os.path.join(source_directory, filename)
            with open(filename, 'r') as input:
                for line in input:
                    line = line.strip().split('\t')
                    if len(line) != 3:
                        continue
                    term = line[0].replace(' ', '_')
                    if term not in candidates:
                        candidates[term] = np.zeros(len(weight_names) + 1)
                    candidates[term][weight_index] = float(line[1])
                    is_term = 1 if line[2] == '+' else 0
                    candidates[term][len(weight_names)] = is_term
    return candidates


def create_ml_file(source_directory, output_file, file_format,
                   auxiliary_directory, file_extension):
    weight_names = []
    for filename in os.listdir(source_directory):
        if filename.endswith(file_extension):
            weight_names.append(os.path.splitext(filename)[0])
    candidates = parse_files(source_directory, weight_names, file_extension)
    if auxiliary_directory is not None:
        candidates.update(parse_files(auxiliary_directory, weight_names,
                                      file_extension))
    with open(output_file, 'w') as output:
        if file_format == 'arff':
            output.write('@RELATION weights\n\n')
            output.write('@ATTRIBUTE extracted_term STRING')
            for weight_name in weight_names:
                output.write('@ATTRIBUTE ' + weight_name + ' NUMERIC\n')
            output.write('@ATTRIBUTE class {term. term_not}\n\n@DATA\n')
            separator = ','
        elif file_format == 'R':
            output.write('extracted_term\t')
            for weight_name in weight_names:
                output.write(weight_name + '\t')
            output.write('class\n')
            separator = '\t'
        else:
            raise Exception("wrong file format")
        for candidate, values in candidates.iteritems():
            output.write(candidate + separator)
            for value in values[0:len(weight_names)]:
                output.write(str(value) + separator)
            if values[-1] == 1:
                output.write('term\n')
            else:
                output.write('term_not\n')


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage='Usage: %prog [options] <source_directory> <output_file>')
    parser.add_option('-f', '--format', dest='file_format', type=str,
                      help='Format of file to be created')
    parser.add_option('-a', '--auxiliary_directory', dest='auxiliary_directory',
                      type=str, help='Directory containing auxiliary terms')
    parser.add_option('-e', '--file_extension', dest='file_extension', type=str,
                      help='Extension of files to be processed')
    options, args = parser.parse_args()
    if len(args) != 2:
        parser.error('Incorrect usage')

    source_directory = args[0]
    output_file = args[1]
    create_ml_file(source_directory, output_file, options.file_format,
                   options.auxiliary_directory, options.file_extension)
