#!/usr/bin/python
import codecs
import numpy as np
import optparse


def load_input(vocabulary_filename, input_filename):
    input_data = []
    vocabulary = []
    pos_vocabulary = []
    with codecs.open(input_filename, 'r', 'utf-8') as input:
        for line in input:
            tokens = line.strip().split(' ')
            input_data.append({})
            if len(tokens) > 0 and int(tokens[0]) > 0:
                for index in range(1, len(tokens)):
                    parts = tokens[index].strip().split(':')
                    input_data[-1][int(parts[0])] = int(parts[1])
    with codecs.open(vocabulary_filename, 'r', 'utf-8') as input:
        for line in input:
            tokens = line.strip().split('\t')
            vocabulary.append(tokens[0])
            pos_vocabulary.append(line.strip())
    return vocabulary, input_data, pos_vocabulary


def load_bigrams(filename, number_bigrams):
    bigrams = set()
    with codecs.open(filename, 'r', 'utf-8') as input:
        for index, line in enumerate(input):
            if index == number_bigrams:
                break
            tokens = line.strip().split('\t')
            bigrams.add(tokens[0])
    return bigrams


def create_result_input(unigram_vocabulary, unigram_input, unigram_pos_vocabulary,
                        bigram_vocabulary, bigram_input, bigram_list):
    assert len(unigram_input) == len(bigram_input)
    result_vocabulary = unigram_pos_vocabulary
    add_bigrams = {}
    result_input = []
    for index in range(0, len(unigram_input)):
        result_input.append({})
        for bigram, frequency in bigram_input[index].iteritems():
            bigram = bigram_vocabulary[bigram]
            if bigram in bigram_list:
                if bigram not in add_bigrams:
                    add_bigrams[bigram] = len(result_vocabulary) + len(add_bigrams)
                bigram_index = add_bigrams[bigram]
                unigrams = bigram.split(' ')
                unigram_input[index][unigram_vocabulary.index(unigrams[0])] -= frequency
                unigram_input[index][unigram_vocabulary.index(unigrams[-1])] -= frequency
                if bigram_index not in result_input[-1]:
                    result_input[-1][bigram_index] = 0
                result_input[-1][bigram_index] += frequency
        for unigram, frequency in unigram_input[index].iteritems():
            if frequency > 0:
                result_input[-1][unigram] = frequency
    for bigram in add_bigrams:
        result_vocabulary.append("")
    for bigram, index in add_bigrams.iteritems():
        result_vocabulary[index] = bigram + '\tB'
    return result_vocabulary, result_input


def print_result_input(vocabulary, input, vocabulary_file, input_file):
    sort_vocabulary_map = {}
    with codecs.open(vocabulary_file, 'w', 'utf-8') as output:
        for index, word in enumerate(np.argsort(vocabulary)):
            sort_vocabulary_map[int(word)] = index
            output.write(vocabulary[word] + '\n')
    with codecs.open(input_file, 'w', 'utf-8') as output:
        for document in input:
            output.write(str(len(document)))
            for word, frequency in document.iteritems():
                output.write(' ' + str(sort_vocabulary_map[word]) + ':' + str(frequency))
            output.write('\n')


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage='Usage: %prog [options] <unigram_voc> <unigram_input> ' +
        '<bigram_voc> <bigram_input> <bigram_list> <output_voc> <output_input>')
    parser.add_option('-n', '--number_bigrams', dest='number_bigrams', type=int,
                      default=100, help='Number of bigrams to add')
    options, args = parser.parse_args()
    if len(args) != 7:
        parser.error('Incorrect usage')

    unigram_vocabulary_file = args[0]
    unigram_input_file = args[1]
    bigram_vocabulary_file = args[2]
    bigram_input_file = args[3]
    bigram_list_file = args[4]
    output_vocabulary_file = args[5]
    output_input_file = args[6]
    unigram_vocabulary, unigram_input, unigram_pos_vocabulary = (
            load_input(unigram_vocabulary_file, unigram_input_file))
    bigram_vocabulary, bigram_input, bigram_pos_vocabulary = (
            load_input(bigram_vocabulary_file, bigram_input_file))
    bigram_list = load_bigrams(bigram_list_file, options.number_bigrams)
    result_vocabulary, result_input = create_result_input(
        unigram_vocabulary, unigram_input, unigram_pos_vocabulary,
        bigram_vocabulary, bigram_input, bigram_list)
    print_result_input(result_vocabulary, result_input,
                       output_vocabulary_file, output_input_file)
