#!/usr/bin/env python
import optparse
import os
import shutil
import subprocess


def lemmatize_wiki(input_directory, output_directory, morphological_analyzer_file):
  morphological_analyzer_dir = os.path.dirname(os.path.abspath(
        morphological_analyzer_file))
  morphological_analyzer_file = os.path.basename(morphological_analyzer_file)
  if not os.path.exists(output_directory):
    os.makedirs(output_directory)
  for dirname in os.listdir(input_directory):
    source_directory = os.path.join(input_directory, dirname)
    dest_directory = os.path.join(output_directory, dirname)
    if not os.path.exists(dest_directory):
      print 'Parsing', source_directory
      current_dirname = os.path.abspath(source_directory)
      current_dirname = current_dirname[len(morphological_analyzer_dir) + 1:]
      current_dirname = current_dirname.replace(os.path.sep, '\\')
      command = ('cd ' + morphological_analyzer_dir + ' && wine ' +
                 morphological_analyzer_file + ' ww "' + current_dirname + '\\*.txt" -p')
      subprocess.call(command, shell=True)
      os.makedirs(dest_directory)
      for filename in os.listdir(source_directory):
        if filename.endswith('.plm'):
          shutil.move(os.path.join(source_directory, filename),
                      os.path.join(dest_directory, os.path.splitext(filename)[0] + '.txt'))


if __name__ == '__main__':
  parser = optparse.OptionParser(usage='Usage: %prog <input_dir> <output_dir> <morhological_analyzer_file>')
  options, args = parser.parse_args()
  if len(args) != 3:
    parser.error('Incorrect usage')

  input_directory = args[0]
  output_directory = args[1]
  morphological_analyzer_file = args[2]
  lemmatize_wiki(input_directory, output_directory, morphological_analyzer_file)