#!/usr/bin/env python
import matplotlib.pyplot as plt
import optparse
import os


def calculate_avp(terms, term_features_list):
  avps = []
  thresholds = []
  true_positive = 0.0
  avp = 0.0
  for index, term in enumerate(term_features_list):
    if terms[term] == '+':
      true_positive += 1.0
      avp += true_positive / (index + 1)
    thresholds.append(index + 1)
    avps.append(avp / true_positive * 100 if true_positive > 0 else 0.0)
  return thresholds, avps


def calculate_avp_from_file(filename):
  avps = []
  thresholds = []
  true_positive = 0.0
  avp = 0.0
  with open(filename, 'r') as input:
    for index, line in enumerate(input):
      tokens = line.strip().split('\t')
      if len(tokens) != 3:
        break
      if tokens[2] == '+':
        true_positive += 1.0
        avp += true_positive / (index + 1)
      thresholds.append(index + 1)
      avps.append(avp / true_positive * 100 if true_positive > 0 else 0.0)
  return thresholds, avps


def plot_avp(first_avps, first_thresholds, first_filename, graph_filename, second_avps=None, second_thresholds=None, second_filename=None):
  plt.xlabel('Number of the most frequent candidates')
  plt.ylabel('AvP,%')
  first_label = os.path.basename(os.path.splitext(first_filename)[0])
  plt.plot(first_thresholds, first_avps, 'b-', label=first_label)
  if second_avps is not None and second_filename is not None:
    second_label = os.path.basename(os.path.splitext(second_filename)[0])
    plt.plot(second_thresholds, second_avps, 'r-', label=second_label)
  plt.ylim((15, 100))
  plt.legend(loc=1)
  plt.savefig(graph_filename, format='png')


if __name__ == '__main__':
  parser = optparse.OptionParser(usage='Usage: %prog [options] <input_file>')
  parser.add_option('-t', '--threshold', dest='threshold', type=int, default=5000,
                    help='Threshold for calculating AvP')
  parser.add_option('-s', '--second_file', dest='second_file', type=str,
                    help='Second file to compare with')
  parser.add_option('-g', '--graph_file', dest='graph_file', type=str,
                    help='File where graphs should be plotted')
  options, args = parser.parse_args()
  if len(args) != 1:
    parser.error('Incorrect usage')

  input_file = args[0]
  first_thresholds, first_avps = calculate_avp_from_file(input_file)
  if options.second_file is None:
    if options.graph_file is not None:
      plot_avp(first_avps, first_thresholds, input_file, options.graph_file)
    print 'AvP for ' + str(options.threshold) + " terms: " + str(first_avps[options.threshold - 1])
  else:
    second_thresholds, second_avps = calculate_avp_from_file(options.second_file)
    if options.graph_file is not None:
      plot_avp(first_avps, first_thresholds, input_file, options.graph_file, second_avps, second_thresholds, options.second_file)
    print 'AvP for ' + str(options.threshold) + " terms in " + input_file + ": " + str(first_avps[options.threshold - 1])
    print 'AvP for ' + str(options.threshold) + " terms in " + options.second_file + ": " + str(second_avps[options.threshold - 1])

