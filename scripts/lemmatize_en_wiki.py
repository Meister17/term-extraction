#!/usr/bin/env python
import optparse
import os
import subprocess


def create_conf_file(conf_file, input_directory, output_directory):
  with open(conf_file, 'w') as output:
    output.write('source_directory_name=' + input_directory + '\n')
    output.write('destination_directory_name=' + output_directory + '\n')


def lemmatize_wiki(input_directory, output_directory, stanfordanalyzer_file, stanfordanalyzer_conf):
  if not os.path.exists(output_directory):
    os.makedirs(output_directory)
  for dirname in os.listdir(input_directory):
    if not os.path.exists(os.path.join(output_directory, dirname)):
      print 'Parsing', os.path.join(input_directory, dirname)
      create_conf_file(stanfordanalyzer_conf,
                       os.path.abspath(os.path.join(input_directory, dirname)),
                       os.path.abspath(os.path.join(output_directory, dirname)))
      subprocess.call(["java", "-Xms1024M", "-Xmx2048M", "-jar", stanfordanalyzer_file])


if __name__ == '__main__':
  parser = optparse.OptionParser(usage='Usage: %prog <input_dir> <output_dir> <stanfordanalyzer_file> <stanfordanalyzer_conf>')
  options, args = parser.parse_args()
  if len(args) != 4:
    parser.error('Incorrect usage')

  input_directory = args[0]
  output_directory = args[1]
  stanfordanalyzer_file = args[2]
  stanfordanalyzer_conf = args[3]
  lemmatize_wiki(input_directory, output_directory, stanfordanalyzer_file, stanfordanalyzer_conf)