#!/usr/bin/env python
import optparse


def cut_topics(input_filename, output_filename, threshold):
  with open(output_filename, 'w') as output:
    with open(input_filename, 'r') as input:
      for line in input:
        if line.strip().startswith('Topic'):
          number_words = 0
          output.write(line)
        elif number_words < threshold:
          number_words += 1
          output.write(line)


if __name__ == '__main__':
  parser = optparse.OptionParser(usage='Usage: %prog <input_file> <output_file> <threshold>')
  options, args = parser.parse_args()
  if len(args) != 3:
    parser.error('Incorrect usage')

  input_file = args[0]
  output_file = args[1]
  threshold = int(args[2])
  cut_topics(input_file, output_file, threshold)