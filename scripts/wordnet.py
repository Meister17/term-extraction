#!/usr/bin/python
from nltk.stem.wordnet import WordNetLemmatizer
from optparse import OptionParser
import os
import shutil
import en
from enchant import Dict
import multiprocessing as mp


def process_file(input_filename, output_filename):
    lemmatizer = WordNetLemmatizer()
    dictionary = Dict('en-US')
    with open(output_filename, 'w') as output:
        with open(input_filename, 'r') as input:
            for line in input:
                line = line.strip().split('\t')
                if len(line) != 3:
                    continue
                if line[2] == 'NN' or line[2] == 'NNS' or line[2] == 'JJ' or \
                   line[2] == 'NNP' or line[2] == 'NNPS':
                    lemma = lemmatizer.lemmatize(line[1].lower())
                    word = line[0].lower()
                    if en.is_adverb(word) or en.is_number(word):
                        line[2] = 'RB'
                else:
                    lemma = line[1].lower()
                output.write(line[0] + '\t' + lemma + '\t' + line[2] + '\t')
                if dictionary.check(lemma):
                    output.write('1\t')
                else:
                    output.write('0\t')
                ambiguity = (1 if en.is_noun(lemma) else 0) + \
                    (1 if en.is_adjective(lemma) else 0) + \
                    (1 if en.is_adverb(lemma) else 0) + \
                    (1 if en.is_number(lemma) else 0) + \
                    (1 if en.is_verb(lemma) else 0)
                if ambiguity > 1:
                    output.write('1\n')
                else:
                    output.write('0\n')
    if os.path.getsize(output_filename) == 0:
        shutil.remove(output_filename)


def pass_through_wordnet(input_directory, output_directory):
    if os.path.exists(output_directory):
        shutil.rmtree(output_directory)
    os.makedirs(output_directory)
    pool = mp.Pool(processes=mp.cpu_count())
    for filename in os.listdir(input_directory):
        pool.apply_async(process_file,
                         (os.path.join(input_directory, filename),
                          os.path.join(output_directory, filename)))
    pool.close()
    pool.join()


if __name__ == '__main__':
    parser = OptionParser(usage='Usage: %prog <input_directory> <output_directory')
    options, args = parser.parse_args()
    if len(args) != 2:
        parser.error('Incorrect usage')

    input_directory = args[0]
    output_directory = args[1]
    pass_through_wordnet(input_directory, output_directory)
