#!/usr/bin/python
from optparse import OptionParser
import operator


def combine_models(single_word_filename, two_word_filename, combined_filename):
    words = {}
    terms = set()
    with open(single_word_filename, 'r') as input:
        for line in input:
            line = line.strip().split('\t')
            if len(line) < 3:
                continue
            words[line[0]] = float(line[1])
            if line[2] == '+':
                terms.add(line[0])
    with open(two_word_filename, 'r') as input:
        for line in input:
            line = line.strip().split('\t')
            if len(line) < 3:
                continue
            phrase = line[0].replace('_', ' ')
            words[phrase] = float(line[1])
            if line[2] == '+':
                terms.add(phrase)
    words_sorted_by_predictions = sorted(words.iteritems(),
                                         key=operator.itemgetter(1),
                                         reverse=True)
    average_precision = 0.0
    true_positive = 0.0
    for index, (word, prediction) in enumerate(words_sorted_by_predictions):
        if word in terms:
            true_positive += 1.0
            average_precision += true_positive / (index + 1)
    if true_positive > 0:
        average_precision /= true_positive
    else:
        average_precision = 0.0
    with open(combined_filename, 'w') as output:
        for (word, prediction) in words_sorted_by_predictions:
            if word in terms:
                output.write(word + '\t' + str(prediction) + '\t+\n')
            else:
                output.write(word + '\t' + str(prediction) + '\t-\n')
        output.write('Average precision: ' + str(average_precision) + '\n')
    print average_precision


if __name__ == '__main__':
    parser = OptionParser(usage='Usage: %prog <single_word_file> <two_word_file> <combined_file>')
    options, args = parser.parse_args()
    if len(args) != 3:
        parser.error('Incorrect usage')
    combine_models(args[0], args[1], args[2])
