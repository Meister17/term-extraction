#!/usr/bin/env python
import operator
import optparse


class VocabularyRestructurer(object):
  def mark_similar_words(self, topics_file, vocabulary_file,
                         output_vocabulary_file, top_similar_number,
                         remain_similar):
    print 'Marking similar words'
    similar_candidates = set()
    with open(topics_file, 'r') as input:
      number_words_in_topic = 0
      for line in input:
        if line.startswith('Topic'):
          number_words_in_topic = 0
        elif number_words_in_topic < top_similar_number:
          candidate = line.strip().split('\t')[0]
          similar_candidates.add(candidate)
          if candidate.count(' ') == 0:
            number_words_in_topic += 1
    similar_words_number = 0
    with open(output_vocabulary_file, 'w') as output:
      with open(vocabulary_file, 'r') as input:
        for line in input:
          tokens = line.strip().split('\t')
          if (tokens[0] in similar_candidates or
              (remain_similar and tokens[-1] == '+')):
            output.write(tokens[0] + '\t' + '\t'.join(tokens[1:-1]) + '\t+\n')
            similar_words_number += 1
          else:
            output.write(tokens[0] + '\t' + '\t'.join(tokens[1:-1]) + '\t-\n')
    print 'Similar words found:', similar_words_number

  def form_bigrams_for_lda(self, topics_file, vocabulary_file, bigrams_file,
                           output_bigrams_file, top_bigrams_number, bigrams_threshold):
    print 'Forming bigrams for LDA'
    bigrams = {}
    with open(bigrams_file, 'r') as input:
      for line in input:
        tokens = line.strip().split('\t')
        bigrams[tokens[0]] = int(tokens[1])
    exist_bigrams = set()
    with open(vocabulary_file, 'r') as input:
      for line in input:
        candidate = line.strip().split('\t')[0]
        if candidate.count(' ') > 0:
          exist_bigrams.add(line.strip().split('\t')[0])
    new_bigrams = {}
    with open(topics_file, 'r') as input:
      top_words = set()
      skip_words = False
      for line in input:
        if line.startswith('Topic'):
         skip_words = False
        elif not skip_words and len(top_words) < top_bigrams_number:
          candidate = line.strip().split('\t')[0]
          if candidate.count(' ') == 0:
            top_words.add(candidate)
        else:
          for first_word in top_words:
            for second_word in top_words:
              if first_word != second_word:
                new_bigram, frequency = self.get_new_bigram(first_word, second_word,
                                                            bigrams, exist_bigrams)
                if new_bigram is not None:
                  new_bigrams[new_bigram] = frequency
          top_words.clear()
          skip_words = True
    print 'Adding to LDA ' + str(len(new_bigrams)) + ' new bigrams'
    with open(output_bigrams_file, 'w') as output:
      for index, (bigram, frequency) in enumerate(sorted(new_bigrams.iteritems(),
																					    				   key=operator.itemgetter(1),
																												 reverse=True)):
				if index >= bigrams_threshold:
				  break
				output.write(bigram + '\n')


  def get_new_bigram(self, first_word, second_word, bigrams, exist_bigrams):
    trying_bigrams = {}
    bigram = first_word + ' ' + second_word
    if bigram in bigrams and not bigram in exist_bigrams:
      trying_bigrams[bigram] = bigrams[bigram]
    bigram = first_word + ' of ' + second_word
    if bigram in bigrams and not bigram in exist_bigrams:
      trying_bigrams[bigram] = bigrams[bigram]
    bigram = second_word + ' ' + first_word
    if bigram in bigrams and not bigram in exist_bigrams:
      trying_bigrams[bigram] = bigrams[bigram]
    bigram = second_word + ' of ' + first_word
    if bigram in bigrams and not bigram in exist_bigrams:
      trying_bigrams[bigram] = bigrams[bigram]
    max_frequency = 0
    best_bigram = None
    for bigram, frequency in trying_bigrams.iteritems():
      if frequency > max_frequency:
        max_frequency = frequency
        best_bigram = bigram
    return best_bigram, max_frequency


if __name__ == '__main__':
  parser = optparse.OptionParser(
      usage='Usage: %prog [options] <topics_file> <bigrams_file> '+
            '<vocabulary_file> <output_bigrams_file> <output_vocabulary_file')
  parser.add_option('-b', '--top_bigrams_number', dest='top_bigrams_number',
                    type=int, default=10,
                    help='Number of top words per topic for bigrams to use')
  parser.add_option('-s', '--top_similar_number', dest='top_similar_number',
                    type=int, default=10,
                    help='Number of top words per topic for similarity to use')
  parser.add_option('-r', '--remain_similar', dest='remain_similar',
                    action='store_true', help='Remain previous similar words?')
  parser.add_option('-t', '--bigrams_threshold', dest='bigrams_threshold',
									  type=int, default=1000,
										help='Maximum number of bigrams to add')
  options, args = parser.parse_args()
  if len(args) != 5:
    parser.error('Incorrect usage')

  topics_file = args[0]
  bigrams_file = args[1]
  vocabulary_file = args[2]
  output_bigrams_file = args[3]
  output_vocabulary_file = args[4]
  vocabulary_restructurer = VocabularyRestructurer()
  vocabulary_restructurer.mark_similar_words(topics_file,
                                             vocabulary_file,
                                             output_vocabulary_file,
                                             options.top_similar_number,
                                             options.remain_similar)
  vocabulary_restructurer.form_bigrams_for_lda(topics_file,
                                               vocabulary_file,
                                               bigrams_file,
                                               output_bigrams_file,
                                               options.top_bigrams_number,
																							 options.bigrams_threshold)
