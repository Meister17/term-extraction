#!/usr/bin/python
import httplib
import inflect
import urllib2
from optparse import OptionParser
import os
from BeautifulSoup import BeautifulSoup


URL = 'http://bnc.bl.uk/saraWeb.php'


def load_candidates(input_filename):
    candidates = {}
    with open(input_filename, 'r') as input:
        for line in input:
            tokens = line.strip().split('\t')
            if len(tokens) > 2:
                candidates[tokens[0]] = tokens[1]
    return candidates


def load_reference_candidates(input_filename):
    reference_candidates_set = set()
    if os.path.exists(input_filename):
        with open(input_filename, 'r') as input:
            input.readline()
            for line in input:
                line = line.strip().split('\t')
                reference_candidates_set.add(line[0])
    return reference_candidates_set


def get_frequency(candidate):
    while True:
        candidate = candidate.replace(' ', '+')
        try:
            response = urllib2.urlopen(URL + '?qy=' + candidate)
            html = response.read()
            soup = BeautifulSoup(html)
            div = soup.find('div', {'id': 'solutions'})
            if div is None:
                return 0
            answer = str(div.p)
            words = answer.split()
            if words[0] == '<p>No':
                return 0
            elif words[0] == '<p>Here':
                return int(words[10])
            elif words[1] == 'one':
                return 1
            else:
                return int(words[1])
        except httplib.BadStatusLine:
            pass
        except urllib2.URLError:
            pass


def download_frequency(candidate, pos, inflect_engine):
    words = candidate.split(' ')
    frequency = 0
    candidates_to_check = set()
    candidates_to_check.add(candidate)
    if len(words) == 1:
        candidates_to_check.add(inflect_engine.plural_noun(candidate))
    elif len(words) == 2:
        second_plural_noun = inflect_engine.plural_noun(words[1])
        candidates_to_check.add(words[0] + ' ' + second_plural_noun)
        if pos == 'B' or pos == 'N':
            first_plural_noun = inflect_engine.plural_noun(words[0])
            candidates_to_check.add(first_plural_noun + ' ' + words[1])
            candidates_to_check.add(first_plural_noun + ' ' + second_plural_noun)
    elif len(words) == 3 and words[1] == 'of':
        first_plural_noun = inflect_engine.plural_noun(words[0])
        second_plural_noun = inflect_engine.plural_noun(words[2])
        second_indefinite_noun = inflect_engine.a(words[2])
        candidates_to_check.add(words[0] + ' of ' + second_plural_noun)
        candidates_to_check.add(words[0] + ' of ' + second_indefinite_noun)
        candidates_to_check.add(words[0] + ' of the ' + words[2])
        candidates_to_check.add(words[0] + ' of the ' + second_plural_noun)
        candidates_to_check.add(first_plural_noun + ' of ' + words[2])
        candidates_to_check.add(first_plural_noun + ' of ' + second_indefinite_noun)
        candidates_to_check.add(first_plural_noun + ' of the ' + words[2])
        candidates_to_check.add(first_plural_noun + ' of ' + second_plural_noun)
        candidates_to_check.add(first_plural_noun + ' of the ' + second_plural_noun)
    for words in candidates_to_check:
        frequency += get_frequency(words)
    return frequency


def get_frequencies(output_filename, reference_candidates_set, candidates):
    inflect_engine = inflect.engine()
    with open(output_filename, 'a') as output:
        if len(reference_candidates_set) == 0:
            output.write('100000000\n')
        for index, (candidate, pos) in enumerate(candidates.iteritems()):
            if candidate in reference_candidates_set:
                continue
            frequency = download_frequency(candidate, pos, inflect_engine)
            print '%d: %s: %d' % (index, candidate, frequency)
            output.write(candidate + '\t' + str(frequency) + '\n')
            output.flush()


def download_zero_frequencies(filename):
    inflect_engine = inflect.engine()
    lines = []
    with open(filename, 'r') as input:
        for index, line in enumerate(input):
            tokens = line.strip().split('\t')
            if tokens[-1] == "0":
                frequency = download_frequency(tokens[0], 'B', inflect_engine)
                print '%d: %s: %d' % (index, tokens[0], frequency)
                lines.append(tokens[0] + '\t' + str(frequency) + '\n')
            else:
                lines.append(line)
    with open(filename, 'w') as output:
        for line in lines:
            output.write(line)


if __name__ == '__main__':
    parser = OptionParser(usage='Usage: %prog <input_file> <output_file>')
    options, args = parser.parse_args()
    if len(args) != 2:
        parser.error('Incorrect usage')
    input_file = args[0]
    output_file = args[1]
    candidates_list = load_candidates(input_file)
    reference_candidates_set = load_reference_candidates(output_file)
    get_frequencies(output_file, reference_candidates_set, candidates_list)
    download_zero_frequencies(output_file)
