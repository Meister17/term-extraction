#!/usr/bin/env python
import optparse
import os


def change_morphology(input_file, output_file, remain_morph_file=None):
  remain_morph_words = set()
  if remain_morph_file is not None and os.path.exists(remain_morph_file):
    with open(remain_morph_file, 'r') as input:
      for line in input:
        remain_morph_words.add(line.strip())
  with open(output_file, 'w') as output:
    with open(input_file, 'r') as input:
      for line in input:
        tokens = line.strip().split('\t')
        if tokens[0] in remain_morph_words:
          output.write(line)
        elif tokens[-1] == '-':
          output.write('\t'.join(tokens[0:-1]) + '\t+\n')
        elif tokens[-1] == '+':
          output.write('\t'.join(tokens[0:-1]) + '\t-\n')
        else:
          output.write(line.strip() + '\t-\n')


if __name__ == '__main__':
  parser = optparse.OptionParser(usage='Usage: %prog [options] <input_file> <output_file>')
  parser.add_option('-r', '--remain_morph_file', dest='remain_morph_file', type=str,
                    help='File containing words that should remain morphology')
  options, args = parser.parse_args()
  if len(args) != 2:
    parser.error('Incorrect usage')

  input_file = args[0]
  output_file = args[1]
  change_morphology(input_file, output_file, options.remain_morph_file)