#!/usr/bin/python
from optparse import OptionParser
import os
import subprocess
from nltk.stem.wordnet import WordNetLemmatizer


def unzip(directory_name):
    print 'Extracting files from archives'
    for filename in os.listdir(directory_name):
        short_filename, file_extension = os.path.splitext(filename)
        if file_extension == '.zip':
            current_file = os.path.join(directory_name, short_filename)
            args = ['unzip', '-d', directory_name, current_file]
            exit_code = subprocess.call(args, stdout=open('/dev/null', 'w'))
            if exit_code != 0:
                assert 'Failed to extract archive for ', filename


def xls_to_csv(directory_name):
    print 'Converting xls files to csv'
    for filename in os.listdir(directory_name):
        short_filename, file_extension = os.path.splitext(filename)
        if file_extension == '.xls':
            args = ['xls2csv', os.path.join(directory_name, filename)]
            output_filename = os.path.join(directory_name, short_filename + '.csv')
            exit_code = subprocess.call(args, stdout=open(output_filename, 'w'))
            if exit_code != 0:
                assert 'Failed to convert XLS file: ', filename
            os.remove(os.path.join(directory_name, filename))


def process_directory(directory_name):
    terms = set()
    unzip(directory_name)
    xls_to_csv(directory_name)
    print 'Processing csv files'
    for filename in os.listdir(directory_name):
        short_filename, file_extension = os.path.splitext(filename)
        if file_extension == '.csv':
            with open(os.path.join(directory_name, filename), 'r') as input:
                line = input.readline()
                for line in input:
                    line = line.strip().split(',')
                    term = line[0]
                    term = term[1:len(term) - 1].lower().split('(')[0].strip()
                    words = term.split()
                    term = ''
                    for word in words:
                        if word != 'a' and word != 'the' and word != 'an':
                            term += word + ' '
                    if term != '':
                        terms.add(term[:-1])
            os.remove(os.path.join(directory_name, filename))
    return terms


def write_thesaurus(terms, filename):
    print 'Creating thesaurus'
    lemmatizer = WordNetLemmatizer()
    with open(filename, 'w') as output:
        for term in terms:
            words = term.lower().split(' ')
            for index, word in enumerate(words):
                try:
                    index = word.index("'")
                    word = word[0:index]
                except ValueError:
                    pass
                output.write(lemmatizer.lemmatize(word))
                if index != len(words) - 1:
                    output.write(' ')
            output.write('\n')

if __name__ == '__main__':
    parser = OptionParser(usage='Usage: %prog <input_directory> <output_file>')
    options, args = parser.parse_args()
    if len(args) != 2:
        parser.error('Incorrect usage')

    input_directory = args[0]
    output_file = args[1]

    terms = process_directory(input_directory)
    write_thesaurus(terms, output_file)
