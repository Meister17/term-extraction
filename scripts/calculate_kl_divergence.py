#!/usr/bin/python
import math
import numpy as np
import optparse


def get_statistics(filename):
	number_topics = 0
	number_documents = 0
	number_words = 0
	with open(filename, 'r') as input:
		for line in input:
			number_documents += 1
			words = line.strip().split()
			for word in words:
				tokens = word.split(':')
				number_topics = max(number_topics, int(tokens[1]))
				number_words = max(number_words, int(tokens[0]))
	return number_topics + 1, number_documents, number_words + 1


def fill_aux_arrays(filename, number_topics, number_documents, number_words):
	nt = np.zeros(number_topics, dtype=int)
	ntd = np.zeros((number_topics, number_documents), dtype=int)
	nwt = np.zeros((number_words, number_topics), dtype=int)
	with open(filename, 'r') as input:
		for document, line in enumerate(input):
			words = line.strip().split()
			for element in words:
				tokens = element.split(':')
				word = int(tokens[0])
				topic = int(tokens[1])
				nt[topic] += 1
				ntd[topic, document] += 1
				nwt[word, topic] += 1
	return nt, ntd, nwt


def calculate_kl_divergence(filename):
	number_topics, number_documents, number_words = get_statistics(filename)
	nt, ntd, nwt = fill_aux_arrays(filename, number_topics, number_documents,
								   number_words)
	kl_divergences = np.zeros(number_topics)
	with open(filename, 'r') as input:
		for document, line in enumerate(input):
			words_map = {}
			words = line.strip().split()
			for element in words:
				tokens = element.split(':')
				word = int(tokens[0])
				topic = int(tokens[1])
				if (word, topic) not in words_map:
					words_map[(word, topic)] = 1
				else:
					words_map[(word, topic)] += 1
			for (word, topic), ndwt in words_map.iteritems():
				kl_divergences[topic] += (float(ndwt) / float(nt[topic]) *
					math.log(float(ndwt) * float(nt[topic]) /
							(float(ntd[topic,document]) * float(nwt[word,topic]))))
	return sum(kl_divergences) / number_topics


if __name__ == '__main__':
	parser = optparse.OptionParser(usage='Usage: %prog <wt_assign_file>')
	options, args = parser.parse_args()
	if len(args) != 1:
		parser.error('Incorrect usage')

	wt_assign_file = args[0]
	kl_divergence = calculate_kl_divergence(wt_assign_file)
	print 'KL-divergence:', kl_divergence