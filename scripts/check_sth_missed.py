#!/usr/bin/env python
import optparse


def check_sth_missed(input_file, thesaurus_file, output_file):
  words_in_thesaurus = []
  with open(thesaurus_file, 'r') as input:
    for line in input:
      words_in_thesaurus.append(line.strip())
  with open(output_file, 'w') as output:
    with open(input_file, 'r') as input:
      for line in input:
        tokens = line.strip().split('\t')
        for index in range(1, len(tokens)):
          if tokens[index] != tokens[0]:
            for term in words_in_thesaurus:
              if tokens[index] in term.split(' '):
                output.write(tokens[index] + '\t' + term + '\n')


if __name__ == '__main__':
  parser = optparse.OptionParser(usage='Usage: %prog <input_file> <thesaurus_file> <output_file>')
  options, args = parser.parse_args()
  if len(args) != 3:
    parser.error('Incorrect usage')

  input_file = args[0]
  thesaurus_file = args[1]
  output_file = args[2]
  check_sth_missed(input_file, thesaurus_file, output_file)