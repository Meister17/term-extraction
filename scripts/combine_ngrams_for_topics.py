#!/usr/bin/env python
import calculate_avp
import operator
import optparse
import os


def load_file(filename, terms):
  add_terms = len(terms) == 0
  feature_values = {}
  with open(filename, 'r') as input:
    for line in input:
      tokens = line.strip().split('\t')
      if len(tokens) != 3:
        continue
      feature_values[tokens[0]] = float(tokens[1])
      if not tokens[0] in terms:
        if add_terms:
          terms[tokens[0]] = tokens[2]
        else:
          raise Exception('Wrong file')
  rangs = {}
  for index, (term, value) in enumerate(sorted(feature_values.iteritems(),
                                               key=operator.itemgetter(1),
                                               reverse=True)):
    rangs[term] = index
  return rangs


def get_avp_by_combining_ngrams(freq_file, tfridf_file, lcontext_file,
                                rcontext_file, result_file, threshold,
                                best_avp=0.0):
  terms = {}
  freq_rangs = load_file(freq_file, terms)
  tfridf_rangs = load_file(tfridf_file, terms)
  lcontext_rangs = load_file(lcontext_file, terms)
  rcontext_rangs = load_file(rcontext_file, terms)
  result_rangs = {}
  for term, index in freq_rangs.iteritems():
    result_rangs[term] = index
  result_list = [None] * threshold
  for index, (term, term_index) in enumerate(sorted(result_rangs.iteritems(),
                                                    key=operator.itemgetter(1))):
    if index == threshold:
      break
    result_list[index] = term
  thresholds, avps = calculate_avp.calculate_avp(terms, result_list)
  if avps[threshold - 1] > best_avp:
    best_avp = avps[threshold - 1]
    with open(result_file, 'w') as output:
      for index, term in enumerate(result_list):
        output.write(term + '\t' + str(index) + '\t' + terms[term] + '\n')
  return avps[threshold - 1]


if __name__ == '__main__':
  parser = optparse.OptionParser(
      usage='Usage: %prog [options] <freq_file> <tfridf_file> <context_dir>' +
            '<result_file>')
  parser.add_option('-t', '--threshold', dest='threshold', type=int,
                    default=5000, help='Number of ngrams to take into account')
  options, args = parser.parse_args()
  if len(args) != 4:
    parser.error('Incorrect usage')

  freq_file = args[0]
  tfridf_file = args[1]
  context_dir = args[2]
  result_file = args[3]
  best_avp = 0
  for lcontext_filename in os.listdir(context_dir):
    if lcontext_filename.endswith('left.txt'):
      for rcontext_filename in os.listdir(context_dir):
        if rcontext_filename.endswith('right.txt'):
          new_avp = get_avp_by_combining_ngrams(
              freq_file, tfridf_file,
              os.path.join(context_dir, lcontext_filename),
              os.path.join(context_dir, rcontext_filename),
              result_file, options.threshold, best_avp)
          print lcontext_filename + '\t' + rcontext_filename + '\t' + str(new_avp)
          if new_avp > best_avp:
            best_avp = new_avp