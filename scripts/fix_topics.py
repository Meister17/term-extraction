#!/usr/bin/env python
import operator
import optparse
import sys


def determine_threshold(input_file):
  terms = {}
  with open(input_file, 'r') as input:
    for line in input:
      if not line.startswith('Topic'):
        tokens = line.strip().split('\t')
        if not tokens[0] in terms:
          terms[tokens[0]] = float(tokens[1])
        else:
          terms[tokens[0]] = max(terms[tokens[0]], float(tokens[1]))
  for term, value in sorted(terms.iteritems(), key=operator.itemgetter(1)):
    if value > 0.0:
      return value
  return 0.0


def fix_topics(input_file, output_file, threshold, number):
  with open(output_file, 'w') as output:
    with open(input_file, 'r') as input:
      word_number = 0
      good_topic = False
      topic_lines = []
      for line in input:
        if line.startswith('Topic'):
          word_number = 0
          if good_topic:
            for topic_line in topic_lines:
              output.write(topic_line)
          topic_lines = []
          topic_lines.append(line)
          good_topic = False
        else:
          tokens = line.strip().split('\t')
          if float(tokens[1]) >= threshold:
            topic_lines.append(line)
          if word_number < number and tokens[0].count(' ') > 0:
            good_topic = True
          word_number += 1
      if good_topic:
        for topic_line in topic_lines:
          output.write(topic_line)


if __name__ == '__main__':
  parser = optparse.OptionParser(usage='Usage: %prog [options] <input_file> <output_file>')
  parser.add_option('-n', '--number', dest='number', type=int, default=10,
                    help='Number of top words to find collocation there')
  options, args = parser.parse_args()
  if len(args) != 2:
    parser.error('Incorrect usage')

  input_file = args[0]
  output_file = args[1]
  threshold = determine_threshold(input_file)
  print threshold
  fix_topics(input_file, output_file, threshold, options.number)