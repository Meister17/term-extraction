#!/usr/bin/python
import optparse


def parse_vocabulary(filename):
    vocabulary = []
    with open(filename, 'r') as input:
        for line in input:
            vocabulary.append(line.strip())
    return vocabulary


def convert_to_gibbs_input(input_file, vocabulary_file, output_file):
    vocabulary = parse_vocabulary(vocabulary_file)
    with open(output_file, 'w') as output:
        with open(input_file, 'r') as input:
            lines = input.readlines()
            output.write(str(len(lines)) + '\n')
            for line in lines:
                line = line.strip().split()
                for word in line[1:]:
                    tokens = word.split(':')
                    for number in xrange(int(tokens[1])):
                        output.write(vocabulary[int(tokens[0])] + ' ')
                output.write('\n')


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage='sage: %prog <input_file> <vocabulary_file> <output_file>')
    options, args = parser.parse_args()
    if len(args) != 3:
        parser.error('Incorrect usage')

    input_file = args[0]
    vocabulary_file = args[1]
    output_file = args[2]
    convert_to_gibbs_input(input_file, vocabulary_file, output_file)