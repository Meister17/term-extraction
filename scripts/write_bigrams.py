#!/usr/bin/python
import optparse

def write_bigrams(terms_file, output_file):
    with open(output_file, 'w') as output:
        with open(terms_file, 'r') as input:
            for line in input:
                words = line.strip().split()
                if len(words) == 2 or (len(words) == 3 and words[1] == 'of'):
                    output.write(line)

if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage='Usage: %prog <terms_file> <output_file>')
    options, args = parser.parse_args()
    if len(args) != 2:
        parser.error('Incorrect usage')

    terms_file = args[0]
    output_file = args[1]
    write_bigrams(terms_file, output_file)