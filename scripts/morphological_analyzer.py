#!/usr/bin/python
import codecs
import fnmatch
import optparse
import os
import rarfile
import shutil
import subprocess
import sys
import zipfile


def extract_zip(archive_name, destination_directory):
    if not os.path.exists(destination_directory):
        os.makedirs(destination_directory)
    number = 0
    with zipfile.ZipFile(archive_name) as archive:
        for zipinfo in archive.infolist():
            if not zipinfo.filename.endswith(os.path.sep):
                zipinfo.filename = str(number) + '.txt'
                archive.extract(zipinfo, path=destination_directory)
                number += 1


def extract_rar(archive_name, destination_directory, files_mask):
    if not os.path.exists(destination_directory):
        os.makedirs(destination_directory)
    number = 0
    for root, dirnames, filenames in os.walk(archive_name):
        for filename in fnmatch.filter(filenames, '*.rar'):
            print 'Parsing file', filename
            try:
                with rarfile.RarFile(os.path.join(root, filename)) as archive:
                    for rarinfo in archive.infolist():
                        if rarinfo.filename.endswith(files_mask[1:]):
                            archive.extract(rarinfo, path=archive_name)
                            dest_filename = rarinfo.filename.replace('\\',
                                                                     os.path.sep)
                            dest_filename = dest_filename.replace('/', os.path.sep)
                            dest_filename = os.path.join(archive_name, dest_filename)
                            new_dest_filename = os.path.join(destination_directory,
                                                             str(number) + '.txt')
                            with codecs.open(dest_filename, 'r', 'utf8') as input:
                                with codecs.open(new_dest_filename, 'w', 'cp1251',
                                                 errors='ignore') as output:
                                    for line in input:
                                        output.write(line)
                            os.remove(dest_filename)
                            number += 1
            except rarfile.NotRarFile:
                print 'Not a rar file was met'


def perform_morphological_analyzer(morphological_analyzer_file,
                                   source_directory, destination_directory,
                                   archive_name, archive_type, files_mask):
    if archive_type == 'zip':
        extract_zip(archive_name, source_directory)
    elif archive_type == 'rar':
        extract_rar(archive_name, source_directory, files_mask)
    morphological_analyzer_dir = os.path.dirname(os.path.abspath(
        morphological_analyzer_file))
    morphological_analyzer_file = os.path.basename(morphological_analyzer_file)
    source_directory = os.path.abspath(source_directory)
    dirname = source_directory[len(morphological_analyzer_dir) + 1:]
    dirname = dirname.replace(os.path.sep, '\\')
    command = ('cd ' + morphological_analyzer_dir + ' && wine ' +
               morphological_analyzer_file + ' ww "' + dirname + '\\' +
               files_mask + '" -p')
    subprocess.call(command, shell=True)
    if not os.path.exists(destination_directory):
        os.makedirs(destination_directory)
    for filename in os.listdir(source_directory):
        if filename.endswith('.plm'):
            shutil.move(os.path.join(source_directory, filename),
                        os.path.join(destination_directory,
                                     os.path.splitext(filename)[0] + '.txt'))


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage='Usage: %prog [options] <morphological_analyzer_file> ' +
        '<source_directory> <destination_directory>')
    parser.add_option('-a', '--archive', dest='archive', type=str,
                      help='Name of archive')
    parser.add_option('-t', '--archive_type', dest='archive_type', type=str,
                      help='Type of archive file')
    parser.add_option('-m', '--mask', dest='mask', type=str,
                      help='Files mask for processing')
    options, args = parser.parse_args()
    if len(args) != 3:
        parser.error('Incorrect usage')

    morphological_analyzer_file = args[0]
    source_directory = args[1]
    destination_directory = args[2]
    perform_morphological_analyzer(morphological_analyzer_file,
                                   source_directory, destination_directory,
                                   options.archive, options.archive_type,
                                   options.mask)
