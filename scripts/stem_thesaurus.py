#!/usr/bin/python
import optparse
from nltk.stem.wordnet import WordNetLemmatizer


def stem_thesaurus(source_filename, output_filename):
    lemmatizer = WordNetLemmatizer()
    with open(output_filename, 'w') as output:
        with open(source_filename, 'r') as input:
            for line in input:
                words = line.strip().lower().split(' ')
                for index, word in enumerate(words):
                    output.write(lemmatizer.lemmatize(word))
                    if index != len(words) - 1:
                        output.write(' ')
                output.write('\n')


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage='Usage: %prog <source_file> <output_file>')
    options, args = parser.parse_args()
    if len(args) != 2:
        parser.error('Incorrect usage')
    source_file = args[0]
    output_file = args[1]
    stem_thesaurus(source_file, output_file)
