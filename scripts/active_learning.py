#!/usr/bin/python
import numpy as np
import optparse
from sklearn import ensemble


def load_file(filename):
    features = []
    words = []
    data = []
    labels = []
    with open(filename, 'r') as input:
        line = input.readline().strip().split('\t')
        for index in range(1, len(line) - 1):
            features.append(line[index])
        for line in input:
            line = line.strip().split('\t')
            words.append(line[0])
            labels.append(1 if line[len(features) + 1] == 'term' else 0)
            new_data = np.zeros(len(features))
            for index in range(0, len(features)):
                new_data[index] = float(line[index + 1])
            data.append(new_data)
    return features, words, data, labels


def train_model(data, labels, estimators, max_depth):
    model = ensemble.GradientBoostingClassifier(
        n_estimators=estimators,
        learning_rate=0.05,
        max_depth=max_depth,
        subsample=0.5,
        random_state=0,
        loss='deviance').fit(data, labels)


def get_prediction_indices(predictions):
    all_indices = range(len(predictions))
    return [index for (prediction, index) in sorted(zip(predictions, all_indices))][::-1]

def predict(model, data, labels):
    predictions = model.predict_proba(data)[:, 1]
    indices = get_prediction_indices(predictions)
    average_precision = 0.0
    true_positive = 0.0
    for index, sorted_index in enumerate(indices):
        if labels[sorted_index] == 1:
            true_positive += 1.0
            average_precision += true_positive / (index + 1)
    return average_precision / true_positive if true_positive > 0 else 0.0, predictons


def get_borrowed_data(data, labels, predictions, borrow_number, borrow_mode):
    borrowed_data = []
    borrowed_labels = []
    indices = get_prediction_indices(predictions)
    best_index = 0.0
    if borrow_mode.startswith('middle'):
        min_deviation = 1.0
        for index in range(0, len(indices) - borrow_number):
            deviation = (abs(predictions[index] - 0.5) +
                         abs(predictions[index + borrow_number] - 0.5)
            if deviation < min_deviation:
                best_index = index
    for index in range(best_index, best_index + borrow_number):
        borrowed_labels.append(labels[indices[index]])
        borrowed_data.append(data[indices[index]].copy())
    if borrow_mode.endswith('remove'):
        for index in range(0, borrow_number):
            labels[best_index:best_index + 1] = []
            data[best_index:best_index + 1] = []
    return borrowed_data, borrowed_labels


def perform_active_learning(train_data, train_labels, test_data, test_labels,
                            estimators, max_depth, iterations, borrow_number,
                            borrow_mode):
    model = train(train_data, train_labels, estimators, max_depth)
    best_average_precision, best_predictions = predict(model, test_data, test_labels)
    predictions = best_predictions
    for iteration in range(0, iterations):
        print 'iteration: ', iteration
        borrowed_data, borrowed_labels =
            get_borrowed_data(test_data, test_labels, predictions, borrow_number,
                              borrow_mode)
        for index in range(0, len(borrowed_data)):
            train_data.append(borrowed_data[index])
            train_labels.append(borrowed_labels[index])
        model = train(train_data, train_labels, estimators, max_depth)
        average_precision, predictions = predict(model, test_data, test_labels)
        if average_precision > best_average_precision:
            best_average_precision = average_precision
            best_predictions = predictions[:]
    return best_average_precision, best_predictions


def print_predictions(filename, words, predictions, labels, average_precision):
    print 'Printing predictions'
    indices = get_prediction_indices(predictions)
    with open(filename, 'w') as output:
        for index in indices:
            output.write(words[index] + '\t' + str(predictions[index]) + '\t');
            if labels[index] == 1:
                output.write('+\n')
            else:
                output.write('-\n')
        output.write('Average Precision: ' + str(average_precision) + '\n')


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage='Usage: %prog [options] <train_file> <test_file> <output_file>')
    parser.add_option('-e', '--estimators', dest='estimators', type=int,
                      default=100, help='Number of trees to use in boosting')
    parser.add_option('-d', '--max_depth', dest='max_depth', type=int,
                      default=7, help='Maximum depth of trees in boosting')
    parser.add_option('-i', '--iterations', dest='iterations', type=int,
                      default=1, help='Number of iterations of active learning')
    parser.add_option('-b', '--borrow_number', dest='borrow_number', type=int,
                      default=10, help='Number of samples to borrow')
    parser.add_option('-m', '--borrow_mode', dest='borrow_mode', type=str,
                      default='top', help='Mode where to borrow samples')
    options, args = parser.parse_args()
    if len(args) != 3:
        parser.error('Incorrect usage')

    train_file = args[0]
    test_file = args[1]
    output_file = args[2]
    train_features, train_words, train_data, train_labels = load_file(train_file)
    test_features, test_words, test_data, test_labels = load_file(test_file)
    average_precision, predictions =
        perform_active_learning(train_data, train_labels, test_data, test_labels,
                                options.estimators, options.max_depth,
                                options.iterations, options.borrow_number,
                                options.borrow_mode)
    print_predictions(output_file, test_words, predictions, test_labels,
                      average_precision)