// Copyright 2011 Michael Nokel
#pragma once

#include <string>
#include <unordered_set>

/**
  @brief Class intended to be used to determine whether extracted terms are
  real terms or not

  This class parses file containing real terms from the specified area, stores
  them in a set. After what, one can use this class to determine whether some
  term is really a term or not.
*/
class ResultsAnalyzer {
 public:
  /**
    Parses given file with real terms from the specified area and stores them in
    a hash set
    @param filename file containing real terms from the specified area
    @throw std::runtime_error in case of any error
  */
  void ParseFile(const std::string& filename);

  /**
    Determines whether given term is a real term
    @param probable_term term to determine what it is
    @return true if it is a real term in the specified area
  */
  bool IsRealTerm(const std::string& probable_term) const {
    return real_terms_set_.find(probable_term) != real_terms_set_.end();
  }

 private:
  /** Hash set containing parsed real terms from the specified area */
  std::unordered_set<std::string> real_terms_set_;
};
