// Copyright 2011 Michael Nokel
#pragma once

#include <string>

/**
  @brief Enumerator containing all possible types of term candidates

  The program can work with single-word and two-word term candidates
*/
enum CandidatesTypes {
  NO_CANDIDATES_TYPE = 0,
  SINGLE_WORD_TERMS = 1,
  TWO_WORD_TERMS = 2
};

/**
  @brief Structure containing all common data about term candidates

  This structure contains term candidate, noun (Noun + Noun), adjective
  (Adjective + Noun), novelty, ambiguity value, average position of the first
  occurrence, Term Frequency (calculated for all terms, only for subjects, only
  for capital and non-initial words), Document Frequency (calculated for all
  terms, only for subjects, only for capital and non-initial words), Domain
  Consensus (calculated for all terms, only for subjects, only for capital and
  non-initial words), Probabilistic Data (term frequency, document frequency,
  maximum term frequency, term score, maximum term score), Term Contribution,
  Term Variance, Term Variance Quality, Frequency Near Terms and Term Frequency
  as is in BM-25.
*/
struct TermCandidateData {
  /**
    Checks whether current term candidate data is empty
    @return true if current term candidate data is empty
  */
  bool IsEmpty() const {
    return term_candidate.empty();
  }

  /** Term candidate */
  std::string term_candidate = "";

  /** Flag indicating whether the candidate is a noun (or Noun + Noun) */
  int is_noun = 0;

  /** Flag indicating whether the candidate is an adjective (or Adj + Noun) */
  int is_adjective = 0;

  /** Novelty value of term candidate */
  int novelty = 0;

  /** Ambiguity value of term candidate */
  int ambiguity = 0;

  /** Average position of the first occurrence in texts */
  double first_occurrence_position = 0.0;

  /** Term Frequency of single term */
  double term_frequency = 0.0;

  /** Term Frequency calculated only for capital words */
  double term_frequency_as_capital_word = 0.0;

  /** Term Frequency calculated only for non-initial words */
  double term_frequency_as_non_initial_word = 0.0;

  /** Term Frequency calculated only for subjects */
  double term_frequency_as_subject = 0.0;

  /** Document Frequency of single terms */
  double document_frequency = 0.0;

  /** Document Frequency calculated only for capital words */
  double document_frequency_as_capital_word = 0.0;

  /** Document Frequency calculated only for non-initial words */
  double document_frequency_as_non_initial_word = 0.0;

  /** Document Frequency calculated only for subjects */
  double document_frequency_as_subject = 0.0;

  /** Domain Consensus of single-word term */
  double domain_consensus = 0.0;

  /** Domain Consensus of capital word */
  double domain_consensus_as_capital_word = 0.0;

  /** Domain Consensus of non-initial word */
  double domain_consensus_as_non_initial_word = 0.0;

  /** Domain Consensus of subject */
  double domain_consensus_as_subject = 0.0;

  /** Probabilistic Term Frequency of the term candidate */
  double probabilistic_term_frequency = 0.0;

  /** Probabilistic Maximum Term Frequency of the term candidate */
  double probabilistic_maximum_term_frequency = 0.0;

  /** Probabilistic Term Score of the term candidate */
  double probabilistic_term_score = 0.0;

  /** Probabilistic Maximum Term Score of the term candidate */
  double probabilistic_maximum_term_score = 0.0;

  /** Term Contribution weight */
  double term_contribution = 0.0;

  /** Term Variance weight */
  double term_variance = 0.0;

  /** Term variance quality */
  double term_variance_quality = 0.0;

  /** Frequency of occurring near some predefined terms */
  unsigned int near_terms_frequency = 0.0;

  /** Term frequency as is in BM-25 */
  double term_frequency_bm25 = 0.0;
};
