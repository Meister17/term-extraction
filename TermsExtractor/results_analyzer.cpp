// Copyright 2011 Michael Nokel
#include "./results_analyzer.h"
#include <algorithm>
#include <fstream>
#include <stdexcept>
#include <string>

using std::getline;
using std::find_if;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::string;

void ResultsAnalyzer::ParseFile(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse file with real terms");
  }
  while (!file.eof()) {
    string line;
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    real_terms_set_.insert(line);
  }
  file.close();
}
