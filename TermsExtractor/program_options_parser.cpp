// Copyright 2011 Michael Nokel
#include "./program_options_parser.h"
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <climits>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>

using namespace boost::program_options;
using boost::filesystem::path;
using std::cout;
using std::ofstream;
using std::runtime_error;
using std::string;

void ProgramOptionsParser::Parse(int argc, char** argv) {
  variables_map program_options_storage;
  string configuration_filename;
  options_description program_options_description("Options for program:");
  program_options_description.add_options()
  ("help,h", value<bool>(&help_message_printed_)->implicit_value(true),
      "Print help message and exit")
  ("configuration_file,c", value<string>(&configuration_filename),
      "Configuration file of program")
  ("threshold_for_term_number",
      value<unsigned int>(&threshold_for_term_number_),
      "Threshold for number of extracting terms")
  ("threshold_for_average_precision,",
      value<unsigned int>(&threshold_for_average_precision_),
      "Threshold for average precision")
  ("average_precision_directory_name",
      value<string>(&average_precision_directory_name_),
      "File for printing Average Precision values for all features")
  ("destination_directory_name", value<string>(&destination_directory_name_),
      "Destination directory where extracted terms will be written")
  ("real_terms_filename", value<string>(&real_terms_filename_),
      "File with real terms from the specified area")
  ("single_word_terms_filename", value<string>(&single_word_terms_filename_),
      "File with single-word terms and their data")
  ("single_word_lda_results_filename",
      value<string>(&single_word_lda_results_filename_),
      "File with results of LDA for single-word term candidates")
  ("single_word_phrases_filename",
      value<string>(&single_word_phrases_filename_),
      "File with noun phrases for single-word term candidates")
  ("reference_collection_single_words_filename",
      value<string>(&reference_collection_single_words_filename_),
      "File with single-word candidates from the reference collection")
  ("single_words_filename", value<string>(&single_words_filename_),
      "File with single words along with their frequencies")
  ("two_word_terms_filename", value<string>(&two_word_terms_filename_),
      "File with two-word terms and their data")
  ("two_word_lda_results_filename",
      value<string>(&two_word_lda_results_filename_),
      "File with results of LDA for two-word term candidates")
  ("two_word_phrases_filename", value<string>(&two_word_phrases_filename_),
      "File with noun phrases for two-word term candidates")
  ("reference_collection_two_words_filename",
      value<string>(&reference_collection_two_words_filename_),
      "File with two-word candidates from the reference collection");
  store(parse_command_line(argc, argv, program_options_description),
        program_options_storage);
  notify(program_options_storage);
  if (!configuration_filename.empty()) {
    store(parse_config_file<char>(configuration_filename.data(),
                                  program_options_description),
          program_options_storage);
    notify(program_options_storage);
  }
  if (help_message_printed_) {
    cout << program_options_description << "\n";
  }
  CorrectDirectoryName(&destination_directory_name_);
  CorrectDirectoryName(&average_precision_directory_name_);
  RecreateFile(average_precision_directory_name_ + "single_words.txt");
  RecreateFile(average_precision_directory_name_ + "two_words.txt");
}

void ProgramOptionsParser::CorrectDirectoryName(string* directory_name) const {
  path path_separator("/");
  string path_separator_string = path_separator.make_preferred().native();
  if (*(directory_name->rbegin()) != path_separator_string[0]) {
    directory_name->push_back(path_separator_string[0]);
  }
}

void ProgramOptionsParser::RecreateFile(const string& filename) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to recreate file for printing AvP");
  }
  file.close();
}
