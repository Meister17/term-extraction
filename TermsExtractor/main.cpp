// Copyright 2011 Michael Nokel
#include "./program_options_parser.h"
#include "./terms_extractor.h"
#include <iostream>
#include <stdexcept>

using std::cout;
using std::exception;

/**
  Main function of program. Starts program
  @param argc number of program command line parameters
  @param argv command line parameters
  @return code of exit
*/
int main(int argc, char** argv) {
  try {
    ProgramOptionsParser program_options_parser;
    program_options_parser.Parse(argc, argv);
    if (program_options_parser.help_message_printed()) {
      return 0;
    }
    TermsExtractor terms_extractor(
        program_options_parser.average_precision_directory_name(),
        program_options_parser.threshold_for_average_precision(),
        program_options_parser.threshold_for_term_number());
    terms_extractor.Initialize(
        program_options_parser.real_terms_filename(),
        program_options_parser.single_word_terms_filename(),
        program_options_parser.single_word_lda_results_filename(),
        program_options_parser.single_word_phrases_filename(),
        program_options_parser.reference_collection_single_words_filename(),
        program_options_parser.single_words_filename(),
        program_options_parser.two_word_terms_filename(),
        program_options_parser.two_word_lda_results_filename(),
        program_options_parser.two_word_phrases_filename(),
        program_options_parser.reference_collection_two_words_filename());
    terms_extractor.ExtractTerms(
        program_options_parser.destination_directory_name());
  } catch (const exception& except) {
    cout << "Fatal error occurred: " << except.what() << "\n";
    return 1;
  }
  return 0;
}
