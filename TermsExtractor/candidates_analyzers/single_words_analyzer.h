// Copyright 2012 Michael Nokel
#pragma once

#include <string>
#include <unordered_map>

/**
  @brief Class for working with previously extracted single words

  This class should be used for parsing file with previously extracted
  single words and some data about them.
*/
class SingleWordsAnalyzer {
 public:
  /**
    Parses file with previously extracted single words from the collection
    @param filename name of file containing single words extracted from the
    text collection
    @throw std::runtime_error in case of any occurred error
  */
  void ParseFile(const std::string& filename);

  /**
    Returns term frequency of the given single word
    @param word single word to return term frequency for
    @return term frequency of the given single word or 0 if no such word was
    found
  */
  unsigned int GetWordFrequency(const std::string& word) const {
    auto found_iterator = single_words_map_.find(word);
    if (found_iterator != single_words_map_.end()) {
      return found_iterator->second;
    }
    return 0;
  }

  unsigned int total_number_words() const {
    return total_number_words_;
  }

 private:
  /** Total number of single words in the text collection */
  unsigned int total_number_words_ = 0;

  /** Hash map containing single words and their term frequencies */
  std::unordered_map<std::string, unsigned int> single_words_map_;
};
