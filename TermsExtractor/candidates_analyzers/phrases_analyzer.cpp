// Copyright 2011 Michael Nokel
#include "./phrases_analyzer.h"
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <cctype>
#include <fstream>
#include <list>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

using boost::is_any_of;
using boost::split;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::list;
using std::max;
using std::min;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::sort;
using std::string;
using std::unordered_map;
using std::vector;

void PhrasesAnalyzer::ParseFile(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse file with phrases");
  }
  string line;
  getline(file, line);
  while (!file.eof()) {
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (!line.empty()) {
      vector<string> tokens;
      split(tokens, line, is_any_of("\t"));
      StoreExtractedPhrase(tokens[0], stoi(tokens[1]));
    }
  }
  file.close();
}

unsigned int PhrasesAnalyzer::GetMaximumPhraseFrequency(const std::string& word)
    const {
  const auto iterator = words_in_phrases_map_.find(word);
  unsigned maximum_frequency = 0;
  if (iterator != words_in_phrases_map_.end()) {
    for (auto frequency: iterator->second) {
      maximum_frequency = max(frequency, maximum_frequency);
    }
  }
  return maximum_frequency;
}

double PhrasesAnalyzer::GetAveragePhrasesFrequency(
    const string& word,
    unsigned int phrases_number) const {
  unsigned int sum_frequencies = 0;
  vector<unsigned int> word_frequencies;
  const auto iterator = words_in_phrases_map_.find(word);
  if (iterator != words_in_phrases_map_.end()) {
    for (auto frequency: iterator->second) {
      word_frequencies.push_back(frequency);
    }
    if (!word_frequencies.empty()) {
      sort(word_frequencies.begin(), word_frequencies.end());
      phrases_number = min(static_cast<unsigned int>(word_frequencies.size()),
                           phrases_number);
      for (size_t index = 1; index <= phrases_number; ++index) {
        sum_frequencies += word_frequencies[word_frequencies.size() - index];
      }
    }
  }
  return static_cast<double>(sum_frequencies) /
         static_cast<double>(phrases_number);
}

unsigned int PhrasesAnalyzer::GetContextWordsFrequenciesSum(
      const string& word) const {
  unsigned int context_words_frequencies_sum = 0;
  const auto found_iterator = context_words_map_.find(word);
  if (found_iterator != context_words_map_.end()) {
    for (const auto& element: found_iterator->second) {
      context_words_frequencies_sum += element.second.pre_frequency +
          element.second.post_frequency;
    }
  }
  return context_words_frequencies_sum;
}

vector<string> PhrasesAnalyzer::GetContextWords(const string& word) const {
  vector<string> context_words_vector;
  const auto found_iterator = context_words_map_.find(word);
  if (found_iterator != context_words_map_.end()) {
    for (const auto& element: found_iterator->second) {
      context_words_vector.push_back(element.first);
    }
  }
  return context_words_vector;
}

unsigned int PhrasesAnalyzer::GetPreFrequenciesSum(const string& word) const {
  const auto found_iterator = context_words_map_.find(word);
  unsigned int pre_frequencies_sum = 0;
  if (found_iterator != context_words_map_.end()) {
    for (const auto& element: found_iterator->second) {
      pre_frequencies_sum += element.second.pre_frequency;
    }
  }
  return pre_frequencies_sum;
}

unsigned int PhrasesAnalyzer::GetPostFrequenciesSum(const string& word) const {
  const auto found_iterator = context_words_map_.find(word);
  unsigned int post_frequencies_sum = 0;
  if (found_iterator != context_words_map_.end()) {
    for (const auto& element: found_iterator->second) {
      post_frequencies_sum += element.second.post_frequency;
    }
  }
  return post_frequencies_sum;
}

unsigned int PhrasesAnalyzer::GetPreOccurrencesNumber(const string& word)
    const {
  const auto found_iterator = context_words_map_.find(word);
  unsigned int pre_occurrences_number = 0;
  if (found_iterator != context_words_map_.end()) {
    for (const auto& element: found_iterator->second) {
      if (element.second.pre_frequency > 0) {
        ++pre_occurrences_number;
      }
    }
  }
  return pre_occurrences_number;
}

unsigned int PhrasesAnalyzer::GetPostOccurrencesNumber(const string& word)
    const {
  const auto found_iterator = context_words_map_.find(word);
  unsigned int post_occurrences_number = 0;
  if (found_iterator != context_words_map_.end()) {
    for (const auto& element: found_iterator->second) {
      if (element.second.post_frequency > 0) {
        ++post_occurrences_number;
      }
    }
  }
  return post_occurrences_number;
}

void PhrasesAnalyzer::StoreExtractedPhrase(const string& phrase,
                                           unsigned int phrase_frequency) {
  vector<string> words;
  split(words, phrase, is_any_of(" "));
  for (size_t first_index = 0; first_index < words.size(); ++first_index) {
    if (words[first_index] != "of") {
      for (size_t second_index = first_index; second_index < words.size();
           ++second_index) {
        if (words[second_index] != "of" &&
            first_index + words.size() - 1 != second_index) {
          string phrase_part = FormPhrase(words, first_index, second_index);
          StoreContextWords(words, first_index, second_index, phrase_frequency);
          InsertPhrasePart(phrase_part, phrase_frequency);
        }
      }
    }
  }
}

void PhrasesAnalyzer::StoreContextWords(const vector<string>& words,
                                        size_t start_index,
                                        size_t end_index,
                                        unsigned int phrase_frequency) {
  string phrase_part = FormPhrase(words, start_index, end_index);
  unordered_map<string, ContextWordFrequencies> temp_context_word_map;
  auto inserted_result = context_words_map_.insert({phrase_part,
                                                    temp_context_word_map});
  ContextWordFrequencies context_pre_frequency(phrase_frequency, 0);
  ContextWordFrequencies context_post_frequency(0, phrase_frequency);
  // store pre frequency
  for (size_t index = 0; index < start_index; ++index) {
    auto inner_result = inserted_result.first->second.insert(
        {words[index], context_pre_frequency});
    if (!inner_result.second) {
      inner_result.first->second.pre_frequency += phrase_frequency;
    }
  }
  // store post frequency
  for (size_t index = end_index + 1; index < words.size(); ++index) {
    auto inner_result = inserted_result.first->second.insert(
        {words[index], context_post_frequency});
    if (!inner_result.second) {
      inner_result.first->second.post_frequency += phrase_frequency;
    }
  }
}
