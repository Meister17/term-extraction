// Copyright 2011 Michael Nokel
#pragma once

#include "./candidates_analyzer.h"
#include <string>
#include <unordered_map>
#include <vector>

/**
  @brief Class for working with previously extracted single-word term candidates

  This class should be used for parsing file with previously extracted
  single-word term candidates and some data about them.
*/
class SingleWordTermsAnalyzer : public CandidatesAnalyzer {
 public:
  /**
    Parses given file with previously extracted single-word term candidates and
    some data about them
    @param filename file containing previously extracted single-word term
    candidates and some data about them
    @param terms_number number of terms to be taken into account
    @throw std::runtime_error in case of any error
  */
  void ParseFile(const std::string& filename, unsigned int terms_number);

  /**
    Returns frequency of the given word in the space of single-word term
    candidates
    @param word given word to get frequency for
    @return frequency of the given word in the space of single-word term
    candidates
  */
  unsigned int GetWordFrequency(const std::string& word) const {
    const auto found_iterator = single_words_map_.find(word);
    if (found_iterator != single_words_map_.end()) {
      return found_iterator->second;
    }
    return 0;
  }

 private:
  /** Hash map containing single words and their frequencies */
  std::unordered_map<std::string, unsigned int> single_words_map_;
};
