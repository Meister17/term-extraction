// Copyright 2012 Michael Nokel
#pragma once

#include <string>
#include <unordered_map>

/**
  @brief Class for working with file obtained from the reference collection

  With the help of this class one can obtain term candidates and their
  frequencies from the reference collection.
*/
class ReferenceCollectionAnalyzer {
 public:
  /**
    Parses file with words from the reference collection
    @param filename name of file containing words from the reference collection
    @throw std::runtime_error in case of any occurred error
  */
  void ParseFile(const std::string& filename);

  /**
    Gets term frequency of the given term candidate
    @param term_candidate term candidate for which term frequency should be
    returned
    @return term frequency of the given term candidate
  */
  unsigned int GetTermFrequency(const std::string& term_candidate) const {
    const auto found_iterator = reference_collection_map_.find(term_candidate);
    if (found_iterator != reference_collection_map_.end()) {
      return found_iterator->second.term_frequency;
    }
    return 0;
  }

  /**
    Gets document frequency of the given term candidate
    @param term_candidate term candidate for which document frequency should be
    returned
    @return document frequency of the given term candidate
  */
  unsigned int GetDocumentFrequency(const std::string& term_candidate) const {
    const auto found_iterator = reference_collection_map_.find(term_candidate);
    if (found_iterator != reference_collection_map_.end()) {
      return found_iterator->second.document_frequency;
    }
    return 0;
  }

  unsigned int total_number_words() const {
    return total_number_words_;
  }

  unsigned int total_number_documents() const {
    return total_number_documents_;
  }

 private:
  /**
    @brief Structure containing term and document frequencies

    This structure contains Term and Document Frequency of the term candidate
  */
  struct ReferenceFrequencies {
    ReferenceFrequencies(unsigned int df, unsigned int tf)
        : document_frequency(df),
          term_frequency(tf) {
    }

    /** Document frequency of the term candidate */
    unsigned int document_frequency;

    /** Term frequency of the term candidate */
    unsigned int term_frequency;
  };

  /** Total number of words in the reference collection */
  unsigned int total_number_words_ = 0;

  /** Total number of documents in the reference collection */
  unsigned int total_number_documents_ = 0;

  /** Hash map containing words from the reference collection as keys and their
      frequencies as values */
  std::unordered_map<std::string, ReferenceFrequencies>
      reference_collection_map_;
};
