// Copyright 2011 Michael Nokel
#pragma once

#include <string>
#include <unordered_map>
#include <vector>

/**
  @brief Class intended to be used for working with extracted phrases and their
  frequencies

  This class parses file with previously found phrases and their frequencies,
  store each word with its frequency in a map. After what this information can
  be used in various ways.
*/
class PhrasesAnalyzer {
 public:
  /**
    Parses given file with previously extracted phrases along with their
    frequencies and stores them in a map
    @param filename file containing previously extracted phrases along with
    their frequencies
    @throw std::runtime_error in case of any occurred error
  */
  void ParseFile(const std::string& filename);

  /**
    Returns maximum frequency of ambient phrase for the given word
    @param word given word to find maximum frequency
    @return maximum frequency of ambient phrase for the given word
  */
  unsigned int GetMaximumPhraseFrequency(const std::string& word) const;

  /**
    Returns average frequency of the given number of ambient phrases
    @param word given word to find desired average frequency
    @param phrases_number number of ambient phrases to be taken into account
    @return average frequency of the given number of ambient phrases
  */
  double GetAveragePhrasesFrequency(const std::string& word,
                                    unsigned int phrases_number) const;

  /**
    Returns number of all ambient phrases for the given word
    @param word given word to get number of all ambient phrases for the given
    word
    @return number of all ambient phrases for the given word
  */
  unsigned int GetAmbientPhrasesNumber(const std::string& word) const {
    return words_in_phrases_map_.count(word);
  }

  /**
    Returns frequencies of all context words for the given word
    @param word given word to calculate frequencies of all its context words
    @return frequencies of all context words for the given word
  */
  unsigned int GetContextWordsFrequenciesSum(const std::string& word) const;

  /**
    Returns number of all context words for given one
    @param word given word to get number of all context words
    @return number of all context words for given one or 0 if no such words have
    been found
  */
  unsigned int GetContextWordsNumber(const std::string& word) const {
    auto found_iterator = context_words_map_.find(word);
    if (found_iterator != context_words_map_.end()) {
      return found_iterator->second.size();
    }
    return 0;
  }

  /**
    Returns all context words for the given one
    @param word given word to find context words for it
    @return vector containing context words for the given one
  */
  std::vector<std::string> GetContextWords(const std::string& word) const;

  /**
    Calculates and returns sum of frequencies of all context words occurring
    before given one
    @param word given word to calculate frequencies
    @return sum of frequencies of all words occurring before given one
  */
  unsigned int GetPreFrequenciesSum(const std::string& word) const;

  /**
    Calculates and returns sum of frequencies of all context words occurring
    after given one
    @param word given word to calculate frequencies
    @return sum of frequencies of all words occurring after given one
  */
  unsigned int GetPostFrequenciesSum(const std::string& word) const;

  /**
    Calculates and returns number of all distinct context words occurring before
    given one
    @param word given word to calculate number
    @return number of all distinct words occurring before given one
  */
  unsigned int GetPreOccurrencesNumber(const std::string& word) const;

  /**
    Calculates and returns number of all distinct context words occurring after
    given one
    @param word given word to calculate number
    @return number of all distinct words occurring after given one
  */
  unsigned int GetPostOccurrencesNumber(const std::string& word) const;

 private:
  /**
    @brief Structure representing frequencies of the context words

    This structure contains pre and post frequencies of the context words found
    in extracted phrases
  */
  struct ContextWordFrequencies {
    ContextWordFrequencies(unsigned int pre_context_frequency,
                           unsigned int post_context_frequency)
        : pre_frequency(pre_context_frequency),
          post_frequency(post_context_frequency) {
    }

    /** Pre frequency of the context word */
    unsigned int pre_frequency = 0;

    /** Post frequency of the context word */
    unsigned int post_frequency = 0;
  };

  /**
    Stores extracted phrase and its frequency in the special structures
    @param phrase extracted phrase
    @param phrase_frequency frequency of the phrase
    @note This function should be called after each successful extraction of
    phrase while parsing file
  */
  void StoreExtractedPhrase(const std::string& phrase,
                            unsigned int phrase_frequency);

  /**
    Inserts part of extracted phrase in the special map
    @param phrase_part part of extracted phrase
    @param phrase_frequency frequency of the extracted phrase
  */
  void InsertPhrasePart(const std::string& phrase_part,
                        unsigned int phrase_frequency) {
    auto inserted_result = words_in_phrases_map_.insert(
        {phrase_part, std::vector<unsigned int>()});
    inserted_result.first->second.push_back(phrase_frequency);
  }

  /**
    Stores context words that forms extracted phrase
    @param words vector containing words that forms extracted phrase
    @param start_index index of the first word in the phrase part
    @param end_index index of the last word in the phrase part
    @param phrase_frequency frequency of the extracted phrase
  */
  void StoreContextWords(const std::vector<std::string>& words,
                         size_t start_index,
                         size_t end_index,
                         unsigned int phrase_frequency);

  /**
    Returns phrase starting from the words[start_index] and ending at the
    words[end_index]
    @param words vector containing words to form the phrase
    @param start_index index of the first word to form the phrase
    @param end_index index of the last word to form the phrase
    @return formed string
    @note It is assumed that end_index >= start_index and both indices are in
    the range of the given vector containing words
  */
  std::string FormPhrase(const std::vector<std::string>& words,
                         size_t start_index,
                         size_t end_index) const {
    std::string phrase = "";
    for (size_t index = start_index; index < end_index; ++index) {
      phrase += words[index] + " ";
    }
    return phrase + words[end_index];
  }

  /** Hash map containing words' frequencies in phrases */
  std::unordered_map<std::string, std::vector<unsigned int> >
      words_in_phrases_map_;

  /** Hash map containing context words and their frequencies */
  std::unordered_map<std::string,
                     std::unordered_map<std::string, ContextWordFrequencies> >
      context_words_map_;
};
