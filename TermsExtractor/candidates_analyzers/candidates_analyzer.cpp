// Copyright 2013 Michael Nokel
#include "./candidates_analyzer.h"
#include "../auxiliary_macros.h"
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <fstream>
#include <stdexcept>
#include <string>
#include <vector>

using boost::is_any_of;
using boost::split;
using std::cout;
using std::getline;
using std::find_if;
using std::ifstream;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::string;
using std::vector;

TermCandidateData CandidatesAnalyzer::ParseLine(ifstream* file) const {
  TermCandidateData term_candidate_data;
  string line;
  getline(*file, line);
  line.erase(find_if(line.rbegin(),
                     line.rend(),
                     not1(ptr_fun<int, int>(isspace))).base(),
             line.end());
  if (line.empty()) {
    return term_candidate_data;
  }
  vector<string> tokens;
  split(tokens, line, is_any_of("\t"));
  if (tokens.size() != 26) {
    throw runtime_error("Failed to parse file with candidates");
  }
  term_candidate_data.term_candidate = tokens[0];
  GetPartOfSpeech(tokens[1], &term_candidate_data);
  term_candidate_data.novelty = stoi(tokens[2]);
  term_candidate_data.ambiguity = stoi(tokens[3]);
  term_candidate_data.first_occurrence_position = stod(tokens[4]);
  term_candidate_data.near_terms_frequency = stod(tokens[5]);
  term_candidate_data.term_frequency = stod(tokens[6]);
  term_candidate_data.term_frequency_as_capital_word = stod(tokens[7]);
  term_candidate_data.term_frequency_as_non_initial_word = stod(tokens[8]);
  term_candidate_data.term_frequency_as_subject = stod(tokens[9]);
  term_candidate_data.document_frequency = stod(tokens[10]);
  term_candidate_data.document_frequency_as_capital_word = stod(tokens[11]);
  term_candidate_data.document_frequency_as_non_initial_word = stod(tokens[12]);
  term_candidate_data.document_frequency_as_subject = stod(tokens[13]);
  term_candidate_data.domain_consensus = stod(tokens[14]);
  term_candidate_data.domain_consensus_as_capital_word = stod(tokens[15]);
  term_candidate_data.domain_consensus_as_non_initial_word = stod(tokens[16]);
  term_candidate_data.domain_consensus_as_subject = stod(tokens[17]);
  term_candidate_data.probabilistic_term_frequency = stod(tokens[18]);
  term_candidate_data.probabilistic_maximum_term_frequency = stod(tokens[19]);
  term_candidate_data.probabilistic_term_score = stod(tokens[20]);
  term_candidate_data.probabilistic_maximum_term_score = stod(tokens[21]);
  term_candidate_data.term_contribution = stod(tokens[22]);
  term_candidate_data.term_variance = stod(tokens[23]);
  term_candidate_data.term_variance_quality = stod(tokens[24]);
  term_candidate_data.term_frequency_bm25 = stod(tokens[25]);
  return term_candidate_data;
}

void CandidatesAnalyzer::GetPartOfSpeech(const string& part_of_speech,
                                         TermCandidateData* term_candidate_data)
                                         const {
  if (part_of_speech == "A") {
    term_candidate_data->is_adjective = 1;
  } else if (part_of_speech == "B") {
    term_candidate_data->is_adjective = 1;
    term_candidate_data->is_noun = 1;
  } else if (part_of_speech == "N") {
    term_candidate_data->is_noun = 1;
  }
}