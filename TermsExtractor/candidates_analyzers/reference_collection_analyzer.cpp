// Copyright 2012 Michael Nokel
#include "./reference_collection_analyzer.h"
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <cctype>
#include <fstream>
#include <stdexcept>
#include <string>
#include <vector>

using boost::is_any_of;
using boost::split;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::stoi;
using std::string;
using std::vector;

void ReferenceCollectionAnalyzer::ParseFile(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse file with reference collection");
  }
  file >> total_number_documents_ >> total_number_words_;
  while (!file.eof()) {
    string line;
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (!line.empty()) {
      vector<string> tokens;
      split(tokens, line, is_any_of("\t"));
      if (tokens.size() == 2) {
        reference_collection_map_.insert(
	    {tokens[0],
             ReferenceFrequencies(stoi(tokens[1]), stoi(tokens[1]))});
      } else if (tokens.size() == 3) {
        reference_collection_map_.insert(
            {tokens[0],
             ReferenceFrequencies(stoi(tokens[1]), stoi(tokens[2]))});
      } else {
        file.close();
        std::cout << line << "\n";
        throw runtime_error("Failed to parse file with reference collection");
      }
    }
  }
  file.close();
  if (reference_collection_map_.empty()) {
    throw runtime_error("Failed to parse file with reference collection");
  }
}
