// Copyright 2012 Michael Nokel
#pragma once

#include <cmath>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

/**
  @brief Class intended to be used for working with results of LDA algorithm

  It parses file containing results of LDA algorithm. After what one can use it
  for determining term frequency or topic frequency of any given single word.
*/
class LDAResultsAnalyzer {
 public:
  /**
    Parses file containing results of LDA algorithm
    @param filename file containing results of LDA algorithm
    @throw std::runtime_error in case of any error
  */
  void ParseFile(const std::string& filename);

  /**
    Returns term frequency of the given term candidate in the space of topics
    @param term_candidate term candidate to get term frequency for
    @return calculated term frequency of the given term candidate in the space
    of topics
  */
  double GetTermFrequency(const std::string& term_candidate) const {
    return GetTermFrequency(term_candidate, lda_results_map_);
  }

  /**
    Returns Term Score of the given term candidate in the space of topics
    @param term_candidate term candidate to get Term Score for
    @return Term Score of the given term candidate in the space of topics
  */
  double GetTermScore(const std::string& term_candidate) const {
    return GetTermFrequency(term_candidate, term_scores_map_);
  }

  /**
    Returns topic frequency of the given term candidate
    @param term_candidate term candidate to get topic frequency for
    @return calculated topic frequency of the given term candidate
  */
  unsigned int GetTopicFrequency(const std::string& term_candidate) const {
    auto found_iterator = lda_results_map_.find(term_candidate);
    if (found_iterator != lda_results_map_.end()) {
      return static_cast<unsigned int>(found_iterator->second.size());
    }
    return 0;
  }

  /**
    Calculates Domain Consensus of the given term candidate in the space of
    topics
    @param term_candidate term candidate to get Domain Consensus for
    @return calculated Domain Consensus value for the given term candidate
  */
  double CalculateDomainConsensus(const std::string& term_candidate) const {
    auto found_iterator = lda_results_map_.find(term_candidate);
    double domain_consensus = 0.0;
    if (found_iterator != lda_results_map_.end()) {
      for (auto element: found_iterator->second) {
        domain_consensus -= element * log(element);
      }
    }
    return domain_consensus;
  }

  /**
    Calculates maximum term frequency of the given term candidate
    @param term_candidate term candidate to get maximum term frequency for
    @return maximum term frequency of the given term candidate
  */
  double GetMaximumTermFrequency(const std::string& term_candidate) const {
    return GetMaximumTermFrequency(term_candidate, lda_results_map_);
  }

  /**
    Calculates maximum Term Score of the given term candidate
    @param term_candidate term candidate to get maximum Term Score for
    @return maximum Term Score of the given term candidate
  */
  double GetMaximumTermScore(const std::string& term_candidate) const {
    return GetMaximumTermFrequency(term_candidate, term_scores_map_);
  }

  unsigned int total_number_topics() const {
    return total_number_topics_;
  }

 private:
  /**
    Computes Term Scores for all words in the output of LDA
    @param lda_results map containing results of LDA
    @param[out] term_score_results map where Term Scores for all words in the
    output of LDA will be written
  */
  void ComputeTermScores(
      const std::unordered_map<std::string, std::vector<double> >& lda_results,
      std::unordered_map<std::string, std::vector<double> >* term_score_results)
      const;

  /**
    Gets total term frequency of the term candidate stored in the given map
    @param term_candidate term candidate for which to compute term frequency
    @param results_map map containing some results of the output of LDA
    @return total term frequency of the term candidate stored in the given map
  */
  double GetTermFrequency(
      const std::string& term_candidate,
      const std::unordered_map<std::string, std::vector<double> >& results_map)
      const {
    auto found_iterator = results_map.find(term_candidate);
    double term_frequency = 0.0;
    if (found_iterator != results_map.end()) {
      for (auto element: found_iterator->second) {
        term_frequency += element;
      }
    }
    return term_frequency;
  }

  /**
    Gets maximum term frequency of the term candidate stored in the given map
    @param term_candidate term candidate for which to compute maximum term
    frequency
    @param results_map map containing some results of the output of LDA
    @return maximum term frequency of the term candidate stored in the given map
  */
  double GetMaximumTermFrequency(
      const std::string& term_candidate,
      const std::unordered_map<std::string, std::vector<double> >& results_map)
      const {
    auto found_iterator = results_map.find(term_candidate);
    double maximum_term_frequency = 0;
    if (found_iterator != results_map.end()) {
      for (auto element: found_iterator->second) {
        if (element > maximum_term_frequency) {
          maximum_term_frequency = element;
        }
      }
    }
    return maximum_term_frequency;
  }

  /** String representation of the word determining start of the topic */
  const std::string kTopic_ = "Topic";

  /** Total number of topics */
  unsigned int total_number_topics_ = 0;

  /** Hash map containing term frequencies of the single words in various topics */
  std::unordered_map<std::string, std::vector<double> > lda_results_map_;

  /** Hash map containing Term Score of the single words in various topics */
  std::unordered_map<std::string, std::vector<double> > term_scores_map_;
};