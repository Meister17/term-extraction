// Copyright 2012 Michael Nokel
#pragma once

#include "../auxiliary_macros.h"
#include <fstream>
#include <string>
#include <vector>

/**
  @brief Abstract class for working with previously extracted term candidates

  Each class intended to work with extracted term candidates should derive this
  one. Sample code of using one can see in each derived class.
*/
class CandidatesAnalyzer {
 public:
  /**
    Parses given file with previously extracted term candidates and some data
    about them
    @param filename name of file with previously extracted term candidates
    @param terms_number number of terms that will be taken into account
    @throw std::runtime_error in case of any error
  */
  virtual void ParseFile(const std::string& filename,
                         unsigned int terms_number) = 0;

  /**
    Returns start iterator to the vector containing term candidates and some
    data about them
    @return start iterator to such vector
  */
  virtual std::vector<TermCandidateData>::const_iterator GetStartIterator()
      const {
    return term_candidates_vector_.begin();
  }

  /**
    Returns end iterator to the vector containing term candidates and some
    data about them
    @return end iterator to such vector
  */
  virtual std::vector<TermCandidateData>::const_iterator GetEndIterator()
      const {
    return term_candidates_vector_.end();
  }

  virtual unsigned int total_number_documents() const {
    return total_number_documents_;
  }

  virtual unsigned int total_number_words() const {
    return total_number_words_;
  }

  /**
    Virtual destructor for the purposes of correct deriving
  */
  virtual ~CandidatesAnalyzer() {
  }

 protected:
  /**
    Parses line and forms term candidate data from it
    @param[out] file file from where to parse line
    @return term candidate data formed from the parsed line
    @throw std::runtime_error in case of wrong line for parsing
  */
  TermCandidateData ParseLine(std::ifstream* file) const;

  /**
    Gets number representation of given string representation of part of speech
    @param part_of_speech given part of speech: 'A' - adjective, 'N' - noun,
    'B' - both
    @param[out] single_word_special_data structure where information about part
    of speech will be written
    @throw std::runtime_error in case of wrong part of speech
  */
  void GetPartOfSpeech(const std::string& part_of_speech,
                       TermCandidateData* term_candidate_data) const;

  /** Total number of documents in the target corpus */
  unsigned int total_number_documents_ = 0;

  /** Total number of words in the target corpus */
  unsigned int total_number_words_ = 0;

  /** Vector containing term candidates and some data about them */
  std::vector<TermCandidateData> term_candidates_vector_;
};
