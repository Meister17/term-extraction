// Copyright 2012 Michael Nokel
#pragma once

#include "./candidates_analyzer.h"
#include <string>
#include <unordered_map>
#include <vector>

/**
  @brief Class for working with previously extracted two-word term candidates

  This class should be used for parsing file with previously extracted two-word
  term candidates and some data about them.
*/
class TwoWordTermsAnalyzer : public CandidatesAnalyzer {
 public:
  /**
    Parses file with previously extracted two-word term candidates and some data
    about them
    @param filename name of file with previously extracted two-word term
    candidates and some data about them
    @param terms_number number of terms that will be taken into account
    @throw std::runtime_error in case of any error
  */
  void ParseFile(const std::string& filename, unsigned int terms_number);
};
