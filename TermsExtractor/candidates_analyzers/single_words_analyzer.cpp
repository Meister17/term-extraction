// Copyright 2012 Michael Nokel
#include "./single_words_analyzer.h"
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <cctype>
#include <fstream>
#include <stdexcept>
#include <string>
#include <vector>

using boost::is_any_of;
using boost::split;
using std::cout;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::stoi;
using std::string;
using std::vector;

void SingleWordsAnalyzer::ParseFile(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to open file with single words");
  }
  file >> total_number_words_;
  while (!file.eof()) {
    string line;
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (!line.empty()) {
      vector<string> tokens;
      split(tokens, line, is_any_of("\t"));
      single_words_map_[tokens.front()] = stoi(tokens.back());
    }
  }
  file.close();
}
