// Copyright 2011 Michael Nokel
#pragma once

#include <string>

/**
  @brief Class using to work with program options

  With the help of this class one can parse program options from command line
  and/or from configuration file, check their correctness and if everything is
  correct one can get the values of the necessary program options.
*/
class ProgramOptionsParser {
 public:
  /**
    Parses program options from command line and/or from configuration file
    @param argc number of program options (from main function)
    @param argv array containing program options (from main function)
    @throw std::runtime_error in case of any error
  */
  void Parse(int argc, char** argv);

  bool help_message_printed() const {
    return help_message_printed_;
  }

  unsigned int threshold_for_term_number() const {
    return threshold_for_term_number_;
  }

  unsigned int threshold_for_average_precision() const {
    return threshold_for_average_precision_;
  }

  std::string average_precision_directory_name() const {
    return average_precision_directory_name_;
  }

  std::string destination_directory_name() const {
    return destination_directory_name_;
  }

  std::string real_terms_filename() const {
    return real_terms_filename_;
  }

  std::string single_word_terms_filename() const {
    return single_word_terms_filename_;
  }

  std::string single_word_lda_results_filename() const {
    return single_word_lda_results_filename_;
  }

  std::string single_word_phrases_filename() const {
    return single_word_phrases_filename_;
  }

  std::string reference_collection_single_words_filename() const {
    return reference_collection_single_words_filename_;
  }

  std::string single_words_filename() const {
    return single_words_filename_;
  }

  std::string two_word_terms_filename() const {
    return two_word_terms_filename_;
  }

  std::string two_word_lda_results_filename() const {
    return two_word_lda_results_filename_;
  }

  std::string two_word_phrases_filename() const {
    return two_word_phrases_filename_;
  }

  std::string reference_collection_two_words_filename() const {
    return reference_collection_two_words_filename_;
  }

private:
  /**
    Corrects directory's name by adding path separator to the end of the
    function
    @param[out] directory_name directory to correct
  */
  void CorrectDirectoryName(std::string* directory_name) const;

  /**
    Recreates given file
    @param filename file to recreate
    @throw std::runtime_error in case of any occurred errors
  */
  void RecreateFile(const std::string& filename) const;

  /** Flag indicating whether help message has been printed */
  bool help_message_printed_ = false;

  /** Threshold for number of extracting terms */
  unsigned int threshold_for_term_number_ = 5000;

  /** Threshold for Average Precision value */
  unsigned int threshold_for_average_precision_ = 5000;

  /** Directory for printing AvP results for all features */
  std::string average_precision_directory_name_ = "";

  /** Destination directory where extracted terms should be placed */
  std::string destination_directory_name_ = "";

  /** File containing real terms from the specified area */
  std::string real_terms_filename_ = "";

  /** File with single-word term candidates and their frequencies */
  std::string single_word_terms_filename_ = "";

  /** File with results of LDA or clustering for single-word term candidates */
  std::string single_word_lda_results_filename_ = "";

  /** File with previously extracted noun phrases for single-word term
      candidates */
  std::string single_word_phrases_filename_ = "";

  /** File with single-word term candidates from the reference collection */
  std::string reference_collection_single_words_filename_ = "";

  /** File with single words along with their frequencies */
  std::string single_words_filename_ = "";

  /** File with two-word term candidates and their frequencies */
  std::string two_word_terms_filename_ = "";

  /** File with results of LDA or clustering for two-word term candidates */
  std::string two_word_lda_results_filename_ = "";

  /** File with previously extracted noun phrases for two-word term
      candidates */
  std::string two_word_phrases_filename_ = "";

  /** File with two-word term candidates from the reference collection */
  std::string reference_collection_two_words_filename_ = "";
};
