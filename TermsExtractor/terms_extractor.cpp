// Copyright 2012 Michael Nokel
#include "./terms_extractor.h"
#include "./auxiliary_macros.h"
#include <boost/filesystem.hpp>
#include <boost/threadpool.hpp>
#include <boost/thread.hpp>
#include <iostream>
#include <string>

using boost::filesystem::path;
using boost::threadpool::pool;
using std::cout;
using std::string;

TermsExtractor::TermsExtractor(
    const std::string& average_precision_directory_name,
    unsigned int average_precision_threshold,
    unsigned int threshold_for_term_number)
    : kNumberOfThreads_(boost::thread::hardware_concurrency()),
      kThresholdForTermNumber_(threshold_for_term_number),
      candidates_type_(NO_CANDIDATES_TYPE),
      single_word_terms_extractor_(
          average_precision_directory_name + "single_words.txt",
          average_precision_threshold,
          &results_analyzer_),
      two_word_terms_extractor_(
          average_precision_directory_name + "two_words.txt",
          average_precision_threshold,
          &results_analyzer_) {
}

void TermsExtractor::Initialize(
    const string& real_terms_filename,
    const string& single_word_terms_filename,
    const string& single_word_lda_results_filename,
    const string& single_word_phrases_filename,
    const string& reference_collection_single_words_filename,
    const string& single_words_filename,
    const string& two_word_terms_filename,
    const string& two_word_lda_results_filename,
    const string& two_word_phrases_filename,
    const string& reference_collection_two_words_filename) {
  cout << "Initializing\n";
  pool thread_pool(kNumberOfThreads_);
  thread_pool.schedule(boost::bind(&ResultsAnalyzer::ParseFile,
                                   &results_analyzer_,
                                   real_terms_filename));
  if (!single_word_terms_filename.empty() &&
      !single_word_lda_results_filename.empty() &&
      !single_word_phrases_filename.empty() &&
      !reference_collection_single_words_filename.empty()) {
    candidates_type_ = static_cast<CandidatesTypes>(candidates_type_ |
                                                    SINGLE_WORD_TERMS);
    thread_pool.schedule(boost::bind(
        &SingleWordTermsExtractor::ParseFiles,
        &single_word_terms_extractor_,
        single_word_terms_filename,
        single_word_lda_results_filename,
        reference_collection_single_words_filename,
        single_word_phrases_filename,
        kThresholdForTermNumber_));
  }
  if (!two_word_terms_filename.empty() &&
      !two_word_lda_results_filename.empty() &&
      !two_word_phrases_filename.empty() &&
      !reference_collection_two_words_filename.empty() &&
      !single_words_filename.empty()) {
    candidates_type_ = static_cast<CandidatesTypes>(candidates_type_ |
                                                    TWO_WORD_TERMS);
    thread_pool.schedule(boost::bind(&TwoWordTermsExtractor::ParseFiles,
                                     &two_word_terms_extractor_,
                                     two_word_terms_filename,
                                     two_word_lda_results_filename,
                                     reference_collection_two_words_filename,
                                     two_word_phrases_filename,
                                     kThresholdForTermNumber_));
    thread_pool.schedule(boost::bind(
        &TwoWordTermsExtractor::ParseSpecialFile,
        &two_word_terms_extractor_,
        single_words_filename));
  }
}

void TermsExtractor::ExtractTerms(const string& destination_directory_name) {
  cout << "Applying various features to term candidates\n";
  pool thread_pool(kNumberOfThreads_);
  path slash("/");
  string path_separator = slash.make_preferred().native();
  if (candidates_type_ & SINGLE_WORD_TERMS) {
    thread_pool.schedule(boost::bind(
        &SingleWordTermsExtractor::ExtractTerms,
        &single_word_terms_extractor_,
        destination_directory_name + "single-word" + path_separator));
  }
  if (candidates_type_ & TWO_WORD_TERMS) {
    thread_pool.schedule(boost::bind(
        &TwoWordTermsExtractor::ExtractTerms,
        &two_word_terms_extractor_,
        destination_directory_name + "two-word" + path_separator));
  }
}
