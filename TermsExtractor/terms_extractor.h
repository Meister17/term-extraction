// Copyright 2012 Michael Nokel
#pragma once

#include "./candidates_extractors/single_word_terms_extractor.h"
#include "./candidates_extractors/two_word_terms_extractor.h"
#include "./results_analyzer.h"
#include <string>

/**
  @brief Main class that manages extracting various term candidates

  This class should be used for proper application of various features and
  reordering term candidates based on these features.
*/
class TermsExtractor {
 public:
  /**
    Initializes object
    @param average_precision_directory_name directory where Average Precisions
    for all features will be printed
    @param average_precision_threshold threshold for calculating Average
    Precision
    @param threshold_for_term_number threshold for number of terms that should
    be taken into account
  */
  TermsExtractor(const std::string& average_precision_directory_name,
                 unsigned int average_precision_threshold,
                 unsigned int threshold_for_term_number);

  /**
    Initializes object by parsing files
    @param real_terms_filename file containing real terms from the specified
    area
    @param single_word_terms_filename file containing previously extracted
    single-word term candidates and some data about them
    @param single_word_lda_results_filename file containing results of LDA or
    clustering for single-word term candidates
    @param single_word_phrases_filename file containing previously extracted
    phrases for single-word term candidates and their frequencies
    @param reference_collection_single_words_filename file containing single
    words from the reference collection
    @param single_words_filename file containing single words along with their
    frequencies
    @param two_word_terms_filename file containing previously extracted two-word
    term candidates and some data about them
    @param two_word_lda_results_filename file containing results of LDA or
    clustering for two-word term candidates
    @param two_word_phrases_filename file containing previously extracted
    phrases for two-word term candidates and their frequencies
    @param reference_collection_two_words_filename file containing two words
    from the reference collection
    @throw std::runtime_error in case of any error
  */
  void Initialize(const std::string& real_terms_filename,
                  const std::string& single_word_terms_filename,
                  const std::string& single_word_lda_results_filename,
                  const std::string& single_word_phrases_filename,
                  const std::string& reference_collection_single_words_filename,
                  const std::string& single_words_filename,
                  const std::string& two_word_terms_filename,
                  const std::string& two_word_lda_results_filename,
                  const std::string& two_word_phrases_filename,
                  const std::string& reference_collection_two_words_filename);

  /**
    Applies various features to various term candidates and prints results
    @param destination_directory_name destination directory where results should
    be printed
    @throw std::runtime_error in case of any errors
  */
  void ExtractTerms(const std::string& destination_directory_name);

 private:
  /** Number of threads in a thread pool */
  const int kNumberOfThreads_;

  /** Threshold for number of terms that should be taken into account */
  const unsigned int kThresholdForTermNumber_;

  /** Type of candidates to work with */
  CandidatesTypes candidates_type_ = NO_CANDIDATES_TYPE;

  /** Object used for finding real terms among extracted term candidates */
  ResultsAnalyzer results_analyzer_;

  /** Object used for working with single-word term candidates */
  SingleWordTermsExtractor single_word_terms_extractor_;

  /** Object used for working with two-word term candidates */
  TwoWordTermsExtractor two_word_terms_extractor_;
};
