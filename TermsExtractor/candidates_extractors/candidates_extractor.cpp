// Copyright 2012 Michael Nokel
#include "./candidates_extractor.h"
#include "../auxiliary_macros.h"
#include "../candidates_analyzers/phrases_analyzer.h"
#include "../candidates_analyzers/single_word_terms_analyzer.h"
#include "../candidates_analyzers/two_word_terms_analyzer.h"
#include "../results_analyzer.h"
#include <boost/thread/mutex.hpp>
#include <boost/threadpool.hpp>
#include <algorithm>
#include <fstream>
#include <map>
#include <stdexcept>
#include <string>
#include <vector>

using boost::threadpool::pool;
using std::count;
using std::cout;
using std::ofstream;
using std::map;
using std::max;
using std::min;
using std::runtime_error;
using std::string;
using std::to_string;
using std::vector;

CandidatesExtractor::CandidatesExtractor(
    const string& term_candidates_type,
    const string& average_precision_filename,
    unsigned int average_precision_threshold,
    ResultsAnalyzer* results_analyzer)
    : kAveragePrecisionFilename_(average_precision_filename),
      kAveragePrecisionThreshold_(average_precision_threshold),
      results_analyzer_(results_analyzer) {
  if (term_candidates_type == "single-word") {
    term_candidates_analyzer_.reset(new SingleWordTermsAnalyzer());
  } else if (term_candidates_type == "two-word") {
    term_candidates_analyzer_.reset(new TwoWordTermsAnalyzer());
  }
}

void CandidatesExtractor::ParseFiles(
    const string& term_candidates_filename,
    const string& lda_results_filename,
    const string& reference_collection_filename,
    const string& phrases_filename,
    unsigned int terms_number) {
  term_candidates_analyzer_->ParseFile(term_candidates_filename,
                                       terms_number);
  reference_collection_analyzer_.ParseFile(reference_collection_filename);
  lda_results_analyzer_.ParseFile(lda_results_filename);
  phrases_analyzer_.ParseFile(phrases_filename);
}

void CandidatesExtractor::ApplyCommonFeatures(
    const string& destination_directory_name) {
  pool thread_pool(kNumberOfThreads_);
  thread_pool.schedule(boost::bind(&CandidatesExtractor::ApplyNouns,
                                   this,
                                   destination_directory_name + "nouns.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyAdjectives,
      this,
      destination_directory_name + "adjectives.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTermLength,
      this,
      destination_directory_name + "term_length.txt"));
  thread_pool.schedule(boost::bind(&CandidatesExtractor::ApplyNovelty,
                                   this,
                                   destination_directory_name + "novelty.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyAmbiguity,
      this,
      destination_directory_name + "ambiguity.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplySpecificity,
      this,
      destination_directory_name + "specificity.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyFirstOccurrence,
      this,
      destination_directory_name + "first_occurrence.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyNearTermsFreq,
      this,
      destination_directory_name + "neartermsfreq.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTermFrequency,
      this,
      destination_directory_name + "term_frequency.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTermFrequencyAsCapitalWords,
      this,
      destination_directory_name + "term_frequency_capital_words.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTermFrequencyAsNonInitialWords,
      this,
      destination_directory_name + "term_frequency_non_initial_words.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTermFrequencyAsSubjects,
      this,
      destination_directory_name + "term_frequency_subjects.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyDocumentFrequency,
      this,
      destination_directory_name + "document_frequency.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyDocumentFrequencyAsCapitalWords,
      this,
      destination_directory_name + "document_frequency_capital_words.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyDocumentFrequencyAsNonInitialWords,
      this,
      destination_directory_name + "document_frequency_non_initial_words.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyDocumentFrequencyAsSubjects,
      this,
      destination_directory_name + "document_frequency_subjects.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyDomainConsensus,
      this,
      destination_directory_name + "domain_consensus.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyDomainConsensusAsCapitalWords,
      this,
      destination_directory_name + "domain_consensus_capital_words.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyDomainConsensusAsNonInitialWords,
      this,
      destination_directory_name + "domain_consensus_non_initial_words.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyDomainConsensusAsSubjects,
      this,
      destination_directory_name + "domain_consensus_subjects.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyNearTermsFreqIDFReference,
      this,
      destination_directory_name + "neartermsfreq_idf_reference.txt"));
  thread_pool.schedule(boost::bind(&CandidatesExtractor::ApplyBM25,
                                   this,
                                   destination_directory_name + "bm_25.txt"));
  thread_pool.schedule(boost::bind(&CandidatesExtractor::ApplyTFIDF,
                                   this,
                                   destination_directory_name + "tfidf.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTFIDFReference,
      this,
      destination_directory_name + "tfidf_reference.txt"));
  thread_pool.schedule(boost::bind(&CandidatesExtractor::ApplyTFRIDF,
                                   this,
                                   destination_directory_name + "tfridf.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTFIDFAsCapitalWords,
      this,
      destination_directory_name + "tfidf_capital_words.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTFIDFReferenceAsCapitalWords,
      this,
      destination_directory_name + "tfidf_reference_capital_words.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTFRIDFAsCapitalWords,
      this,
      destination_directory_name + "tfridf_capital_words.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTFIDFAsNonInitialWords,
      this,
      destination_directory_name + "tfidf_non_initial_words.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTFIDFReferenceAsNonInitialWords,
      this,
      destination_directory_name + "tfidf_reference_non_initial_words.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTFRIDFAsNonInitialWords,
      this,
      destination_directory_name + "tfridf_non_initial_words.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTFIDFAsSubjects,
      this,
      destination_directory_name + "tfidf_subjects.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTFIDFReferenceAsSubjects,
      this,
      destination_directory_name + "tfidf_reference_subjects.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTFRIDFAsSubjects,
      this,
      destination_directory_name + "tfridf_subjects.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyWeirdness,
      this,
      destination_directory_name + "weirdness.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyReferenceWeight,
      this,
      destination_directory_name + "reference_weight.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyRelevance,
      this,
      destination_directory_name + "relevance.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyContrastiveWeight,
      this,
      destination_directory_name + "contrastive_weight.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyDiscriminativeWeight,
      this,
      destination_directory_name + "discriminative_weight.txt"));
  thread_pool.schedule(boost::bind(&CandidatesExtractor::ApplyKFIDF,
                                   this,
                                   destination_directory_name + "kfidf.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyLogLikelihood,
      this,
      destination_directory_name + "loglikelihood.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyProbabilisticTermFrequency,
      this,
      destination_directory_name + "probabilistic_term_frequency.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyProbabilisticTFIDF,
      this,
      destination_directory_name + "probabilistic_tfidf.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyProbabilisticMaximumTermFrequency,
      this,
      destination_directory_name + "probabilistic_maximum_term_frequecy.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyProbabilisticTermScore,
      this,
      destination_directory_name + "probabilistic_term_score.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyProbabilisticTFIDFTermScore,
      this,
      destination_directory_name + "probabilistic_tfidf_term_score.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyProbabilisticMaximumTermScore,
      this,
      destination_directory_name + "probabilistic_maximum_term_score.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTermContribution,
      this,
      destination_directory_name + "term_contribution.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTermVariance,
      this,
      destination_directory_name + "term_variance.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTermVarianceQuality,
      this,
      destination_directory_name + "term_variance_quality.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTermFrequencyLDA,
      this,
      destination_directory_name + "term_frequency_lda.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTFIDFLDA,
      this,
      destination_directory_name + "tfidf_lda.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyDomainConsensusLDA,
      this,
      destination_directory_name + "domain_consensus_lda.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyMaximumTermFrequencyLDA,
      this,
      destination_directory_name + "maximum_term_frequency_lda.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTermScoreLDA,
      this,
      destination_directory_name + "term_score_lda.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTFIDFTermScoreLDA,
      this,
      destination_directory_name + "tfidf_term_score_lda.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyMaximumTermScoreLDA,
      this,
      destination_directory_name + "maximum_term_score_lda.txt"));
  thread_pool.schedule(boost::bind(&CandidatesExtractor::ApplyCValue,
                                   this,
                                   destination_directory_name + "cvalue.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyMCValue,
      this,
      destination_directory_name + "mc_value.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyNCValue,
      this,
      destination_directory_name + "nc_value.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyMNCValue,
      this,
      destination_directory_name + "mnc_value.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyInsideness,
      this,
      destination_directory_name + "insideness.txt"));
  unsigned int parameter = 3;
  thread_pool.schedule(boost::bind(&CandidatesExtractor::ApplySumN,
                                   this,
                                   destination_directory_name + "sum3.txt",
                                   parameter));
  parameter = 10;
  thread_pool.schedule(boost::bind(&CandidatesExtractor::ApplySumN,
                                   this,
                                   destination_directory_name + "sum10.txt",
                                   parameter));
  parameter = 50;
  thread_pool.schedule(boost::bind(&CandidatesExtractor::ApplySumN,
                                   this,
                                   destination_directory_name + "sum50.txt",
                                   parameter));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTokenLR,
      this,
       destination_directory_name + "token_lr.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTokenFLR,
      this,
      destination_directory_name + "token_flr.txt"));
  thread_pool.schedule(boost::bind(&CandidatesExtractor::ApplyTypeLR,
                                   this,
                                   destination_directory_name + "type_lr.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyTypeFLR,
      this,
      destination_directory_name + "type_flr.txt"));
  thread_pool.schedule(boost::bind(
      &CandidatesExtractor::ApplyModifiedGravityCount,
      this,
      destination_directory_name + "modified_gravity_count.txt"));
}

void CandidatesExtractor::ApplyTermLength(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 static_cast<double>(count(iterator->term_candidate.begin(),
                                           iterator->term_candidate.end(),
                                           ' ')),
                 &weights);
  }
  PrintTermCandidates(filename, "Term Length", weights);
}

void CandidatesExtractor::ApplyNouns(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate, iterator->is_noun, &weights);
  }
  PrintTermCandidates(filename, "Nouns", weights);
}

void CandidatesExtractor::ApplyAdjectives(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate, iterator->is_adjective, &weights);
  }
  PrintTermCandidates(filename, "Adjectives", weights);
}

void CandidatesExtractor::ApplyNovelty(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate, iterator->novelty, &weights);
  }
  PrintTermCandidates(filename, "Novelty", weights);
}

void CandidatesExtractor::ApplyAmbiguity(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate, iterator->ambiguity, &weights);
  }
  PrintTermCandidates(filename, "Ambiguity", weights);
}

void CandidatesExtractor::ApplySpecificity(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = 0.0;
    if (!reference_collection_analyzer_.GetTermFrequency(
            iterator->term_candidate)) {
      weight_value = 1.0;
    }
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Specificity", weights);
}

void CandidatesExtractor::ApplyFirstOccurrence(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->first_occurrence_position,
                 &weights);
  }
  PrintTermCandidates(filename, "First Occurrence", weights);
}

void CandidatesExtractor::ApplyTermFrequency(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate, iterator->term_frequency, &weights);
  }
  PrintTermCandidates(filename, "Term Frequency", weights);
}

void CandidatesExtractor::ApplyTermFrequencyAsCapitalWords(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->term_frequency_as_capital_word,
                 &weights);
  }
  PrintTermCandidates(filename, "Term Frequency Capital Words", weights);
}

void CandidatesExtractor::ApplyTermFrequencyAsNonInitialWords(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->term_frequency_as_non_initial_word,
                 &weights);
  }
  PrintTermCandidates(filename, "Term Frequency Non-Initial Words", weights);
}

void CandidatesExtractor::ApplyTermFrequencyAsSubjects(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->term_frequency_as_subject,
                 &weights);
  }
  PrintTermCandidates(filename, "Term Frequency Subjects", weights);
}

void CandidatesExtractor::ApplyDocumentFrequency(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->document_frequency,
                 &weights);
  }
  PrintTermCandidates(filename, "Document Frequency", weights);
}

void CandidatesExtractor::ApplyDocumentFrequencyAsCapitalWords(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->document_frequency_as_capital_word,
                 &weights);
  }
  PrintTermCandidates(filename, "Document Frequency Capital Words", weights);
}

void CandidatesExtractor::ApplyDocumentFrequencyAsNonInitialWords(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->document_frequency_as_non_initial_word,
                 &weights);
  }
  PrintTermCandidates(filename,
                      "Document Frequency Non-Initial Words",
                      weights);
}

void CandidatesExtractor::ApplyDocumentFrequencyAsSubjects(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->document_frequency_as_subject,
                 &weights);
  }
  PrintTermCandidates(filename, "Document Frequency Subjects", weights);
}

void CandidatesExtractor::ApplyDomainConsensus(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->domain_consensus,
                 &weights);
  }
  PrintTermCandidates(filename, "Domain Consensus", weights);
}

void CandidatesExtractor::ApplyDomainConsensusAsCapitalWords(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->domain_consensus_as_capital_word,
                 &weights);
  }
  PrintTermCandidates(filename, "Domain Consensus Capital Words", weights);
}

void CandidatesExtractor::ApplyDomainConsensusAsNonInitialWords(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->domain_consensus_as_non_initial_word,
                 &weights);
  }
  PrintTermCandidates(filename, "Domain Consensus Non-Initial Words", weights);
}

void CandidatesExtractor::ApplyDomainConsensusAsSubjects(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->domain_consensus_as_subject,
                 &weights);
  }
  PrintTermCandidates(filename, "Domain Consensus Subjects", weights);
}

void CandidatesExtractor::ApplyNearTermsFreq(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->near_terms_frequency,
                 &weights);
  }
  PrintTermCandidates(filename, "NearTermsFreq", weights);
}

void CandidatesExtractor::ApplyNearTermsFreqIDFReference(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = static_cast<double>(iterator->near_terms_frequency) *
        log(static_cast<double>(
            reference_collection_analyzer_.total_number_documents()) /
            static_cast<double>(
                max(reference_collection_analyzer_.GetDocumentFrequency(
                        iterator->term_candidate),
                    kMinimumFrequency_)));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "NearTermsFreq-IDF Reference", weights);
}

void CandidatesExtractor::ApplyBM25(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = iterator->term_frequency_bm25 *
        (static_cast<double>(
            term_candidates_analyzer_->total_number_documents() -
            iterator->document_frequency) + 0.5) /
        (static_cast<double>(iterator->document_frequency) + 0.5);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "BM-25", weights);
}

void CandidatesExtractor::ApplyTFIDF(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = static_cast<double>(iterator->term_frequency) *
        log(static_cast<double>(
                term_candidates_analyzer_->total_number_documents()) /
            static_cast<double>(iterator->document_frequency));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "TF-IDF", weights);
}

void CandidatesExtractor::ApplyTFIDFReference(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = static_cast<double>(iterator->term_frequency) *
        log(static_cast<double>(
            reference_collection_analyzer_.total_number_documents()) /
            static_cast<double>(
                max(reference_collection_analyzer_.GetDocumentFrequency(
                        iterator->term_candidate),
                    kMinimumFrequency_)));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "TF-IDF Reference", weights);
}

void CandidatesExtractor::ApplyTFRIDF(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = static_cast<double>(iterator->term_frequency) *
        (log(static_cast<double>(
                term_candidates_analyzer_->total_number_documents()) /
             static_cast<double>(iterator->document_frequency)) +
         log(1.0 -
             exp(-static_cast<double>(iterator->term_frequency) /
                 static_cast<double>(
                    term_candidates_analyzer_->total_number_documents()))));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "TF-RIDF", weights);
}

void CandidatesExtractor::ApplyTFIDFAsCapitalWords(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = 0.0;
    if (iterator->term_frequency_as_capital_word) {
      weight_value =
          static_cast<double>(iterator->term_frequency_as_capital_word) *
          log(static_cast<double>(
                  term_candidates_analyzer_->total_number_documents()) /
              static_cast<double>(
                  iterator->document_frequency_as_capital_word));
    }
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "TF-IDF Capital Words", weights);
}

void CandidatesExtractor::ApplyTFIDFReferenceAsCapitalWords(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = 0.0;
    if (iterator->term_frequency_as_capital_word) {
      weight_value =
          static_cast<double>(iterator->term_frequency_as_capital_word) *
          log(static_cast<double>(
                  reference_collection_analyzer_.total_number_documents()) /
              static_cast<double>(
                  max(reference_collection_analyzer_.GetDocumentFrequency(
                          iterator->term_candidate),
                      kMinimumFrequency_)));
    }
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "TF-IDF Reference Capital Words", weights);
}

void CandidatesExtractor::ApplyTFRIDFAsCapitalWords(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = kMinimalDoubleValue_;
    if (iterator->term_frequency_as_capital_word) {
      weight_value =
          static_cast<double>(iterator->term_frequency_as_capital_word) *
          (log(static_cast<double>(
                  term_candidates_analyzer_->total_number_documents()) /
               static_cast<double>(
                  iterator->document_frequency_as_capital_word)) +
           log(1.0 -
               exp(-static_cast<double>(
                       iterator->term_frequency_as_capital_word) /
                   static_cast<double>(
                       term_candidates_analyzer_->total_number_documents()))));
    }
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "TF-RIDF Capital Words", weights);
}

void CandidatesExtractor::ApplyTFIDFAsNonInitialWords(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = 0.0;
    if (iterator->term_frequency_as_non_initial_word) {
      weight_value =
          static_cast<double>(iterator->term_frequency_as_non_initial_word) *
          log(static_cast<double>(
                  term_candidates_analyzer_->total_number_documents()) /
              static_cast<double>(
                  iterator->document_frequency_as_non_initial_word));
    }
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "TF-IDF Non-Initial Words", weights);
}

void CandidatesExtractor::ApplyTFIDFReferenceAsNonInitialWords(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = 0.0;
    if (iterator->term_frequency_as_subject) {
      weight_value =
          static_cast<double>(iterator->term_frequency_as_non_initial_word) *
          log(static_cast<double>(
                  reference_collection_analyzer_.total_number_documents()) /
              static_cast<double>(
                  max(reference_collection_analyzer_.GetDocumentFrequency(
                          iterator->term_candidate),
                      kMinimumFrequency_)));
    }
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "TFIDF Reference Non-Initial Words", weights);
}

void CandidatesExtractor::ApplyTFRIDFAsNonInitialWords(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = kMinimalDoubleValue_;
    if (iterator->term_frequency_as_non_initial_word) {
      weight_value =
          static_cast<double>(iterator->term_frequency_as_non_initial_word) *
          (log(static_cast<double>(
                    term_candidates_analyzer_->total_number_documents()) /
               static_cast<double>(
                  iterator->document_frequency_as_non_initial_word)) +
           log(1.0 -
               exp(-static_cast<double>(
                       iterator->term_frequency_as_non_initial_word) /
                   static_cast<double>(
                       term_candidates_analyzer_->total_number_documents()))));
    }
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "TF-RIDF Non-Initial Words", weights);
}

void CandidatesExtractor::ApplyTFIDFAsSubjects(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = 0.0;
    if (iterator->term_frequency_as_subject) {
      weight_value = static_cast<double>(iterator->term_frequency_as_subject) *
          log(static_cast<double>(
                  term_candidates_analyzer_->total_number_documents()) /
              static_cast<double>(iterator->document_frequency_as_subject));
    }
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "TF-IDF Subjects", weights);
}

void CandidatesExtractor::ApplyTFIDFReferenceAsSubjects(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = 0.0;
    if (iterator->term_frequency_as_subject) {
      weight_value = static_cast<double>(iterator->term_frequency_as_subject) *
          log(static_cast<double>(
                  reference_collection_analyzer_.total_number_documents()) /
              static_cast<double>(
                  max(reference_collection_analyzer_.GetDocumentFrequency(
                          iterator->term_candidate),
                      kMinimumFrequency_)));
    }
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "TF-IDF Reference Subjects", weights);
}

void CandidatesExtractor::ApplyTFRIDFAsSubjects(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = kMinimalDoubleValue_;
    if (iterator->term_frequency_as_subject) {
      weight_value = static_cast<double>(iterator->term_frequency_as_subject) *
          (log(static_cast<double>(
                  term_candidates_analyzer_->total_number_documents()) /
               static_cast<double>(iterator->document_frequency_as_subject)) +
           log(1.0 -
               exp(-static_cast<double>(
                      iterator->term_frequency_as_subject) /
                   static_cast<double>(
                      term_candidates_analyzer_->total_number_documents()))));
    }
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "TF-RIDF Subjects", weights);
}

void CandidatesExtractor::ApplyWeirdness(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = CalculateWeirdnessForTerm(iterator->term_candidate,
                                                    iterator->term_frequency);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Weirdness", weights);
}

void CandidatesExtractor::ApplyRelevance(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = 1.0 - 1.0 / log(2.0 +
        static_cast<double>(iterator->term_frequency) *
        static_cast<double>(iterator->document_frequency) /
        static_cast<double>(
            max(reference_collection_analyzer_.GetTermFrequency(
                    iterator->term_candidate),
                kMinimumFrequency_)));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Relevance", weights);
}

void CandidatesExtractor::ApplyContrastiveWeight(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = log(static_cast<double>(iterator->term_frequency)) *
        log(static_cast<double>(term_candidates_analyzer_->total_number_words()
                + reference_collection_analyzer_.total_number_words()) /
            static_cast<double>(iterator->term_frequency +
                reference_collection_analyzer_.GetTermFrequency(
                    iterator->term_candidate)));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Contrastive Weight", weights);
}

void CandidatesExtractor::ApplyDiscriminativeWeight(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double domain_tendency =
        log10((static_cast<double>(iterator->term_frequency) + 1.0) /
              (static_cast<double>(
                  reference_collection_analyzer_.GetTermFrequency(
                      iterator->term_candidate) + 1.0)) + 1.0) / log10(2.0);
    double domain_prevalence =
        log10(static_cast<double>(iterator->term_frequency) + 10.0) *
        log10(static_cast<double>(
                  term_candidates_analyzer_->total_number_words() +
                  reference_collection_analyzer_.total_number_words()) /
              static_cast<double>(iterator->term_frequency +
                  reference_collection_analyzer_.GetTermFrequency(
                      iterator->term_candidate)) + 10.0);
    InsertWeight(iterator->term_candidate,
                 domain_tendency * domain_prevalence,
                 &weights);
  }
  PrintTermCandidates(filename, "Discriminative Weight", weights);
}

void CandidatesExtractor::ApplyKFIDF(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int number_of_domains_with_term = 1;
    if (reference_collection_analyzer_.GetTermFrequency(
            iterator->term_candidate) > 0) {
      number_of_domains_with_term = 2;
    }
    double weight_value = static_cast<double>(iterator->document_frequency) *
        log(2.0 / static_cast<double>(number_of_domains_with_term) + 1.0);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "KF-IDF", weights);
}

void CandidatesExtractor::ApplyLogLikelihood(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = 0.0;
    unsigned int reference_term_frequency =
        reference_collection_analyzer_.GetTermFrequency(
            iterator->term_candidate);
    if (static_cast<double>(iterator->term_frequency) *
        static_cast<double>(
            reference_collection_analyzer_.total_number_words()) >
        static_cast<double>(reference_term_frequency) *
        static_cast<double>(term_candidates_analyzer_->total_number_words())) {
      double expected_term_frequency =
          static_cast<double>(iterator->term_frequency +
                              reference_term_frequency) /
          static_cast<double>(term_candidates_analyzer_->total_number_words() +
              reference_collection_analyzer_.total_number_words());
      weight_value = 2.0 * (static_cast<double>(iterator->term_frequency) *
           log(static_cast<double>(iterator->term_frequency) /
               (static_cast<double>(
                    term_candidates_analyzer_->total_number_words()) *
                static_cast<double>(expected_term_frequency))) +
           log(static_cast<double>(max(reference_term_frequency,
                                       kMinimumFrequency_)) /
               (static_cast<double>(
                    reference_collection_analyzer_.total_number_words())
                * static_cast<double>(expected_term_frequency))));
    }
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "LogLikelihood", weights);
}

void CandidatesExtractor::ApplyProbabilisticTermFrequency(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->probabilistic_term_frequency,
                 &weights);
  }
  PrintTermCandidates(filename, "Probabilistic Term Frequency", weights);
}

void CandidatesExtractor::ApplyProbabilisticTFIDF(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value =
        iterator->probabilistic_term_frequency *
        log(static_cast<double>(
                term_candidates_analyzer_->total_number_documents()) /
            static_cast<double>(iterator->document_frequency));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Probabilistic TF-IDF", weights);
}

void CandidatesExtractor::ApplyProbabilisticMaximumTermFrequency(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->probabilistic_maximum_term_frequency,
                 &weights);
  }
  PrintTermCandidates(filename,
                      "Probabilistic Maximum Term Frequency",
                      weights);
}

void CandidatesExtractor::ApplyProbabilisticTermScore(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->probabilistic_term_score,
                 &weights);
  }
  PrintTermCandidates(filename, "Probabilistic Term Score", weights);
}

void CandidatesExtractor::ApplyProbabilisticTFIDFTermScore(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value =
        iterator->probabilistic_term_score *
        log(static_cast<double>(
                term_candidates_analyzer_->total_number_documents()) /
            static_cast<double>(iterator->document_frequency));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Probabilistic TF-IDF Term Score", weights);
}

void CandidatesExtractor::ApplyProbabilisticMaximumTermScore(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->probabilistic_maximum_term_score,
                 &weights);
  }
  PrintTermCandidates(filename, "Probabilistic Maximum Term Score", weights);
}

void CandidatesExtractor::ApplyTermContribution(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value =
        iterator->term_contribution *
        pow(log(static_cast<double>(
                    term_candidates_analyzer_->total_number_documents()) /
                static_cast<double>(iterator->document_frequency)),
            2.0);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Term Contribution", weights);
}

void CandidatesExtractor::ApplyTermVariance(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate, iterator->term_variance, &weights);
  }
  PrintTermCandidates(filename, "Term Variance", weights);
}

void CandidatesExtractor::ApplyTermVarianceQuality(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->term_candidate,
                 iterator->term_variance_quality,
                 &weights);
  }
  PrintTermCandidates(filename, "Term Variance Quality", weights);
}

void CandidatesExtractor::ApplyTermFrequencyLDA(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = lda_results_analyzer_.GetTermFrequency(
        iterator->term_candidate);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Term Frequency LDA", weights);
}

void CandidatesExtractor::ApplyTFIDFLDA(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = 0.0;
    double term_frequency_lda = lda_results_analyzer_.GetTermFrequency(
        iterator->term_candidate);
    if (term_frequency_lda > 0) {
        weight_value = term_frequency_lda *
        log(static_cast<double>(lda_results_analyzer_.total_number_topics()) /
            static_cast<double>(lda_results_analyzer_.GetTopicFrequency(
                                iterator->term_candidate)));
    }
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "TF-IDF LDA", weights);
}

void CandidatesExtractor::ApplyDomainConsensusLDA(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = lda_results_analyzer_.CalculateDomainConsensus(
        iterator->term_candidate);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Domain Consensus LDA", weights);
}

void CandidatesExtractor::ApplyMaximumTermFrequencyLDA(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = lda_results_analyzer_.GetMaximumTermFrequency(
        iterator->term_candidate);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Maximum Term Frequency LDA", weights);
}

void CandidatesExtractor::ApplyTermScoreLDA(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = lda_results_analyzer_.GetTermScore(
        iterator->term_candidate);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Term Score LDA", weights);
}

void CandidatesExtractor::ApplyTFIDFTermScoreLDA(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = 0.0;
    double term_score_lda = lda_results_analyzer_.GetTermScore(
        iterator->term_candidate);
    if (term_score_lda > 0) {
        weight_value = term_score_lda *
        log(static_cast<double>(lda_results_analyzer_.total_number_topics()) /
            static_cast<double>(lda_results_analyzer_.GetTopicFrequency(
                                iterator->term_candidate)));
    }
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "TF-IDF Term Score LDA", weights);
}

void CandidatesExtractor::ApplyMaximumTermScoreLDA(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = lda_results_analyzer_.GetMaximumTermScore(
        iterator->term_candidate);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Maximum Term Score LDA", weights);
}

void CandidatesExtractor::ApplyCValue(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = CalculateCValueForTerm(iterator->term_candidate,
                                                 iterator->term_frequency);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "C-Value", weights);
}

void CandidatesExtractor::ApplyMCValue(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    size_t term_length = count(iterator->term_candidate.begin(),
                               iterator->term_candidate.end(),
                               ' ') + 1;
    double weight_value = static_cast<double>(term_length) *
        log(iterator->term_frequency) + 3.5 *
        phrases_analyzer_.GetAmbientPhrasesNumber(iterator->term_candidate);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "MC-Value", weights);
}

void CandidatesExtractor::ApplyMNCValue(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value =
        0.8 * CalculateCValueForTerm(iterator->term_candidate,
                                     iterator->term_frequency) +
        0.2 * phrases_analyzer_.GetContextWordsFrequenciesSum(
            iterator->term_candidate);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "MNC-value", weights);
}

void CandidatesExtractor::ApplyInsideness(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = static_cast<double>(
        phrases_analyzer_.GetMaximumPhraseFrequency(iterator->term_candidate))
        / static_cast<double>(iterator->term_frequency);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Insideness", weights);
}

void CandidatesExtractor::ApplySumN(const string& filename,
                                    unsigned int parameter) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = phrases_analyzer_.GetAveragePhrasesFrequency(
        iterator->term_candidate,
        parameter);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Sum" + to_string(parameter), weights);
}

void CandidatesExtractor::ApplyTokenLR(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = CalculateTokenLRForTerm(iterator->term_candidate);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Token-LR", weights);
}

void CandidatesExtractor::ApplyTokenFLR(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = static_cast<double>(iterator->term_frequency) *
        CalculateTokenLRForTerm(iterator->term_candidate);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Token-FLR", weights);
}

void CandidatesExtractor::ApplyTypeLR(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = CalculateTypeLRForTerm(iterator->term_candidate);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Type-LR", weights);
}

void CandidatesExtractor::ApplyTypeFLR(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = static_cast<double>(iterator->term_frequency) *
        CalculateTypeLRForTerm(iterator->term_candidate);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Type-FLR", weights);
}

void CandidatesExtractor::PrintTermCandidates(
    const string& filename,
    const string& feature_name,
    const map<double, vector<string> >& term_candidates_map) {
  if (!term_candidates_map.empty()) {
    ofstream file(filename.data());
    if (!file) {
      throw runtime_error("Failed to open file for printing term candidates");
    }
    vector<bool> terms_candididates;
    unsigned int current_term_number = 0;
    unsigned int real_terms_number = 0;
    for (auto iterator = term_candidates_map.rbegin();
         iterator != term_candidates_map.rend();  ++iterator) {
      for (const auto& candidate: iterator->second) {
        file << candidate << "\t" << iterator->first << "\t";
        bool candidate_is_term = results_analyzer_->IsRealTerm(candidate);
        if (candidate_is_term) {
          file << "+\n";
          ++real_terms_number;
        } else {
          file << "-\n";
        }
        if (current_term_number < kAveragePrecisionThreshold_) {
          terms_candididates.push_back(candidate_is_term);
          ++current_term_number;
        }
      }
    }
    double average_precision = CalculateAveragePrecision(terms_candididates,
                                                         real_terms_number);
    file << "AvP for " << kAveragePrecisionThreshold_ << "terms: " <<
        average_precision << "\n";
    file.close();
    PrintAveragePrecision(feature_name, average_precision);
  }
}

double CandidatesExtractor::CalculateAveragePrecision(
    const vector<bool>& term_candidates,
    unsigned int real_terms_number) const {
  if (real_terms_number == 0) {
    return 1;
  }
  unsigned int threshold =
      min(static_cast<unsigned int>(term_candidates.size()),
          kAveragePrecisionThreshold_);
  real_terms_number = min(real_terms_number, kAveragePrecisionThreshold_);
  unsigned int current_real_terms_number = 0;
  double average_precision = 0;
  for (unsigned int index = 0; index < threshold; ++index) {
    if (term_candidates[index]) {
      ++current_real_terms_number;
      average_precision += static_cast<double>(current_real_terms_number) /
          static_cast<double>(index + 1);
    }
  }
  return average_precision / static_cast<double>(real_terms_number);
}

void CandidatesExtractor::PrintAveragePrecision(const std::string& feature_name,
                                                double average_precision) {
  boost::mutex::scoped_lock lock(average_precision_mutex_);
  ofstream file(kAveragePrecisionFilename_.data(), std::ios::app);
  if (!file) {
    throw runtime_error("Failed to open file for printing AvP");
  }
  file << "AvP for " << feature_name << "\t" << average_precision << "\n";
}
