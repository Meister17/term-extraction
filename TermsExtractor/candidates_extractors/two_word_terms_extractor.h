// Copyright 2012 Michael Nokel
#pragma once

#include "./candidates_extractor.h"
#include "../candidates_analyzers/single_words_analyzer.h"
#include <boost/algorithm/string.hpp>
#include <cmath>
#include <string>

class ResultsAnalyzer;

/**
  @brief Class for applying various features for two-word term candidates

  This class should be used for applying various features for previously
  extracted two-word term candidates, reordering term candidates and printing
  obtained results to the given directory
*/
class TwoWordTermsExtractor : public CandidatesExtractor {
 public:
  TwoWordTermsExtractor(const std::string& average_precision_filename,
                        unsigned int average_precision_threshold,
                        ResultsAnalyzer* results_analyzer);

  /**
    Parses file that is special for two-word term candidates
    @param filename file containing single words and their frequencies
    @throw std::runtime_error in case of any occurred error
  */
  void ParseSpecialFile(const std::string& filename) {
    single_words_analyzer_.ParseFile(filename);
  }

  /**
    Applies various features in a thread pool and prints results to the given
    directory and AvP to the given file
    @param destination_directory_name directory where results should be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ExtractTerms(const std::string& destination_directory_name);

 private:
  /**
    Applies Lexical Cohesion feature to the two-word term candidates
    @param filename file where term candidate list ordered by Lexical Cohesion
    should be printed
  */
  void ApplyLexicalCohesion(const std::string& filename);

  /**
    Calculates and returns Lexical Cohesion feature for the given term candidate
    @param term_candidate term candidate to calculate Lexical Cohesion for
    @param term_frequency term frequency of the term candidate
    @return calculated Lexical Cohesion
  */
  double CalculateLexicalCohesionForTerm(const std::string& term_candidate,
                                         unsigned int term_frequency) const {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(term_candidate, first_frequency, second_frequency);
    return static_cast<double>(2.0 * term_frequency) *
        log10(static_cast<double>(term_frequency)) /
        static_cast<double>(first_frequency + second_frequency);
  }

  /**
    Applies TermExtractor feature to the two-word term candidates
    @param filename file where term candidate list ordered by TermExtractor
    should be printed
  */
  void ApplyTermExtractor(const std::string& filename);

  /**
    Applies Reference Weight feature to the two-word term candidates
    @param filename file where term candidate list ordered by Reference Weight
    should be printed
  */
  void ApplyReferenceWeight(const std::string& filename);

  /**
    Applies NC-value feature to two-word term candidates
    @param filename file where term candidate list ordered by NC-value should be
    printed
  */
  void ApplyNCValue(const std::string& filename);

  /**
    Applies Modified Gravity Count feature to the two-word term candidates
    @param filename file where term candidate list ordered by Modified Gravity
    Count should be printed
  */
  void ApplyModifiedGravityCount(const std::string& filename);

  /**
    Applies Gravity Count feature to the two-word term candidates
    @param filename file where term candidate list ordered by Gravity Count
    should be printed
  */
  void ApplyGravityCount(const std::string& filename);

  /**
    Calculates Mutual Information for the given term
    @param whole_term_frequency term frequency of the whole term candidate
    @param first_term_frequency term frequency of the first word in candidate
    @param second_term_frequency term frequency of the second word in candidate
    @return calculated Mutual Information
  */
  double CalculateMutualInformation(unsigned int whole_term_frequency,
                                    unsigned int first_term_frequency,
                                    unsigned int second_term_frequency) const {
    return log(
        static_cast<double>(single_words_analyzer_.total_number_words()) *
        static_cast<double>(whole_term_frequency) /
        (static_cast<double>(first_term_frequency) *
         static_cast<double>(second_term_frequency)));
  }

  /**
    Applies Mutual Information to the two-word term candidates
    @param filename file where term candidate list ordered by Mutual Information
    should be printed
  */
  void ApplyMutualInformation(const std::string& filename);

  /**
    Applies Augmented Mutual Information to the two-word term candidates
    @param filename file where term candidate list ordered by Augmented Mutual
    Information should be printed
  */
  void ApplyAugmentedMutualInformation(const std::string& filename);

  /**
    Applies Normalized Pointwise Mutual Information to the two-word term
    candidates
    @param filename file where term candidate list ordered by Normalized
    Pointwise Mutual Information should be printed
  */
  void ApplyNormalizedPointwiseMutualInformation(const std::string& filename);

  /**
    Applies Cubic Mutual Information to the two-word term candidates
    @param filename file where term candidate list ordered by Cubic Mutual
    Information should be printed
  */
  void ApplyCubicMutualInformation(const std::string& filename);

  /**
    Applies True Mutual Information to the two-word term candidates
    @param filename file where term candidate list ordered by True Mutual
    Information should be printed
  */
  void ApplyTrueMutualInformation(const std::string& filename);

  /**
    Applies Symmetrical Conditional Probability to the two-word term candidates
    @param filename file where term candidate list ordered by Symmetrical
    Conditional Probability should be printed
  */
  void ApplySymmetricalConditionalProbability(const std::string& filename);

  /**
    Applies Z-Score to the two-word term candidates
    @param filename name of file where term candidate list ordered by Z-Score
    should be printed
  */
  void ApplyZScore(const std::string& filename);

  /**
    Calculates T-Score for the given two-word term candidate
    @param term_candidate term candidate to calculate T-Score for
    @param term_frequency term frequency of the given term candidate
    @return calculated T-Score
  */
  double CalculateTScoreForTerm(const std::string& term_candidate,
                                unsigned int term_frequency) const {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(term_candidate, first_frequency, second_frequency);
    return (static_cast<double>(term_frequency) -
        static_cast<double>(first_frequency) *
        static_cast<double>(second_frequency) /
        static_cast<double>(single_words_analyzer_.total_number_words())) /
        sqrt(static_cast<double>(term_frequency));
  }

  /**
    Applies T-Score to the two-word term candidates
    @param filename file where term candidate list ordered by T-Score should be
    printed
  */
  void ApplyTScore(const std::string& filename);


  /**
    Calculates and returns TC-Value for the given two-word term candidate
    @param term_candidate term candidate to calculate TC-Value for
    @param term_frequency term frequency of the given term candidate
    @return calculated TC-Value
  */
  double CalculateTCValueForTerm(const std::string& term_candidate,
                                 unsigned int term_frequency) const;

  /**
    Applies TC-Value to the two-word term candidates
    @param filename file where term candidate list ordered by TC-Value should be
    printed
  */
  void ApplyTCValue(const std::string& filename);

  /**
    Applies NTC-Value to the two-word term candidates
    @param filename file where term candidate list ordered by NTC-Value should
    be printed
  */
  void ApplyNTCValue(const std::string& filename);

  /**
    Applies Dice Coefficient to the two-word term candidates
    @param filename file where term candidate list ordered by Dice Coefficient
    should be printed
  */
  void ApplyDiceCoefficient(const std::string& filename);

  /**
    Applies Modified Dice Coefficient to the two-word term candidates
    @param filename file where term candidate list ordered by Modified Dice
    Coefficient should be printed
  */
  void ApplyModifiedDiceCoefficient(const std::string& filename);

  /**
    Applies Simple Matching Coefficient to the two-word term candidates
    @param filename file where term candidate list ordered by Simple Matching
    Coefficient should be printed
  */
  void ApplySimpleMatchingCoefficient(const std::string& filename);

  /**
    Applies Kulczinsky Coefficient to the two-word term candidates
    @param filename file where term candidate list ordered by Kulczinsky
    Coefficient should be printed
  */
  void ApplyKulczinksyCoefficient(const std::string& filename);

  /**
    Applies Ochiai Coefficient to the two-word term candidates
    @param filename file where term candidate list ordered by Ochiai Coefficient
    should be printed
  */
  void ApplyOchiaiCoefficient(const std::string& filename);

  /**
    Applies Yule Coefficient to the two-word term candidates
    @param filename file where term candidate list ordered by Yule Coefficient
    should be printed
  */
  void ApplyYuleCoefficient(const std::string& filename);

  /**
    Applies Jaccard Coefficient to the two-word term candidates
    @param filename file where term candidate list ordered by Jaccard
    Coefficient should be printed
  */
  void ApplyJaccardCoefficient(const std::string& filename);

  /**
    Applies Chi Square to the two-word term candidates
    @param filename file where term candidate list ordered by Chi Square should
    be printed
  */
  void ApplyChiSquare(const std::string& filename);

  /**
    Calculates part of LogLikelihoodRatio
    @param whole_term_frequency term frequency of the two-word term candidate
    @param first_word_frequency frequency of the first word forming two-word
    term candidate
    @param second_word_frequency frequency of the second word forming two-word
    term candidate
    @return calculated part of LoglikelihoodRatio
  */
  double CalculateLogLikelihoodRatioPart(unsigned int whole_term_frequency,
                                         unsigned int first_word_frequency,
                                         unsigned int second_word_frequency)
                                         const {
    return static_cast<double>(whole_term_frequency) *
        log(static_cast<double>(single_words_analyzer_.total_number_words()) *
            static_cast<double>(whole_term_frequency) /
            (static_cast<double>(first_word_frequency) *
             static_cast<double>(second_word_frequency)));
  }

  /**
    Applies Log-Likelihood Ratio to the two-word term candidates
    @param filename file where term candidate list ordered by Log-Likelihood
    Ratio should be printed
  */
  void ApplyLogLikelihoodRatio(const std::string& filename);

  /**
    Returns single words forming the given two-word term candidate
    @param term_candidate given two-word term candidate
    @param[out] first_frequency frequency of the first word forming given
    two-word term candidate
    @param[out] second_frequency frequency of the second word forming given
    two-word term candidate
  */
  void GetSingleFrequencies(const std::string& term_candidate,
                            unsigned int& first_frequency,
                            unsigned int& second_frequency) const {
    std::vector<std::string> words = GetSingleWords(term_candidate);
    first_frequency = single_words_analyzer_.GetWordFrequency(words.front());
    second_frequency = single_words_analyzer_.GetWordFrequency(words.back());
  }

  /**
    Returns single words that forms the given term candidate
    @param term_candidate given two-word term candidate
    @return vector containing words that forms the given term candidate
  */
  std::vector<std::string> GetSingleWords(const std::string& term_candidate)
      const {
    std::vector<std::string> words;
    boost::split(words, term_candidate, boost::is_any_of(" "));
    return words;
  }

  /** Object for working with single word terms */
  SingleWordsAnalyzer single_words_analyzer_;
};
