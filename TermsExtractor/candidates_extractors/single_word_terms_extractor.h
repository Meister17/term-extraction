// Copyright 2011 Michael Nokel
#pragma once

#include "./candidates_extractor.h"
#include <string>

class ResultsAnalyzer;

/**
  @brief Class for applying various features for single-word term candidates

  This class should be used for applying various features for previously
  extracted single-word term candidates, reordering term candidates and printing
  obtained results to the given directory
*/
class SingleWordTermsExtractor : public CandidatesExtractor {
 public:
  SingleWordTermsExtractor(
      const std::string& average_precision_filename,
      unsigned int average_precision_threshold,
      ResultsAnalyzer* results_analyzer);

  /**
    Applies various features in a thread pool and prints results to the given
    directory and AvP to the given file
    @param destination_directory_name directory where results should be printed
    @return true if applying features and printing was successful
  */
  void ExtractTerms(const std::string& destination_directory_name);

 private:
  /**
    Applies Reference Weight feature to the term candidates
    @param filename file where term candidate list ordered by Reference Weight
    should be printed
  */
  void ApplyReferenceWeight(const std::string& filename);

  /**
    Applies NC-value feature to the single-word term candidates
    @param filename file where term candidate list ordered by NC-value should be
    printed
  */
  void ApplyNCValue(const std::string& filename);

  /**
    Applies Modified Gravity Count to the single-word term candidates
    @param filename file where term candidate list ordered by Modified Gravity
    Count should be printed
  */
  void ApplyModifiedGravityCount(const std::string& filename);
};
