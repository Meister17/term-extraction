// Copyright 2012 Michael Nokel
#include "./two_word_terms_extractor.h"
#include "../candidates_analyzers/two_word_terms_analyzer.h"
#include "../results_analyzer.h"
#include <boost/threadpool.hpp>
#include <algorithm>
#include <cmath>
#include <map>
#include <string>
#include <vector>
#include <utility>

using boost::threadpool::pool;
using std::make_pair;
using std::map;
using std::max;
using std::min;
using std::pair;
using std::string;
using std::vector;

TwoWordTermsExtractor::TwoWordTermsExtractor(
    const string& average_precision_filename,
    unsigned int average_precision_threshold,
    ResultsAnalyzer* results_analyzer)
    : CandidatesExtractor("two-word",
                          average_precision_filename,
                          average_precision_threshold,
                          results_analyzer) {
}

void TwoWordTermsExtractor::ExtractTerms(
    const string& destination_directory_name) {
  ApplyCommonFeatures(destination_directory_name);
  pool thread_pool(kNumberOfThreads_);
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplyLexicalCohesion,
      this,
      destination_directory_name + "lexical_cohesion.txt"));
  thread_pool.schedule(boost::bind(&TwoWordTermsExtractor::ApplyTermExtractor,
                       this,
                       destination_directory_name + "term_extractor.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplyGravityCount,
      this,
      destination_directory_name + "gravity_count.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplyMutualInformation,
      this,
      destination_directory_name + "mutual_information.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplyAugmentedMutualInformation,
      this,
      destination_directory_name + "augmented_mutual_information.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplyNormalizedPointwiseMutualInformation,
      this,
      destination_directory_name + "normalized_pmi.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplyCubicMutualInformation,
      this,
      destination_directory_name + "cubic_mutual_information.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplyTrueMutualInformation,
      this,
      destination_directory_name + "true_mutual_information.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplySymmetricalConditionalProbability,
      this,
      destination_directory_name + "symmetrical_conditional_probability.txt"));
  thread_pool.schedule(boost::bind(&TwoWordTermsExtractor::ApplyZScore,
                                   this,
                                   destination_directory_name + "z_score.txt"));
  thread_pool.schedule(boost::bind(&TwoWordTermsExtractor::ApplyTScore,
                                   this,
                                   destination_directory_name + "t_score.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplyTCValue,
      this,
      destination_directory_name + "tc_value.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplyNTCValue,
      this,
      destination_directory_name + "ntc_value.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplyDiceCoefficient,
      this,
      destination_directory_name + "dice_coefficient.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplyModifiedDiceCoefficient,
      this,
      destination_directory_name + "modified_dice_coefficient.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplySimpleMatchingCoefficient,
      this,
      destination_directory_name + "simple_matching_coefficient.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplyKulczinksyCoefficient,
      this,
      destination_directory_name + "kulczinksy_coefficient.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplyOchiaiCoefficient,
      this,
      destination_directory_name + "ochiai_coefficient.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplyYuleCoefficient,
      this,
      destination_directory_name + "yule_coefficient.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplyJaccardCoefficient,
      this,
      destination_directory_name + "jaccard_coefficient.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplyChiSquare,
      this,
      destination_directory_name + "chi_square.txt"));
  thread_pool.schedule(boost::bind(
      &TwoWordTermsExtractor::ApplyLogLikelihoodRatio,
      this,
      destination_directory_name + "log_likelihood_ratio.txt"));
}

void TwoWordTermsExtractor::ApplyLexicalCohesion(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = CalculateLexicalCohesionForTerm(
        iterator->term_candidate,
        iterator->term_frequency);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Lexical Cohesion", weights);
}

void TwoWordTermsExtractor::ApplyTermExtractor(const string& filename) {
  map<double,vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = 1.0 / 3.0 *
        (CalculateLexicalCohesionForTerm(iterator->term_candidate,
                                         iterator->term_frequency) +
         iterator->domain_consensus +
         CalculateWeirdnessForTerm(iterator->term_candidate,
                                   iterator->term_frequency));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "TermExtractor", weights);
}

void TwoWordTermsExtractor::ApplyReferenceWeight(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    vector<string> words = GetSingleWords(iterator->term_candidate);
    double weight_value = static_cast<double>(iterator->term_frequency) *
        static_cast<double>(iterator->term_frequency) / 2 *
        (log(static_cast<double>(first_frequency) / static_cast<double>(
               term_candidates_analyzer_->total_number_words())) -
         log(static_cast<double>(max(
                reference_collection_analyzer_.GetTermFrequency(words.front()),
                kMinimumFrequency_)) / static_cast<double>(
                    reference_collection_analyzer_.total_number_words())) +
         log(static_cast<double>(second_frequency) / static_cast<double>(
             term_candidates_analyzer_->total_number_words())) -
         log(static_cast<double>(max(
                reference_collection_analyzer_.GetTermFrequency(words.back()),
                kMinimumFrequency_)) / static_cast<double>(
                    reference_collection_analyzer_.total_number_words())));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Reference Weight", weights);
}

void TwoWordTermsExtractor::ApplyNCValue(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    vector<string> context_words = phrases_analyzer_.GetContextWords(
        iterator->term_candidate);
    double cweight = 1.0;
    for (size_t index = 0; index < context_words.size(); ++index) {
      unsigned int context_word_frequency =
          single_words_analyzer_.GetWordFrequency(context_words[index]);
      if (context_word_frequency) {
        cweight += 0.5 *
            (static_cast<double>(phrases_analyzer_.GetContextWordsNumber(
                context_words[index])) / static_cast<double>(
                    term_candidates_analyzer_->total_number_words())
           + static_cast<double>(
                phrases_analyzer_.GetContextWordsFrequenciesSum(
                    context_words[index])) /
             static_cast<double>(context_word_frequency));
      }
    }
    double weight_value = CalculateCValueForTerm(iterator->term_candidate,
                                                 iterator->term_frequency) *
        cweight /
        static_cast<double>(term_candidates_analyzer_->total_number_words());
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "NC-value", weights);
}

void TwoWordTermsExtractor::ApplyModifiedGravityCount(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    vector<string> words = GetSingleWords(iterator->term_candidate);
    double weight_value = log(static_cast<double>(iterator->term_frequency) *
        static_cast<double>(
            max(phrases_analyzer_.GetPreOccurrencesNumber(words.front()),
                kMinimumFrequency_)) / static_cast<double>(first_frequency)) +
        log(static_cast<double>(iterator->term_frequency) *
            static_cast<double>(max(
                phrases_analyzer_.GetPostOccurrencesNumber(words.back()),
                kMinimumFrequency_)) / static_cast<double>(second_frequency));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Modified Gravity Count", weights);
}

void TwoWordTermsExtractor::ApplyGravityCount(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    vector<string> words = GetSingleWords(iterator->term_candidate);
    double weight_value = log(static_cast<double>(iterator->term_frequency) *
        static_cast<double>(
            max(phrases_analyzer_.GetPostOccurrencesNumber(words.front()),
                kMinimumFrequency_)) / static_cast<double>(first_frequency)) +
        log(static_cast<double>(iterator->term_frequency) *
            static_cast<double>(max(
                phrases_analyzer_.GetPreOccurrencesNumber(words.back()),
                kMinimumFrequency_)) / static_cast<double>(second_frequency));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Gravity Count", weights);
}

void TwoWordTermsExtractor::ApplyMutualInformation(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    double weight_value = CalculateMutualInformation(iterator->term_frequency,
                                                     first_frequency,
                                                     second_frequency);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Mutual Information", weights);
}

void TwoWordTermsExtractor::ApplyAugmentedMutualInformation(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    unsigned int first_multiplier = first_frequency - iterator->term_frequency;
    unsigned int second_multiplier =
        second_frequency - iterator->term_frequency;
    if (second_multiplier == 0) {
      second_multiplier = 1;
    }
    if (first_multiplier == 0) {
      first_multiplier = 1;
    }
    double weight_value = CalculateMutualInformation(iterator->term_frequency,
                                                     first_multiplier,
                                                     second_multiplier);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Augmented Mutual Information", weights);
}

void TwoWordTermsExtractor::ApplyNormalizedPointwiseMutualInformation(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    double weight_value = CalculateMutualInformation(iterator->term_frequency,
                                                     first_frequency,
                                                     second_frequency) /
        (log(static_cast<double>(single_words_analyzer_.total_number_words()) /
             static_cast<double>(iterator->term_frequency)));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename,
                      "Normalized Pointwise Mutual Information",
                      weights);
}

void TwoWordTermsExtractor::ApplyCubicMutualInformation(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    double weight_value = CalculateMutualInformation(iterator->term_frequency,
                                                     first_frequency,
                                                     second_frequency) +
        log(static_cast<double>(iterator->term_frequency) /
            static_cast<double>(single_words_analyzer_.total_number_words())) +
        log(static_cast<double>(iterator->term_frequency) /
            static_cast<double>(single_words_analyzer_.total_number_words()));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Cubic Mutual Information", weights);
}

void TwoWordTermsExtractor::ApplyTrueMutualInformation(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    double weight_value = CalculateMutualInformation(iterator->term_frequency,
                                                     first_frequency,
                                                     second_frequency) *
        static_cast<double>(iterator->term_frequency);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "True Mutual Information", weights);
}

void TwoWordTermsExtractor::ApplySymmetricalConditionalProbability(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    double weight_value =
        static_cast<double>(iterator->term_frequency) *
        static_cast<double>(iterator->term_frequency) /
        (static_cast<double>(first_frequency) *
         static_cast<double>(second_frequency));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Symmetrical Conditional Probability", weights);
}

void TwoWordTermsExtractor::ApplyZScore(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    double weight_value = (static_cast<double>(iterator->term_frequency) -
        static_cast<double>(first_frequency) *
        static_cast<double>(second_frequency) /
        static_cast<double>(single_words_analyzer_.total_number_words())) /
        sqrt(static_cast<double>(first_frequency) *
             static_cast<double>(second_frequency));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Z-Score", weights);
}

void TwoWordTermsExtractor::ApplyTScore(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = CalculateTScoreForTerm(iterator->term_candidate,
                                                 iterator->term_frequency);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "T-Score", weights);
}

double TwoWordTermsExtractor::CalculateTCValueForTerm(
    const string& term_candidate,
    unsigned int term_frequency) const {
  double t_score = CalculateTScoreForTerm(term_candidate, term_frequency);
  double weight_value = static_cast<double>(term_frequency);
    if (t_score >= 0) {
      weight_value *= log(2.0 + t_score);
    }
    unsigned int ambient_phrases_number =
        phrases_analyzer_.GetAmbientPhrasesNumber(term_candidate);
    if (ambient_phrases_number) {
       weight_value -= phrases_analyzer_.GetAveragePhrasesFrequency(
          term_candidate,
          ambient_phrases_number);
    }
    return weight_value;
}

void TwoWordTermsExtractor::ApplyTCValue(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = CalculateTCValueForTerm(iterator->term_candidate,
                                                  iterator->term_frequency);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "TC-Value", weights);
}

void TwoWordTermsExtractor::ApplyNTCValue(const string& filename) {
  map<double, vector<string> > weights;
  for (auto  iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = 0.8 * CalculateTCValueForTerm(
        iterator->term_candidate,
        iterator->term_frequency) +
        0.2 * phrases_analyzer_.GetContextWordsFrequenciesSum(
            iterator->term_candidate);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "NTC-Value", weights);
}

void TwoWordTermsExtractor::ApplyDiceCoefficient(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    double weight_value = 2.0 * static_cast<double>(iterator->term_frequency) /
        (static_cast<double>(first_frequency) +
         static_cast<double>(second_frequency));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Dice Coefficient", weights);
}

void TwoWordTermsExtractor::ApplyModifiedDiceCoefficient(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    double weight_value = log(static_cast<double>(iterator->term_frequency) *
        2.0 * static_cast<double>(iterator->term_frequency) /
        (static_cast<double>(first_frequency) +
         static_cast<double>(second_frequency)));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Modified Dice Coefficient", weights);
}

void TwoWordTermsExtractor::ApplySimpleMatchingCoefficient(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    double weight_value = (static_cast<double>(iterator->term_frequency) +
        static_cast<double>(single_words_analyzer_.total_number_words() +
            iterator->term_frequency - first_frequency - second_frequency)) /
        static_cast<double>(single_words_analyzer_.total_number_words());
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Simple Matching Coefficient", weights);
}

void TwoWordTermsExtractor::ApplyKulczinksyCoefficient(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    double weight_value = 0.5 *
        (static_cast<double>(iterator->term_frequency) /
         static_cast<double>(first_frequency) +
         static_cast<double>(iterator->term_frequency) /
         static_cast<double>(second_frequency));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Kulczinsky Coefficient", weights);
}

void TwoWordTermsExtractor::ApplyOchiaiCoefficient(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    double weight_value = static_cast<double>(iterator->term_frequency) /
        sqrt(static_cast<double>(first_frequency) *
             static_cast<double>(second_frequency));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Ochiai Coefficient", weights);
}

void TwoWordTermsExtractor::ApplyYuleCoefficient(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    double weight_value = (static_cast<double>(iterator->term_frequency) *
        static_cast<double>(single_words_analyzer_.total_number_words() +
            iterator->term_frequency - first_frequency - second_frequency) -
        static_cast<double>(first_frequency - iterator->term_frequency) *
        static_cast<double>(second_frequency - iterator->term_frequency)) /
        (static_cast<double>(iterator->term_frequency) *
         static_cast<double>(single_words_analyzer_.total_number_words() +
            iterator->term_frequency - first_frequency - second_frequency) +
         static_cast<double>(first_frequency - iterator->term_frequency) *
         static_cast<double>(second_frequency - iterator->term_frequency));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Yule Coefficient", weights);
}

void TwoWordTermsExtractor::ApplyJaccardCoefficient(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    double weight_value = static_cast<double>(iterator->term_frequency) /
        static_cast<double>(first_frequency + second_frequency -
                            iterator->term_frequency);
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Jaccard Coefficient", weights);
}

void TwoWordTermsExtractor::ApplyChiSquare(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    double weight_value = static_cast<double>(iterator->term_frequency) -
        static_cast<double>(first_frequency) *
        static_cast<double>(second_frequency) /
        static_cast<double>(single_words_analyzer_.total_number_words());
    weight_value *= weight_value / (static_cast<double>(first_frequency) *
        static_cast<double>(second_frequency));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Chi Square", weights);
}

void TwoWordTermsExtractor::ApplyLogLikelihoodRatio(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    unsigned int first_frequency, second_frequency;
    GetSingleFrequencies(iterator->term_candidate,
                         first_frequency,
                         second_frequency);
    double weight_value = 2.0 * (
        CalculateLogLikelihoodRatioPart(iterator->term_frequency,
                                        first_frequency,
                                        second_frequency) +
        CalculateLogLikelihoodRatioPart(
            first_frequency - iterator->term_frequency,
            first_frequency,
            single_words_analyzer_.total_number_words() - second_frequency) +
        CalculateLogLikelihoodRatioPart(
            second_frequency - iterator->term_frequency,
            single_words_analyzer_.total_number_words() - first_frequency,
            second_frequency) +
        CalculateLogLikelihoodRatioPart(
            single_words_analyzer_.total_number_words() +
            iterator->term_frequency - first_frequency - second_frequency,
            single_words_analyzer_.total_number_words() - first_frequency,
            single_words_analyzer_.total_number_words() - second_frequency));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "LogLikelihood Ratio", weights);
}
