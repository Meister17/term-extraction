// Copyright 2011 Michael Nokel
#include "./single_word_terms_extractor.h"
#include "../candidates_analyzers/single_word_terms_analyzer.h"
#include "../results_analyzer.h"
#include <boost/threadpool.hpp>
#include <algorithm>
#include <cmath>
#include <map>
#include <string>
#include <vector>

using boost::threadpool::pool;
using std::map;
using std::max;
using std::string;
using std::vector;

SingleWordTermsExtractor::SingleWordTermsExtractor(
    const string& average_precision_filename,
    unsigned int average_precision_threshold,
    ResultsAnalyzer* results_analyzer)
    : CandidatesExtractor("single-word",
                          average_precision_filename,
                          average_precision_threshold,
                          results_analyzer) {
}

void SingleWordTermsExtractor::ExtractTerms(
    const string& destination_directory_name) {
  ApplyCommonFeatures(destination_directory_name);
}

void SingleWordTermsExtractor::ApplyReferenceWeight(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value = static_cast<double>(iterator->term_frequency) *
        static_cast<double>(iterator->term_frequency) *
        (log(static_cast<double>(iterator->term_frequency) /
             static_cast<double>(
                term_candidates_analyzer_->total_number_words())) -
         log(static_cast<double>(max(
                reference_collection_analyzer_.GetTermFrequency(
                    iterator->term_candidate),
                kMinimumFrequency_)) / static_cast<double>(
                    reference_collection_analyzer_.total_number_words())));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Reference Weight", weights);
}

void SingleWordTermsExtractor::ApplyNCValue(const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    vector<string> context_words = phrases_analyzer_.GetContextWords(
        iterator->term_candidate);
    double cweight = 1.0;
    for (size_t index = 0; index < context_words.size(); ++index) {
      unsigned int context_word_frequency =
          static_cast<SingleWordTermsAnalyzer*>(
              term_candidates_analyzer_.get())->GetWordFrequency(
                  context_words[index]);
      if (context_word_frequency) {
        cweight += 0.5 *
            (static_cast<double>(phrases_analyzer_.GetContextWordsNumber(
                context_words[index])) / static_cast<double>(
                    term_candidates_analyzer_->total_number_words())
           + static_cast<double>(
                phrases_analyzer_.GetContextWordsFrequenciesSum(
                    context_words[index])) /
             static_cast<double>(context_word_frequency));
      }
    }
    double weight_value = CalculateCValueForTerm(iterator->term_candidate,
                                                 iterator->term_frequency) *
        cweight /
        static_cast<double>(term_candidates_analyzer_->total_number_words());
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "NC-value", weights);
}

void SingleWordTermsExtractor::ApplyModifiedGravityCount(
    const string& filename) {
  map<double, vector<string> > weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value =
        log(static_cast<double>(max(phrases_analyzer_.GetPreOccurrencesNumber(
                                        iterator->term_candidate),
                                    kMinimumFrequency_))) +
        log(static_cast<double>(max(phrases_analyzer_.GetPostOccurrencesNumber(
                                        iterator->term_candidate),
                                    kMinimumFrequency_)));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Modified Gravity Count", weights);
}
