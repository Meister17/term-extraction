// Copyright 2012 Michael Nokel
#pragma once

#include "../candidates_analyzers/candidates_analyzer.h"
#include "../candidates_analyzers/lda_results_analyzer.h"
#include "../candidates_analyzers/phrases_analyzer.h"
#include "../candidates_analyzers/reference_collection_analyzer.h"
#include <boost/thread/mutex.hpp>
#include <boost/thread.hpp>
#include <algorithm>
#include <fstream>
#include <map>
#include <memory>
#include <string>
#include <vector>

class ResultsAnalyzer;

/**
  @brief Abstract class for applying various features to previously extracted
  term candidates and reordering them

  This class should be derived by all classes that are intended to apply
  features to term candidates and reorder them.
*/
class CandidatesExtractor {
 public:
  /**
    Initializes object
    @param term_candidates_type particular type of term candidates under
    consideration
    @param average_precision_filename file where Average Precisions obtained for
    various features will be written
    @param average_precision_threshold threshold for calculating Average
    Precision
    @param[out] results_analyzer pointer to the object used for determining
    real terms among term candidates
  */
  CandidatesExtractor(
      const std::string& term_candidates_type,
      const std::string& average_precision_filename,
      unsigned int average_precision_threshold,
      ResultsAnalyzer* results_analyzer);

  /**
    Parses file with previously extracted term candidates and some data about
    them and file with previously extracted term candidates and their
    frequencies from the reference collection
    @param term_candidates_filename file containing previously extracted term
    candidates and some data about them
    @param lda_results_filename file containing LDA or clustering for term
    candidates
    @param reference_collection_filename file containing previously extracted
    term candidates and their frequencies from the reference collection
    @param phrases_filename file containing previously extracted noun phrases
    @param terms_number number of terms to be taken into account
    @throw std::runtime_error in case of any error
  */
  virtual void ParseFiles(const std::string& term_candidates_filename,
                          const std::string& lda_results_filename,
                          const std::string& reference_collection_filename,
                          const std::string& phrases_filename,
                          unsigned int terms_number);

  /**
    Applies various features and prints results to the given destination
    directory and AvP to the specified file
    @param destination_directory_name directory where results should be printed
    @throw std::runtime_error in case of any error
  */
  virtual void ExtractTerms(const std::string& destination_directory_name) = 0;

  /**
    Virtual destructor for the purposes of correct deriving
  */
  virtual ~CandidatesExtractor() {
  }

 protected:
  /**
    Applies common features to the term candidates in a thread pool
    @param destination_directory_name destination directory for printing results
    @throw std::runtime_error in case of any error
  */
  virtual void ApplyCommonFeatures(
      const std::string& destination_directory_name);

  /**
    Applies feature TermLength to the term candidates
    @param filename file where term candidate list ordered by TermLength should
    be printed
  */
  virtual void ApplyTermLength(const std::string& filename);

  /**
    Applies feature Nouns to the term candidates
    @param filename file where term candidate list ordered by Nouns should be
    printed
  */
  virtual void ApplyNouns(const std::string& filename);

  /**
    Applies feature Adjectives to the term candidates
    @param filename file where term candidate list ordered by Adjectives should
    be printed
  */
  virtual void ApplyAdjectives(const std::string& filename);

  /**
    Applies feature Novelty to the term candidates
    @param filename file where term candidate list ordered by Novelty should be
    printed
  */
  virtual void ApplyNovelty(const std::string& filename);

  /**
    Applies feature Ambiguity to the term candidates
    @param filename file where term candidate list ordered by Ambiguity should be printed
  */
  virtual void ApplyAmbiguity(const std::string& filename);

  /**
    Applies feature Specificity to the single-word term candidate
    @param filename file where term candidate list ordered by Specificity should
    be printed
  */
  virtual void ApplySpecificity(const std::string& filename);

  /**
    Applies average position of the first occurrence to the term candidates
    @param filename file where term candidate list ordered by average position
    of the first occurrence should be printed
  */
  virtual void ApplyFirstOccurrence(const std::string& filename);

  /**
    Applies Term Frequency feature to the term candidates
    @param filename file where term candidate list ordered by Term Frequency
    should be printed
  */
  virtual void ApplyTermFrequency(const std::string& filename);

  /**
    Applies Term Frequency feature only to capital words among term candidates
    @param filename file where term candidate list ordered by Term Frequency
    Capital Words should be printed
  */
  virtual void ApplyTermFrequencyAsCapitalWords(const std::string& filename);

  /**
    Applies Term Frequency feature only to non-initial words among term
    candidates
    @param filename file where term candidate list ordered by Term Frequency
    Non-Initial Words should be printed
  */
  virtual void ApplyTermFrequencyAsNonInitialWords(const std::string& filename);

  /**
    Applies Term Frequency feature only to subjects among term candidates
    @param filename file where term candidate list ordered by Term Frequency
    Subjects should be printed
  */
  virtual void ApplyTermFrequencyAsSubjects(const std::string& filename);

  /**
    Applies Document Frequency feature to the term candidates
    @param filename file where term candidate list ordered by Document Frequency
    should be printed
  */
  virtual void ApplyDocumentFrequency(const std::string& filename);

  /**
    Applies Document Frequency feature only to capital words among term
    candidates
    @param filename file where term candidate list ordered by Document Frequency
    Capital Words should be printed
  */
  virtual void ApplyDocumentFrequencyAsCapitalWords(
      const std::string& filename);

  /**
    Applies Document Frequency feature only to non-initial words among term
    candidates
    @param filename file where term candidate list ordered by Document Frequency
    Non-Initial Words should be printed
  */
  virtual void ApplyDocumentFrequencyAsNonInitialWords(
      const std::string& filename);

  /**
    Applies Document Frequency feature only to subjects among term candidates
    @param filename file where term candidate list ordered by Document Frequency
    Subjects should be printed
  */
  virtual void ApplyDocumentFrequencyAsSubjects(const std::string& filename);

  /**
    Applies Domain Consensus feature to the term candidates
    @param filename file where term candidate list ordered by Domain Consensus
    should be printed
  */
  virtual void ApplyDomainConsensus(const std::string& filename);

  /**
    Applies Domain Consensus feature only to capital words among term candidates
    @param filename file where term candidate list ordered by Domain Consensus
    Capital Words should be printed
  */
  virtual void ApplyDomainConsensusAsCapitalWords(const std::string& filename);

  /**
    Applies Domain Consensus feature only to non-initial words among term
    candidates
    @param filename file where term candidate list ordered by Domain Consensus
    Non-Initial Words should be printed
  */
  virtual void ApplyDomainConsensusAsNonInitialWords(
      const std::string& filename);

  /**
    Applies Domain Consensus feature only to subjects among term candidates
    @param filename file where term candidate list ordered by Domain Consensus
    Subjects should be printed
  */
  virtual void ApplyDomainConsensusAsSubjects(const std::string& filename);

  /**
    Applies feature "NearTermsFreq" to term candidates
    @param filename file where term candidate list ordered by NearTermsFreq
    should be printed
  */
  virtual void ApplyNearTermsFreq(const std::string& filename);

  /**
    Applies NearTermsFreq-IDF reference feature to term candidates
    @param filename file where term candidate list ordered by NearTermsFreq-IDF
    reference should be printed
  */
  virtual void ApplyNearTermsFreqIDFReference(const std::string& filename);

  /**
    Applies BM-25 feature to term candidates
    @param filename file where term candidate list ordered by BM-25 should be
    printed
  */
  virtual void ApplyBM25(const std::string& filename);

  /**
    Applies TF-IDF feature to the term candidates
    @param filename file where term candidate list ordered by TF-IDF should be
    printed
  */
  virtual void ApplyTFIDF(const std::string& filename);

  /**
    Applies TF-IDF reference feature to the term candidates
    @param filename file where term candidate list ordered by TF-IDF Reference
    should be printed
  */
  virtual void ApplyTFIDFReference(const std::string& filename);

  /**
    Applies TF-RIDF feature to the term candidates
    @param filename file where term candidate list ordered by TF-RIDF should be
    printed
  */
  virtual void ApplyTFRIDF(const std::string& filename);

  /**
    Applies TF-IDF feature only to capital words among term candidates
    @param filename file where term candidate list ordered by TF-IDF Capital
    Words should be printed
  */
  virtual void ApplyTFIDFAsCapitalWords(const std::string& filename);

  /**
    Applies TF-IDF Reference feature only to capital words among term candidates
    @param filename file where term candidate list ordered by TF-IDF Reference
    Captial Words should be printed
  */
  virtual void ApplyTFIDFReferenceAsCapitalWords(const std::string& filename);

  /**
    Applies TF-RIDF feature only to capital words among term candidates
    @param filename file where term candidate list ordered by TF-RIDF Capital
    Words should be printed
  */
  virtual void ApplyTFRIDFAsCapitalWords(const std::string& filename);

  /**
    Applies TF-IDF feature only to non-initial words among single-word term
    candidates
    @param filename file where term candidate list ordered by TF-IDF Non-Initial
    Words should be printed
  */
  virtual void ApplyTFIDFAsNonInitialWords(const std::string& filename);

  /**
    Applies TF-IDF reference feature only to non-initial words among term
    candidates
    @param filename file where term candidate list ordered by TF-IDF Reference
    Non-Initial Words should be printed
  */
  virtual void ApplyTFIDFReferenceAsNonInitialWords(
      const std::string& filename);

  /**
    Applies TF-RIDF feature only to non-initial words among term candidates
    @param filename file where term candidate list ordered by TF-RIDF
    Non-Initial Words should be printed
  */
  virtual void ApplyTFRIDFAsNonInitialWords(const std::string& filename);

  /**
    Applies TF-IDF feature only to subjects among term candidates
    @param filename file where term candidate list ordered by TF-IDF Subjects
    should be printed
  */
  virtual void ApplyTFIDFAsSubjects(const std::string& filename);

  /**
    Applies TF-IDF reference feature only to subjects among term candidates
    @param filename file where term candidate list ordered by TF-IDF Reference
    Subjects should be printed
  */
  virtual void ApplyTFIDFReferenceAsSubjects(const std::string& filename);

  /**
    Applies TF-RIDF feature only to subjects among term candidates
    @param filename file where term candidate list ordered by TF-RIDF Subjects
    should be printed
  */
  virtual void ApplyTFRIDFAsSubjects(const std::string& filename);

  /**
    Calculates and returns Weirdness feature for given term candidate
    @param term_candidate term candidate to calculate Weirdness feature for
    @param term_frequency term frequency of the given term candidate
    @return calculated Weirdness feature
  */
  virtual double CalculateWeirdnessForTerm(const std::string& term_candidate,
                                           unsigned int term_frequency) const {
    return static_cast<double>(term_frequency) * static_cast<double>(
        reference_collection_analyzer_.total_number_words()) /
        static_cast<double>(
            std::max(reference_collection_analyzer_.GetTermFrequency(
                        term_candidate),
                     kMinimumFrequency_)) *
        static_cast<double>(term_candidates_analyzer_->total_number_words());
  }

  /**
    Applies Weirdness feature to the term candidates
    @param filename file where term candidate list ordered by Weirdness should
    be printed
  */
  virtual void ApplyWeirdness(const std::string& filename);

  /**
    Applies Reference Weight feature to the term candidates
    @param filename file where term candidate list ordered by Reference Weight
    should be printed
  */
  virtual void ApplyReferenceWeight(const std::string& filename) = 0;

  /**
    Applies Relevance feature to the term candidates
    @param filename file where term candidate list ordered by Relevance should
    be printed
  */
  virtual void ApplyRelevance(const std::string& filename);

  /**
    Applies Contrastive Weight feature to the term candidates
    @param filename file where term candidate list ordered by Contrastive Weight
    should be printed
  */
  virtual void ApplyContrastiveWeight(const std::string& filename);

  /**
    Applies Discriminative Weight feature to the term candidates
    @param filename file where term candidate list ordered by Discriminative
    Weight should be printed
  */
  virtual void ApplyDiscriminativeWeight(const std::string& filename);

  /**
    Applies KF-IDF feature to the term candidates
    @param filename file where term candidate list ordered by KF-IDF should be
    printed
  */
  virtual void ApplyKFIDF(const std::string& filename);

  /**
    Applies LogLikelihood feature to the term candidates
    @param filename file where term candidate list ordered by LogLikelihood
    should be printed
  */
  virtual void ApplyLogLikelihood(const std::string& filename);

  /**
    Applies Probabilistic Term Frequency feature to the term candidates
    @param filename file where term candidate list ordered by Probabilistic Term
    Frequency should be printed
  */
  virtual void ApplyProbabilisticTermFrequency(const std::string& filename);

  /**
    Applies Probabilistic TF-IDF feature to the term candidates
    @param filename file where term candidate list ordered by Probabilistic
    TF-IDF should be printed
  */
  void ApplyProbabilisticTFIDF(const std::string& filename);

  /**
    Applies Probabilistic Maximum Term Frequency feature to the term candidates
    @param filename file where term candidate list ordered by Probabilistic
    Maximum Term Frequency should be printed
  */
  virtual void ApplyProbabilisticMaximumTermFrequency(
      const std::string& filename);

  /**
    Applies Probabilistic Term Score feature to the term candidates
    @param filename file where term candidate list ordered by Probabilistic Term
    Score should be printed
  */
  virtual void ApplyProbabilisticTermScore(const std::string& filename);

  /**
    Applies Probabilistic TF-IDF Term Score feature to the term candidates
    @param filename file where term candidate list ordered by Probabilistic
    TF-IDF Term Score should be printed
  */
  virtual void ApplyProbabilisticTFIDFTermScore(const std::string& filename);

  /**
    Applies Probabilistic Maximum Term Score feature to the term candidates
    @param filename file where term candidate list ordered by Probabilistic
    Maximum Term Score should be printed
  */
  virtual void ApplyProbabilisticMaximumTermScore(const std::string& filename);

  /**
    Applies Term Contribution feature to the term candidates
    @param filename file where term candidate list ordered by Term Contribution
    should be printed
  */
  virtual void ApplyTermContribution(const std::string& filename);

  /**
    Applies Term Variance feature to the term candidates
    @param filename file where term candidate list ordered by Term Variance
    should be printed
  */
  virtual void ApplyTermVariance(const std::string& filename);

  /**
    Applie Term Variance Quality feature to the term candidates
    @param filename file where term candidate lsit ordered by Term Variance
    Quality should be printed
  */
  virtual void ApplyTermVarianceQuality(const std::string& filename);

  /**
    Applies Term Frequency LDA feature to the term candidates
    @param filename file where term candidate list ordered by Term Frequency LDA
    should be printed
  */
  virtual void ApplyTermFrequencyLDA(const std::string& filename);

  /**
    Applies TF-IDF LDA feature to the term candidates
    @param filename file where term candidate list ordered by TF-IDF LDA should
    be printed
  */
  virtual void ApplyTFIDFLDA(const std::string& filename);

  /**
    Applies Domain Consensus LDA feature to the term candidates
    @param filename file where term candidate list ordered by Domain Consensus
    should be printed
  */
  virtual void ApplyDomainConsensusLDA(const std::string& filename);

  /**
    Applies Maximum Term Frequency LDA feature to the term candidates
    @param filename file where term candidate list ordered by Maximum Term
    Frequency LDA should be printed
  */
  virtual void ApplyMaximumTermFrequencyLDA(const std::string& filename);

  /**
    Applies Term Score LDA feature to the term candidates
    @param filename file where term candidate list ordered by Term Score LDA
    should be printed
  */
  virtual void ApplyTermScoreLDA(const std::string& filename);

  /**
    Applies TF-IDF Term Score LDA feature to single-word term candidates
    @param filename file where term candidate list ordered by TF-IDF Term Score
    should be printed
  */
  virtual void ApplyTFIDFTermScoreLDA(const std::string& filename);

  /**
    Applies Maximum Term Score LDA feature to the term candidates
    @param filename file where term candidate list ordered by Maximum Term Score
    LDA should be printed
  */
  virtual void ApplyMaximumTermScoreLDA(const std::string& filename);

  /**
    Calculates and returns C-value feature for given term candidate
    @param term_candidate term candidate to calculate C-value feature for
    @param term_frequency term frequency of given term candidate
    @return calculated C-value feature
    @note This value is calculated without using the length of term candidate
  */
  virtual double CalculateCValueForTerm(const std::string& term_candidate,
                                        unsigned int term_frequency) const {
    unsigned int ambient_phrases_number =
        phrases_analyzer_.GetAmbientPhrasesNumber(term_candidate);
    if (ambient_phrases_number) {
      return static_cast<double>(term_frequency) -
          phrases_analyzer_.GetAveragePhrasesFrequency(term_candidate,
                                                       ambient_phrases_number);
    }
    return static_cast<double>(term_frequency);
  }

  /**
    Applies C-value feature to the term candidates
    @param filename file where term candidate list ordered by C-value should be
    printed
  */
  virtual void ApplyCValue(const std::string& filename);

  /**
    Applies Modified C-Value feature to the term candidates
    @param filename file where term candidate list ordered by Modified C-Value
    should be printed
  */
  virtual void ApplyMCValue(const std::string& filename);

  /**
    Applies NC-value feature to the word term candidates
    @param filename file where term candidate list ordered by NC-value should be
    printed
  */
  virtual void ApplyNCValue(const std::string& filename) = 0;

  /**
    Applies MNC-value feature to the term candidates
    @param filename file where term candidate list ordered by MNC-value should
    be printed
  */
  virtual void ApplyMNCValue(const std::string& filename);

  /**
    Applies Insideness feature to the term candidates
    @param filename file where term candidate list ordered by Insideness should
    be printed
  */
  virtual void ApplyInsideness(const std::string& filename);

  /**
    Applies SumN feature to the term candidates
    @param filename file where term candidate list ordered by SumN should be
    printed
    @param parameter number of ambient phrases to be taken into account for
    calculating this feature
  */
  virtual void ApplySumN(const std::string& filename, unsigned int parameter);

  /**
    Calculates and returns Token-LR feature for the given term candidate
    @param term_candidate given term candidate to calculate Token-LR feature for
    @return calculated Token-LR feature
  */
  virtual double CalculateTokenLRForTerm(const std::string& term_candidate)
      const {
    return sqrt(
        static_cast<double>(
            phrases_analyzer_.GetPreFrequenciesSum(term_candidate) + 1) *
        static_cast<double>(
            phrases_analyzer_.GetPostFrequenciesSum(term_candidate) + 1));
  }

  /**
    Applies Token-LR feature to the term candidates
    @param filename file where term candidate list ordered by Token-LR should be
    printed
  */
  virtual void ApplyTokenLR(const std::string& filename);

  /**
    Applies Token-FLR feature to the term candidates
    @param filename file where term candidate list ordered by Token-FLR should
    be printed
  */
  virtual void ApplyTokenFLR(const std::string& filename);

  /**
    Calculates and returns Type-LR feature for the given term candidate
    @param term_candidate given term candidate to calculate Type-LR feature for
    @return calculated Type-LR feature
  */
  virtual double CalculateTypeLRForTerm(const std::string& term_candidate)
      const {
    return sqrt(
        static_cast<double>(
            phrases_analyzer_.GetPreOccurrencesNumber(term_candidate) + 1) *
        static_cast<double>(
            phrases_analyzer_.GetPostOccurrencesNumber(term_candidate) + 1));
  }

  /**
    Applies Type-LR feature to the term candidates
    @param filename file where term candidate list ordered by Type-LR should be
    printed
  */
  virtual void ApplyTypeLR(const std::string& filename);

  /**
    Applies Type-FLR feature to the term candidates
    @param filename file where term candidate list ordered by Type-FLR should be
    printed
  */
  virtual void ApplyTypeFLR(const std::string& filename);

  /**
    Applies Modified Gravity Count to the term candidates
    @param filename file where term candidate list ordered by Modified Gravity
    Count should be printed
  */
  virtual void ApplyModifiedGravityCount(const std::string& filename) = 0;

  /**
    Inserts calculated feature for term candidate in a given map
    @param term_candidate term candidate for which feature was calculated
    @param weight_value calculated feature
    @param[out] weights_map map containing term candidates and their feature
    values
  */
  virtual void InsertWeight(
      const std::string& term_candidate,
      double weight_value,
      std::map<double, std::vector<std::string> >* weights_map) const {
    auto inserted_result = weights_map->insert({weight_value,
                                                std::vector<std::string>()});
    inserted_result.first->second.push_back(term_candidate);
  }

  /**
    Prints term candidate list ordered by given feature
    @param filename file where term candidate list should be printed
    @param feature_name name of feature by which term candidate list should be
    ordered
    @param term_candidates_map map containing weights and term candidates
    @throw std::runtime_error in case of any occurred error
  */
  virtual void PrintTermCandidates(
      const std::string& filename,
      const std::string& feature_name,
      const std::map<double, std::vector<std::string> >& term_candidates_map);

  /**
    Calculates Average Precision for given vector of term candidates
    @param term_candidates vector containing flags indicating whether term
    candidates are real terms or not
    @param real_terms_number number of real terms in the whole list of term
    candidates
    @return calculated Average Precision
  */
  virtual double CalculateAveragePrecision(
      const std::vector<bool>& term_candidates,
      unsigned int real_terms_number) const;

  /**
    Prints calculated Average Precision for given feature in the specified file
    @param feature_name name of feature for which Average Precision was
    calculated
    @param average_precision calculated Average Precision for given feature
    @throw std::runtime_error in case of any error
  */
  virtual void PrintAveragePrecision(const std::string& feature_name,
                                     double average_precision);

  /** Number of threads in a thread pool */
  const int kNumberOfThreads_ =
      static_cast<int>(boost::thread::hardware_concurrency());

  /** Minimal value for calculating double weights */
  const double kMinimalDoubleValue_ = -1000.0;

  /** Minimal frequency of term (equals to one) */
  const unsigned int kMinimumFrequency_ = 1;

  /** File where Average Precision values should be printed */
  const std::string kAveragePrecisionFilename_;

  /** Threshold for calculating Average Precision */
  const unsigned int kAveragePrecisionThreshold_;

  /** Object for determining real terms among extracted term candidates */
  const ResultsAnalyzer* results_analyzer_;

  /** Object for working with term candidates */
  std::unique_ptr<CandidatesAnalyzer> term_candidates_analyzer_;

  /** Object for working with results of LDA or clustering */
  LDAResultsAnalyzer lda_results_analyzer_;

  /** Object for working with previously extracted noun phrases */
  PhrasesAnalyzer phrases_analyzer_;

  /** Object for working with previously processed reference collection */
  ReferenceCollectionAnalyzer reference_collection_analyzer_;

  /** Mutex for synchronization writing Average Precision values to file */
  boost::mutex average_precision_mutex_;
};
