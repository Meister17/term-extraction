// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_BIGRAMSADDER_PROGRAM_OPTIONS_PARSER_H_
#define EXPERIMENTS_BIGRAMSADDER_PROGRAM_OPTIONS_PARSER_H_

#include <string>

/**
  @brief Class for working with program options

  This class should be used for parsing program options from command line and/or
  from configuration file. After what one can access each necessary program
  option.
*/
class ProgramOptionsParser {
 public:
  /**
    Parses program options from command line and/or from configuration file
    @param argc number of command line arguments (from main function)
    @param argv array containing command line arguments (from main function)
  */
  void Parse(int argc, char** argv);

 private:
  /** Flag indicating whether help message has been already printed or not */
  bool help_message_printed_ = false;

  /** Flag indicating whether morphology should be used or not */
  bool use_morphology_ = false;

  /** File containing unigram vocabulary for LDA */
  std::string unigram_vocabulary_filename_ = "";

  /** File containing unigram input data for LDA */
  std::string unigram_input_filename_ = "";

  /** File containing bigram vocabulary for LDA */
  std::string bigram_vocabulary_filename_ = "";

  /** File containing bigram input data for LDA */
  std::string bigram_input_filename_ = "";

  /** File containing list of adding bigrams */
  std::string bigram_list_filename_ = "";

  /** Number of bigrams to add */
  int bigrams_number_ = 0;

  /** File where output mixed vocabulary for LDA will be printed */
  std::string output_vocabulary_filename_ = "";

  /** File where output mixed input data for LDA will be printed */
  std::string output_input_filename_ = "";

 public:
  bool help_message_printed() const noexcept {
    return help_message_printed_;
  }

  bool use_morphology() const noexcept {
    return use_morphology_;
  }

  std::string unigram_vocabulary_filename() const noexcept {
    return unigram_vocabulary_filename_;
  }

  std::string unigram_input_filename() const noexcept {
    return unigram_input_filename_;
  }

  std::string bigram_vocabulary_filename() const noexcept {
    return bigram_vocabulary_filename_;
  }

  std::string bigram_input_filename() const noexcept {
    return bigram_input_filename_;
  }

  std::string bigram_list_filename() const noexcept {
    return bigram_list_filename_;
  }

  int bigrams_number() const noexcept {
    return bigrams_number_;
  }

  std::string output_vocabulary_filename() const noexcept {
    return output_vocabulary_filename_;
  }

  std::string output_input_filename() const noexcept {
    return output_input_filename_;
  }
};

#endif  // EXPERIMENTS_BIGRAMSADDER_PROGRAM_OPTIONS_PARSER_H_
