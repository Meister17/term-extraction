// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "./bigrams_adder.h"

using boost::is_any_of;
using boost::split;
using std::cout;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ofstream;
using std::ptr_fun;
using std::runtime_error;
using std::stoi;
using std::string;
using std::to_string;
using std::unordered_map;
using std::unordered_set;
using std::vector;

void BigramsAdder::Init(const string& unigram_vocabulary_filename,
                        const string& unigram_input_filename,
                        const string& bigram_vocabulary_filename,
                        const string& bigram_input_filename,
                        const string& bigram_list_filename,
                        int bigrams_number) {
  LoadVocabulary(unigram_vocabulary_filename,
                 &unigram_vocabulary_,
                 &unigram_vocabulary_indexes_);
  unigram_input_ = LoadInputData(unigram_input_filename);
  LoadVocabulary(bigram_vocabulary_filename,
                 &bigram_vocabulary_,
                 &bigram_vocabulary_indexes_);
  bigram_input_ = LoadInputData(bigram_input_filename);
  top_bigrams_ = LoadTopBigrams(bigram_list_filename, bigrams_number);
}

void BigramsAdder::AddBigramsToLDA(bool use_morphology) {
  cout << "Adding to Topic Model " << top_bigrams_.size() << " bigrams\n";
  if (unigram_input_.size() != bigram_input_.size()) {
    throw runtime_error("Wrong input data files");
  }
  unordered_map<std::string, size_t> adding_bigrams_frequencies;
  unordered_map<std::string, size_t> adding_bigrams;
  for (size_t index = 0; index < unigram_input_.size(); ++index) {
    result_input_.push_back(unordered_map<size_t, size_t>());
    for (const auto& bigram_element : bigram_input_[index]) {
      string bigram = bigram_vocabulary_[bigram_element.first].item;
      if (top_bigrams_.find(bigram) != top_bigrams_.end()) {
        size_t bigram_index =
            adding_bigrams.size() + unigram_vocabulary_.size();
        auto insert_result = adding_bigrams.insert({bigram, bigram_index});
        if (!insert_result.second) {
          bigram_index = insert_result.first->second;
        }
        auto bigram_result = adding_bigrams_frequencies.insert(
            {bigram, bigram_element.second});
        if (!bigram_result.second) {
          bigram_result.first->second += bigram_element.second;
        }
        vector<string> unigrams;
        split(unigrams, bigram, is_any_of(" "));
        for (const auto& unigram : unigrams) {
          auto iterator = unigram_vocabulary_indexes_.find(unigram);
          if (iterator != unigram_vocabulary_indexes_.end()) {
            if (unigram_input_[index][iterator->second] <
                bigram_element.second) {
              unigram_input_[index][iterator->second] = 0;
            } else {
              unigram_input_[index][iterator->second] -= bigram_element.second;
            }
          }
        }
        auto result = result_input_.back().insert({bigram_index,
                                                   bigram_element.second});
        if (!result.second) {
          result.first->second += bigram_element.second;
        }
      }
    }
    for (auto& unigram_element : unigram_input_[index]) {
      if (unigram_element.second > 0) {
        result_input_.back()[unigram_element.first] = unigram_element.second;
      }
    }
  }
  unigram_vocabulary_.resize(
      unigram_vocabulary_.size() + adding_bigrams.size());
  string bigram_data = "B\t-";
  if (use_morphology) {
    bigram_data = "B\t+";
  }
  for (const auto& bigram : adding_bigrams) {
    string adding_data = to_string(adding_bigrams_frequencies[bigram.first]) +
        '\t' + bigram_data;
    unigram_vocabulary_[bigram.second] = VocabularyItemData(bigram.first,
                                                            adding_data) ;
  }
  vector<size_t> idx(unigram_vocabulary_.size());
  for (size_t index = 0; index < unigram_vocabulary_.size(); ++index) {
    idx[index] = index;
  }
  vector<size_t> sort_vocabulary_map(idx.size());
  sort(idx.begin(), idx.end(),
       [this](size_t first_index, size_t second_index) {
          return this->unigram_vocabulary_[first_index].item <
                 this->unigram_vocabulary_[second_index].item; });
  for (size_t index = 0; index < idx.size(); ++index) {
    result_vocabulary_.push_back(unigram_vocabulary_[idx[index]]);
    sort_vocabulary_map[idx[index]] = index;
  }
  for (auto& document : result_input_) {
    unordered_map<size_t, size_t> new_document;
    for (auto& element : document) {
      new_document[sort_vocabulary_map[element.first]] = element.second;
    }
    document = new_document;
  }
}

void BigramsAdder::PrintResults(const string& output_vocabulary_filename,
                                const string& output_input_filename) const {
  PrintVocabulary(output_vocabulary_filename);
  PrintInputData(output_input_filename);
}

void BigramsAdder::LoadVocabulary(
    const string& filename,
    vector<VocabularyItemData>* vocabulary,
    unordered_map<string, size_t>* vocabulary_indexes) const {
  cout << "Loading vocabulary from " << filename << "\n";
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to load vocabulary for LDA");
  }
  string line;
  while (!file.eof()) {
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                      line.rend(),
                      not1(ptr_fun<int, int>(isspace))).base(),
              line.end());
    if (line.empty()) {
      continue;
    }
    vector<string> tokens;
    split(tokens, line, is_any_of("\t"));
    if (tokens.size() <= 2) {
      throw runtime_error("Failed to load vocabulary for LDA");
    }
    vocabulary_indexes->insert({tokens[0], vocabulary->size()});
    string item_data = tokens[1];
    for (size_t index = 2; index < tokens.size(); ++index) {
      item_data += "\t" + tokens[index];
    }
    vocabulary->push_back(VocabularyItemData(tokens[0], item_data));
  }
  file.close();
}

vector<unordered_map<size_t, size_t>> BigramsAdder::LoadInputData(
    const string& filename) const {
  cout << "Loading input data from " << filename << "\n";
  vector<unordered_map<size_t, size_t>> input_data;
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to load input data for LDA");
  }
  string line;
  while (!file.eof()) {
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (line.empty()) {
      if (!file.eof()) {
        input_data.push_back(unordered_map<size_t, size_t>());
        continue;
      } else {
        break;
      }
    }
    input_data.push_back(unordered_map<size_t, size_t>());
    vector<string> tokens;
    split(tokens, line, is_any_of(" \t"));
    for (const auto& token : tokens) {
      vector<string> parts;
      split(parts, token, is_any_of(":"));
      if (parts.size() != 2) {
        throw runtime_error("Failed to load input data for LDA");
      }
      input_data.back()[stoi(parts.front())] = stoi(parts.back());
    }
  }
  file.close();
  return input_data;
}

unordered_set<string> BigramsAdder::LoadTopBigrams(const string& filename,
                                                   int bigrams_number) const {
  cout << "Loading top bigrams from " << filename << "\n";
  unordered_set<string> bigrams;
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to load top bigrams to add for LDA");
  }
  string line;
  while (!file.eof()) {
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (line.empty()) {
      continue;
    }
    vector<string> tokens;
    split(tokens, line, is_any_of("\t"));
    bigrams.insert(tokens[0]);
    if (bigrams.size() == static_cast<size_t>(bigrams_number)) {
      break;
    }
  }
  file.close();
  return bigrams;
}

void BigramsAdder::PrintVocabulary(const string& filename) const {
  cout << "Printing vocabulary\n";
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print output mixed vocabulary");
  }
  for (const auto& vocabulary_item : result_vocabulary_) {
    file << vocabulary_item.item << "\t" << vocabulary_item.item_data << "\n";
  }
  file.close();
}

void BigramsAdder::PrintInputData(const string& filename) const {
  cout << "Printing input data\n";
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print output mixed input data");
  }
  for (const auto& document : result_input_) {
    for (const auto& element : document) {
      file << element.first << ":" << element.second << "\t";
    }
    file << '\n';
  }
  file.close();
}
