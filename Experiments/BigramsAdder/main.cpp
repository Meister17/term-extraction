// Copyright 2014 Michael Nokel
#include <iostream>
#include <stdexcept>
#include "./bigrams_adder.h"
#include "./program_options_parser.h"

using std::cerr;
using std::exception;

/**
  Main function to start the program
  @param argc number of command line arguments
  @param argv array containing command line arguments
*/
int main(int argc, char** argv) {
  try {
    ProgramOptionsParser program_options_parser;
    program_options_parser.Parse(argc, argv);
    if (program_options_parser.help_message_printed()) {
      return 0;
    }
    BigramsAdder bigrams_adder;
    bigrams_adder.Init(program_options_parser.unigram_vocabulary_filename(),
                       program_options_parser.unigram_input_filename(),
                       program_options_parser.bigram_vocabulary_filename(),
                       program_options_parser.bigram_input_filename(),
                       program_options_parser.bigram_list_filename(),
                       program_options_parser.bigrams_number());
    bigrams_adder.AddBigramsToLDA(program_options_parser.use_morphology());
    bigrams_adder.PrintResults(
        program_options_parser.output_vocabulary_filename(),
        program_options_parser.output_input_filename());
  } catch (const exception& except) {
    cerr << "Fatal error occurred: " << except.what() << "\n";
    return 1;
  }
  return 0;
}
