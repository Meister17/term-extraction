// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_BIGRAMSADDER_BIGRAMS_ADDER_H_
#define EXPERIMENTS_BIGRAMSADDER_BIGRAMS_ADDER_H_

#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

/**
  @brief Main class that adds bigrams to the vocabulary add input data for LDA

  This class should be used for loading unigram and bigram vocabulary and input
  data for LDA, as well as file with list of bigrams for adding. After what it
  should be used for adding them to LDA and printing resulted mixed vocabulary
  and input data.
*/
class BigramsAdder {
 public:
  /**
    Initializes object by parsing unigram and bigram vocabulary and input data
    for LDA, as well as file with list of bigrams for adding
    @param unigram_vocabulary_filename file containing unigram vocabulary
    @param unigram_input_filename file containing unigram input data
    @param bigram_vocabulary_filename file containing bigram vocabulary
    @param bigram_input_filename file containing bigram input data
    @param bigram_list_filename file containing list of bigrams for adding
    @param bigrams_number number of top bigrams to add
    @throw std::runtime_error in case of any occurred error
  */
  void Init(const std::string& unigram_vocabulary_filename,
            const std::string& unigram_input_filename,
            const std::string& bigram_vocabulary_filename,
            const std::string& bigram_input_filename,
            const std::string& bigram_list_filename,
            int bigrams_number);

 private:
  /**
    @brief Structure containing vocabulary item's data

    It contains vocabulary item's data and item itself
  */
  struct VocabularyItemData {
    VocabularyItemData() {
    }

    VocabularyItemData(const std::string& word,
                       const std::string& word_data)
        : item(word),
          item_data(word_data) {
    }

    /** Vocabulary item */
    std::string item = "";

    /** String representation of item's data */
    std::string item_data = "";
  };

  /** Vector containing unigram vocabulary for LDA */
  std::vector<VocabularyItemData> unigram_vocabulary_;

  /** Hash map containing unigram vocabulary indexes for LDA */
  std::unordered_map<std::string, size_t> unigram_vocabulary_indexes_;

  /** Vector containing unigram input data for LDA */
  std::vector<std::unordered_map<size_t, size_t>> unigram_input_;

  /** Vector containing bigram vocabulary for LDA */
  std::vector<VocabularyItemData> bigram_vocabulary_;

  /** Hash map containing bigram vocabulary indexes for LDA */
  std::unordered_map<std::string, size_t> bigram_vocabulary_indexes_;

  /** Vector containing bigram input data for LDA */
  std::vector<std::unordered_map<size_t, size_t>> bigram_input_;

  /** Hash set containing bigrams to add for LDA */
  std::unordered_set<std::string> top_bigrams_;

  /** Vector containing result vocabulary for LDA */
  std::vector<VocabularyItemData> result_vocabulary_;

  /** Vector containing result input data for LDA */
  std::vector<std::unordered_map<size_t, size_t>> result_input_;

 public:
  /**
    Adds bigrams to LDA. Each two unigrams forming one bigram are replaced
    @param use_morphology flag indicating whether we are using morphology or not
  */
  void AddBigramsToLDA(bool use_morphology);

  /**
    Prints results of mixed vocabulary and input data to the given files
    @param output_vocabulary_filename file where output mixed vocabulary will be
    printed
    @param output_input_filename file where output mixed input data will be
    printed
    @throw std::runtime_error in case of any occurred errors
  */
  void PrintResults(const std::string& output_vocabulary_filename,
                    const std::string& output_input_filename) const;

 private:
  /**
    Loads vocabulary for LDA
    @param filename file containing vocabulary for LDA
    @param[out] vocabulary vector where vocabulary for LDA will be placed
    @param[out] vocabulary_indexes hash map where vocabulary indexes wiil be
    placed
    @throw std::runtime_error in case of any occurred errors
  */
  void LoadVocabulary(
      const std::string& filename,
      std::vector<VocabularyItemData>* vocabulary,
      std::unordered_map<std::string, size_t>* vocabulary_indexes) const;

  /**
    Loads input data for LDA
    @param filename file containing input data for LDA
    @return vector containing input data for LDA
    @throw std::runtime_error in case of any occurred errors
  */
  std::vector<std::unordered_map<size_t, size_t>> LoadInputData(
      const std::string& filename) const;

  /**
    Loads top bigrams from the given file
    @param filename file containing list of bigrams for adding to LDA
    @param bigrams_number number of bigrams to load
    @return hash set containing bigrams to load
    @throw std::runtime_error in case of any occurred errors
  */
  std::unordered_set<std::string> LoadTopBigrams(const std::string& filename,
                                                 int bigrams_number) const;

  /**
    Prints output mixed vocabulary for LDA
    @param filename file containing output mixed vocabulary for LDA
    @throw std::runtime_error in case of any occurred error
  */
  void PrintVocabulary(const std::string& filename) const;

  /**
    Prints output mixed input data for LDA
    @param filename file where output mixed input data for LDA will be printed
    @throw std::runtime_error in case of any occurred errors
  */
  void PrintInputData(const std::string& filename) const;
};

#endif  // EXPERIMENTS_BIGRAMSADDER_BIGRAMS_ADDER_H_
