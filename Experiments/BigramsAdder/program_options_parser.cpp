// Copyright 2014 Michael Nokel
#include <boost/program_options.hpp>
#include <iostream>
#include <stdexcept>
#include <string>
#include "./program_options_parser.h"

using boost::program_options::notify;
using boost::program_options::options_description;
using boost::program_options::parse_command_line;
using boost::program_options::parse_config_file;
using boost::program_options::store;
using boost::program_options::value;
using boost::program_options::variables_map;
using std::cout;
using std::runtime_error;
using std::string;

void ProgramOptionsParser::Parse(int argc, char** argv) {
  variables_map program_options_storage;
  string configuration_filename = "";
  options_description program_options_description("Options for program:");
  program_options_description.add_options()
      ("help,h", value<bool>(&help_message_printed_)->implicit_value(true),
          "Print help message and exit")
      ("configuration_filename,c", value<string>(&configuration_filename),
          "Configuration file")
      ("use_morphology", value<bool>(&use_morphology_)->implicit_value(true),
          "Use morphology or not")
      ("unigram_vocabulary_filename",
          value<string>(&unigram_vocabulary_filename_),
          "File containing unigram vocabulary for LDA")
      ("unigram_input_filename", value<string>(&unigram_input_filename_),
          "File containing unigram input data for LDA")
      ("bigram_vocabulary_filename",
          value<string>(&bigram_vocabulary_filename_),
          "File containing bigram vocabulary for LDA")
      ("bigram_input_filename", value<string>(&bigram_input_filename_),
          "File containing bigram input data for LDA")
      ("bigram_list_filename", value<string>(&bigram_list_filename_),
          "File containing list of bigrams for adding")
      ("bigrams_number", value<int>(&bigrams_number_),
          "Number of bigrams to add")
      ("output_vocabulary_filename",
          value<string>(&output_vocabulary_filename_),
          "File where output mixed vocabulary for LDA will be printed")
      ("output_input_filename", value<string>(&output_input_filename_),
          "File where output mixed input data for LDA will be printed");
  store(parse_command_line(argc, argv, program_options_description),
        program_options_storage);
  notify(program_options_storage);
  if (!configuration_filename.empty()) {
    store(parse_config_file<char>(configuration_filename.data(),
                                  program_options_description),
          program_options_storage);
    notify(program_options_storage);
  }
  if (help_message_printed_) {
    cout << program_options_description << "\n";
  }
  if (bigrams_number_ <= 0) {
    throw runtime_error("Wrong number of bigrams is given");
  }
}
