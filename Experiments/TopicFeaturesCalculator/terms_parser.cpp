// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <cctype>
#include <fstream>
#include <functional>
#include <stdexcept>
#include <string>
#include <vector>
#include "./terms_parser.h"

using boost::is_any_of;
using boost::split;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::string;
using std::vector;

void TermsParser::ParseFile(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse file with terms from thesaurus");
  }
  while (!file.eof()) {
    string line;
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (!line.empty()) {
      vector<string> tokens;
      split(tokens, line, is_any_of("\t"));
      terms_set_.insert(tokens[0]);
    }
  }
  file.close();
  if (terms_set_.empty()) {
    throw runtime_error("Failed to parse file with terms from thesaurus");
  }
}

bool TermsParser::IsTerm(const string& candidate) const noexcept {
  return terms_set_.find(candidate) != terms_set_.end();
}
