// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <cctype>
#include <fstream>
#include <functional>
#include <stdexcept>
#include <string>
#include <vector>
#include "./topics_loader.h"
#include "./terms_parser.h"

using boost::is_any_of;
using boost::split;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::string;
using std::vector;

void TopicsLoader::ParseFiles(const string& topics_filename,
                              const string& vocabulary_filename) {
  TermsParser vocabulary_parser;
  vocabulary_parser.ParseFile(vocabulary_filename);
  int number_topics = GetTopicsNumber(topics_filename);
  ifstream file(topics_filename.data());
  if (!file) {
    throw runtime_error("Failed to parse file containing topics");
  }
  int topic_number = -1;
  vector<double> sum_probabilities(number_topics, 0.0);
  while (!file.eof()) {
    string line;
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    line.erase(line.begin(),
               find_if(line.begin(),
                       line.end(),
                       not1(ptr_fun<int, int>(isspace))));
    if (line.empty()) {
      continue;
    }
    vector<string> tokens;
    split(tokens, line, is_any_of("\t"));
    if (tokens[0].find("Topic") != string::npos) {
      ++topic_number;
    } else {
      if (tokens.size() != 2) {
        throw runtime_error("Failed to parse file containing topics");
      } else {
        vector<string> words;
        split(words, tokens[0], is_any_of(" "));
        if (words.size() == 1) {
          for (const auto& word : words) {
            if (vocabulary_parser.IsTerm(word)) {
              auto result = topics_.insert({word,
                                            vector<double>(number_topics, 0.0)});
              result.first->second[topic_number] += atof(tokens[1].data());
              sum_probabilities[topic_number] += atof(tokens[1].data());
            }
          }
        }
      }
    }
  }
  for (auto& element : topics_) {
    for (size_t topic_number = 0; topic_number < element.second.size();
         ++topic_number) {
      //element.second[topic_number] /= sum_probabilities[topic_number];
    }
  }
  std::cout << topics_.size() << "\n";
  file.close();
}

int TopicsLoader::GetTopicsNumber(const string& filename) const {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse file containing topics");
  }
  int topics_number = 0;
  while (!file.eof()) {
    string line;
    getline(file, line);
    if (line.find("Topic") != string::npos) {
      ++topics_number;
    }
  }
  return topics_number;
}
