// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_TOPICFEATURESCALCULATOR_TOPICS_LOADER_H_
#define EXPERIMENTS_TOPICFEATURESCALCULATOR_TOPICS_LOADER_H_

#include <string>
#include <unordered_map>
#include <vector>

/**
  @brief Class that should be used for parsing inferred topics

  This class should be used for parsing inferred topics. After what one can
  access distribution over topics for every required word or phrase.
*/
class TopicsLoader {
 public:
  /**
    Parses file containing inferred topics using information about vocabulary
    @param topics_filename name of file containing inferred topics
    @param vocabulary_filename name of file containing vocabulary
    @throw std::runtime_error in case of any occurred error
  */
  void ParseFiles(const std::string& topics_filename,
                  const std::string& vocabulary_filename);

 private:
  /** Hash map containing words and phrases along with their distributions over
      the inferred topics */
  std::unordered_map<std::string, std::vector<double>> topics_;

 public:
  /**
    Returns iterator to the beginning of the map containing data about topics
    @return iterator to the beginning of the map containing data about topics
  */
  std::unordered_map<std::string, std::vector<double>>::const_iterator
      GetStartIterator() const noexcept {
    return topics_.begin();
  }

  /**
    Returns iterator to the end of the map containing data about topics
    @return iterator to the end of the map containing data about topics
  */
  std::unordered_map<std::string, std::vector<double>>::const_iterator
      GetEndIterator() const noexcept {
    return topics_.end();
  }

 private:
  /**
    Gets number of topics
    @param filename name of file with inferred topics
    @return number of topics
  */
  int GetTopicsNumber(const std::string& filename) const;
};

#endif  // EXPERIMENTS_TOPICFEATURESCALCULATOR_TOPICS_LOADER_H_
