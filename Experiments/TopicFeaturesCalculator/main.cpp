// Copyright 2014 Michael Nokel
#include <iostream>
#include <stdexcept>
#include "./program_options_parser.h"
#include "./topic_features_calculator.h"

using std::cerr;
using std::exception;

/**
  Main function that starts the program
  @param argc number of command line arguments
  @param argv command line arguments
  @return exit code of success
*/
int main(int argc, char** argv) {
  try {
    ProgramOptionsParser program_options_parser;
    program_options_parser.Parse(argc, argv);
    if (program_options_parser.help_message_printed()) {
      return 0;
    }
    TopicFeaturesCalculator topic_features_calculator(
        program_options_parser.average_precision_filename(),
        program_options_parser.average_precision_threshold());
    topic_features_calculator.ParseFiles(
        program_options_parser.topics_filename(),
        program_options_parser.vocabulary_filename(),
        program_options_parser.thesaurus_filename());
    topic_features_calculator.CalculateFeatures(
        program_options_parser.output_directory_name());
    return 0;
  } catch (const exception& except) {
    cerr << "Fatal error occurred: " << except.what() << "\n";
    return 1;
  }
}
