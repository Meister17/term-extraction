// Copyright 2014 Michael Nokel
#include "./program_options_parser.h"
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>

using boost::filesystem::create_directories;
using boost::filesystem::path;
using boost::program_options::notify;
using boost::program_options::options_description;
using boost::program_options::parse_command_line;
using boost::program_options::parse_config_file;
using boost::program_options::store;
using boost::program_options::value;
using boost::program_options::variables_map;
using std::cout;
using std::ifstream;
using std::ofstream;
using std::runtime_error;
using std::string;

void ProgramOptionsParser::Parse(int argc, char** argv) {
  variables_map program_options_storage;
  string configuration_filename = "";
  options_description program_options_description("Options for program:");
  program_options_description.add_options()
      ("help,h", value<bool>(&help_message_printed_)->implicit_value(true),
          "Print help message and exit")
      ("config_filename,c", value<string>(&configuration_filename),
          "Configuration file")
      ("topics_filename", value<string>(&topics_filename_),
          "File containing inferred topics")
      ("vocabulary_filename", value<string>(&vocabulary_filename_),
          "File containing vocabulary")
      ("output_directory_name", value<string>(&output_directory_name_),
          "Directory where computed features will be written")
      ("average_precision_filename",
          value<string>(&average_precision_filename_),
          "File for printing AvP for computed features")
      ("average_precision_threshold",
          value<size_t>(&average_precision_threshold_),
          "Threshold for computing AvP")
      ("thesaurus_filename", value<string>(&thesaurus_filename_),
          "File containing words and phrases from thesaurus");
  store(parse_command_line(argc, argv, program_options_description),
        program_options_storage);
  notify(program_options_storage);
  if (!configuration_filename.empty()) {
    store(parse_config_file<char>(configuration_filename.data(),
                                  program_options_description),
          program_options_storage);
    notify(program_options_storage);
  }
  if (help_message_printed_) {
    cout << program_options_description << "\n";
  } else {
    CheckExistingFile(topics_filename_);
    CheckExistingFile(vocabulary_filename_);
    CheckExistingFile(thesaurus_filename_);
    CheckOutputDirectory(&output_directory_name_);
    CheckOutputFile(average_precision_filename_);
  }
}

void ProgramOptionsParser::CheckExistingFile(const string& filename) const {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to open existing file");
  }
  file.close();
}

void ProgramOptionsParser::CheckOutputDirectory(string* directory_name) const {
  path path_separator("/");
  string path_separator_string = path_separator.make_preferred().native();
  if (directory_name->back() != path_separator_string[0]) {
    directory_name->push_back(path_separator_string[0]);
  }
  create_directories(*directory_name);
}

void ProgramOptionsParser::CheckOutputFile(const string& filename) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to open file for printing");
  }
  file.close();
}