// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_TOPICFEATURESCALCULATOR_TOPIC_FEATURES_CALCULATOR_H_
#define EXPERIMENTS_TOPICFEATURESCALCULATOR_TOPIC_FEATURES_CALCULATOR_H_

#include <boost/thread.hpp>
#include <map>
#include <string>
#include <vector>
#include "./terms_parser.h"
#include "./topics_loader.h"

/**
  @brief Main class for parsing files and calculating topic-based features

  This class should be used as a controller for parsing files containing
  vocabularies, inferred topics and terms from thesaurus, and calculating
  topic-based features
*/
class TopicFeaturesCalculator {
 public:
  /**
    Initializes object
    @param average_precision_filename name of file for printing AvP for features
    @param average_precision_threshold threshold for computing AvP for features
  */
  TopicFeaturesCalculator(const std::string average_precision_filename,
                          size_t average_precision_threshold)
      : kAveragePrecisionFilename_(average_precision_filename),
        kAveragePrecisionThreshold_(average_precision_threshold) {
  }

  /**
    Parses given files containing vocabulary, inferred topics and terms from
    thesaurus
    @param topics_filename name of file containing inferred topics
    @param vocabulary_filename name of file containing vocabulary
    @param thesaurus_filename name of file containing terms from thesaurus
    @throw std::runtime_error in case of wrong files
  */
  void ParseFiles(const std::string& topics_filename,
                  const std::string& vocabulary_filename,
                  const std::string& thesaurus_filename);

 private:
  /** Number of threads to use in a thread pool */
  const int kNumberOfThreads_ =
      static_cast<int>(boost::thread::hardware_concurrency());

  /** Name of file for printing AvP for computed features */
  const std::string kAveragePrecisionFilename_ = "";

  /** Threshold for computing AvP for features */
  const size_t kAveragePrecisionThreshold_ = 5000;

  /** Mutex for printing AvP for features */
  boost::mutex average_precision_mutex_;

  /** Object that should be used for loading input files */
  TopicsLoader topics_loader_;

  /** Object that should be used for parsing file containing terms from
      thesaurus */
  TermsParser thesaurus_parser_;

 public:
  /**
    Calculates topic-based features, calculates AvP and prints results in the
    given directory
    @param output_directory_name name of directory where results should be
    written
    @throw std::runtime_error in case of any occurred error
  */
  void CalculateFeatures(const std::string& output_directory_name);

 private:
  /**
    Calculates TF based on inferred topics
    @param filename name of file for printing words and phrases ordered by TF
    @throw std::runtime_error in case of any occurred error
  */
  void CalculateTF(const std::string& filename);

  /**
    Calculates TF-IDF based on inferred topics
    @param filename name of file for printing words and phrases ordered by
    TF-IDF
    @throw std::runtime_error in case of any occurred error
  */
  void CalculateTFIDF(const std::string& filename);

  /**
    Calculates Domain Consensus based on inferred topics
    @param filename name of file for printing words and phrases ordered by
    Domain Consensus
    @throw std::runtime_error in case of any occurred error
  */
  void CalculateDomainConsensus(const std::string& filename);

  /**
    Calculates Maximum TF based on inferred topics
    @param filename name of file for printing words and phrases ordered by
    Maximum TF
    @throw std::runtime_error in case of any occurred error
  */
  void CalculateMaximumTF(const std::string& filename);

  /**
    Calculates TS based on inferred topics
    @param filename name of file for printing words and phrases ordered by TS
    @throw std::runtime_error in case of any occurred error
  */
  void CalculateTS(const std::string& filename);

  /**
    Calculates TS-IDF based on inferred topics
    @param filename name of file for printing words and phrases ordered by
    TS-IDF
    @throw std::runtime_error in case of any occurred error
  */
  void CalculateTSIDF(const std::string& filename);

  /**
    Calculates Maximum TF based on inferred topics
    @param filename name of file for printing words and phrases ordered by
    Maximum TS
    @throw std::runtime_error in case of any occurred error
  */
  void CalculateMaximumTS(const std::string& filename);

  /**
    Calculates Expectation based on inferred topics
    @param filename name of file for printing words and phrases ordered by
    Expectation
    @throw std:::runtime_error in case of any occurred error
  */
  void CalculateExpectation(const std::string& filename);

  /**
    Calculates Variance based on inferred topics
    @param filename name of file for printing words and phrases ordered by
    Variance
    @throw std::runtime_error in case of any occurred error
  */
  void CalculateVariance(const std::string& filename);

  /**
    Prints feature results in the given file
    @param filename name of file for printing words and phrase ordered by
    given feature
    @param feature_results map containing calculated values of feature for every
    word and phrase
    @throw std::runtime_error in case of any occurred error
  */
  void PrintFeatureResults(
      const std::string& filename,
      const std::map<double, std::vector<std::string>>& feature_results);

  /**
    Calculates TF for the given term based on inferred topics
    @param probabilities vector containing probability distribution over topics
    @return calculated TF for the given term
  */
  double CalculateTFValue(const std::vector<double>& probabilities)
      const noexcept;

  /**
    Calculates DF for the given term based on inferred topics
    @param probabilities vector containing probability distribution over topics
    @return calculated DF for the given term
  */
  int CalculateDFValue(const std::vector<double>& probabilities) const noexcept;

  /**
    Calculates vector containing Term Scores for the given term
    @param probabilities vector containing probability distribution over topics
    @return calculated vector containing Term Scores for the given term
  */
  std::vector<double> CalculateTSVector(
      const std::vector<double>& probabilities) const noexcept;

  /**
    Inserts calculated feature value into the storage
    @param term term for which feature was calculated
    @param value calculated feature value
    @param[out] feature_results map where term with its calculated feature will
    be stored
  */
  void InsertFeatureValue(
      const std::string& term,
      double value,
      std::map<double, std::vector<std::string>>* feature_results)
      const noexcept;

  /**
    Prints AvP for given feature
    @param feature_name name of feature for which AvP should be written
    @param average_precision AvP of computed feature to print
    @throw std::runtime_error in case of any occurred error
  */
  void PrintAveragePrecisionForFeature(const std::string& feature_name,
                                       double average_precision);
};

#endif  // EXPERIMENTS_TOPICFEATURESCALCULATOR_TOPIC_FEATURES_CALCULATOR_H_
