// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_TOPICFEATURESCALCULATOR_PROGRAM_OPTIONS_PARSER_H_
#define EXPERIMENTS_TOPICFEATURESCALCULATOR_PROGRAM_OPTIONS_PARSER_H_

#include <string>

/**
  @brief Class for parsing program options

  This class should be used for parsing program options from command line and/or
  from configuration file. After what one can access each necessary program
  option
*/
class ProgramOptionsParser {
 public:
  /**
    Parses program options from command line and/or from configuration file
    @param argc number of command line arguments (from main functions)
    @param argv command line arguments (from main function)
    @throw std::exception in case of wrong program options
  */
  void Parse(int argc, char** argv);

 private:
  /** Flag indicating whether help message has been printed or not */
  bool help_message_printed_ = false;

  /** Name of file containing inferred topics */
  std::string topics_filename_ = "";

  /** Name of file containing vocabulary */
  std::string vocabulary_filename_ = "";

  /** Name of directory where computed features should be printed */
  std::string output_directory_name_ = "";

  /** Name of file for printing AvP of features */
  std::string average_precision_filename_ = "";

  /** Threshold for computing AvP */
  size_t average_precision_threshold_ = 5000;

  /** Name of file containing words and phrases from thesaurus */
  std::string thesaurus_filename_ = "";

 public:
  bool help_message_printed() const noexcept {
    return help_message_printed_;
  }

  std::string topics_filename() const noexcept {
    return topics_filename_;
  }

  std::string vocabulary_filename() const noexcept {
    return vocabulary_filename_;
  }

  std::string output_directory_name() const noexcept {
    return output_directory_name_;
  }

  std::string average_precision_filename() const noexcept {
    return average_precision_filename_;
  }

  size_t average_precision_threshold() const noexcept {
    return average_precision_threshold_;
  }

  std::string thesaurus_filename() const noexcept {
    return thesaurus_filename_;
  }

 private:
  /**
    Checks whether given file exists or not
    @param filename name of file for checking
    @throw std::runtime_error in case of wrong file
  */
  void CheckExistingFile(const std::string& filename) const;

  /**
    Checks output directory for correctness (creates if it does not exist)
    @param[out] directory_name name of directory to check
    @throw std::exception in case of wrong directory
  */
  void CheckOutputDirectory(std::string* directory_name) const;

  /**
    Checks output file for printing
    @param filename name of file for checking
    @throw std::runtime_error in case of wrong file
  */
  void CheckOutputFile(const std::string& filename) const;
};

#endif  // EXPERIMENTS_TOPICFEATURESCALCULATOR_PROGRAM_OPTIONS_PARSER_H_
