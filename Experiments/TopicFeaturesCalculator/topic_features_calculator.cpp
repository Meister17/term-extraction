// Copyright 2014 Michael Nokel
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>
#include <boost/threadpool.hpp>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <limits>
#include <map>
#include <stdexcept>
#include <string>
#include <vector>
#include "./topic_features_calculator.h"

using boost::filesystem::path;
using boost::threadpool::pool;
using std::accumulate;
using std::cout;
using std::map;
using std::max;
using std::max_element;
using std::numeric_limits;
using std::ofstream;
using std::runtime_error;
using std::string;
using std::vector;

void TopicFeaturesCalculator::ParseFiles(const string& topics_filename,
                                         const string& vocabulary_filename,
                                         const string& thesaurus_filename) {
  cout << "Parsing files\n";
  pool thread_pool(kNumberOfThreads_);
  thread_pool.schedule(boost::bind(&TopicsLoader::ParseFiles,
                                   &topics_loader_,
                                   topics_filename,
                                   vocabulary_filename));
  thread_pool.schedule(boost::bind(&TermsParser::ParseFile,
                                   &thesaurus_parser_,
                                   thesaurus_filename));
}

void TopicFeaturesCalculator::CalculateFeatures(
    const string& output_directory_name) {
  cout << "Calculating features\n";
  pool thread_pool(kNumberOfThreads_);
  thread_pool.schedule(boost::bind(&TopicFeaturesCalculator::CalculateTF,
                                   this,
                                   output_directory_name + "tf.txt"));
  thread_pool.schedule(boost::bind(&TopicFeaturesCalculator::CalculateTFIDF,
                                   this,
                                   output_directory_name + "tf_idf.txt"));
  thread_pool.schedule(boost::bind(
      &TopicFeaturesCalculator::CalculateDomainConsensus,
      this,
      output_directory_name + "domain_consensus.txt"));
  thread_pool.schedule(boost::bind(&TopicFeaturesCalculator::CalculateMaximumTF,
                                   this,
                                   output_directory_name + "maximum_tf.txt"));
  thread_pool.schedule(boost::bind(&TopicFeaturesCalculator::CalculateTS,
                                   this,
                                   output_directory_name + "ts.txt"));
  thread_pool.schedule(boost::bind(&TopicFeaturesCalculator::CalculateTSIDF,
                                   this,
                                   output_directory_name + "ts_idf.txt"));
  thread_pool.schedule(boost::bind(&TopicFeaturesCalculator::CalculateMaximumTS,
                                   this,
                                   output_directory_name + "maximum_ts.txt"));
  thread_pool.schedule(boost::bind(
      &TopicFeaturesCalculator::CalculateExpectation,
      this,
      output_directory_name + "expectation.txt"));
  thread_pool.schedule(boost::bind(&TopicFeaturesCalculator::CalculateVariance,
                                   this,
                                   output_directory_name + "variance.txt"));
}

void TopicFeaturesCalculator::CalculateTF(const string& filename) {
  map<double, vector<string>> feature_results;
  for (auto iterator = topics_loader_.GetStartIterator(); iterator !=
       topics_loader_.GetEndIterator(); ++iterator) {
    double value = CalculateTFValue(iterator->second);
    InsertFeatureValue(iterator->first, value, &feature_results);
  }
  PrintFeatureResults(filename, feature_results);
}

void TopicFeaturesCalculator::CalculateTFIDF(const string& filename) {
  map<double, vector<string>> feature_results;
  for (auto iterator = topics_loader_.GetStartIterator(); iterator !=
       topics_loader_.GetEndIterator(); ++iterator) {
    double value = CalculateTFValue(iterator->second) *
        static_cast<double>(iterator->second.size()) /
        static_cast<double>(CalculateDFValue(iterator->second));
    InsertFeatureValue(iterator->first, value, &feature_results);
  }
  PrintFeatureResults(filename, feature_results);
}

void TopicFeaturesCalculator::CalculateDomainConsensus(const string& filename) {
  map<double, vector<string>> feature_results;
  for (auto iterator = topics_loader_.GetStartIterator(); iterator !=
       topics_loader_.GetEndIterator(); ++iterator) {
    double value = 0.0;
    for (auto probability : iterator->second) {
      if (probability > 0) {
        value -= probability * log(probability);
      }
    }
    InsertFeatureValue(iterator->first, value, &feature_results);
  }
  PrintFeatureResults(filename, feature_results);
}

void TopicFeaturesCalculator::CalculateMaximumTF(const string& filename) {
  map<double, vector<string>> feature_results;
  for (auto iterator = topics_loader_.GetStartIterator(); iterator !=
       topics_loader_.GetEndIterator(); ++iterator) {
    double value = *max_element(iterator->second.begin(),
                                iterator->second.end());
    InsertFeatureValue(iterator->first, value, &feature_results);
  }
  PrintFeatureResults(filename, feature_results);
}

void TopicFeaturesCalculator::CalculateTS(const string& filename) {
  map<double, vector<string>> feature_results;
  for (auto iterator = topics_loader_.GetStartIterator(); iterator !=
       topics_loader_.GetEndIterator(); ++iterator) {
    vector<double> ts_vector = CalculateTSVector(iterator->second);
    double value = accumulate(ts_vector.begin(), ts_vector.end(), 0.0);
    InsertFeatureValue(iterator->first, value, &feature_results);
  }
  PrintFeatureResults(filename, feature_results);
}

void TopicFeaturesCalculator::CalculateTSIDF(const string& filename) {
  map<double, vector<string>> feature_results;
  for (auto iterator = topics_loader_.GetStartIterator(); iterator !=
       topics_loader_.GetEndIterator(); ++iterator) {
    vector<double> ts_vector = CalculateTSVector(iterator->second);
    double ts = accumulate(ts_vector.begin(), ts_vector.end(), 0.0);
    double value = ts * static_cast<double>(ts_vector.size()) /
        static_cast<double>(CalculateDFValue(iterator->second));
    InsertFeatureValue(iterator->first, value, &feature_results);
  }
  PrintFeatureResults(filename, feature_results);
}

void TopicFeaturesCalculator::CalculateMaximumTS(const string& filename) {
  map<double, vector<string>> feature_results;
  for (auto iterator = topics_loader_.GetStartIterator(); iterator !=
       topics_loader_.GetEndIterator(); ++iterator) {
    vector<double> ts_vector = CalculateTSVector(iterator->second);
    double value = *max_element(ts_vector.begin(), ts_vector.end());
    InsertFeatureValue(iterator->first, value, &feature_results);
  }
  PrintFeatureResults(filename, feature_results);
}

void TopicFeaturesCalculator::CalculateExpectation(const string& filename) {
  map<double, vector<string>> feature_results;
  for (auto iterator = topics_loader_.GetStartIterator(); iterator !=
       topics_loader_.GetEndIterator(); ++iterator) {
    double value = CalculateTFValue(iterator->second) /
        CalculateDFValue(iterator->second);
    InsertFeatureValue(iterator->first, value, &feature_results);
  }
  PrintFeatureResults(filename, feature_results);
}

void TopicFeaturesCalculator::CalculateVariance(const string& filename) {
  map<double, vector<string>> feature_results;
  for (auto iterator = topics_loader_.GetStartIterator(); iterator !=
       topics_loader_.GetEndIterator(); ++iterator) {
    double df = CalculateDFValue(iterator->second);
    double expectation = CalculateTFValue(iterator->second) /
        CalculateDFValue(iterator->second);
    double value = 0.0;
    for (const auto probability: iterator->second) {
      value += (probability - expectation) * (probability - expectation);
    }
    InsertFeatureValue(iterator->first, value / df, &feature_results);
  }
  PrintFeatureResults(filename, feature_results);
}

void TopicFeaturesCalculator::PrintFeatureResults(
    const string& filename,
    const map<double, vector<string>>& feature_results) {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to open file for printing feature results");
  }
  double average_precision = 0.0;
  size_t true_positive = 0;
  size_t term_number = 0;
  for (auto iterator = feature_results.rbegin(); iterator !=
       feature_results.rend(); ++iterator) {
    for (const auto& term : iterator->second) {
      ++term_number;
      if (thesaurus_parser_.IsTerm(term)) {
        if (term_number <= kAveragePrecisionThreshold_) {
          ++true_positive;
          average_precision += static_cast<double>(true_positive) /
              static_cast<double>(term_number);
        }
        file << term << "\t" << iterator->first << "\t+\n";
      } else {
        file << term << "\t" << iterator->first << "\t-\n";
      }
    }
  }
  file.close();
  path feature_path(filename);
  PrintAveragePrecisionForFeature(
      feature_path.stem().string(),
      average_precision / static_cast<double>(true_positive) * 100);
}

double TopicFeaturesCalculator::CalculateTFValue(
    const vector<double>& probabilities) const noexcept {
  return accumulate(probabilities.begin(), probabilities.end(), 0.0);
}

int TopicFeaturesCalculator::CalculateDFValue(
    const vector<double>& probabilities) const noexcept {
  int df = 0;
  for (auto probability : probabilities) {
    if (probability > 0) {
      ++df;
    }
  }
  return df;
}

vector<double> TopicFeaturesCalculator::CalculateTSVector(
    const vector<double>& probabilities) const noexcept {
  vector<double> ts_vector(probabilities.size(), 0.0);
  double denominator = 0.0;
  for (auto probability : probabilities) {
    if (probability > 0) {
      denominator -= log(probability);
    }
  }
  denominator *= 1.0 / CalculateDFValue(probabilities);
  if (denominator > 0) {
    for (size_t index = 0; index < probabilities.size(); ++index) {
      if (probabilities[index] > 0) {
        ts_vector[index] = probabilities[index] *
            (log(probabilities[index]) + denominator);
      }
    }
  }
  return ts_vector;
}

void TopicFeaturesCalculator::InsertFeatureValue(
    const string& term,
    double value,
    map<double, vector<string>>* feature_results) const noexcept {
  auto result = feature_results->insert({value, vector<string>()});
  result.first->second.push_back(term);
}

void TopicFeaturesCalculator::PrintAveragePrecisionForFeature(
    const string& feature_name,
    double average_precision) {
  boost::mutex::scoped_lock lock(average_precision_mutex_);
  ofstream file(kAveragePrecisionFilename_.data(), std::ios::app);
  if (!file) {
    throw runtime_error("Failed to print Average Precision");
  }
  file << "AvP for " << feature_name << "\t" << average_precision << "\n";
  file.close();
}