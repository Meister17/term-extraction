// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_TOPICFEATURESCALCULATOR_TERMS_PARSER_H_
#define EXPERIMENTS_TOPICFEATURESCALCULATOR_TERMS_PARSER_H_

#include <string>
#include <unordered_set>

/**
  @brief Class for parsing terms

  This class should be used for parsing file containing terms. After parsing one
  can check whether word or phrase is among terms or not
*/
class TermsParser {
 public:
  /**
    Parses file containing terms
    @param filename file containing terms
    @throw std::runtime_error in case of wrong file
  */
  void ParseFile(const std::string& filename);

 private:
  /** Hash set containing parsed terms */
  std::unordered_set<std::string> terms_set_;

 public:
  /**
    Checks whether given candidate is among terms or not
    @param candidate candidate to check
    @return true if candidate is among terms
  */
  bool IsTerm(const std::string& candidate) const noexcept;
};

#endif  // EXPERIMENTS_TOPICFEATURESCALCULATOR_TERMS_PARSER_H_
