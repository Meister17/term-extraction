// Copyright 2013 Michael Nokel
#include "./program_options_parser.h"
#include "./auxiliary.h"
#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <stdexcept>

using namespace boost::program_options;
using boost::algorithm::to_lower;
using boost::is_any_of;
using boost::split;
using std::cout;
using std::runtime_error;
using std::string;

void ProgramOptionsParser::Parse(int argc, char** argv) {
  variables_map program_options_storage;
  string configuration_filename = "";
  string distance_type_string = "";
  string distance_aggregator_type_string = "";
  options_description program_options_description("Options for program:");
  program_options_description.add_options()
      ("help,h", value<bool>(&help_message_printed_)->implicit_value(true),
          "Print help message and exit")
      ("config_filename,c", value<string>(&configuration_filename),
          "Configuration file")
      ("input_filename,i", value<string>(&input_filename_), "Input file")
      ("output_filename,o", value<string>(&output_filename_), "Output file")
      ("distance_type,d", value<string>(&distance_type_string),
          "Type of distance to use for calculating between context windows")
      ("distance_aggregator_type,a",
          value<string>(&distance_aggregator_type_string),
          "Type of distance aggregator to use in context windows")
      ("context_window_size,s", value<int>(&context_window_size_),
          "Size of context window")
      ("frequency_threshold,t", value<int>(&frequency_threshold_),
          "Frequency threshold");
  store(parse_command_line(argc, argv, program_options_description),
        program_options_storage);
  notify(program_options_storage);
  if (!configuration_filename.empty()) {
    store(parse_config_file<char>(configuration_filename.data(),
                                  program_options_description),
          program_options_storage);
    notify(program_options_storage);
  }
  if (help_message_printed_) {
    cout << program_options_description << "\n";
  }
  distance_type_ = ParseDistanceType(distance_type_string);
  distance_aggregator_type_ = ParseDistanceAggregatorType(
      distance_aggregator_type_string);
  if (context_window_size_ <= 0 || frequency_threshold_ <= 0) {
    throw runtime_error("Failed to parse program options");
  }
}

DistanceType ProgramOptionsParser::ParseDistanceType(
    const string& distance_type_string) const {
  string distance_type = distance_type_string;
  to_lower(distance_type);
  if (distance_type == "euclidean") {
    return DistanceType::EUCLIDEAN;
  } else if (distance_type == "cosine") {
    return DistanceType::COSINE;
  } else {
    throw runtime_error("Failed to parse distance type");
  }
}

DistanceAggregatorType ProgramOptionsParser::ParseDistanceAggregatorType(
    const string& distance_aggregator_type_string) const {
  string distance_aggregator_type = distance_aggregator_type_string;
  to_lower(distance_aggregator_type);
  if (distance_aggregator_type == "sum") {
    return DistanceAggregatorType::SUM;
  } else if (distance_aggregator_type == "maximum") {
    return DistanceAggregatorType::MAXIMUM;
  } else {
    throw runtime_error("Failed to parse distance aggregator type");
  }
}