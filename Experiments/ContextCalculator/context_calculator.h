// Copyright 2013 Michael Nokel
#pragma once

#include "./auxiliary.h"
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <map>
#include <set>
#include <string>
#include <unordered_map>
#include <vector>

class ContextCalculator {
 public:
  /**
    Creates object
    @param distance_type type of distance to use in context windows
    @param distance_aggregator_type type of distance aggregator to use in
    context windows
    @param context_window_size size of context window to use
    @param frequency_threshold frequency threshold
  */
  ContextCalculator(const DistanceType& distance_type,
                    const DistanceAggregatorType& distance_aggregator_type,
                    int context_window_size,
                    int frequency_threshold);

  /**
    Initializes object by parsing file containing previously extracted contexts
    @param context_filename file containing previously extracted contexts
    @throw std::runtime_error in case of wrong file for parsing
  */
  void Initialize(const std::string& context_filename);

 private:
  /**
    @brief Structure that represents ngram occurrence in the document

    It contains index of the ngram in vocabulary and number of main word in the
    document
  */
  struct NgramOccurrence {
    NgramOccurrence(size_t number, size_t index)
        : word_number(number),
          ngram_index(index) {
    }

    /** Number of the main word of ngram in the document */
    size_t word_number = 0;

    /** Index of ngram in vocabulary */
    size_t ngram_index = 0;

    /**
      Comparison operator that is necessary for comparing ngram occurrences
      @param other other ngram occurrence to compare with
      @return true if this ngram should be before the other one
    */
    bool operator<(const NgramOccurrence& other) const {
      if (ngram_index == other.ngram_index) {
        return word_number < other.word_number;
      }
      return ngram_index < other.ngram_index;
    }
  };

  /** Default frequency of unigram occurrence (equals to 1) */
  const size_t kDefaultFrequency_ = 1;

  /** Number of threads to use in thread pool */
  const int kNumberThreads_ =
      static_cast<int>(boost::thread::hardware_concurrency());

  /** Type of distance to use in context windows */
  const DistanceType kDistanceType_;

  /** Type of distance aggregator to use in context windows */
  const DistanceAggregatorType kDistanceAggregatorType_;

  /** Size of context window to use */
  const size_t kContextWindowSize_;

  /** Frequency threshold */
  const size_t kFrequencyThreshold_;

  /** Mutex for synchronization in forming unigram contexts */
  boost::mutex unigrams_mutex_;

  /** Mutex for synchronization in forming bigram contexts */
  boost::mutex bigrams_mutex_;

  /** Vector containing all unigrams in texts */
  std::vector<std::string> unigrams_;

  /** Vector containing contexts of all unigrams in texts */
  std::vector<std::map<size_t, size_t>> unigrams_contexts_;

  /** Vector containing all bigrams in texts */
  std::vector<std::string> bigrams_;

  /** Frequencies of bigrams (to exclude hem in context distances) */
  std::vector<size_t> bigrams_frequencies_;

  /** Vector containing contexts of all bigrams in texts */
  std::vector<std::map<size_t, size_t>> bigrams_contexts_;

  /** Map containing bigram weights calculated based on context windows */
  std::map<double, std::vector<std::string>> bigram_weights_;

 public:
  /**
    Calculate bigram weights based on context distances to unigrams
    @throw std::runime_error in case of any occurred error
  */
  void CalculateContexts();

  /**
    Prints bigrams in ascending order by their computed weights
    @param output_filename file where bigrams should be printed
    @throw std::runtime_error in case of any error
  */
  void PrintResults(const std::string& output_filename) const;

 private:
  /**
    Forms vocabulary of unigrams and bigrams from the given file
    @param context_filename file from which to form unigrams and bigrams
    @tthrow std::runtime_error in case of any occurred error
  */
  void FormVocabulary(const std::string& context_filename);

  /**
    Forms vocabulary of unigrams and bigrams in a thread pool
    @param[out] file pointer to file object from which to parse lines
    @param[out] bigrams_map pointer to hash map containing bigrams along with
    their frequencies
  */
  void FormVocabularyInThreadPool(
      std::ifstream* file,
      std::unordered_map<std::string, double>* bigrams_map);

  /**
    Parses document in a separate thread
    @param line line representing document for parsing
    @param[out] bigrams_map map where bigrams were stored
    @throw std::runtime_error in case of any occurred error during parsing
  */
  void ParseDocument(std::string line,
                     std::unordered_map<std::string, double>* bigrams_map);

  /**
    Stores unigrams found in document
    @param unigrams_in_document vector containing unigrams found in document
  */
  void StoreUnigrams(const std::vector<std::string>& unigrams_in_document)
      noexcept;

  /**
    Stores bigrams found in document
    @param bigrams_in_document hash map containing bigrams found in document
    along with their frequencies
    @param[out] bigrams_map hash map containing bigrams and their frequencies
  */
  void StoreBigrams(
      const std::unordered_map<std::string, double>& bigrams_in_document,
      std::unordered_map<std::string, double>* bigrams_map) noexcept;

  /**
    Forms vocabulary of bigrams
    @param bigrams_map unordered map containing bigrams that were met in the
    collection along with their frequencies
  */
  void FormBigramsVocabulary(
      const std::unordered_map<std::string, double>& bigrams_map) noexcept;

  /**
    Forms unigram contexts for the given unigrams in document
    @param unigrams_in_document vector containing all found unigrams in document
    @param[out] unigrams_contexts vector containing all found unigram contexts
    for given unigrams
  */
  void FormUnigramContexts(
      const std::vector<NgramOccurrence>& unigrams_in_document,
      std::vector<std::map<size_t, size_t>>* unigrams_contexts) const noexcept;

  /**
    Forms contexts from the given line (called in thread pool)
    @param line line to form contexts from
  */
  void FormContextsFromLine(std::string line) noexcept;

  /**
    Forms unigram contexts for the given ngrams in document
    @param are_bigrams flag indicating whether we are processing bigrams
    @param unigrams_in_document vector containing all found unigrams in document
    @param ngrams_in_document vector containing all found ngrams in document
    @param[out] ngrams_contexts vector containing all found unigram contexts
    for given ngrams
  */
  void FormNgramContexts(
      bool are_bigrams,
      const std::vector<NgramOccurrence>& unigrams_in_document,
      const std::vector<NgramOccurrence>& ngrams_in_document,
      std::vector<std::map<size_t, size_t>>* ngrams_contexts) noexcept;

  /**
    Calculates context distances for given bigram in a separate thread
    @param bigram_index index of the bigram to calculate context distances for
    @throw std::runtime_error if calculating distances failed
  */
  void CalculateContextDistances(size_t bigram_index);

  /**
    Calculates distance between two given contexts: bigram and unigram ones
    @param bigram bigram context to calculate distance for
    @param unigram unigram context to calculate distance for
    @param exclude_unigram_index index of the unigram context to exclude
    @param exclude_frequency frequency to subtract from the excluding unigram
    @return calculated distance between two given contexts
    @throw std::runtime_error in case of wrong distance
  */
  double CalculateDistance(const std::map<size_t, size_t>& bigram,
                           const std::map<size_t, size_t>& unigram,
                           size_t exclude_unigram_index,
                           size_t exclude_frequency) const;

  /**
    Calculates Euclidean distance between two given contexts: bigram and unigram
    ones
    @param bigram bigram context to calculate distance for
    @param unigram unigram context to calculate distance for
    @param exclude_unigram_index index of the unigram context to exclude
    @param exclude_frequency frequency to subtract from the excluding unigram
    @return calculated Euclidean distance between two given contexts
  */
  double CalculateEuclideanDistance(const std::map<size_t, size_t>& bigram,
                                    const std::map<size_t, size_t>& unigram,
                                    size_t exclude_unigram_index,
                                    size_t exclude_frequency) const noexcept;

  /**
    Calculates Cosine distance between two given contexts: bigram and unigram
    ones
    @param bigram bigram context to calculate distance for
    @param unigram unigram context to calculate distance for
    @param exclude_unigram_index index of the unigram context to exclude
    @param exlucde_frequency frequency to subtract from the excluding unigram
    @return calculated Cosine Distance between two given contexts
  */
  double CalculateCosineDistance(const std::map<size_t, size_t>& bigram,
                                 const std::map<size_t, size_t>& unigram,
                                 size_t exclude_unigram_index,
                                 size_t exclude_frequency) const noexcept;

  /**
    Calculates norm of the given bigram context
    @param bigram bigram context to calculate norm for
    @return calculated norm of the given bigram context
  */
  double CalculateBigramNorm(const std::map<size_t, size_t>& bigram)
      const noexcept;

  /**
    Calculated norm of the given unigram context
    @param unigram unigram context to calculated norm for
    @param exclude_unigram_index index of the unigram context to exclude
    @param exclude_frequency frequency to subtract from the excluding unigram
    @return calculated norm of the given unigram context
  */
  double CalculateUnigramNorm(const std::map<size_t, size_t>& unigram,
                              size_t exclude_unigram_index,
                              size_t exlucde_frequency) const noexcept;

  /**
    Inserts bigram's weight in the storage
    @param bigram bigram to store
    @param distance distance of bigram to store
  */
  void InsertBigramWeight(const std::string& bigram, double distance) noexcept;
};