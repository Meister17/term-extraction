// Copyright 2013 Michael Nokel
#pragma once

#include "./auxiliary.h"
#include <string>

/**
  @brief Class for working with program options

  This class should be used for parsing program options from command line and/or
  from configuration file. After what one can access each necessary program
  option.
*/
class ProgramOptionsParser {
 public:
  /**
    Parses program options from command line and/or from configuration file
    @param argc number of command line arguments (from main function)
    @param argv command line arguments (from main function)
    @throw std::exception in case of any occurred error
  */
  void Parse(int argc, char** argv);

 private:
  /** Flag indicating whether help message has been printed or not */
  bool help_message_printed_ = false;

  /** File containing input data for calculating contexts */
  std::string input_filename_ = "";

  /** File where output data will be written */
  std::string output_filename_ = "";

  /** Type of distance to calculate between context windows */
  DistanceType distance_type_ = DistanceType::NO_DISTANCE;

  /** Type of distance aggregator to use in context windows */
  DistanceAggregatorType distance_aggregator_type_ =
      DistanceAggregatorType::NO_DISTANCE_AGGREGATOR;

  /** Context window size */
  int context_window_size_ = 0;

  /** Threshold for frequency */
  int frequency_threshold_ = 0;

 public:
  bool help_message_printed() const {
    return help_message_printed_;
  }

  std::string input_filename() const {
    return input_filename_;
  }

  std::string output_filename() const {
    return output_filename_;
  }

  DistanceType distance_type() const {
    return distance_type_;
  }

  DistanceAggregatorType distance_aggregator_type() const {
    return distance_aggregator_type_;
  }

  int context_window_size() const {
    return context_window_size_;
  }

  int frequency_threshold() const {
    return frequency_threshold_;
  }

 private:
  /**
    Parses distance type from its string representation
    @param distance_type_string string representation of the distance type
    @return distance type
    @throw std::runtime_error in case of wrong distance type
  */
  DistanceType ParseDistanceType(const std::string& distance_type_string) const;

  /**
    Parses distance aggregator type from its string representation
    @param distance_ggregator_type_string representation of the distance
    aggregator type
    @throw std::runtime_error in case of wrong distance aggregator type
  */
  DistanceAggregatorType ParseDistanceAggregatorType(
      const std::string& distance_aggregator_type_string) const;
};