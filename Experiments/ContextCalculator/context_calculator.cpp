// Copyright 2013 Michael Nokel
#include "./context_calculator.h"
#include "./auxiliary.h"
#include <boost/algorithm/string.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/threadpool.hpp>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

using boost::is_any_of;
using boost::split;
using boost::threadpool::pool;
using std::cout;
using std::getline;
using std::find_if;
using std::ifstream;
using std::isspace;
using std::lower_bound;
using std::make_pair;
using std::map;
using std::max;
using std::not1;
using std::ofstream;
using std::ptr_fun;
using std::runtime_error;
using std::set;
using std::sort;
using std::string;
using std::unique;
using std::unordered_map;
using std::vector;

ContextCalculator::ContextCalculator(
    const DistanceType& distance_type,
    const DistanceAggregatorType& distance_aggregator_type,
    int context_window_size,
    int frequency_threshold)
  : kDistanceType_(distance_type),
    kDistanceAggregatorType_(distance_aggregator_type),
    kContextWindowSize_(context_window_size),
    kFrequencyThreshold_(frequency_threshold) {
}

void ContextCalculator::Initialize(const string& context_filename) {
  FormVocabulary(context_filename);
  unigrams_contexts_.resize(unigrams_.size());
  bigrams_contexts_.resize(bigrams_.size());
  cout << "Creating contexts\n";
  ifstream file(context_filename);
  if (!file) {
    throw runtime_error("Failed to parse file with contexts");
  }
  string line;
  pool thread_pool(kNumberThreads_);
  while (!file.eof()) {
    getline(file, line);
    thread_pool.schedule(boost::bind(&ContextCalculator::FormContextsFromLine,
                                     this,
                                     line));
  }
  file.close();
}

void ContextCalculator::FormVocabulary(const string& context_filename) {
  cout << "Forming vocabulary\n";
  ifstream file(context_filename);
  if (!file) {
    throw runtime_error("Failed to form vocabulary from file with contexts");
  }
  unordered_map<string, double> bigrams_map;
  FormVocabularyInThreadPool(&file, &bigrams_map);
  file.close();
  sort(unigrams_.begin(), unigrams_.end());
  unigrams_.resize(unique(unigrams_.begin(), unigrams_.end()) -
                   unigrams_.begin());
  FormBigramsVocabulary(bigrams_map);
}

void ContextCalculator::FormVocabularyInThreadPool(
    ifstream* file,
    unordered_map<string, double>* bigrams_map) {
  string line;
  pool thread_pool(kNumberThreads_);
  while (!file->eof()) {
    getline(*file, line);
    thread_pool.schedule(boost::bind(&ContextCalculator::ParseDocument,
                                     this,
                                     line,
                                     bigrams_map));
  }
}

void ContextCalculator::ParseDocument(
    string line,
    unordered_map<string, double>* bigrams_map) {
  vector<string> unigrams_in_document;
  unordered_map<string, double> bigrams_in_document;
  line.erase(find_if(line.rbegin(),
                     line.rend(),
                     not1(ptr_fun<int, int>(isspace))).base(),
             line.end());
  vector<string> tokens;
  split(tokens, line, is_any_of(" "));
  if (tokens.size() == 0 || tokens[0].empty()) {
    return;
  }
  for (const auto& token: tokens) {
    vector<string> elements;
    split(elements, token, is_any_of(":"));
    if (elements.size() < 2) {
      throw runtime_error("Failed to parse file with contexts");
    }
    unigrams_in_document.push_back(elements[elements.size() - 2]);
    if (elements.size() > 2) {
      string bigram = elements[0];
      for (size_t index = 1; index < elements.size() - 1; ++index) {
        bigram += " " + elements[index];
      }
      auto result = bigrams_in_document.insert(make_pair(bigram,
                                                         kDefaultFrequency_));
      if (!result.second) {
        ++result.first->second;
      }
    }
  }
  StoreUnigrams(unigrams_in_document);
  StoreBigrams(bigrams_in_document, bigrams_map);
}

void ContextCalculator::StoreUnigrams(
    const vector<string>& unigrams_in_document) noexcept {
  boost::mutex::scoped_lock lock(unigrams_mutex_);
  for (const auto& unigram: unigrams_in_document) {
    unigrams_.push_back(unigram);
  }
}

void ContextCalculator::StoreBigrams(
    const unordered_map<string, double>& bigrams_in_document,
    unordered_map<string, double>* bigrams_map) noexcept {
  boost::mutex::scoped_lock lock(bigrams_mutex_);
  for (const auto& bigram: bigrams_in_document) {
    auto result = bigrams_map->insert(bigram);
    if (!result.second) {
      result.first->second += bigram.second;
    }
  }
}

void ContextCalculator::FormBigramsVocabulary(
    const unordered_map<string, double>& bigrams) noexcept {
  map<double, vector<string>> bigrams_map;
  for (const auto& bigram: bigrams) {
    auto result = bigrams_map.insert(make_pair(bigram.second,
                                               vector<string>()));
    result.first->second.push_back(bigram.first);
  }
  size_t bigram_number = 0;
  set<string> bigrams_set;
  for (auto iterator = bigrams_map.rbegin(); iterator != bigrams_map.rend() &&
       bigram_number < kFrequencyThreshold_; ++iterator) {
    for (size_t index = 0; index < iterator->second.size() &&
         bigram_number < kFrequencyThreshold_; ++index, ++bigram_number) {
      bigrams_set.insert(iterator->second[index]);
    }
  }
  for (const auto& bigram: bigrams_set) {
    bigrams_.push_back(bigram);
    bigrams_frequencies_.push_back(bigrams.at(bigram));
  }
}

void ContextCalculator::FormContextsFromLine(string line) noexcept {
  line.erase(find_if(line.rbegin(),
                     line.rend(),
                     not1(ptr_fun<int, int>(isspace))).base(),
             line.end());
  vector<string> tokens;
  split(tokens, line, is_any_of(" "));
  if (tokens.size() == 0 || tokens[0].empty()) {
    return;
  }
  vector<NgramOccurrence> unigrams_in_document;
  vector<NgramOccurrence> bigrams_in_document;
  for (const auto& token: tokens) {
    vector<string> elements;
    split(elements, token, is_any_of(":"));
    if (elements.size() < 2) {
      continue;
    }
    size_t index = lower_bound(unigrams_.begin(),
                               unigrams_.end(),
                               elements[elements.size() - 2]) -
        unigrams_.begin();
    int word_number = stoi(elements[elements.size() - 1]);
    unigrams_in_document.push_back(NgramOccurrence(word_number, index));
    if (elements.size() > 2) {
      string bigram = elements[0];
      for (size_t index = 1; index < elements.size() - 1; ++index) {
        bigram += " " + elements[index];
      }
      index = lower_bound(bigrams_.begin(), bigrams_.end(), bigram) -
          bigrams_.begin();
      if (index < bigrams_.size() && bigrams_[index] == bigram) {
        bigrams_in_document.push_back(NgramOccurrence(word_number, index));
      }
    }
  }
  bool are_bigrams = false;
  FormNgramContexts(are_bigrams,
                    unigrams_in_document,
                    unigrams_in_document,
                    &unigrams_contexts_);
  are_bigrams = true;
  FormNgramContexts(are_bigrams,
                    unigrams_in_document,
                    bigrams_in_document,
                    &bigrams_contexts_);
}

void ContextCalculator::FormNgramContexts(
    bool are_bigrams,
    const vector<NgramOccurrence>& unigrams_in_document,
    const vector<NgramOccurrence>& ngrams_in_document,
    vector<map<size_t, size_t>>* ngrams_contexts) noexcept {
  boost::mutex::scoped_lock unigrams_lock(unigrams_mutex_);
  size_t modifier = 0;
  if (are_bigrams) {
    ++modifier;
  }
  auto previous_iterator = unigrams_in_document.begin();
  for (const auto& ngram_occurrence: ngrams_in_document) {
    while (kContextWindowSize_ + modifier + previous_iterator->word_number <
           ngram_occurrence.word_number) {
      ++previous_iterator;
    }
    for (auto iterator = previous_iterator; iterator !=
         unigrams_in_document.end() && ngram_occurrence.word_number +
         kContextWindowSize_ >= iterator->word_number; ++iterator) {
      if (ngram_occurrence.word_number != iterator->word_number &&
          ngram_occurrence.word_number != iterator->word_number + modifier) {
        auto result = ngrams_contexts->at(ngram_occurrence.ngram_index).insert(
            make_pair(iterator->ngram_index, kDefaultFrequency_));
        if (!result.second) {
          ++result.first->second;
        }
      }
    }
  }
}


void ContextCalculator::CalculateContexts() {
  cout << "Calculating contexts\n";
  pool thread_pool(kNumberThreads_);
  for (size_t index = 0; index < bigrams_.size(); ++index) {
    thread_pool.schedule(boost::bind(
        &ContextCalculator::CalculateContextDistances,
        this,
        index));
  }
}

void ContextCalculator::CalculateContextDistances(size_t bigram_index) {
  vector<string> unigrams;
  split(unigrams, bigrams_[bigram_index], is_any_of(" "));
  if (unigrams.size() < 2 || unigrams.size() > 3) {
    throw runtime_error("Failed to calculate contexts");
  }
  size_t left_index =
      lower_bound(unigrams_.begin(), unigrams_.end(), unigrams.front()) -
      unigrams_.begin();
  size_t right_index =
      lower_bound(unigrams_.begin(), unigrams_.end(), unigrams.back()) -
      unigrams_.begin();
  if (unigrams_[left_index] != unigrams.front() ||
      unigrams_[right_index] != unigrams.back()) {
    throw runtime_error("Failed to calculate contexts");
  }
  double left_distance = CalculateDistance(bigrams_contexts_[bigram_index],
                                           unigrams_contexts_[left_index],
                                           right_index,
                                           bigrams_frequencies_[bigram_index]);
  double right_distance = CalculateDistance(bigrams_contexts_[bigram_index],
                                            unigrams_contexts_[right_index],
                                            left_index,
                                            bigrams_frequencies_[bigram_index]);
  double distance = 0.0;
  switch (kDistanceAggregatorType_) {
    case DistanceAggregatorType::SUM:
      distance = left_distance + right_distance;
      break;
    case DistanceAggregatorType::MAXIMUM:
      distance = max(left_distance, right_distance);
      break;
    case DistanceAggregatorType::NO_DISTANCE_AGGREGATOR: default:
      throw runtime_error("Failed to calculate contexts");
  }
  InsertBigramWeight(bigrams_[bigram_index], distance);
}

double ContextCalculator::CalculateDistance(const map<size_t, size_t>& bigram,
                                            const map<size_t, size_t>& unigram,
                                            size_t exclude_unigram_index,
                                            size_t exclude_frequency) const {
  switch (kDistanceType_) {
    case DistanceType::EUCLIDEAN:
      return CalculateEuclideanDistance(bigram,
                                        unigram,
                                        exclude_unigram_index,
                                        exclude_frequency);
    case DistanceType::COSINE:
      return CalculateCosineDistance(bigram,
                                     unigram,
                                     exclude_unigram_index,
                                     exclude_frequency);
    case DistanceType::NO_DISTANCE: default:
      throw runtime_error("Failed to calculate distance");
  }
}

double ContextCalculator::CalculateEuclideanDistance(
    const map<size_t, size_t>& bigram,
    const map<size_t, size_t>& unigram,
    size_t exclude_unigram_index,
    size_t exclude_frequency) const noexcept {
  double bigram_norm = CalculateBigramNorm(bigram);
  double unigram_norm = CalculateUnigramNorm(unigram,
                                             exclude_unigram_index,
                                             exclude_frequency);
  double distance = 0.0;
  auto bigram_iterator = bigram.begin();
  auto unigram_iterator = unigram.begin();
  while (bigram_iterator != bigram.end() && unigram_iterator != unigram.end()) {
    if (bigram_iterator->first < unigram_iterator->first) {
      distance +=
          pow(static_cast<double>(bigram_iterator->second) / bigram_norm, 2.0);
      ++bigram_iterator;
    } else {
      double subtract_frequency = 0.0;
      if (unigram_iterator->first == exclude_unigram_index) {
        subtract_frequency += static_cast<double>(exclude_frequency);
      }
      if (bigram_iterator->first == unigram_iterator->first) {
        distance += pow(
            static_cast<double>(bigram_iterator->second) / bigram_norm -
                (static_cast<double>(unigram_iterator->second) -
                 subtract_frequency) / unigram_norm,
            2.0);
        ++bigram_iterator;
        ++unigram_iterator;
      } else {
        distance += pow(
          (static_cast<double>(unigram_iterator->second) - subtract_frequency) /
              unigram_norm,
          2.0);
        ++unigram_iterator;
      }
    }
  }
  return sqrt(distance);
}

double ContextCalculator::CalculateCosineDistance(
    const map<size_t, size_t>& bigram,
    const map<size_t, size_t>& unigram,
    size_t exclude_unigram_index,
    size_t exclude_frequency) const noexcept {
  double distance = 0.0;
  double bigram_norm = CalculateBigramNorm(bigram);
  double unigram_norm = CalculateUnigramNorm(unigram,
                                             exclude_unigram_index,
                                             exclude_frequency);
  auto bigram_iterator = bigram.begin();
  auto unigram_iterator = unigram.begin();
  while (bigram_iterator != bigram.end() && unigram_iterator != unigram.end()) {
    if (bigram_iterator->first < unigram_iterator->first) {
      ++bigram_iterator;
    } else if (bigram_iterator->first > unigram_iterator->first) {
      ++unigram_iterator;
    } else {
      double subtract_frequency = 0.0;
      if (unigram_iterator->first == exclude_unigram_index) {
        subtract_frequency += static_cast<double>(exclude_frequency);
      }
      distance += static_cast<double>(bigram_iterator->second) /
          bigram_norm * (static_cast<double>(unigram_iterator->second) -
                         subtract_frequency) / unigram_norm;
      ++unigram_iterator;
      ++bigram_iterator;
    }
  }
  return 1 - distance;
}

double ContextCalculator::CalculateBigramNorm(const map<size_t, size_t>& bigram)
    const noexcept {
  double norm = 0.0;
  for (const auto& occurrence : bigram) {
    norm += pow(occurrence.second, 2.0);
  }
  return sqrt(norm);
}

double ContextCalculator::CalculateUnigramNorm(
    const map<size_t, size_t>& unigram,
    size_t exclude_unigram_index,
    size_t exclude_frequency) const noexcept {
  double norm = 0.0;
  for (const auto& occurrence: unigram) {
    if (occurrence.first == exclude_unigram_index) {
      norm += pow(occurrence.second - exclude_frequency, 2.0);
    } else {
      norm += pow(occurrence.second, 2.0);
    }
  }
  return sqrt(norm);
}

void ContextCalculator::InsertBigramWeight(const std::string& bigram,
                                           double distance) noexcept {
  boost::mutex::scoped_lock lock(bigrams_mutex_);
  auto result = bigram_weights_.insert(make_pair(distance, vector<string>()));
  result.first->second.push_back(bigram);
}

void ContextCalculator::PrintResults(const string& output_filename) const {
  cout << "Printing results\n";
  ofstream file(output_filename);
  if (!file) {
    throw runtime_error("Failed to print results");
  }
  for (auto iterator = bigram_weights_.rbegin();
       iterator != bigram_weights_.rend(); ++iterator) {
    if (iterator->first > 0) {
      for (const auto& bigram: iterator->second) {
        file << bigram << "\t" << iterator->first << "\r\n";
      }
    }
  }
  file.close();
}
