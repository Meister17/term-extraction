// Copyright 2013 Michael Nokel
#pragma once

/**
  @brief Enum containing all possible distance types

  It contains euclidean distance, cosine distance and no distance (meaning an
  error)
*/
enum class DistanceType {
  NO_DISTANCE,
  EUCLIDEAN,
  COSINE
};

/**
  @brief Enum containing all possible types of aggregating distances

  It contains sum, maximum and no distance aggregator type (meaning an error)
*/
enum class DistanceAggregatorType {
  NO_DISTANCE_AGGREGATOR,
  SUM,
  MAXIMUM
};
