// Copyright 2013 Michael Nokel
#include "./context_calculator.h"
#include "./program_options_parser.h"
#include <iostream>
#include <stdexcept>

using std::cerr;
using std::exception;

/**
  Main function that starts the program
  @param argc number of command line arguments
  @param argv array containing command line arguments
*/
int main(int argc, char** argv) {
  try {
    ProgramOptionsParser program_options_parser;
    program_options_parser.Parse(argc, argv);
    ContextCalculator context_calculator(
        program_options_parser.distance_type(),
        program_options_parser.distance_aggregator_type(),
        program_options_parser.context_window_size(),
        program_options_parser.frequency_threshold());
    context_calculator.Initialize(program_options_parser.input_filename());
    context_calculator.CalculateContexts();
    context_calculator.PrintResults(program_options_parser.output_filename());
  } catch(const exception& except) {
    cerr << "Fatal error occurred: " << except.what() << "\n";
    return 1;
  }
	return 0;
}