// Copyright 2014 Michael Nokel
#include <iostream>
#include <exception>
#include "./program_options_parser.h"
#include "./wikipedia_parser.h"

using std::cerr;
using std::exception;

/**
  Main function that starts the program
  @param argc number of command line arguments
  @param argv array containing command line arguments
  @return exit code of success
*/
int main(int argc, char** argv) {
  try {
    ProgramOptionsParser program_options_parser;
    program_options_parser.Parse(argc, argv);
    if (program_options_parser.help_message_printed()) {
      return 0;
    }
    WikipediaParser wikipedia_parser;
    wikipedia_parser.ParseVocabulary(
        program_options_parser.vocabulary_filename());
    wikipedia_parser.ProcessDirectory(
        program_options_parser.source_directory_name(),
        program_options_parser.language());
    wikipedia_parser.PrintResults(program_options_parser.output_filename());
    return 0;
  } catch (const exception& except) {
    cerr << "Fatal error occurred: " << except.what() << "\n";
    return 1;
  }
}
