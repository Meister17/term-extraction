// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_WIKIPEDIAPARSER_PROGRAM_OPTIONS_PARSER_H_
#define EXPERIMENTS_WIKIPEDIAPARSER_PROGRAM_OPTIONS_PARSER_H_

#include <string>
#include "./auxiliary.h"

/**
  @brief Class for working with program options

  This class should be used for parsing program options from command line and/or
  from configuration file. After what one can access each necessary program
  option
*/
class ProgramOptionsParser {
 public:
  /**
    Parses program options from command line and/or from configuration file
    @param argc number of command line arguments (from main function)
    @param argv array containing command line arguments (from main function)
    @throw std::runtime_error in case of any occurred errors
  */
  void Parse(int argc, char** argv);

 private:
  /** Flag indicating whether help message should be printed or not */
  bool help_message_printed_ = false;

  /** Language of the text collection */
  Language language_ = Language::NO_LANGUAGE;

  /** Source directory for processing */
  std::string source_directory_name_ = "";

  /** File containing vocabulary */
  std::string vocabulary_filename_ = "";

  /** File where inverted index will be stored */
  std::string output_filename_ = "";

 public:
  bool help_message_printed() const noexcept {
    return help_message_printed_;
  }

  Language language() const noexcept {
    return language_;
  }

  std::string source_directory_name() const noexcept {
    return source_directory_name_;
  }

  std::string vocabulary_filename() const noexcept {
    return vocabulary_filename_;
  }

  std::string output_filename() const noexcept {
    return output_filename_;
  }

 private:
  /**
    Checks parsed program options for their correctness
    @param language_string string representation of language
    @throw std::runtime_error in case of any wrong program option
  */
  void CheckProgramOptions(const std::string& language_string);
};

#endif  // EXPERIMENTS_WIKIPEDIAPARSER_PROGRAM_OPTIONS_PARSER_H_
