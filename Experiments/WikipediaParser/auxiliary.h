// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_WIKIPEDIAPARSER_AUXILIARY_H_
#define EXPERIMENTS_WIKIPEDIAPARSER_AUXILIARY_H_

#include <string>

/**
  @brief Enumerator containing all supported languages of the text corpus

  This enumerator contains only Russian and English languages, while NO_LANGUAGE
  indicates an error
*/
enum class Language {
  NO_LANGUAGE,
  RUSSIAN,
  ENGLISH
};

/**
  @brief Structure containing ngram index and co-occurrence frequency

  This structure contains index of the ngram and its co-occurrence frequency
*/
struct CooccurrenceElement {
  CooccurrenceElement(size_t word_index, size_t cooccurrence_frequency)
      : ngram_index(word_index),
        frequency(cooccurrence_frequency) {
  }

  /** Index of the ngram */
  size_t ngram_index;

  /** Co-occurrence frequency */
  size_t frequency;
};

/**
  @brief Structure representing found occurrence via Aho-Corasick algorithm

  This structure contains index of the found ngram and number of spaces between
  words forming found ngram
*/
struct AhoCorasickElement {
  AhoCorasickElement(size_t element_index, size_t number_of_spaces)
      : ngram_index(element_index),
        spaces_number(number_of_spaces) {
  }

  /** Index of the found ngram */
  size_t ngram_index;

  /** Number of spaces between words forming found ngram */
  size_t spaces_number;

  /**
    Comparison operator that is necessary for organizing a set
    @param other other element to compare with
  */
  bool operator<(const AhoCorasickElement& other) const noexcept {
    return ngram_index < other.ngram_index;
  }
};

#endif  // EXPERIMENTS_WIKIPEDIAPARSER_AUXILIARY_H_
