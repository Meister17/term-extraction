// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <iostream>
#include <stdexcept>
#include <string>
#include "./program_options_parser.h"
#include "./auxiliary.h"

using boost::algorithm::to_lower;
using boost::filesystem::exists;
using boost::filesystem::is_directory;
using boost::program_options::notify;
using boost::program_options::options_description;
using boost::program_options::parse_command_line;
using boost::program_options::parse_config_file;
using boost::program_options::store;
using boost::program_options::value;
using boost::program_options::variables_map;
using std::cout;
using std::runtime_error;
using std::string;

void ProgramOptionsParser::Parse(int argc, char** argv) {
  variables_map program_options_storage;
  string configuration_filename = "";
  string language_string = "";
  options_description program_options_description("Options for program:");
  program_options_description.add_options()
      ("help,h", value<bool>(&help_message_printed_)->implicit_value(true),
          "Print help message and exit")
      ("configuration_filename,c", value<string>(&configuration_filename),
          "Configuration file")
      ("language", value<string>(&language_string),
          "Language of the text corpus")
      ("source_directory_name", value<string>(&source_directory_name_),
          "Source directory for parsing")
      ("vocabulary_filename", value<string>(&vocabulary_filename_),
          "File containing vocabulary")
      ("output_filename", value<string>(&output_filename_),
          "File where inverted index will be written");
  store(parse_command_line(argc, argv, program_options_description),
        program_options_storage);
  notify(program_options_storage);
  if (!configuration_filename.empty()) {
    store(parse_config_file<char>(configuration_filename.data(),
                                  program_options_description),
          program_options_storage);
    notify(program_options_storage);
  }
  if (help_message_printed_) {
    cout << program_options_description << "\n";
  }
  CheckProgramOptions(language_string);
}

void ProgramOptionsParser::CheckProgramOptions(const string& language_string) {
  if (!exists(source_directory_name_) ||
      !is_directory(source_directory_name_)) {
    throw runtime_error("Wrong source directory");
  }
  string language_name = language_string;
  to_lower(language_name);
  if (language_name == "russian") {
    language_ = Language::RUSSIAN;
  } else if (language_name == "english") {
    language_ = Language::ENGLISH;
  } else {
    throw runtime_error("Wrong language of the text corpus");
  }
}
