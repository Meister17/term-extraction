// Copyright 2014 Michael Nokel
#include <boost/filesystem.hpp>
#include <boost/threadpool.hpp>
#include <iostream>
#include <stdexcept>
#include <string>
#include "./wikipedia_parser.h"
#include "./auxiliary.h"
#include "./directory_processors/english_directory_processor.h"
#include "./directory_processors/russian_directory_processor.h"
#include "./inverted_index_accumulator.h"

using boost::filesystem::directory_iterator;
using boost::filesystem::is_directory;
using boost::threadpool::pool;
using std::cout;
using std::runtime_error;
using std::string;

void WikipediaParser::ParseVocabulary(const string& filename) {
  cout << "Parsing vocabulary\n";
  aho_corasick_.ParseVocabulary(filename);
  inverted_index_accumulator_.reset(
      new InvertedIndexAccumulator(aho_corasick_.GetTotalNumberNGrams()));
}

void WikipediaParser::ProcessDirectory(const string& directory_name,
                                       const Language& language) {
  cout << "Processing source directory\n";
  pool thread_pool(kNumberThreads_);
  for (directory_iterator root_iterator(directory_name); root_iterator !=
       directory_iterator(); ++root_iterator) {
    if (is_directory(root_iterator->status())) {
      switch (language) {
        case Language::RUSSIAN:
          thread_pool.schedule(
              boost::bind(&WikipediaParser::ProcessRussianDirectory,
                          this,
                          root_iterator->path().string()));
          break;
        case Language::ENGLISH:
          thread_pool.schedule(
              boost::bind(&WikipediaParser::ProcessEnglishDirectory,
                          this,
                          root_iterator->path().string()));
          break;
        case Language::NO_LANGUAGE: default:
          throw runtime_error("Wrong language of the text corpus");
      }
    }
  }
}

void WikipediaParser::PrintResults(
    const string& output_filename) const {
  cout << "Printing results\n";
  inverted_index_accumulator_->PrintProbabilities(output_filename,
                                                  aho_corasick_);
}

void WikipediaParser::ProcessRussianDirectory(const string& directory_name) {
  cout << "Processing directory " << directory_name << "\n";
  RussianDirectoryProcessor russian_dir_processor(&aho_corasick_);
  russian_dir_processor.ProcessDirectory(directory_name);
  inverted_index_accumulator_->AccumulateDocumentNGrams(
      russian_dir_processor.GetStartIteratorDocumentNGrams(),
      russian_dir_processor.GetEndIteratorDocumentNGrams());
}

void WikipediaParser::ProcessEnglishDirectory(const string& directory_name) {
  cout << "Processing directory " << directory_name << "\n";
  EnglishDirectoryProcessor english_dir_processor(&aho_corasick_);
  english_dir_processor.ProcessDirectory(directory_name);
  inverted_index_accumulator_->AccumulateDocumentNGrams(
      english_dir_processor.GetStartIteratorDocumentNGrams(),
      english_dir_processor.GetEndIteratorDocumentNGrams());
}
