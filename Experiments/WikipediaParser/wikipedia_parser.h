// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_WIKIPEDIAPARSER_WIKIPEDIA_PARSER_H_
#define EXPERIMENTS_WIKIPEDIAPARSER_WIKIPEDIA_PARSER_H_

#include <boost/thread.hpp>
#include <memory>
#include <string>
#include "./auxiliary.h"
#include "./directory_processors/aho_corasick.h"
#include "./inverted_index_accumulator.h"

/**
  @brief Main class that parses Wikipedia lemmatized texts and counts desired
  probabilities of given words

  This class should be used for parsing file containing vocabulary and parsing
  source directory for counting word probabilities via sliding window and
  documents. After what one can print results
*/
class WikipediaParser {
 public:
  /**
    Parses file containing vocabulary
    @param filename name of file containing vocabulary
    @throw std::runtime_error in case of any occurred error
  */
  void ParseVocabulary(const std::string& filename);

  /**
    Processes source directory for counting word probabilities via sliding
    window and documents
    @param directory_name name of directory for processing
    @param language language of the text corpus
    @throw std::runtime_error in case of any occurred error
  */
  void ProcessDirectory(const std::string& directory_name,
                        const Language& language);

  /**
    Prints computed inverted index
    @param output_filename name of file where inverted index will be written
    @throw std::runtime_error in case of any occurred error
  */
  void PrintResults(const std::string& output_filename) const;

 private:
  /** Number of threads to use in pool */
  const size_t kNumberThreads_ = boost::thread::hardware_concurrency();

  /** Object for parsing vocabulary file and finding occurrences in the text */
  AhoCorasick aho_corasick_;

  /** Accumulator for inverted index */
  std::unique_ptr<InvertedIndexAccumulator> inverted_index_accumulator_;

 private:
  /**
    Processes russian directory and counts probabilities for words found in it
    @param filename name of russian directory for processing
    @throw std::runtime_error in case of any occurred error
  */
  void ProcessRussianDirectory(const std::string& directory_name);

  /**
    Processes english directory and counts probabilities for words found in it
    @param filename name of english directory for processing
    @throw std::runtime_error in case of any occurred error
  */
  void ProcessEnglishDirectory(const std::string& directory_name);
};

#endif  // EXPERIMENTS_WIKIPEDIAPARSER_WIKIPEDIA_PARSER_H_
