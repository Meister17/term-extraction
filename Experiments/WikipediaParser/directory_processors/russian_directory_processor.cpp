// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <fstream>
#include <functional>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>
#include "./russian_directory_processor.h"

using boost::is_any_of;
using boost::split;
using boost::token_compress_on;
using std::cout;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::set;
using std::string;
using std::vector;

void RussianDirectoryProcessor::ProcessFile(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to process Russian file");
  }
  vector<int> current_trie_vertices;
  vector<size_t> ngrams_in_article;
  bool is_end_article = false;
  string line;
  getline(file, line);
  while (!file.eof()) {
    set<string> words;
    ExtractSameWordForms(&file, &words, is_end_article);
    if (is_end_article) {
      FinishProcessingArticle(&current_trie_vertices, &ngrams_in_article);
      is_end_article = false;
    } else if (!words.empty()) {
      set<AhoCorasickElement> new_ngrams =
          aho_corasick_->ReadWords(words, &current_trie_vertices);
      if (!new_ngrams.empty()) {
        for (const auto& element : new_ngrams) {
          ngrams_in_article.push_back(element.ngram_index);
        }
      }
    }
  }
  file.close();
}

void RussianDirectoryProcessor::ExtractSameWordForms(ifstream* file,
                                                     set<string>* words,
                                                     bool& is_end_article)
                                                     const noexcept {
  int first_letter = 0;
  do {
    string line;
    getline(*file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    line.erase(line.begin(),
               find_if(line.begin(),
                       line.end(),
                       not1(ptr_fun<int, int>(isspace))));
    if (line.empty()) {
      continue;
    }
    vector<string> tokens;
    split(tokens, line, is_any_of(" \t"), token_compress_on);
    if (tokens[0] == "</doc>") {
      is_end_article = true;
    } else if (tokens[4] == "ЦК" || tokens[4].find("ЛЕ") != string::npos) {
      words->insert(tokens[tokens.size() - 2]);
    }
    first_letter = file->get();
  } while (!file->eof() && isspace(first_letter));
  if (!file->eof()) {
    file->unget();
  }
}
