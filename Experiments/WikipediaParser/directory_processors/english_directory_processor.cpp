// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <fstream>
#include <functional>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>
#include "./english_directory_processor.h"
#include "../auxiliary.h"

using boost::is_any_of;
using boost::split;
using boost::to_lower;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::set;
using std::string;
using std::vector;

void EnglishDirectoryProcessor::ProcessFile(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to process file");
  }
  vector<int> current_trie_vertices;
  vector<size_t> ngrams_in_article;
  string line;
  while (!file.eof()) {
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (line.empty()) {
      continue;
    }
    vector<string> tokens;
    split(tokens, line, is_any_of("\t"));
    if (tokens[0] == "</doc>") {
      FinishProcessingArticle(&current_trie_vertices, &ngrams_in_article);
      continue;
    }
    set<string> words = {tokens[1]};
    set<AhoCorasickElement> new_ngrams =
        aho_corasick_->ReadWords(words, &current_trie_vertices);
    if (!new_ngrams.empty()) {
      for (const auto& element : new_ngrams) {
        ngrams_in_article.push_back(element.ngram_index);
      }
    }
  }
  file.close();
}
