// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_WIKIPEDIAPARSER_DIRECTORY_PROCESSORS_DIRECTORY_PROCESSOR_H_
#define EXPERIMENTS_WIKIPEDIAPARSER_DIRECTORY_PROCESSORS_DIRECTORY_PROCESSOR_H_

#include <boost/filesystem.hpp>
#include <algorithm>
#include <string>
#include <vector>
#include "./aho_corasick.h"

/**
  @brief Base class for processing directories

  This class should be used as base for Russian and English directory
  processors. After processing directory one can obtain inverted index computed
  by files found in the processing directory
*/
class DirectoryProcessor {
 public:
  /**
    Creates object
    @param aho_corasick_pointer pointer to the object containing parsed
    vocabulary for finding words and collocations in it
  */
  explicit DirectoryProcessor(const AhoCorasick* aho_corasick_pointer)
      : aho_corasick_(aho_corasick_pointer) {
  }

  /**
    Processes given directory and extracts all found unique ngrams in the file
    @param filename name of file for processing
  */
  virtual void ProcessDirectory(const std::string& directory_name) {
    for (boost::filesystem::directory_iterator file_iterator(directory_name);
         file_iterator != boost::filesystem::directory_iterator();
         ++file_iterator) {
      ProcessFile(file_iterator->path().string());
    }
  }

 protected:
  /** Object for finding extracted words and collocations in the vocabulary */
  const AhoCorasick* aho_corasick_;

  /** Vector containing vector of unique ngrams found in documents */
  std::vector<std::vector<size_t>> document_ngrams_;

 public:
  /**
    Returns start iterator to the vector containing vectors of unique ngrams
    found in each parsed document
    @return start iterator to the vector containing vectors of unique ngrams
    found in each parsed document
  */
  virtual std::vector<std::vector<size_t>>::const_iterator
      GetStartIteratorDocumentNGrams() const noexcept {
    return document_ngrams_.cbegin();
  }

  /**
    Returns end iterator to the vector containing sets of ngrams found in each
    parsed document
    @return end iterator to the vector containing sets of ngrams found in each
    parsed document
  */
  virtual std::vector<std::vector<size_t>>::const_iterator
      GetEndIteratorDocumentNGrams() const noexcept {
    return document_ngrams_.cend();
  }

 protected:
  /**
    Processes file
    @param filename file for processing
    @throw std::runtime_error in case of any occurred error
  */
  virtual void ProcessFile(const std::string& filename) = 0;

  /**
    Finishes processing article
    @param[out] current_trie_vertices current vertices in trie (will be cleared)
    @param[out] ngrams_in_article vector containing all found ngrams in article
    (will be cleared)
  */
  virtual void FinishProcessingArticle(std::vector<int>* current_trie_vertices,
                                       std::vector<size_t>* ngrams_in_article)
                                       noexcept {
    current_trie_vertices->clear();
    std::sort(ngrams_in_article->begin(), ngrams_in_article->end());
    ngrams_in_article->resize(std::unique(ngrams_in_article->begin(),
                                          ngrams_in_article->end()) -
                              ngrams_in_article->begin());
    document_ngrams_.push_back(*ngrams_in_article);
    ngrams_in_article->clear();
  }
};

#endif  // EXPERIMENTS_WIKIPEDIAPARSER_DIRECTORY_PROCESSORS_DIRECTORY_PROCESSOR_H_
