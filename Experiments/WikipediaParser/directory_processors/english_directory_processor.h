// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_WIKIPEDIAPARSER_DIRECTORY_PROCESSORS_ENGLISH_DIRECTORY_PROCESSOR_H_
#define EXPERIMENTS_WIKIPEDIAPARSER_DIRECTORY_PROCESSORS_ENGLISH_DIRECTORY_PROCESSOR_H_

#include <string>
#include "./aho_corasick.h"
#include "./directory_processor.h"

/**
  @brief Class for processing directory with English files

  This class should be used for processing directory with English files and
  counting single word and word co-occurrence frequencies via sliding window and
  documents
*/
class EnglishDirectoryProcessor : public DirectoryProcessor {
 public:
  /**
    Creates object
    @param aho_corasick_pointer pointer to the object containing parsed
  */
  EnglishDirectoryProcessor(const AhoCorasick* aho_corasick_pointer)
      : DirectoryProcessor(aho_corasick_pointer) {
  }

 private:
  /**
    Processes given English file and counts various word frequencies in it
    @param filename name of English file for processing
    @throw std::runtime_error in case of any occurred error
  */
  void ProcessFile(const std::string& filename) override;
};

#endif  // EXPERIMENTS_WIKIPEDIAPARSER_DIRECTORY_PROCESSORS_ENGLISH_DIRECTORY_PROCESSOR_H_
