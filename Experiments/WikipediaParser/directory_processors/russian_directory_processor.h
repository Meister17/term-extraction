// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_WIKIPEDIAPARSER_DIRECTORY_PROCESSORS_RUSSIAN_DIRECTORY_PROCESSOR_H_
#define EXPERIMENTS_WIKIPEDIAPARSER_DIRECTORY_PROCESSORS_RUSSIAN_DIRECTORY_PROCESSOR_H_

#include <fstream>
#include <set>
#include <string>
#include "./aho_corasick.h"
#include "./directory_processor.h"

/**
  @brief Class for processing directory with Russian files

  This class should be used for processing directory with Russian files and
  counting single word and word co-occurrence frequencies via sliding window and
  documents
*/
class RussianDirectoryProcessor : public DirectoryProcessor {
 public:
  /**
    Creates object
    @param aho_corasick_pointer pointer to the object containing parsed
    vocabulary for finding words and collocations in it
  */
  RussianDirectoryProcessor(const AhoCorasick* aho_corasick_pointer)
      : DirectoryProcessor(aho_corasick_pointer) {
  }

 private:
  /**
    Processes given Russian file and counts various word frequencies in it
    @param filename name of Russian file for processing
    @throw std::runtime_error in case of any occurred error
  */
  void ProcessFile(const std::string& filename) override;

  /**
    Extracts words that have same word forms from the file
    @param[out] file pointer to file from where words should be extracted
    @param[out] words set where new extracted words will be stored
    @param[out] is_end_article flag indicating that end of article was met
    was encountered or not
    @throw std::runtime_error in case of any occurred errors
  */
  void ExtractSameWordForms(std::ifstream* file,
                            std::set<std::string>* words,
                            bool& is_end_article) const noexcept;
};

#endif  // EXPERIMENTS_WIKIPEDIAPARSER_DIRECTORY_PROCESSORS_RUSSIAN_DIRECTORY_PROCESSOR_H_
