// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <fstream>
#include <functional>
#include <queue>
#include <set>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>
#include "./aho_corasick.h"
#include "../auxiliary.h"

using boost::is_any_of;
using boost::split;
using std::count;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::queue;
using std::runtime_error;
using std::set;
using std::sort;
using std::string;
using std::unique;
using std::unordered_map;
using std::vector;

AhoCorasick::AhoCorasick() {
  transition_functions_.push_back(unordered_map<string, int>());
  terminal_vertices_.push_back(vector<int>());
}

void AhoCorasick::ParseVocabulary(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse vocabulary file");
  }
  string line;
  while (!file.eof()) {
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (line.empty()) {
      continue;
    }
    vector<string> tokens;
    split(tokens, line, is_any_of("\t"));
    AddNGram(tokens[0]);
  }
  file.close();
  InitializeFaultFunctions();
}

set<AhoCorasickElement> AhoCorasick::ReadWords(const set<string>& words,
                                               vector<int>* current_vertices)
                                               const noexcept {
  if (current_vertices->empty()) {
    current_vertices->push_back(0);
  }
  set<AhoCorasickElement> new_occurrences;
  for (const auto& word : words) {
    for (size_t index = 0; index < current_vertices->size(); ++index) {
      while (transition_functions_[current_vertices->at(index)].find(word) ==
             transition_functions_[current_vertices->at(index)].end() &&
             current_vertices->at(index) != 0) {
        current_vertices->at(index) =
            fault_functions_[current_vertices->at(index)];
      }
      auto iterator = transition_functions_[current_vertices->at(index)].find(
          word);
      if (iterator !=
          transition_functions_[current_vertices->at(index)].end()) {
        current_vertices->at(index) =
            transition_functions_[current_vertices->at(index)].at(word);
      }
      for (auto ngram_index : terminal_vertices_[current_vertices->at(index)]) {
        size_t spaces_number = count(
            ngrams_[ngram_index].begin(), ngrams_[ngram_index].end(), ' ');
        new_occurrences.insert(AhoCorasickElement(ngram_index, spaces_number));
      }
      int vertex = compressed_fault_functions_[current_vertices->at(index)];
      while (vertex != kEmptyTransition_) {
        for (auto ngram_index : terminal_vertices_[vertex]) {
          size_t spaces_number = count(
              ngrams_[ngram_index].begin(), ngrams_[ngram_index].end(), ' ');
          new_occurrences.insert(AhoCorasickElement(ngram_index,
                                                    spaces_number));
        }
        vertex = compressed_fault_functions_[vertex];
      }
    }
  }
  sort(current_vertices->begin(), current_vertices->end());
  current_vertices->resize(
      unique(current_vertices->begin(), current_vertices->end()) -
      current_vertices->begin());
  return new_occurrences;
}

void AhoCorasick::AddNGram(const string& ngram) noexcept {
  int ngram_index = ngrams_.size();
  ngrams_.push_back(ngram);
  int vertex = 0;
  vector<string> words;
  split(words, ngram, is_any_of(" "));
  for (const auto& word : words) {
    auto insert_result = transition_functions_[vertex].insert(
        {word, transition_functions_.size()});
    if (insert_result.second) {
      transition_functions_.push_back(unordered_map<string, int>());
      terminal_vertices_.push_back(vector<int>());
    }
    vertex = insert_result.first->second;
  }
  terminal_vertices_[vertex].push_back(ngram_index);
}

void AhoCorasick::InitializeFaultFunctions() noexcept {
  int vertex = 0;
  fault_functions_.resize(transition_functions_.size(), kEmptyTransition_);
  queue<int> vertices;
  vertices.push(vertex);
  while (!vertices.empty()) {
    vertex = vertices.front();
    vertices.pop();
    for (const auto& element : transition_functions_[vertex]) {
      if (element.second != kEmptyTransition_) {
        vertices.push(element.second);
        int fault_vertex = fault_functions_[vertex];
        bool found_fault = false;
        while (fault_vertex != kEmptyTransition_ && !found_fault) {
          if (transition_functions_[fault_vertex].find(element.first) ==
              transition_functions_[fault_vertex].end()) {
            fault_vertex = fault_functions_[fault_vertex];
          } else {
            found_fault = true;
          }
        }
        if (!found_fault) {
          fault_functions_[element.second] = 0;
        } else {
          fault_functions_[element.second] =
              transition_functions_[fault_vertex][element.first];
        }
      }
    }
  }
  CompressFaultFunctions();
}

void AhoCorasick::CompressFaultFunctions() noexcept {
  compressed_fault_functions_.resize(fault_functions_.size(),
                                     kEmptyTransition_);
  int vertex = 0;
  queue<int> vertices;
  vertices.push(vertex);
  while (!vertices.empty()) {
    vertex = vertices.front();
    vertices.pop();
    for (const auto& element : transition_functions_[vertex]) {
      if (element.second != kEmptyTransition_ &&
          fault_functions_[element.second] != kEmptyTransition_) {
        if (!terminal_vertices_[fault_functions_[element.second]].empty()) {
          compressed_fault_functions_[element.second] =
              fault_functions_[element.second];
        } else {
          compressed_fault_functions_[element.second] =
              compressed_fault_functions_[fault_functions_[element.second]];
        }
        vertices.push(element.second);
      }
    }
  }
}

string AhoCorasick::GetNGram(size_t index) const {
  if (index >= ngrams_.size()) {
    throw runtime_error("Failed to get ngram - wrong index");
  }
  return ngrams_[index];
}