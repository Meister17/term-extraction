// Copyright 2014 Michael Nokel
#include <boost/filesystem.hpp>
#include <boost/threadpool.hpp>
#include <boost/thread/mutex.hpp>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <unordered_set>
#include <vector>
#include "./lda_input_creator.h"
#include "./file_processors/english_file_processor.h"
#include "./file_processors/russian_file_processor.h"
#include "./ngrams_accumulators/ngrams_accumulator.h"

using boost::filesystem::directory_iterator;
using boost::filesystem::is_regular_file;
using boost::threadpool::pool;
using std::cout;
using std::runtime_error;
using std::sort;
using std::string;
using std::unordered_set;
using std::vector;

void LDAInputCreator::CreateLDAInput(const string& directory_name) {
  ProcessInputDirectory(directory_name);
  cout << "Filtering LDA vocabulary\n";
  unordered_set<string> filtered_ngrams;
  filtered_ngrams = unigrams_accumulator_.FilterLDAVocabulary(
      filtered_ngrams,
      kMinimumFrequency_);
  filtered_ngrams = bigrams_accumulator_.FilterLDAVocabulary(
      filtered_ngrams,
      kMinimumFrequency_);
}

void LDAInputCreator::PrintResults(
    const string& unigram_lda_input_filename,
    const string& unigram_lda_vocabulary_filename,
    const string& bigram_lda_input_filename,
    const string& bigram_lda_vocabulary_filename) const {
  cout << "Printing results\n";
  pool thread_pool(kNumberOfThreads_);
  thread_pool.schedule(boost::bind(&NGramsAccumulator::PrintLDAInput,
                                   &unigrams_accumulator_,
                                   unigram_lda_input_filename));
  thread_pool.schedule(boost::bind(&NGramsAccumulator::PrintLDAVocabulary,
                                   &unigrams_accumulator_,
                                   unigram_lda_vocabulary_filename));
  thread_pool.schedule(boost::bind(&NGramsAccumulator::PrintLDAInput,
                                   &bigrams_accumulator_,
                                   bigram_lda_input_filename));
  thread_pool.schedule(boost::bind(&NGramsAccumulator::PrintLDAVocabulary,
                                   &bigrams_accumulator_,
                                   bigram_lda_vocabulary_filename));
}

void LDAInputCreator::CalculateWordAssociationMeasures(
    const vector<WordAssociationMeasure>& measures,
    const string& directory_name) const {
  cout << "Calculating word association measures\n";
  measures_computer_.ComputeMeasures(measures, directory_name);
}

void LDAInputCreator::ProcessInputDirectory(const string& directory_name) {
  cout << "Scanning input directory\n";
  vector<string> files;
  for (directory_iterator iterator(directory_name); iterator !=
       directory_iterator(); ++iterator) {
    if (is_regular_file(iterator->status())) {
      files.push_back(iterator->path().string());
    }
  }
  sort(files.begin(), files.end());
  cout << files.size() << " files were found\n";
  unigrams_accumulator_.ReserveForFiles(files.size());
  bigrams_accumulator_.ReserveForFiles(files.size());
  pool thread_pool(kNumberOfThreads_);
  switch (kLanguage_) {
    case Language::RUSSIAN:
      for (size_t file_number = 0; file_number < files.size(); ++file_number) {
        thread_pool.schedule(boost::bind(&LDAInputCreator::ProcessRussianFile,
                                        this,
                                        files[file_number],
                                        file_number));
      }
      break;
    case Language::ENGLISH:
      for (size_t file_number = 0; file_number < files.size(); ++file_number) {
        thread_pool.schedule(boost::bind(&LDAInputCreator::ProcessEnglishFile,
                                         this,
                                         files[file_number],
                                         file_number));
      }
      break;
    case Language::NO_LANGUAGE: default:
      throw runtime_error("Wrong language of text corpus");
  }
}

void LDAInputCreator::ProcessRussianFile(const string& filename,
                                         size_t file_number) {
  RussianFileProcessor russian_file_processor;
  russian_file_processor.ProcessFile(filename);
  boost::mutex::scoped_lock lock(mutex_);
  unigrams_accumulator_.AccumulateNGrams(
      russian_file_processor.GetStartUnigramIterator(),
      russian_file_processor.GetEndUnigramIterator(),
      file_number);
  bigrams_accumulator_.AccumulateNGrams(
      russian_file_processor.GetStartBigramIterator(),
      russian_file_processor.GetEndBigramIterator(),
      file_number);
}

void LDAInputCreator::ProcessEnglishFile(const string& filename,
                                         size_t file_number) {
  EnglishFileProcessor english_file_processor;
  english_file_processor.ProcessFile(filename);
  boost::mutex::scoped_lock lock(mutex_);
  unigrams_accumulator_.AccumulateNGrams(
      english_file_processor.GetStartUnigramIterator(),
      english_file_processor.GetEndUnigramIterator(),
      file_number);
  bigrams_accumulator_.AccumulateNGrams(
      english_file_processor.GetStartBigramIterator(),
      english_file_processor.GetEndBigramIterator(),
      file_number);
}
