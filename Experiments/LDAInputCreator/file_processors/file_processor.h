// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_LDAINPUTCREATOR_FILE_PROCESSORS_FILE_PROCESSOR_H_
#define EXPERIMENTS_LDAINPUTCREATOR_FILE_PROCESSORS_FILE_PROCESSOR_H_

#include <string>
#include <unordered_map>
#include "../auxiliary.h"

/**
  @brief Abstract lass for processing files in separate threads

  This class should be derived by each class implementing processing files of
  any particular language
*/
class FileProcessor {
 public:
  /**
    Processes given file and extracts all found here unigrams and bigrams with
    some statistical information about them
    @param filename file for processing and extracting unigrams and bigrams
  */
  virtual void ProcessFile(const std::string& filename) = 0;

  /**
    Returns iterator to the beginning of hash map containing all extracted
    unigrams from the file
    @return iterator to the beginning of hash map containing all extracted
    unigrams from the file
  */
  virtual std::unordered_map<std::string, NGramItem>::const_iterator
      GetStartUnigramIterator() const noexcept = 0;

  /**
    Returns iterator to the end of hash map containing all extracted unigrams
    from the file
    @return iterator to the end of hash map containing all extracted unigrams
    from the file
  */
  virtual std::unordered_map<std::string, NGramItem>::const_iterator
      GetEndUnigramIterator() const noexcept = 0;

  /**
    Returns iterator to the beginning of hash map containing all extracted
    bigrams from the file
    @return iterator to the beginning of hash map containing all extracted
    bigrams from the file
  */
  virtual std::unordered_map<std::string, NGramItem>::const_iterator
      GetStartBigramIterator() const noexcept = 0;

  /**
    Returns iterator to the end of hash map containing all extracted bigrams
    from the file
    @return iterator to the end of hash map containing all extracted bigrams
    from the file
  */
  virtual std::unordered_map<std::string, NGramItem>::const_iterator
      GetEndBigramIterator() const noexcept = 0;
};

#endif  // EXPERIMENTS_LDAINPUTCREATOR_FILE_PROCESSORS_FILE_PROCESSOR_H_
