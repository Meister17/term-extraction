// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <cctype>
#include <fstream>
#include <functional>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>
#include "./english_file_processor.h"

using boost::is_any_of;
using boost::split;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::set;
using std::string;
using std::vector;

void EnglishFileProcessor::ProcessFile(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to process English file");
  }
  string line;
  while (!file.eof()) {
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    line.erase(line.begin(),
               find_if(line.begin(),
                       line.end(),
                       not1(ptr_fun<int, int>(isspace))));
    if (line.empty()) {
      continue;
    }
    vector<string> tokens;
    split(tokens, line, is_any_of("\t"));
/* if (tokens.size() != 5) {
      throw runtime_error("Failed to process English file");
    }*/
    PartOfSpeech part_of_speech = PartOfSpeech::NO_PART_OF_SPEECH;
    if (tokens.size() == 5) {
      bool is_good_word = tokens[2] == "IN" || tokens[2] == "DT" ||
          (tokens[1].size() > 2 && tokens[1] != "the" && tokens[1] != "not");
      if (is_good_word && tokens.size() == 5) {
        for (const auto& letter : tokens[1]) {
          if (!isalpha(letter)) {
            is_good_word = false;
            break;
          }
        }
        if (is_good_word) {
          if (tokens[2] == "VB" || tokens[2] == "VBD" || tokens[2] == "VBG" ||
              tokens[2] == "VBN" || tokens[2] == "VBP" || tokens[2] == "VBZ") {
            part_of_speech = PartOfSpeech::VERB;
          } else if (tokens[2] == "RB" || tokens[2] == "RBR" ||
                     tokens[2] == "RBS" || tokens[2] == "WRB") {
            part_of_speech = PartOfSpeech::ADVERB;
          } else if (tokens[2] == "JJ" || tokens[2] == "JJR" ||
                     tokens[2] == "JJS") {
            part_of_speech = PartOfSpeech::ADJECTIVE;
          } else if (tokens[2] == "NN" || tokens[2] == "NNS" ||
                     tokens[2] == "NNP" || tokens[2] == "NNPS") {
            part_of_speech = PartOfSpeech::NOUN;
          } else if (tokens[2] == "DT" && (tokens[1] == "a" ||
                     tokens[1] == "an" || tokens[1] == "the")) {
            part_of_speech = PartOfSpeech::DETERMINER;
          } else if (tokens[2] == "IN" && tokens[1] == "of") {
            part_of_speech = PartOfSpeech::PREPOSITION;
          }
        }
      }
    }
    unigrams_extractor_.AddWord(tokens[1], part_of_speech);
    bigrams_extractor_.AddWord(tokens[1], part_of_speech);
  }
  file.close();
}
