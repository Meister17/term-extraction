// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_LDAINPUTCREATOR_FILE_PROCESSORS_ENGLISH_FILE_PROCESSOR_H_
#define EXPERIMENTS_LDAINPUTCREATOR_FILE_PROCESSORS_ENGLISH_FILE_PROCESSOR_H_

#include <string>
#include <unordered_map>
#include "./file_processor.h"
#include "../auxiliary.h"
#include "../english_ngrams_extractors/english_unigrams_extractor.h"
#include "../english_ngrams_extractors/english_bigrams_extractor.h"

/**
  @brief Class for extracting English unigrams and bigrams during parsing file

  This class should be used for extracting unigrams and bigrams from English
  file
*/
class EnglishFileProcessor : public FileProcessor {
 public:
  /**
    Processes file and extracts unigrams and bigrams from it
    @param filename file for processing and extracting unigrams and bigrams
    @throw std::runtime_error in case of any occurred error
  */
  void ProcessFile(const std::string& filename) override;

  /**
    Returns iterator to the beginning of hash map containing all unigrams
    extracted from the file
    @return iterator to the beginning of hash map containing all unigrams
    extracted from the file
  */
  std::unordered_map<std::string, NGramItem>::const_iterator
      GetStartUnigramIterator() const noexcept override {
    return unigrams_extractor_.GetStartIterator();
  }

  /**
    Returns iterator to the end of hash map containing all unigrams extracted
    from the file
    @return iterator to the end of hash map containing all unigrams extracted
    from the file
  */
  std::unordered_map<std::string, NGramItem>::const_iterator
      GetEndUnigramIterator() const noexcept override {
    return unigrams_extractor_.GetEndIterator();
  }

  /**
    Returns iterator to the beginning of hash map containing all bigrams
    extracted from the file
    @return iterator to the beginning of hash map containing all bigrams
    extracted from the file
  */
  std::unordered_map<std::string, NGramItem>::const_iterator
      GetStartBigramIterator() const noexcept override {
    return bigrams_extractor_.GetStartIterator();
  }

  /**
    Returns iterator to the end of hash map containing all bigrams extracted
    from the file
    @return iterator to the end of hash map containing all bigrams extracted
    from the file
  */
  std::unordered_map<std::string, NGramItem>::const_iterator
      GetEndBigramIterator() const noexcept override {
    return bigrams_extractor_.GetEndIterator();
  }

 private:
  /** Object for extracting English unigrams */
  EnglishUnigramsExtractor unigrams_extractor_;

  /** Object for extracting English bigrams */
  EnglishBigramsExtractor bigrams_extractor_;
};

#endif  // EXPERIMENTS_LDAINPUTCREATOR_FILE_PROCESSORS_ENGLISH_FILE_PROCESSOR_H_
