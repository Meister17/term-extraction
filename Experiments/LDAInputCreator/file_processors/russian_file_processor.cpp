// Copyright 2014 Michael Nokel
#include <iconv.h>
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <fstream>
#include <functional>
#include <set>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>
#include "./russian_file_processor.h"
#include "../auxiliary.h"

using boost::is_any_of;
using boost::split;
using boost::token_compress_on;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::set;
using std::string;
using std::unordered_map;
using std::vector;

void RussianFileProcessor::ProcessFile(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to process Russian file");
  }
  string line;
  getline(file, line);
  while (!file.eof()) {
    unordered_map<string, set<PartOfSpeech>> words = ExtractSameWordForms(
        &file);
    if (words.size() != 1 || !words.begin()->first.empty()) {
      unigrams_extractor_.AddWords(words);
      bigrams_extractor_.AddWords(words);
    }
  }
  file.close();
}

unordered_map<string, set<PartOfSpeech>>
    RussianFileProcessor::ExtractSameWordForms(ifstream* file) const {
  unordered_map<string, set<PartOfSpeech>> words;
  int first_letter = -1;
  do {
    string line;
    getline(*file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    line.erase(line.begin(),
               find_if(line.begin(),
                       line.end(),
                       not1(ptr_fun<int, int>(isspace))));
    if (line.empty()) {
      continue;
    }
    line = ConvertFromCP1251ToUTF8(line);
    vector<string> tokens;
    split(tokens, line, is_any_of(" \t"), token_compress_on);
    if (tokens.size() <= 4) {
      throw runtime_error("Failed to process Russian file");
    }
    string word = tokens[tokens.size() - 2];
    string is_main_word = tokens[tokens.size() - 3];
    if (tokens[4] == "ЛЕ" && tokens.back() != "??" &&
        (is_main_word == "+" || is_main_word == "-")) {
      set<PartOfSpeech> parts_of_speech;
      for (size_t index = 0; index < tokens.back().size(); index += 4) {
        string main_sign = tokens.back().substr(index, 2);
        string auxiliary_sign = tokens.back().substr(index + 2, 2);
        if (string("абвгдежзи").find(main_sign) != string::npos) {
          if (string("бзмн").find(auxiliary_sign) != string::npos) {
            parts_of_speech.insert(PartOfSpeech::GENITIVE_NOUN);
          } else {
            parts_of_speech.insert(PartOfSpeech::NOUN);
          }
        } else if (main_sign == "й") {
          parts_of_speech.insert(PartOfSpeech::ADJECTIVE);
        } else if (string("клмнопрстуфхц").find(main_sign) != string::npos) {
          parts_of_speech.insert(PartOfSpeech::VERB);
        } else if (main_sign == "я" &&
                   string("аби").find(auxiliary_sign) != string::npos) {
          parts_of_speech.insert(PartOfSpeech::ADVERB);
        }
      }
      auto insert_result = words.insert({word, parts_of_speech});
      if (!insert_result.second) {
        insert_result.first->second.insert(parts_of_speech.begin(),
                                           parts_of_speech.end());
      }
    } else if (tokens[4] == "РЗД") {
      words.insert({"", set<PartOfSpeech>()});
    }
    first_letter = file->get();
  } while (!file->eof() && isspace(first_letter));
  if (!file->eof()) {
    file->unget();
  }
  return words;
}

string RussianFileProcessor::ConvertFromCP1251ToUTF8(
    const string& string_cp1251) const noexcept {
  iconv_t cd = iconv_open("utf-8", "cp1251");
  if (cd == (iconv_t) - 1) {
    return "";
  }
  size_t input_size = string_cp1251.length();
  size_t output_size = 2*input_size + 1;
  size_t total_output_size = output_size;
#ifdef _WIN32
  const char* input_string_pointer = string_cp1251.data();
#else
  char* input_string_pointer = const_cast<char*>(string_cp1251.data());
#endif
  char* output_string = new char[output_size];
  char* output_string_pointer = output_string;
  memset(output_string, 0, output_size);
  if ((iconv(cd, &input_string_pointer, &input_size, &output_string_pointer,
             &output_size)) == (size_t) - 1) {
    iconv_close(cd);
    delete[] output_string;
    return "";
  }
  iconv_close(cd);
  string string_utf8 = string(output_string, total_output_size - output_size);
  delete[] output_string;
  return string_utf8;
}
