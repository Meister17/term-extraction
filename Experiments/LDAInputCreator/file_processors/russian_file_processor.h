// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_LDAINPUTCREATOR_FILE_PROCESSORS_RUSSIAN_FILE_PROCESSOR_H_
#define EXPERIMENTS_LDAINPUTCREATOR_FILE_PROCESSORS_RUSSIAN_FILE_PROCESSOR_H_

#include <fstream>
#include <set>
#include <string>
#include <unordered_map>
#include "./file_processor.h"
#include "../auxiliary.h"
#include "../russian_ngrams_extractors/russian_unigrams_extractor.h"
#include "../russian_ngrams_extractors/russian_bigrams_extractor.h"

/**
  @brief Class for extracting Russian unigrams and bigrams from processing file

  This class should be used for extracting unigrams and bigrams from Russian
  file
*/
class RussianFileProcessor : public FileProcessor {
 public:
  /**
    Processes Russian file and extracts unirams and bigrams from it
    @param filename file for processing and extracting unigrams and bigrams
    @throw std::runtime_error in case of any occurred error
  */
  void ProcessFile(const std::string& filename) override;

  /**
    Returns iterator to the beginning of hash map containing all extracted
    unigrams from the file
    @return iterator to the beginning of hash map containing all extracted
    unigrams from the file
  */
  std::unordered_map<std::string, NGramItem>::const_iterator
      GetStartUnigramIterator() const noexcept override {
    return unigrams_extractor_.GetStartIterator();
  }

  /**
    Returns iterator to the end of hash map containing all extracted unigrams
    from the file
    @return iterator to the end of hash map containing all extracted unigrams
    from the file
  */
  std::unordered_map<std::string, NGramItem>::const_iterator
      GetEndUnigramIterator() const noexcept override {
    return unigrams_extractor_.GetEndIterator();
  }

  /**
    Returns iterator to the beginning of hash map containing all extracted
    bigrams from the file
    @return iterator to the beginning of hash map containing all extracted
    bigrams from the file
  */
  std::unordered_map<std::string, NGramItem>::const_iterator
      GetStartBigramIterator() const noexcept override {
    return bigrams_extractor_.GetStartIterator();
  }

  /**
    Returns iterator to the end of hash map containing all extracted bigrams
    from the file
    @return iterator to the end of hash map containing all extracted bigrams
    from the file
  */
  std::unordered_map<std::string, NGramItem>::const_iterator
      GetEndBigramIterator() const noexcept override {
    return bigrams_extractor_.GetEndIterator();
  }

 private:
  /** Object for extracting Russian unigrams */
  RussianUnigramsExtractor unigrams_extractor_;

  /** Object for extracting Russian bigrams */
  RussianBigramsExtractor bigrams_extractor_;

 private:
  /**
    Extracts words that have same form from the given file
    @param[out] file pointer to file from where to extract words
    @return hash map containing extracted words with their parts of speech
    @throw std::runtime_error in case of any occurred error
  */
  std::unordered_map<std::string, std::set<PartOfSpeech>> ExtractSameWordForms(
      std::ifstream* file) const;

  /**
    Converts string from its CP-1251 encoding to UTF-8 encoding
    @param string_cp1251 string in CP-1251 encoding
    @return string in UTF-8 encoding
  */
  std::string ConvertFromCP1251ToUTF8(const std::string& string_cp1251) const
      noexcept;
};

#endif  // EXPERIMENTS_LDAINPUTCREATOR_FILE_PROCESSORS_RUSSIAN_FILE_PROCESSOR_H_
