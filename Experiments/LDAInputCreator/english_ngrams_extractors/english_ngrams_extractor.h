// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_LDAINPUTCREATOR_ENGLISH_NGRAMS_EXTRACTORS_ENGLISH_NGRAMS_EXTRACTOR_H_
#define EXPERIMENTS_LDAINPUTCREATOR_ENGLISH_NGRAMS_EXTRACTORS_ENGLISH_NGRAMS_EXTRACTOR_H_

#include <string>
#include <unordered_map>
#include "../auxiliary.h"


/**
  @brief Class that should be used for extracting English ngrams from the
  parsing file

  This class should be used to form statistics for all ngrams extracted during
  parsing English file
*/
class EnglishNGramsExtractor {
 public:
  /**
    Adds newly extracted word to the hash map containing all extracted words
    @param word newly extracted word to add
    @param part_of_speech part of speech of newly extracted word
  */
  virtual void AddWord(const std::string& word,
                       const PartOfSpeech& part_of_speech) noexcept = 0;

  /**
    Returns iterator to the beginning of hash map containing extracted ngrams
    with some information about them
  */
  virtual std::unordered_map<std::string, NGramItem>::const_iterator
      GetStartIterator() const noexcept {
    return ngrams_.begin();
  }

  /**
    Returns iterator to the end of hash map containing extracted ngrams with
    some information about them
  */
  virtual std::unordered_map<std::string, NGramItem>::const_iterator
      GetEndIterator() const noexcept {
    return ngrams_.end();
  }

 protected:
  /** Default frequency of ngram (equals to one) */
  const size_t kDefaultFrequency_ = 1;

  /** Hash map containing all necessary information about extracted ngrams */
  std::unordered_map<std::string, NGramItem> ngrams_;
};

#endif  // EXPERIMENTS_LDAINPUTCREATOR_ENGLISH_NGRAMS_EXTRACTORS_ENGLISH_NGRAMS_EXTRACTOR_H_
