// Copyright 2014 Michael Nokel
#include <iostream>
#include <set>
#include <string>
#include "./english_unigrams_extractor.h"
#include "../auxiliary.h"

using std::set;
using std::string;

void EnglishUnigramsExtractor::AddWord(const string& word,
                                       const PartOfSpeech& part_of_speech)
                                       noexcept {
  if (part_of_speech != PartOfSpeech::NO_PART_OF_SPEECH) {
    if (part_of_speech == PartOfSpeech::NOUN ||
        part_of_speech == PartOfSpeech::GENITIVE_NOUN ||
        part_of_speech == PartOfSpeech::ADJECTIVE ||
        part_of_speech == PartOfSpeech::ADVERB ||
        part_of_speech == PartOfSpeech::VERB) {
      set<PartOfSpeech> pos;
      if (part_of_speech == PartOfSpeech::GENITIVE_NOUN) {
        pos.insert(PartOfSpeech::NOUN);
      } else {
        pos.insert(part_of_speech);
      }
      NGramItem item(kDefaultFrequency_, pos);
      auto insert_result = ngrams_.insert({word, item});
      if (!insert_result.second) {
        ++insert_result.first->second.frequency;
        insert_result.first->second.parts_of_speech.insert(pos.begin(),
                                                           pos.end());
      }
    }
    /*if (!previous_word_.empty() &&
        previous_part_of_speech_ != PartOfSpeech::NO_PART_OF_SPEECH) {
      if (part_of_speech == PartOfSpeech::NOUN ||
          part_of_speech == PartOfSpeech::GENITIVE_NOUN) {
        if (previous_word_in_bigram_) {
          auto found_iterator = ngrams_.find(previous_word_);
          ++found_iterator->second.frequency;
        }
        previous_part_of_speech_ = PartOfSpeech::NOUN;
        previous_word_ = word;
        previous_word_in_bigram_ = true;
      } else if (part_of_speech == PartOfSpeech::ADJECTIVE) {
        previous_word_ = word;
        previous_part_of_speech_ = PartOfSpeech::ADJECTIVE;
        previous_word_in_bigram_ = false;
      } else if (part_of_speech == PartOfSpeech::PREPOSITION && word == "of" &&
                 previous_part_of_speech_ == PartOfSpeech::NOUN) {
        previous_part_of_speech_ = PartOfSpeech::PREPOSITION;
      } else if (part_of_speech != PartOfSpeech::DETERMINER) {
        previous_word_.clear();
        previous_part_of_speech_ = PartOfSpeech::NO_PART_OF_SPEECH;
        previous_word_in_bigram_ = false;
      }
    } else {
      previous_word_in_bigram_ = false;
      if (part_of_speech == PartOfSpeech::NOUN ||
          part_of_speech == PartOfSpeech::GENITIVE_NOUN) {
        previous_part_of_speech_ = PartOfSpeech::NOUN;
        previous_word_ = word;
      } else if (part_of_speech == PartOfSpeech::ADJECTIVE) {
        previous_part_of_speech_ = PartOfSpeech::ADJECTIVE;
        previous_word_ = word;
      } else {
        previous_word_.clear();
        previous_part_of_speech_ = PartOfSpeech::NO_PART_OF_SPEECH;
      }
    }
  } else {
    previous_word_.clear();
    previous_part_of_speech_ = PartOfSpeech::NO_PART_OF_SPEECH;
    previous_word_in_bigram_ = false;
  }*/
  }
}
