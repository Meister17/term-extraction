// Copyright 2014 Michael Nokel
#include <set>
#include <string>
#include "./english_bigrams_extractor.h"
#include "../auxiliary.h"

using std::set;
using std::string;

void EnglishBigramsExtractor::AddWord(const string& word,
                                      const PartOfSpeech& part_of_speech)
                                      noexcept {
  if (part_of_speech == PartOfSpeech::NOUN ||
      part_of_speech == PartOfSpeech::GENITIVE_NOUN) {
    if (!previous_word_.empty() &&
        previous_part_of_speech_ != PartOfSpeech::NO_PART_OF_SPEECH) {
      set<PartOfSpeech> bigrams_set;
      bigrams_set.insert(PartOfSpeech::BIGRAM);
      NGramItem item(kDefaultFrequency_, bigrams_set);
      auto insert_result = ngrams_.insert({previous_word_ + " " + word, item});
      if (!insert_result.second) {
        ++insert_result.first->second.frequency;
      }
    }
    previous_word_ = word;
    previous_part_of_speech_ = PartOfSpeech::NOUN;
  } else if (part_of_speech == PartOfSpeech::ADJECTIVE) {
    previous_word_ = word;
    previous_part_of_speech_ = PartOfSpeech::ADJECTIVE;
  } else if (part_of_speech == PartOfSpeech::PREPOSITION && word == "of") {
    if (!previous_word_.empty() &&
        previous_part_of_speech_ == PartOfSpeech::NOUN) {
      previous_word_ += " " + word;
      previous_part_of_speech_ = PartOfSpeech::PREPOSITION;
    } else {
      previous_word_.clear();
      previous_part_of_speech_ = PartOfSpeech::NO_PART_OF_SPEECH;
    }
  } else if (part_of_speech != PartOfSpeech::DETERMINER) {
    previous_word_.clear();
    previous_part_of_speech_ = PartOfSpeech::NO_PART_OF_SPEECH;
  }
}
