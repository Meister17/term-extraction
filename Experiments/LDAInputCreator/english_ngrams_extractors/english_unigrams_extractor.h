// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_LDAINPUTCREATOR_ENGLISH_NGRAMS_EXTRACTORS_ENGLISH_UNIGRAMS_EXTRACTOR_H_
#define EXPERIMENTS_LDAINPUTCREATOR_ENGLISH_NGRAMS_EXTRACTORS_ENGLISH_UNIGRAMS_EXTRACTOR_H_

#include <string>
#include "./english_ngrams_extractor.h"
#include "../auxiliary.h"


/**
  @brief Class that should be used for extracting English unigrams from the
  parsing file

  This class should be used for forming statistics about extracted unigrams
  during parsing English file
*/
class EnglishUnigramsExtractor : public EnglishNGramsExtractor {
 public:
  /**
    Adds newly extracted word to hash map containing all extracted unigrams
    @param word newly extracted word to add
    @param part_of_speech part of speech of the newly extracted word
  */
  void AddWord(const std::string& word, const PartOfSpeech& part_of_speech)
      noexcept override;

 private:
  /** Previous word: only noun or adjective */
//  std::string previous_word_ = "";

  /** Part of speech of the previous word */
//  PartOfSpeech previous_part_of_speech_ = PartOfSpeech::NO_PART_OF_SPEECH;

  /** Flag indicating whether previous word was in bigram or not */
//  bool previous_word_in_bigram_ = false;
};


#endif  // EXPERIMENTS_LDAINPUTCREATOR_ENGLISH_NGRAMS_EXTRACTORS_ENGLISH_UNIGRAMS_EXTRACTOR_H_
