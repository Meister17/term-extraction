// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_LDAINPUTCREATOR_AUXILIARY_H_
#define EXPERIMENTS_LDAINPUTCREATOR_AUXILIARY_H_

#include <set>
#include <string>

/**
  @brief Enumerator containing all possible languages of the text corpus

  This enumerator currently contains Russian and English languages, while
  NO_LANGUAGE indicates an error
*/
enum class Language {
  NO_LANGUAGE,
  RUSSIAN,
  ENGLISH
};

/**
  @brief Enumerator containing all possible word association measures

  This enumerator currently contains Term Frequency, Mutual Information,
  Augmented Mutual Information, Normalized Mutual Information, True Mutual
  Information, Cubic Mutual Information, Symmetrical Conditional Probability,
  Lexical Cohesion, Gravity Count, Z-Score, T-Score, Dice Coefficient, Modified
  Dice Coefficient, Simple Matching Coefficient, Kulczinsky Coefficient, Ochiai
  Coefficient, Yule Coefficient, Jaccard Coefficient, Chi-Square, Loglikelihood
  Ratio, while NO_MEASURE indicates an error
*/
enum class WordAssociationMeasure {
  NO_MEASURE,
  TERM_FREQUENCY,
  MUTUAL_INFORMATION,
  AUGMENTED_MUTUAL_INFORMATION,
  NORMALIZED_MUTUAL_INFORMATION,
  TRUE_MUTUAL_INFORMATION,
  CUBIC_MUTUAL_INFORMATION,
  SYMMETRICAL_CONDITIONAL_PROBABILITY,
  LEXICAL_COHESION,
  GRAVITY_COUNT,
  Z_SCORE,
  T_SCORE,
  DICE_COEFFICIENT,
  MODIFIED_DICE_COEFFICIENT,
  SIMPLE_MATCHING_COEFFICIENT,
  KULCZINSKY_COEFFICIENT,
  OCHIAI_COEFFICIENT,
  YULE_COEFFICIENT,
  JACCARD_COEFFICIENT,
  CHI_SQUARE,
  LOGLIKELIHOOD_RATIO
};

/**
  @brief Enumerator containing all possible parts of speech that we consider

  It contains Adjectives, Adverbs, Nouns, Verbs and Bigrams, while
  NO_PART_OF_SPEECH indicates an error (Genitive Nouns is a subset of Nouns
  necessary to extract Russian bigrams)
*/
enum class PartOfSpeech {
  NO_PART_OF_SPEECH,
  ADJECTIVE,
  ADVERB,
  NOUN,
  GENITIVE_NOUN,
  VERB,
  PREPOSITION,
  DETERMINER,
  BIGRAM
};

/**
  @brief Structure containing all necessary information about extracted ngram

  This structure contains set of all possible parts of speech for the given
  ngram and is frequency
*/
struct NGramItem {
  NGramItem(size_t term_frequency, const std::set<PartOfSpeech>& pos)
      : frequency(term_frequency),
        parts_of_speech(pos) {
  }

  /** Frequency of the word */
  size_t frequency;

  /** Set containing all possible parts of speech for the word */
  std::set<PartOfSpeech> parts_of_speech;
};

/**
  @brief Structure derived from NGramItem by adding index of the word

  This structure contains set of all possible parts of speech for the given
  ngram, its frequency and index
*/
struct NGramIndexItem : public NGramItem {
  NGramIndexItem(size_t number,
                 size_t term_frequency,
                 const std::set<PartOfSpeech>& pos)
      : NGramItem(term_frequency, pos),
        index(number) {
  }

  /** Index of the ngram in vocabulary */
  size_t index;
};

/**
  @brief Structure containing number of different words occurred to the left or
  to the right of unigram (in extractted bigrams)
*/
struct PrePostOccurrences {
  /** Number of different words occurred to the left of the unigram */
  size_t pre_occurrences_number = 0;

  /** Number of different words occurred to the right of the unigram */
  size_t post_occurrences_number = 0;
};

#endif  // EXPERIMENTS_LDAINPUTCREATOR_AUXILIARY_H_
