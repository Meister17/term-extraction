// Copyright 2014 Michael Nokel
#include "./program_options_parser.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

using boost::program_options::notify;
using boost::program_options::options_description;
using boost::program_options::parse_command_line;
using boost::program_options::parse_config_file;
using boost::program_options::store;
using boost::program_options::value;
using boost::program_options::variables_map;
using boost::algorithm::to_lower;
using boost::filesystem::create_directories;
using boost::filesystem::path;
using boost::is_any_of;
using boost::split;
using std::cout;
using std::runtime_error;
using std::string;
using std::vector;

void ProgramOptionsParser::Parse(int argc, char** argv) {
  variables_map program_options_storage;
  string configuration_filename = "";
  string language_string = "";
  string word_association_measures_string = "";
  options_description program_options_description("Options for program:");
  program_options_description.add_options()
      ("help,h", value<bool>(&help_message_printed_)->implicit_value(true),
          "Print help message and exit")
      ("configuration_filename,c", value<string>(&configuration_filename),
          "Configuration file")
      ("language", value<string>(&language_string),
          "Language of the text corpus")
      ("minimum_frequency", value<int>(&minimum_frequency_),
          "Minimum frequency of the word to take it into account")
      ("input_directory_name", value<string>(&input_directory_name_),
          "Name of input directory for processing")
      ("unigram_lda_input_filename",
          value<string>(&unigram_lda_input_filename_),
          "File where unigram result LDA input will be printed")
      ("unigram_lda_vocabulary_filename",
          value<string>(&unigram_lda_vocabulary_filename_),
          "File where unigram result LDA vocabulary will be printed")
      ("bigram_lda_input_filename", value<string>(&bigram_lda_input_filename_),
          "File where bigram result LDA input will be printed")
      ("bigram_lda_vocabulary_filename",
          value<string>(&bigram_lda_vocabulary_filename_),
          "File where bigram result LDA vocabulary will be printed")
      ("word_association_measures",
          value<string>(&word_association_measures_string),
          "String containing word association measures to compute")
      ("word_association_directory_name",
          value<string>(&word_association_directory_name_),
          "Directory where bigrams ordered by measures will be placed");
  store(parse_command_line(argc, argv, program_options_description),
        program_options_storage);
  notify(program_options_storage);
  if (!configuration_filename.empty()) {
    store(parse_config_file<char>(configuration_filename.data(),
                                  program_options_description),
          program_options_storage);
    notify(program_options_storage);
  }
  if (help_message_printed_) {
    cout << program_options_description << "\n";
  }
  if (minimum_frequency_ <= 0) {
    throw runtime_error("Wrong minimum frequency is given");
  }
  language_ = ParseLanguage(language_string);
  word_association_measures_ =
      ParseWordAssociationMeasures(word_association_measures_string);
  CorrectOutputDirectoryName(&word_association_directory_name_);
}

Language ProgramOptionsParser::ParseLanguage(const string& language_string)
    const {
  string corpus_language = language_string;
  to_lower(corpus_language);
  if (corpus_language == "russian") {
    return Language::RUSSIAN;
  } else if (corpus_language == "english") {
    return Language::ENGLISH;
  } else {
    throw runtime_error("Wrong corpus language has been specified");
  }
}

vector<WordAssociationMeasure>
    ProgramOptionsParser::ParseWordAssociationMeasures(
        const string& word_association_measures_string) const {
  vector<WordAssociationMeasure> word_association_vector;
  vector<string> tokens;
  split(tokens, word_association_measures_string, is_any_of(","));
  for (const auto& token : tokens) {
    string measure = token;
    to_lower(measure);
    if (measure == "term_frequency") {
      word_association_vector.push_back(WordAssociationMeasure::TERM_FREQUENCY);
    } else if (measure == "mutual_information") {
      word_association_vector.push_back(
          WordAssociationMeasure::MUTUAL_INFORMATION);
    } else if (measure == "augmented_mutual_information") {
      word_association_vector.push_back(
          WordAssociationMeasure::AUGMENTED_MUTUAL_INFORMATION);
    } else if (measure == "normalized_mutual_information") {
      word_association_vector.push_back(
          WordAssociationMeasure::NORMALIZED_MUTUAL_INFORMATION);
    } else if (measure == "true_mutual_information") {
      word_association_vector.push_back(
          WordAssociationMeasure::TRUE_MUTUAL_INFORMATION);
    } else if (measure == "cubic_mutual_information") {
      word_association_vector.push_back(
          WordAssociationMeasure::CUBIC_MUTUAL_INFORMATION);
    } else if (measure == "symmetrical_conditional_probability") {
      word_association_vector.push_back(
          WordAssociationMeasure::SYMMETRICAL_CONDITIONAL_PROBABILITY);
    } else if (measure == "lexical_cohesion") {
      word_association_vector.push_back(
          WordAssociationMeasure::LEXICAL_COHESION);
    } else if (measure == "gravity_count") {
      word_association_vector.push_back(WordAssociationMeasure::GRAVITY_COUNT);
    } else if (measure == "z_score") {
      word_association_vector.push_back(WordAssociationMeasure::Z_SCORE);
    } else if (measure == "t_score") {
      word_association_vector.push_back(WordAssociationMeasure::T_SCORE);
    } else if (measure == "dice_coefficient") {
      word_association_vector.push_back(
          WordAssociationMeasure::DICE_COEFFICIENT);
    } else if (measure == "modified_dice_coefficient") {
      word_association_vector.push_back(
          WordAssociationMeasure::MODIFIED_DICE_COEFFICIENT);
    } else if (measure == "simple_matching_coefficient") {
      word_association_vector.push_back(
          WordAssociationMeasure::SIMPLE_MATCHING_COEFFICIENT);
    } else if (measure == "kulczinsky_coefficient") {
      word_association_vector.push_back(
          WordAssociationMeasure::KULCZINSKY_COEFFICIENT);
    } else if (measure == "ochiai_coefficient") {
      word_association_vector.push_back(
          WordAssociationMeasure::OCHIAI_COEFFICIENT);
    } else if (measure == "yule_coefficient") {
      word_association_vector.push_back(
          WordAssociationMeasure::YULE_COEFFICIENT);
    } else if (measure == "jaccard_coefficient") {
      word_association_vector.push_back(
          WordAssociationMeasure::JACCARD_COEFFICIENT);
    } else if (measure == "chi_square") {
      word_association_vector.push_back(WordAssociationMeasure::CHI_SQUARE);
    } else if (measure == "loglikelihood_ratio") {
      word_association_vector.push_back(
          WordAssociationMeasure::LOGLIKELIHOOD_RATIO);
    } else {
      throw runtime_error("Wrong association measure has been specified");
    }
  }
  return word_association_vector;
}

void ProgramOptionsParser::CorrectOutputDirectoryName(string* directory_name)
    const {
  path path_separator("/");
  string path_separator_string = path_separator.make_preferred().native();
  if (directory_name->back() != path_separator_string[0]) {
    directory_name->push_back(path_separator_string[0]);
  }
  create_directories(*directory_name);
}
