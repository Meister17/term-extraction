// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_LDAINPUTCREATOR_PROGRAM_OPTIONS_PARSER_H_
#define EXPERIMENTS_LDAINPUTCREATOR_PROGRAM_OPTIONS_PARSER_H_

#include <string>
#include <vector>
#include "./auxiliary.h"

/**
  @brief Class for working with program options

  This class should be used for parsing program options from command line and/or
  from configuration file. After what one can access each necessary program
  option
*/
class ProgramOptionsParser {
 public:
  /**
    Parses program options from command line and/or from configuration file
    @param argc number of command line arguments (from main function)
    @param argv array containing command line arguments (from main function)
    @throw std::runtime_error in case of any occurred error
  */
  void Parse(int argc, char** argv);

 private:
  /** Flag indicating whether help message has been already printed or not */
  bool help_message_printed_ = false;

  /** Language of the text corpus */
  Language language_ = Language::NO_LANGUAGE;

  /** Name of input directory for processing */
  std::string input_directory_name_ = "";

  /** Minimum frequency of the word that should be taken into account */
  int minimum_frequency_ = 0;

  /** Name of file for printing unigram result input for LDA */
  std::string unigram_lda_input_filename_ = "";

  /** Name of file for printing unigram result vocabulary for LDA */
  std::string unigram_lda_vocabulary_filename_ = "";

  /** Name of file for printing bigram result input for LDA */
  std::string bigram_lda_input_filename_ = "";

  /** Name of file for printing bigram result vocabulary for LDA */
  std::string bigram_lda_vocabulary_filename_ = "";

  /** Vector containing all word association measures to compute */
  std::vector<WordAssociationMeasure> word_association_measures_;

  /** Name of directory for printing bigrams ordered by computed word
      association measures */
  std::string word_association_directory_name_ = "";

 public:
  bool help_message_printed() const noexcept {
    return help_message_printed_;
  }

  Language language() const noexcept {
    return language_;
  }

  int minimum_frequency() const noexcept {
    return minimum_frequency_;
  }

  std::string input_directory_name() const noexcept {
    return input_directory_name_;
  }

  std::string unigram_lda_input_filename() const noexcept {
    return unigram_lda_input_filename_;
  }

  std::string unigram_lda_vocabulary_filename() const noexcept {
    return unigram_lda_vocabulary_filename_;
  }

  std::string bigram_lda_input_filename() const noexcept {
    return bigram_lda_input_filename_;
  }

  std::string bigram_lda_vocabulary_filename() const noexcept {
    return bigram_lda_vocabulary_filename_;
  }

  std::vector<WordAssociationMeasure> word_association_measures()
      const noexcept {
    return word_association_measures_;
  }

  std::string word_association_directory_name() const noexcept {
    return word_association_directory_name_;
  }

 private:
  /**
    Parses language from its string representation
    @param language_string string representation of language
    @return parsed language
    @throw std::runtime_error in case of wrong language
  */
  Language ParseLanguage(const std::string& language_string) const;

  /**
    Creates vector containing word association measures to compute
    @param word_association_measures_string string containing all word
    association measures to compute separated by comma
    @return vector containing word association measures to compute
  */
  std::vector<WordAssociationMeasure> ParseWordAssociationMeasures(
      const std::string& word_association_measures_string) const;

  /**
    Checks output directory name, corrects it and creates the directory if it
    doesn't exist
    @param[out] directory_name name of directory to check and correct
    @throw std::exception in case of any occurred error
  */
  void CorrectOutputDirectoryName(std::string* directory_name) const;
};

#endif  // EXPERIMENTS_LDAINPUTCREATOR_PROGRAM_OPTIONS_PARSER_H_
