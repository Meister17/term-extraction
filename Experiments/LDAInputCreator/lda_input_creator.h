// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_LDAINPUTCREATOR_LDA_INPUT_CREATOR_H_
#define EXPERIMENTS_LDAINPUTCREATOR_LDA_INPUT_CREATOR_H_

#include <boost/thread.hpp>
#include <string>
#include <vector>
#include "./auxiliary.h"
#include "./measures_computers/measures_computer.h"
#include "./ngrams_accumulators/ngrams_accumulator.h"


/**
  @brief Class for creating input for LDA from the given input directory

  This class should be used for processing input directory and forming input and
  vocabulary for LDA
*/
class LDAInputCreator {
 public:
  /**
    Initializes object
    @param language using language of the text corpus
    @param minimum_frequency minimum frequency of the ngram to be considered
  */
  LDAInputCreator(const Language& language,
                  int minimum_frequency)
      : kLanguage_(language),
        kMinimumFrequency_(minimum_frequency),
        measures_computer_(&unigrams_accumulator_, &bigrams_accumulator_) {
  }

 private:
  /** Using language of the text corpus */
  const Language kLanguage_ = Language::NO_LANGUAGE;

  /** Minimum frequency of ngram to be considered */
  const int kMinimumFrequency_ = 0;

  /** Number of threads to use it in thread pool */
  const int kNumberOfThreads_ =
      static_cast<int>(boost::thread::hardware_concurrency());

  /** Mutex for synchronizaion processing results */
  boost::mutex mutex_;

  /** Object used to accumulate extracted unigrams */
  NGramsAccumulator unigrams_accumulator_;

  /** Object used to accumulate extracted bigrams */
  NGramsAccumulator bigrams_accumulator_;

  /** Object used for computing measures */
  MeasuresComputer measures_computer_;

 public:
  /**
    Processes given directory in a thread pool and creates result LDA input
    and vocabulary
    @param directory_name input directory that should be processed
    @throw std::runtime_error in case of any occurred error
  */
  void CreateLDAInput(const std::string& directory_name);

  /**
    Prints unigram and bigram result LDA input and vocabulary to the given files
    @param unigram_lda_input_filename file where unigram result LDA input will
    be printed
    @param unigram_lda_vocabulary_filename file where unigram result LDA
    vocabulary will be printed
    @param bigram_lda_input_filename file where bigram result LDA input will be
    printed
    @param bigram_lda_vocabulary_filename file where bigram result LDA
    vocabulary will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void PrintResults(const std::string& unigram_lda_input_filename,
                    const std::string& unigram_lda_vocabulary_filename,
                    const std::string& bigram_lda_input_filename,
                    const std::string& bigram_lda_vocabulary_filename) const;

  /**
    Calculates given word association measures and prints result to the text
    files inside given directory
    @param measures vector containing word association measures to compute
    @param directory_name name of directory for printing
    @throw std::runtime_error in case of any occurred error
  */
  void CalculateWordAssociationMeasures(
      const std::vector<WordAssociationMeasure>& measures,
      const std::string& directory_name) const;

 private:
  /**
    Processes input directory and processes each found text file
    @param directory_name name of directory to process
  */
  void ProcessInputDirectory(const std::string& directory_name);

  /**
    Processes Russian file in its own thread
    @param filename file that should be processed
    @param file_number number of processing file
    @throw std::runtime_error in case of any occurred error
  */
  void ProcessRussianFile(const std::string& filename, size_t file_number);

  /**
    Processes English file in its own thread
    @param filename file that should be processed
    @param file_number number of processing file
    @throw std::runtime_error in case of any occurred error
  */
  void ProcessEnglishFile(const std::string& filename, size_t file_number);
};

#endif  // EXPERIMENTS_LDAINPUTCREATOR_LDA_INPUT_CREATOR_H_
