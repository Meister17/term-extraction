// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_LDAINPUTCREATOR_RUSSIAN_NGRAMS_EXTRACTORS_RUSSIAN_BIGRAMS_EXTRACTOR_H_
#define EXPERIMENTS_LDAINPUTCREATOR_RUSSIAN_NGRAMS_EXTRACTORS_RUSSIAN_BIGRAMS_EXTRACTOR_H_

#include <set>
#include <string>
#include <unordered_map>
#include "./russian_ngrams_extractor.h"
#include "../auxiliary.h"

/**
  @brief Class that should be used for extracting bigrams from the parsing file

  his class should be used to form statistics for all bigrams extracted during
  parsing file
*/
class RussianBigramsExtractor : public RussianNGramsExtractor {
 public:
  /**
    Adds newly extracted words that have same form to the hash map containing
    all extracted bigrams
    @param words hash map containing newly extracted words and their parts of
    speech to add
  */
  void AddWords(
      const std::unordered_map<std::string, std::set<PartOfSpeech>>& words)
      noexcept override;

 private:
  /** Hash map containing previously extracted words and their parts of
      speech */
  std::unordered_map<std::string, std::set<PartOfSpeech>> previous_words_;
};

#endif  // EXPERIMENTS_LDAINPUTCREATOR_RUSSIAN_NGRAMS_EXTRACTORS_RUSSIAN_BIGRAMS_EXTRACTOR_H_
