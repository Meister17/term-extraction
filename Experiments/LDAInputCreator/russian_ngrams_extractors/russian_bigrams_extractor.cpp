// Copyright 2014 Michael Nokel
#include <set>
#include <string>
#include <unordered_map>
#include "./russian_bigrams_extractor.h"
#include "../auxiliary.h"

using std::set;
using std::string;
using std::unordered_map;

void RussianBigramsExtractor::AddWords(
    const unordered_map<string, set<PartOfSpeech>>& words) noexcept {
  set<PartOfSpeech> bigrams_set;
  bigrams_set.insert(PartOfSpeech::BIGRAM);
  for (const auto& new_element : words) {
    bool is_genitive = new_element.second.find(PartOfSpeech::GENITIVE_NOUN) !=
        new_element.second.end();
    if (is_genitive || new_element.second.find(PartOfSpeech::NOUN) !=
        new_element.second.end()) {
      for (const auto& previous_element : previous_words_) {
        if (is_genitive ||
            previous_element.second.find(PartOfSpeech::ADJECTIVE) !=
            previous_element.second.end()) {
          NGramItem item(kDefaultFrequency_, bigrams_set);
          auto result = ngrams_.insert(
              {previous_element.first + " " + new_element.first, item});
          if (!result.second) {
            ++result.first->second.frequency;
          }
        }
      }
    }
  }
  previous_words_.clear();
  for (const auto& element : words) {
    set<PartOfSpeech> pos;
    for (const auto& part_of_speech : element.second) {
      if (part_of_speech == PartOfSpeech::GENITIVE_NOUN) {
        pos.insert(PartOfSpeech::NOUN);
      } else if (part_of_speech == PartOfSpeech::NOUN ||
                 part_of_speech == PartOfSpeech::ADJECTIVE) {
        pos.insert(part_of_speech);
      }
    }
    if (!pos.empty()) {
      previous_words_[element.first] = pos;
    }
  }
}
