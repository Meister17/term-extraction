// Copyright 2014 Michael Nokel
#include <iostream>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include "./russian_unigrams_extractor.h"
#include "../auxiliary.h"

using std::set;
using std::string;
using std::unordered_map;
using std::unordered_set;

void RussianUnigramsExtractor::AddWords(
    const unordered_map<string, set<PartOfSpeech>>& words) noexcept {
  unordered_set<string> words_in_bigrams;
  for (const auto& element : words) {
    if (!element.second.empty() && (element.second.size() > 1 ||
        element.second.find(PartOfSpeech::NO_PART_OF_SPEECH) ==
        element.second.end())) {
      set<PartOfSpeech> pos;
      for (const auto& part_of_speech : element.second) {
        if (part_of_speech == PartOfSpeech::GENITIVE_NOUN) {
          pos.insert(PartOfSpeech::NOUN);
        } else {
          pos.insert(part_of_speech);
        }
      }
      NGramItem item(kDefaultFrequency_, pos);
      auto insert_result = ngrams_.insert({element.first, item});
      if (!insert_result.second) {
        ++insert_result.first->second.frequency;
        insert_result.first->second.parts_of_speech.insert(pos.begin(),
                                                           pos.end());
      }
      /*bool is_genitive = element.second.find(PartOfSpeech::GENITIVE_NOUN) !=
          element.second.end();
      if (is_genitive || element.second.find(PartOfSpeech::NOUN) !=
          element.second.end()) {
        for (const auto& previous_element : previous_words_) {
          if (is_genitive ||
              previous_element.second.find(PartOfSpeech::ADJECTIVE) !=
              previous_element.second.end()) {
            auto bigram_result = words_in_bigrams.insert(element.first);
            if (!bigram_result.second) {
              auto found_iterator = ngrams_.find(element.first);
              ++found_iterator->second.frequency;
            }
            bigram_result = previous_words_in_bigrams_.insert(
                previous_element.first);
            if (!bigram_result.second) {
              auto found_iterator = ngrams_.find(previous_element.first);
              ++found_iterator->second.frequency;
            }
          }
        }
      }*/
    }
  }
  /*previous_words_in_bigrams_ = words_in_bigrams;
  previous_words_.clear();
  for (const auto& element : words) {
    set<PartOfSpeech> pos;
    for (const auto& part_of_speech : element.second) {
      if (part_of_speech == PartOfSpeech::GENITIVE_NOUN) {
        pos.insert(PartOfSpeech::NOUN);
      } else if (part_of_speech == PartOfSpeech::NOUN ||
                 part_of_speech == PartOfSpeech::ADJECTIVE) {
        pos.insert(part_of_speech);
      }
    }
    if (!pos.empty()) {
      previous_words_[element.first] = pos;
    }
  }*/
}
