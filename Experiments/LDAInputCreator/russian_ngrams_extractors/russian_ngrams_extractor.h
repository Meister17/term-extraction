// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_LDAINPUTCREATOR_RUSSIAN_NGRAMS_EXTRACTORS_RUSSIAN_NGRAMS_EXTRACTOR_H_
#define EXPERIMENTS_LDAINPUTCREATOR_RUSSIAN_NGRAMS_EXTRACTORS_RUSSIAN_NGRAMS_EXTRACTOR_H_

#include <set>
#include <string>
#include <unordered_map>
#include "../auxiliary.h"

/**
  @brief Class that should be used for extracting ngrams from the parsing file

  This class should be used to form statistics for all ngrams extracted during
  parsing file
*/
class RussianNGramsExtractor {
 public:
  /**
    Adds newly extracted words that have same form to the hash map containing
    all extracted words
    @param words hash map containing newly extracted words and their parts of
    speech to add
  */
  virtual void AddWords(
      const std::unordered_map<std::string, std::set<PartOfSpeech>>& words)
      noexcept = 0;

  /**
    Returns iterator to the beginning of hash map containing extracted ngrams
    with some information about them
    @return start iterator of hash map containing extracted ngrams with some
    information about them
  */
  virtual std::unordered_map<std::string, NGramItem>::const_iterator
      GetStartIterator() const noexcept {
    return ngrams_.begin();
  }

  /**
    Returns iterator to the end of hash map containing extracted ngrams with
    some information about them
    @return end iterator of hash map containing extracted ngrams with some
    information about them
  */
  virtual std::unordered_map<std::string, NGramItem>::const_iterator
      GetEndIterator() const noexcept {
    return ngrams_.end();
  }

 protected:
  /** Default frequency of ngram (equals to one)*/
  const size_t kDefaultFrequency_ = 1;

  /** Hash map containing all necessary information about extracted ngrams */
  std::unordered_map<std::string, NGramItem> ngrams_;
};

#endif  // EXPERIMENTS_LDAINPUTCREATOR_RUSSIAN_NGRAMS_EXTRACTORS_RUSSIAN_NGRAMS_EXTRACTOR_H_
