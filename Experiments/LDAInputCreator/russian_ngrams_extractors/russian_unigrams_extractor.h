// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_LDAINPUTCREATOR_RUSSIAN_NGRAMS_EXTRACTORS_RUSSIAN_UNIGRAMS_EXTRACTOR_H_
#define EXPERIMENTS_LDAINPUTCREATOR_RUSSIAN_NGRAMS_EXTRACTORS_RUSSIAN_UNIGRAMS_EXTRACTOR_H_

#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include "./russian_ngrams_extractor.h"
#include "../auxiliary.h"

/**
  @brief Class that should be used for extracting unigrams from the parsing file

  This class should be used for forming statistics about extracted unigrams
  during parsing file
*/
class RussianUnigramsExtractor : public RussianNGramsExtractor {
 public:
  /**
    Adds given words that have same form to hash map containing all extracted
    unigrams
    @param words hash map containing newly extracted words and their parts of
    speech to add
  */
  void AddWords(
      const std::unordered_map<std::string, std::set<PartOfSpeech>>& words)
      noexcept override;

 private:
  /** Hash map containing previously extracted words and their parts of
      speech */
//  std::unordered_map<std::string, std::set<PartOfSpeech>> previous_words_;

  /** Hash set containing previous words found in bigrams */
  //std::unordered_set<std::string> previous_words_in_bigrams_;
};

#endif  // EXPERIMENTS_LDAINPUTCREATOR_RUSSIAN_NGRAMS_EXTRACTORS_RUSSIAN_UNIGRAMS_EXTRACTOR_H_
