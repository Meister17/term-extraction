// Copyright 2014 Michael Nokel
#include <iostream>
#include <stdexcept>
#include "./lda_input_creator.h"
#include "./program_options_parser.h"

using std::exception;
using std::cerr;

/**
  Main function that starts the program
  @param argc number of command-line arguments
  @param argv command-line arguments
  @return exit code of success
*/
int main(int argc, char** argv) {
  try {
    ProgramOptionsParser program_options_parser;
    program_options_parser.Parse(argc, argv);
    if (program_options_parser.help_message_printed()) {
      return 0;
    }
    LDAInputCreator lda_input_creator(
        program_options_parser.language(),
        program_options_parser.minimum_frequency());
    lda_input_creator.CreateLDAInput(
        program_options_parser.input_directory_name());
    lda_input_creator.PrintResults(
        program_options_parser.unigram_lda_input_filename(),
        program_options_parser.unigram_lda_vocabulary_filename(),
        program_options_parser.bigram_lda_input_filename(),
        program_options_parser.bigram_lda_vocabulary_filename());
    lda_input_creator.CalculateWordAssociationMeasures(
        program_options_parser.word_association_measures(),
        program_options_parser.word_association_directory_name());
    return 0;
  } catch (const exception& except) {
    cerr << "Fatal error occurred: " << except.what() << "\n";
    return 1;
  }
}
