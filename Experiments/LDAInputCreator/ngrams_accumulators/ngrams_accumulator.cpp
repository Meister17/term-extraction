// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <fstream>
#include <map>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "./ngrams_accumulator.h"
#include "../auxiliary.h"

using boost::is_any_of;
using boost::split;
using std::map;
using std::ofstream;
using std::runtime_error;
using std::sort;
using std::string;
using std::to_string;
using std::unordered_map;
using std::unordered_set;
using std::vector;

void NGramsAccumulator::ReserveForFiles(size_t number_files) noexcept {
  lda_input_.resize(number_files, unordered_map<size_t, size_t>());
}

void NGramsAccumulator::AccumulateNGrams(
    unordered_map<string, NGramItem>::const_iterator start_iterator,
    unordered_map<string, NGramItem>::const_iterator end_iterator,
    size_t file_number) noexcept {
  for (auto iterator = start_iterator; iterator != end_iterator; ++iterator) {
    total_number_ngrams_ += iterator->second.frequency;
    size_t item_index = lda_vocabulary_.size();
    NGramIndexItem item(item_index,
                        iterator->second.frequency,
                        iterator->second.parts_of_speech);
    auto insert_result = lda_vocabulary_.insert({iterator->first, item});
    if (!insert_result.second) {
      item_index = insert_result.first->second.index;
      insert_result.first->second.frequency += iterator->second.frequency;
      insert_result.first->second.parts_of_speech.insert(
          item.parts_of_speech.begin(),
          item.parts_of_speech.end());
    }
    lda_input_[file_number][item_index] = iterator->second.frequency;
  }
}

unordered_set<string> NGramsAccumulator::FilterLDAVocabulary(
    const unordered_set<string>& filtering_ngrams,
    size_t minimum_frequency) noexcept {
  unordered_set<string> filtered_ngrams;
  vector<string> vocabulary;
  auto iterator = lda_vocabulary_.begin();
  while (iterator != lda_vocabulary_.end()) {
    vector<string> unigrams;
    split(unigrams, iterator->first, is_any_of(" "));
    bool filter = false;
    for (const auto& unigram: unigrams) {
      if (filtering_ngrams.find(unigram) != filtering_ngrams.end()) {
        filter = true;
        break;
      }
    }
    if (!filter && iterator->second.parts_of_speech.size() < 5 &&
        iterator->second.frequency >= minimum_frequency) {
      vocabulary.push_back(iterator->first);
      ++iterator;
    } else {
      filtered_ngrams.insert(iterator->first);
      iterator = lda_vocabulary_.erase(iterator);
    }
  }
  sort(vocabulary.begin(), vocabulary.end());
  unordered_map<size_t, size_t> reindex;
  for (size_t index = 0; index < vocabulary.size(); ++index) {
    auto iterator = lda_vocabulary_.find(vocabulary[index]);
    reindex[iterator->second.index] = index;
    iterator->second.index = index;
  }
  for (auto& document : lda_input_) {
    unordered_map<size_t, size_t> new_document;
    for (const auto& element : document) {
      auto found_iterator = reindex.find(element.first);
      if (found_iterator != reindex.end()) {
        new_document[found_iterator->second] = element.second;
      }
    }
    document = new_document;
  }
  FillPrePostOccurrences();
  return filtered_ngrams;
}

size_t NGramsAccumulator::GetFrequency(const string& ngram) const noexcept {
  auto found_iterator = lda_vocabulary_.find(ngram);
  if (found_iterator == lda_vocabulary_.end()) {
    return 0;
  }
  return found_iterator->second.frequency;
}

size_t NGramsAccumulator::GetPreOccurrencesNumber(const string& unigram)
    const noexcept {
  auto found_iterator = pre_post_occurrences_.find(unigram);
  if (found_iterator == pre_post_occurrences_.end()) {
    return 0;
  }
  return found_iterator->second.pre_occurrences_number;
}

size_t NGramsAccumulator::GetPostOccurrencesNumber(const string& unigram)
    const noexcept {
  auto found_iterator = pre_post_occurrences_.find(unigram);
  if (found_iterator == pre_post_occurrences_.end()) {
    return 0;
  }
  return found_iterator->second.post_occurrences_number;
}

void NGramsAccumulator::PrintLDAVocabulary(const string& filename) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print LDA vocabulary");
  }
  vector<string> vocabulary(lda_vocabulary_.size());
  for (const auto& item : lda_vocabulary_) {
    vocabulary[item.second.index] = item.first;
  }
  for (const auto& word : vocabulary) {
    file << word << "\t" << lda_vocabulary_.at(word).frequency;
    for (const auto& part_of_speech :
         lda_vocabulary_.at(word).parts_of_speech) {
      switch (part_of_speech) {
        case PartOfSpeech::ADJECTIVE:
          file << "\tA";
          break;
        case PartOfSpeech::NOUN: case PartOfSpeech::GENITIVE_NOUN:
          file << "\tN";
          break;
        case PartOfSpeech::ADVERB:
          file << "\tD";
          break;
        case PartOfSpeech::VERB:
          file << "\tV";
          break;
        case PartOfSpeech::BIGRAM:
          file << "\tB";
          break;
        case PartOfSpeech::NO_PART_OF_SPEECH: case PartOfSpeech::PREPOSITION:
        case PartOfSpeech::DETERMINER: default:
          throw runtime_error("Failed to print LDA vocabulary");
      }
    }
    file << "\t-\n";
  }
  file.close();
}

void NGramsAccumulator::PrintLDAInput(const string& filename) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print LDA input");
  }
  for (const auto& document : lda_input_) {
    map<size_t, size_t> current_document;
    for (const auto& element : document) {
      current_document[element.first] = element.second;
    }
    for (const auto& element : current_document) {
      file << element.first << ":" << element.second << "\t";
    }
    file << "\n";
  }
  file.close();
}

void NGramsAccumulator::FillPrePostOccurrences() noexcept {
  for (const auto& ngram_element : lda_vocabulary_) {
    vector<string> unigrams;
    split(unigrams, ngram_element.first, is_any_of(" "));
    if (unigrams.size() ==  2 || unigrams.size() == 3) {
      auto insert_result = pre_post_occurrences_.insert({unigrams.front(),
                                                         PrePostOccurrences()});
      ++insert_result.first->second.post_occurrences_number;
      insert_result = pre_post_occurrences_.insert({unigrams.back(),
                                                    PrePostOccurrences()});
      ++insert_result.first->second.pre_occurrences_number;
    }
  }
}
