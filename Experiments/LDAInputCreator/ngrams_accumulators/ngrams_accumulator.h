// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_LDAINPUTCREATOR_NGRAMS_ACCUMULATORS_NGRAMS_ACCUMULATOR_H_
#define EXPERIMENTS_LDAINPUTCREATOR_NGRAMS_ACCUMULATORS_NGRAMS_ACCUMULATOR_H_

#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "../auxiliary.h"

/**
  @brief Class for accumulating and printing extracted ngrams

  This class should be used for accumulating extracted ngrams and printing
  resulted vocabulary and input data for LDA
*/
class NGramsAccumulator {
 public:
  /**
    Reserves storage for further processing files
    @param number_files number of files in the whole collection for processing
  */
  void ReserveForFiles(size_t number_files) noexcept;

  /**
    Accumulates extracted ngrams
    @param start_iterator iterator to the beginning of the hash map containing
    extracted words with some information about them
    @param end_iterator iterator to the end of the hash map containing extracted
    words with some information about them
    @param file_number number of processed file
  */
  void AccumulateNGrams(
      std::unordered_map<std::string, NGramItem>::const_iterator start_iterator,
      std::unordered_map<std::string, NGramItem>::const_iterator end_iterator,
      size_t file_number) noexcept;

  /**
    Filters vocabulary created for LDA by removing low frequent items and items
    that can be all parts of speech (Nouns, Adjectives, Adverbs and Verbs)
    @param filtering_words hash set containing ngrams to filter
    @param minimum_frequency minimum frequency to remain item in vocabulary
    @return hash set containing filtered ngrams
  */
  std::unordered_set<std::string> FilterLDAVocabulary(
      const std::unordered_set<std::string>& filtering_ngrams,
      size_t minimum_frequency) noexcept;

  /**
    Prints vocabulary containing ngrams for LDA
    @param filename file where vocabulary should be printed
    @throw std::runtime_error in case of any occurred error
  */
  void PrintLDAVocabulary(const std::string& filename) const;

  /**
    Prints input for LDA
    @param filename file where input for LDA should be printed
    @throw std::runtime_error in case of any occurred error
  */
  void PrintLDAInput(const std::string& filename) const;

  /**
    Returns frequency of the given ngram
    @param ngram ngram to get frequency for
    @return frequency of the given ngram or zero if such ngram wasn't extracted
  */
  size_t GetFrequency(const std::string& ngram) const noexcept;

  /**
    Returns number of different words occurred to the left of the given unigram
    @param unigram unigram to get pre words number
    @return number of different words occurred to the left of the given unigram
  */
  size_t GetPreOccurrencesNumber(const std::string& unigram) const noexcept;

  /**
    Returns number of different words occurred to the right of the given unigram
    @param unigram unigram to get post words number
    @return number of different words occurred to the right of the given unigram
  */
  size_t GetPostOccurrencesNumber(const std::string& unigram) const noexcept;

  /**
    Returs iterator to the beginning of the hash map containing extracted
    ngrams with their indexes and frequencies
    @return iterator to the beginning of the hash map containing extracted
    ngrams with their indexes and frequencies
  */
  std::unordered_map<std::string, NGramIndexItem>::const_iterator
      GetStartIterator() const noexcept {
    return lda_vocabulary_.begin();
  }

  /**
    Returns iterator to the end of the hash map containing extracted ngrams with
    their indexes and frequencies
    @return iterator to the end of the hash map containing extracted ngrams with
    their indexes and frequencies
  */
  std::unordered_map<std::string, NGramIndexItem>::const_iterator
      GetEndIterator() const noexcept {
    return lda_vocabulary_.end();
  }

  size_t total_number_ngrams() const noexcept {
    return total_number_ngrams_;
  }

 private:
  /** Total number of ngrams found in text collection */
  size_t total_number_ngrams_ = 0;

  /** Hash map containing all extracted words with indexes and frequencies in
      vocabulary */
  std::unordered_map<std::string, NGramIndexItem> lda_vocabulary_;

  /** Vector containing lda input for further printing */
  std::vector<std::unordered_map<size_t, size_t>> lda_input_;

  /** Hash map containing all extracted unigrams with pre and post occurrences
      numbers in extracted bigrams */
  std::unordered_map<std::string, PrePostOccurrences> pre_post_occurrences_;

  /**
    Fill pre and post occurrences for unigrams
  */
  void FillPrePostOccurrences() noexcept;
};

#endif  // EXPERIMENTS_LDAINPUTCREATOR_NGRAMS_ACCUMULATORS_NGRAMS_ACCUMULATOR_H_
