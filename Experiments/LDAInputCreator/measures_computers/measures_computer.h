// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_LDAINPUTCREATOR_MEASURES_COMPUTERS_MEASURES_COMPUTER_H_
#define EXPERIMENTS_LDAINPUTCREATOR_MEASURES_COMPUTERS_MEASURES_COMPUTER_H_

#include <boost/thread.hpp>
#include <map>
#include <string>
#include <vector>
#include "../auxiliary.h"

class NGramsAccumulator;

class MeasuresComputer {
 public:
  /**
    Creates and initializes object
    @param unigrams_accumulator pointer to the object containing frequencies of
    all extracted unigrams
    @param bigrams_accumulator pointer to the object containing frequencies of
    all extracted bigrams
  */
  MeasuresComputer(const NGramsAccumulator* unigrams_accumulator,
                   const NGramsAccumulator* bigrams_accumulator)
      : unigrams_accumulator_(unigrams_accumulator),
        bigrams_accumulator_(bigrams_accumulator) {
  }

 private:
  /** Number of threads to use in pool */
  const int kNumberOfThreads_ =
      static_cast<int>(boost::thread::hardware_concurrency());

  /** Default number of words for pre-/post- occurrences counters */
  const size_t kDefaultWordsNumber_ = 1;

  /** Pointer to the class containing frequencies of all unigrams */
  const NGramsAccumulator* unigrams_accumulator_;

  /** Pointer to the class containing frequencies of all bigrams */
  const NGramsAccumulator* bigrams_accumulator_;

 public:
  /**
    Computes given word association measures and prints results in separate
    text files in the given directory
    @param measures vector containing word association measures to compute
    @param directory_name directory where results will be written
    @throw std::runtime_error in case of any occurred error
    @note Bigrams will be printed ordered by descending each word association
    measure
  */
  void ComputeMeasures(const std::vector<WordAssociationMeasure>& measures,
                       const std::string& directory_name) const;

 private:
  /**
    Computes Term Frequency for each extracted bigram and prints them ordered by
    descending of values
    @param filename file where bigrams ordered by Term Frequency will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeTermFrequency(const std::string& filename) const;

  /**
    Computes Mutual Information for the given bigram
    @param bigram_frequency frequency of the bigram to compute MI for
    @param left_unigram_frequency frequency of the left unigram forming bigram
    @param right_unigram_frequency frequency of the right unigram forming bigram
    @return Mutual Information of the given bigram
  */
  double ComputeMIForBigram(size_t bigram_frequency,
                            size_t left_unigram_frequency,
                            size_t right_unigram_frequency) const noexcept;

  /**
    Computes Mutual Information for each extracted bigram and prints them
    ordered by descending of values
    @param filename file where bigrams ordered by Mutual Information will be
    printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeMutualInformation(const std::string& filename) const;

  /**
    Computes Augmeneted Mutual Information for each extracted bigram and prints
    them ordered by descending of values
    @param filename file where bigrams ordered by Augmented Mutual Information
    will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeAugmentedMutualInformation(const std::string& filename) const;

  /**
    Computes Normalized Mutual Information for each extracted bigram and prints
    them ordered by descending of values
    @param filename file where bigrams ordered by Normalized Mutual Information
    will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeNormalizedMutualInformation(const std::string& filename) const;

  /**
    Computes True Mutual Information for each extracted bigram and prints them
    ordered by descending of values
    @param filename file where bigrams ordered by True Mutual Information will
    be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeTrueMutualInformation(const std::string& filename) const;

  /**
    Computes Cubic Mutual Information for each extracted bigram and prints them
    ordered by descending of values
    @param filename file where bigrams ordered by Cubic Mutual Information will
    be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeCubicMutualInformation(const std::string& filename) const;

  /**
    Computes Symmetrical Conditional Probability for each extracted bigram and
    prints them ordered by descending of values
    @param filename file where bigrams ordered by Symmetrical Conditional
    Probability will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeSymmetricalConditionalProbability(const std::string& filename)
      const;

  /**
    Computes Lexical Cohesion for each extracted bigram and prints them ordered
    by descending of values
    @param filename file where bigrams ordered by Lexical Cohesion will be
    printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeLexicalCohesion(const std::string& filename) const;

  /**
    Computes Gravity Count for each extracted bigram and prints them ordered by
    descending of values
    @param filename file where bigrams ordered by Gravity Count will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeGravityCount(const std::string& filename) const;

  /**
    Computes Z-Score for each extracted bigram and prints them ordered by
    descending of values
    @param filename file where bigrams ordered by Z-Score will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeZScore(const std::string& filename) const;

  /**
    Computes T-Score for each extracted bigram and prints them ordered by
    descending of values
    @param filename file where bigrams ordered by T-Score will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeTScore(const std::string& filename) const;

  /**
    Computes Dice Coefficient for each extracted bigram and prints them ordered
    by descending of values
    @param filename file where bigrams ordered by Dice Coefficient will be
    printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeDiceCoefficient(const std::string& filename) const;

  /**
    Computes Modified Dice Coefficient for each extracted bigram and prints them
    ordered by descending of values
    @param filename file where bigrams ordered by Modified Dice Coefficient will
    be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeModifiedDiceCoefficient(const std::string& filename) const;

  /**
    Computes Simple Matching Coefficient for each extracted bigram and prints
    them ordered by descending of values
    @param filename file where bigrams ordered by Simple Matching Coefficient
    will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeSimpleMatchingCoefficient(const std::string& filename) const;

  /**
    Computes Kulczinsky Coefficient for each extracted bigram and prints them
    ordered by descending of values
    @param filename file where bigrams ordered by Loglikelihood Ratio will be
    printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeKulczinskyCoefficient(const std::string& filename) const;

  /**
    Computes Ochiai Coefficient for each extracted bigram and prints them
    ordered by descending of values
    @param filename file where bigrams ordered by Ochiai Coefficient will be
    printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeOchiaiCoefficient(const std::string& filename) const;

  /**
    Computes Yule Coefficient for each extracted bigram and prints them ordered
    by descending of values
    @param filename file where bigrams ordered by Yule Coefficient will be
    printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeYuleCoefficient(const std::string& filename) const;

  /**
    Computes Jaccard Coefficient for each extracted bigram and prints them
    ordered by descending of values
    @param filename file where bigrams ordered by Jaccard Coefficient will be
    printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeJaccardCoefficient(const std::string& filename) const;

  /**
    Computes Chi-Square for each extracted bigram and prints them ordered by
    descending of values
    @param filename file where bigram ordered by Chi-Square will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeChiSquare(const std::string& filename) const;

  /**
    Computes LogLikelihood Ratio part for given bigram
    @param bigram_frequency frequency of the given bigram
    @param left_unigram_frequency frequency of the left unigram forming given
    bigram
    @param right_unigram_frequency frequency of the right unigram forming given
    bigram
    @return LogLikelihood Ratio part for given bigram
  */
  double ComputeLogLikelihoodRatioPart(size_t bigram_frequency,
                                       size_t left_unigram_frequency,
                                       size_t right_unigram_frequency)
                                       const noexcept;

  /**
    Computes LogLikelihood Ratio for each extracted bigram and prints them
    ordered by descending of values
    @param filename file where bigrams ordered by LogLikelihood Ratio will be
    printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeLogLikelihoodRatio(const std::string& filename) const;

  /**
    Returns first and last unigram forming given bigram
    @param bigram bigram to get unigrams for
    @param[out] left_unigram first unigram forming given bigram
    @param[out] right_unigram last unigram forming given bigram
  */
  void GetUnigrams(const std::string& bigram,
                   std::string* left_unigram,
                   std::string* right_unigram) const noexcept;

  /**
    Returns frequencies of both unigrams that form the whole bigram
    @param bigram bigram to get unigram frequencies for
    @param[out] left_unigram_frequency frequency of the left unigram forming
    bigram
    @param[out] right_unigram_frequency frequency of the right unigram forming
    bigram
  */
  void GetUnigramFrequencies(const std::string& bigram,
                             size_t& left_unigram_frequency,
                             size_t& right_unigram_frequency) const noexcept;

  /**
    Returns pre and post frequencies of the unigrams forming given bigram
    @param bigram bigram to get pre- and post- frequencies for
    @param[out] left_post_frequency number of different words occurred to the
    right of the first unigram forming given bigram
    @param[out] right_pre_frequency number of different words occurred to the
    left of the last unigram forming given bigram
  */
  void GetPrePostFrequencies(const std::string& bigram,
                             size_t& left_post_frequency,
                             size_t& right_pre_frequency) const noexcept;
  /**
    Inserts computed weight for given bigram in the given storage
    @param bigram bigram to insert weight for
    @param weight computed weight to insert
    @param[out] weights storage containing all bigrams with their weights
  */
  void InsertWeight(const std::string& bigram,
                    double weight,
                    std::map<double, std::vector<std::string>>* weights)
                    const noexcept;

  /**
    Prints bigrams in descending order by given weights
    @param filename file where bigrams ordered by descending order of weights
    will be printed
    @param[out] weights storage containing all bigrams with their weights (each
    item will be sorted)
    @throw std::runtime_error in case of any occurred error
  */
  void PrintBigrams(const std::string& filename,
                    std::map<double, std::vector<std::string>>* weights)
                    const;
};

#endif  // EXPERIMENTS_LDAINPUTCREATOR_MEASURES_COMPUTERS_MEASURES_COMPUTER_H_
