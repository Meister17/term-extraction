// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <boost/threadpool.hpp>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <map>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>
#include "./measures_computer.h"
#include "../auxiliary.h"
#include "../ngrams_accumulators/ngrams_accumulator.h"

using boost::is_any_of;
using boost::split;
using boost::threadpool::pool;
using std::map;
using std::max;
using std::ofstream;
using std::runtime_error;
using std::sort;
using std::string;
using std::vector;

void MeasuresComputer::ComputeMeasures(
    const vector<WordAssociationMeasure>& measures,
    const string& directory_name) const {
  pool thread_pool(kNumberOfThreads_);
  for (const auto& measure : measures) {
    switch (measure) {
      case WordAssociationMeasure::TERM_FREQUENCY:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeTermFrequency,
            this,
            directory_name + "term_frequency.txt"));
      case WordAssociationMeasure::MUTUAL_INFORMATION:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeMutualInformation,
            this,
            directory_name + "mutual_information.txt"));
        break;
      case WordAssociationMeasure::AUGMENTED_MUTUAL_INFORMATION:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeAugmentedMutualInformation,
            this,
            directory_name + "augmented_mutual_information.txt"));
        break;
      case WordAssociationMeasure::NORMALIZED_MUTUAL_INFORMATION:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeNormalizedMutualInformation,
            this,
            directory_name + "normalized_mutual_infromation.txt"));
        break;
      case WordAssociationMeasure::TRUE_MUTUAL_INFORMATION:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeTrueMutualInformation,
            this,
            directory_name + "true_mutual_information.txt"));
        break;
      case WordAssociationMeasure::CUBIC_MUTUAL_INFORMATION:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeCubicMutualInformation,
            this,
            directory_name + "cubic_mutual_information.txt"));
        break;
      case WordAssociationMeasure::SYMMETRICAL_CONDITIONAL_PROBABILITY:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeSymmetricalConditionalProbability,
            this,
            directory_name + "symmetrical_conditional_probability.txt"));
        break;
      case WordAssociationMeasure::LEXICAL_COHESION:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeLexicalCohesion,
            this,
            directory_name + "lexical_cohesion.txt"));
        break;
      case WordAssociationMeasure::GRAVITY_COUNT:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeGravityCount,
            this,
            directory_name + "gravity_count.txt"));
        break;
      case WordAssociationMeasure::Z_SCORE:
        thread_pool.schedule(boost::bind(&MeasuresComputer::ComputeZScore,
                                         this,
                                         directory_name + "z_score.txt"));
      case WordAssociationMeasure::T_SCORE:
        thread_pool.schedule(boost::bind(&MeasuresComputer::ComputeTScore,
                                         this,
                                         directory_name + "t_score.txt"));
        break;
      case WordAssociationMeasure::DICE_COEFFICIENT:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeDiceCoefficient,
            this,
            directory_name + "dice_coefficient.txt"));
      case WordAssociationMeasure::MODIFIED_DICE_COEFFICIENT:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeModifiedDiceCoefficient,
            this,
            directory_name + "modified_dice_coefficient.txt"));
        break;
      case WordAssociationMeasure::SIMPLE_MATCHING_COEFFICIENT:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeSimpleMatchingCoefficient,
            this,
            directory_name + "simple_matching_coefficient.txt"));
        break;
      case WordAssociationMeasure::KULCZINSKY_COEFFICIENT:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeKulczinskyCoefficient,
            this,
            directory_name + "kulczinsky_coefficient.txt"));
        break;
      case WordAssociationMeasure::OCHIAI_COEFFICIENT:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeOchiaiCoefficient,
            this,
            directory_name + "ochiai_coefficient.txt"));
        break;
      case WordAssociationMeasure::YULE_COEFFICIENT:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeYuleCoefficient,
            this,
            directory_name + "yule_coefficient.txt"));
        break;
      case WordAssociationMeasure::JACCARD_COEFFICIENT:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeJaccardCoefficient,
            this,
            directory_name + "jaccard_coefficient.txt"));
        break;
      case WordAssociationMeasure::CHI_SQUARE:
        thread_pool.schedule(boost::bind(&MeasuresComputer::ComputeChiSquare,
                                         this,
                                         directory_name + "chi_square.txt"));
        break;
      case WordAssociationMeasure::LOGLIKELIHOOD_RATIO:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeLogLikelihoodRatio,
            this,
            directory_name + "loglikelihood_ratio.txt"));
        break;
      case WordAssociationMeasure::NO_MEASURE: default:
        throw runtime_error("Failed to compute word association measures");
    }
  }
}

void MeasuresComputer::ComputeTermFrequency(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    InsertWeight(iterator->first, iterator->second.frequency, &weights);
  }
  PrintBigrams(filename, &weights);
}

double MeasuresComputer::ComputeMIForBigram(size_t bigram_frequency,
                                            size_t left_unigram_frequency,
                                            size_t right_unigram_frequency)
                                            const noexcept {
  return log(static_cast<double>(unigrams_accumulator_->total_number_ngrams()) *
      static_cast<double>(bigram_frequency) /
      (static_cast<double>(left_unigram_frequency) *
       static_cast<double>(right_unigram_frequency)));
}

void MeasuresComputer::ComputeMutualInformation(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = ComputeMIForBigram(iterator->second.frequency,
                                       left_frequency,
                                       right_frequency);
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeAugmentedMutualInformation(const string& filename)
    const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double left_multiplier = left_frequency - iterator->second.frequency;
    double right_multiplier = right_frequency - iterator->second.frequency;
    if (left_multiplier == 0) {
      left_multiplier = 1;
    }
    if (right_multiplier == 0) {
      right_multiplier = 1;
    }
    double weight = ComputeMIForBigram(iterator->second.frequency,
                                       left_multiplier,
                                       right_multiplier);
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeNormalizedMutualInformation(
    const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = ComputeMIForBigram(iterator->second.frequency,
                                       left_frequency,
                                       right_frequency) /
        log(static_cast<double>(unigrams_accumulator_->total_number_ngrams()) /
            static_cast<double>(iterator->second.frequency));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeCubicMutualInformation(
    const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = ComputeMIForBigram(iterator->second.frequency,
                                       left_frequency,
                                       right_frequency) +
        log(static_cast<double>(iterator->second.frequency) /
            static_cast<double>(unigrams_accumulator_->total_number_ngrams())) +
        log(static_cast<double>(iterator->second.frequency) /
            static_cast<double>(unigrams_accumulator_->total_number_ngrams()));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeTrueMutualInformation(const string& filename)
    const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = iterator->second.frequency *
        ComputeMIForBigram(iterator->second.frequency,
                           left_frequency,
                           right_frequency);
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeSymmetricalConditionalProbability(
    const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = static_cast<double>(iterator->second.frequency) *
        static_cast<double>(iterator->second.frequency) /
        (static_cast<double>(left_frequency) *
         static_cast<double>(right_frequency));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeLexicalCohesion(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = 2.0 * static_cast<double>(iterator->second.frequency) *
        log10(static_cast<double>(iterator->second.frequency)) /
        static_cast<double>(left_frequency + right_frequency);
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeGravityCount(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    size_t left_post_frequency, right_pre_frequency;
    GetPrePostFrequencies(iterator->first,
                          left_post_frequency,
                          right_pre_frequency);
    double weight = log(static_cast<double>(iterator->second.frequency) *
                        max(static_cast<double>(left_post_frequency),
                            static_cast<double>(kDefaultWordsNumber_)) /
                        static_cast<double>(left_frequency)) +
        log(static_cast<double>(iterator->second.frequency) *
            max(static_cast<double>(right_pre_frequency),
                static_cast<double>(kDefaultWordsNumber_)) /
            static_cast<double>(right_frequency));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeZScore(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = (static_cast<double>(iterator->second.frequency) -
        static_cast<double>(left_frequency) *
        static_cast<double>(right_frequency) /
        static_cast<double>(unigrams_accumulator_->total_number_ngrams())) /
        sqrt(static_cast<double>(left_frequency) *
             static_cast<double>(right_frequency));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeTScore(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = (static_cast<double>(iterator->second.frequency) -
        static_cast<double>(left_frequency) *
        static_cast<double>(right_frequency) /
        static_cast<double>(unigrams_accumulator_->total_number_ngrams())) /
        sqrt(iterator->second.frequency);
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeDiceCoefficient(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = 2.0 * iterator->second.frequency /
        static_cast<double>(left_frequency + right_frequency);
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeModifiedDiceCoefficient(const string& filename)
    const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = log(static_cast<double>(iterator->second.frequency)) *
        2 * static_cast<double>(iterator->second.frequency) /
        static_cast<double>(left_frequency + right_frequency);
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeSimpleMatchingCoefficient(const string& filename)
    const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = (2.0 * static_cast<double>(iterator->second.frequency) +
        static_cast<double>(unigrams_accumulator_->total_number_ngrams() -
                            left_frequency - right_frequency)) /
        static_cast<double>(unigrams_accumulator_->total_number_ngrams());
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeKulczinskyCoefficient(const string& filename)
    const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = 0.5 * static_cast<double>(iterator->second.frequency) *
        (1.0 / static_cast<double>(left_frequency) +
         1.0 / static_cast<double>(right_frequency));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeOchiaiCoefficient(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = static_cast<double>(iterator->second.frequency) /
        sqrt(static_cast<double>(left_frequency) *
             static_cast<double>(right_frequency));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeYuleCoefficient(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = (static_cast<double>(iterator->second.frequency) *
        static_cast<double>(unigrams_accumulator_->total_number_ngrams() +
            iterator->second.frequency - left_frequency - right_frequency) -
        static_cast<double>(left_frequency - iterator->second.frequency) *
        static_cast<double>(right_frequency - iterator->second.frequency)) /
        (static_cast<double>(iterator->second.frequency) *
         static_cast<double>(unigrams_accumulator_->total_number_ngrams() +
            iterator->second.frequency - left_frequency - right_frequency) +
         static_cast<double>(left_frequency - iterator->second.frequency) *
         static_cast<double>(right_frequency - iterator->second.frequency));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeJaccardCoefficient(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = static_cast<double>(iterator->second.frequency) /
        static_cast<double>(left_frequency + right_frequency -
                            iterator->second.frequency);
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeChiSquare(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = static_cast<double>(iterator->second.frequency) -
        static_cast<double>(left_frequency) *
        static_cast<double>(right_frequency) /
        static_cast<double>(unigrams_accumulator_->total_number_ngrams());
    weight *= weight / (static_cast<double>(left_frequency) *
                        static_cast<double>(right_frequency));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

double MeasuresComputer::ComputeLogLikelihoodRatioPart(
    size_t bigram_frequency,
    size_t left_unigram_frequency,
    size_t right_unigram_frequency) const noexcept {
  return static_cast<double>(bigram_frequency) *
      log(static_cast<double>(unigrams_accumulator_->total_number_ngrams()) *
          static_cast<double>(bigram_frequency) /
          (static_cast<double>(left_unigram_frequency) *
           static_cast<double>(right_unigram_frequency)));
}

void MeasuresComputer::ComputeLogLikelihoodRatio(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = bigrams_accumulator_->GetStartIterator(); iterator !=
       bigrams_accumulator_->GetEndIterator(); ++iterator) {
    size_t left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = 2.0 * (
        ComputeLogLikelihoodRatioPart(iterator->second.frequency,
                                      left_frequency,
                                      right_frequency) +
        ComputeLogLikelihoodRatioPart(
            left_frequency - iterator->second.frequency,
            left_frequency,
            unigrams_accumulator_->total_number_ngrams() - right_frequency) +
        ComputeLogLikelihoodRatioPart(
            right_frequency - iterator->second.frequency,
            unigrams_accumulator_->total_number_ngrams() - left_frequency,
            right_frequency) +
        ComputeLogLikelihoodRatioPart(
            unigrams_accumulator_->total_number_ngrams() +
            iterator->second.frequency - left_frequency - right_frequency,
            unigrams_accumulator_->total_number_ngrams() - left_frequency,
            unigrams_accumulator_->total_number_ngrams() - right_frequency));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::GetUnigrams(const string& bigram,
                                   string* left_unigram,
                                   string* right_unigram) const noexcept {
  vector<string> unigrams;
  split(unigrams, bigram, is_any_of(" "));
  if (unigrams.size() != 2 && unigrams.size() != 3) {
    throw runtime_error("Failed to compute word association measures");
  }
  *left_unigram = unigrams.front();
  *right_unigram = unigrams.back();
}

void MeasuresComputer::GetUnigramFrequencies(const string& bigram,
                                             size_t& left_unigram_frequency,
                                             size_t& right_unigram_frequency)
                                             const noexcept {
  string left_unigram, right_unigram;
  GetUnigrams(bigram, &left_unigram, &right_unigram);
  left_unigram_frequency = unigrams_accumulator_->GetFrequency(left_unigram);
  right_unigram_frequency = unigrams_accumulator_->GetFrequency(right_unigram);
}

void MeasuresComputer::GetPrePostFrequencies(const string& bigram,
                                             size_t& left_post_frequency,
                                             size_t& right_pre_frequency)
                                             const noexcept {
  string left_unigram, right_unigram;
  GetUnigrams(bigram, &left_unigram, &right_unigram);
  left_post_frequency = bigrams_accumulator_->GetPostOccurrencesNumber(
      left_unigram);
  right_pre_frequency = bigrams_accumulator_->GetPreOccurrencesNumber(
      right_unigram);
}

void MeasuresComputer::InsertWeight(const string& bigram,
                                    double weight,
                                    map<double, vector<string>>* weights)
                                    const noexcept {
  auto insert_result = weights->insert({weight, vector<string>()});
  insert_result.first->second.push_back(bigram);
}

void MeasuresComputer::PrintBigrams(const string& filename,
                                    map<double, vector<string>>* weights)
                                    const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print bigrams");
  }
  for (auto iterator = weights->rbegin(); iterator != weights->rend();
       ++iterator) {
    sort(iterator->second.begin(), iterator->second.end());
    for (const auto& bigram : iterator->second) {
      file << bigram << "\t" << iterator->first << "\n";
    }
  }
  file.close();
}
