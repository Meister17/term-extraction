// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_THESAURUSLDAINPUTCREATOR_THESAURUS_LDA_INPUT_CREATOR_H_
#define EXPERIMENTS_THESAURUSLDAINPUTCREATOR_THESAURUS_LDA_INPUT_CREATOR_H_

#include <boost/thread.hpp>
#include <string>
#include <vector>
#include "./aho_corasick.h"
#include "./auxiliary.h"
#include "./terms_accumulator.h"

/**
  @brief Class for creating LDA input with terms from thesaurus

  This class should be used for parsing file with terms from thesaurus and
  creating input for further LDA working
*/
class ThesaurusLDAInputCreator {
 public:
  /**
    Parses file containing terms from thesaurus
    @param filename file containing terms from thesaurus
    @throw std::runtime_error in case of any occurred error
  */
  void ParseThesaurusFile(const std::string& filename);

 private:
  /** Number of threads to use in a thread pool */
  const int kNumberOfThreads_ =
      static_cast<int>(boost::thread::hardware_concurrency());

  /** Mutex for synchronization processing results */
  boost::mutex mutex_;

  /** Object that implements Aho-Corasick algorithm */
  AhoCorasick aho_corasick_;

  /** Object used for accumulating extracted thesaurus terms */
  TermsAccumulator terms_accumulator_;

 public:
  /**
    Analyzes given directory and creates LDA vocabulary with input for LDA
    @param input_directory_name directory with input files for parsing
    @param language language of the text collection
    @param minimum_frequency minimum frequency of the thesaurus term to consider
    it
  */
  void CreateLDAInput(const std::string& input_directory_name,
                      const Language& language,
                      size_t minimum_frequency);

  /**
    Prints vocabulary and input for LDA
    @param lda-vocabulary_filename file for printing LDA vocabulary
    @param lda_input_filename file for printing LDA input
  */
  void PrintResults(const std::string& lda_vocabulary_filename,
                    const std::string& lda_input_filename);

 private:
  /**
    Processes files in a thread pool
    @param language language of the processing files
    @param files vector containing files for processing
  */
  void ProcessFiles(const Language& language,
                    const std::vector<std::string>& files);

  /**
    Processes Russian file in its own thread
    @param filename file that should be processed
    @param file_number number of processing file
    @throw std::runtime_error in case of any occurred error
  */
  void ProcessRussianFile(const std::string& filename,
                          size_t file_number);

  /**
    Processes English file in its own thread
    @param filename file that should be processed
    @param file_number number of processing file
    @throw std::runtime_error in case of any occurred error
  */
  void ProcessEnglishFile(const std::string& filename,
                          size_t file_number);
};

#endif  // EXPERIMENTS_THESAURUSLDAINPUTCREATOR_THESAURUS_LDA_INPUT_CREATOR_H_
