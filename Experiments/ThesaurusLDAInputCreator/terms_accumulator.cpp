// Copyright 2014 Michael Nokel
#include <algorithm>
#include <fstream>
#include <map>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>
#include "./auxiliary.h"
#include "./terms_accumulator.h"

using std::map;
using std::ofstream;
using std::runtime_error;
using std::sort;
using std::string;
using std::unordered_map;
using std::vector;

void TermsAccumulator::ReserveForFiles(size_t number_files) noexcept {
  lda_input_.resize(number_files, unordered_map<size_t, size_t>());
}

void TermsAccumulator::AccumulateTerms(
    unordered_map<string, size_t>::const_iterator start_iterator,
    unordered_map<string, size_t>::const_iterator end_iterator,
    size_t file_number) noexcept {
  for (auto iterator = start_iterator; iterator != end_iterator; ++iterator) {
    size_t term_index = lda_vocabulary_.size();
    TermItem item(term_index, iterator->second);
    auto insert_result = lda_vocabulary_.insert({iterator->first, item});
    if (!insert_result.second) {
      term_index = insert_result.first->second.index;
      insert_result.first->second.frequency += iterator->second;
    }
    lda_input_[file_number][term_index] = iterator->second;
  }
}

void TermsAccumulator::FilterLDAVocabulary(size_t minimum_frequency) {
  vector<string> vocabulary;
  auto iterator = lda_vocabulary_.begin();
  while (iterator != lda_vocabulary_.end()) {
    if (iterator->second.frequency >= minimum_frequency) {
      vocabulary.push_back(iterator->first);
      ++iterator;
    } else {
      iterator = lda_vocabulary_.erase(iterator);
    }
  }
  sort(vocabulary.begin(), vocabulary.end());
  unordered_map<size_t, size_t> reindex;
  for (size_t index = 0; index < vocabulary.size(); ++index) {
    auto iterator = lda_vocabulary_.find(vocabulary[index]);
    if (iterator == lda_vocabulary_.end()) {
      throw runtime_error("Failed to filter LDA vocabulary");
    }
    reindex[iterator->second.index] = index;
    iterator->second.index = index;
  }
  for (auto& document : lda_input_) {
    unordered_map<size_t, size_t> new_document;
    for (const auto& element : document) {
      auto iterator = reindex.find(element.first);
      if (iterator != reindex.end()) {
        new_document[iterator->second] = element.second;
      }
    }
    document = new_document;
  }
}

void TermsAccumulator::PrintLDAInput(const string& filename) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print LDA input");
  }
  for (const auto& document : lda_input_) {
    map<size_t, size_t> current_document;
    for (const auto& element : document) {
      current_document[element.first] = element.second;
    }
    for (const auto& element : current_document) {
      file << element.first << ":" << element.second << "\t";
    }
    file << "\n";
  }
  file.close();
}

void TermsAccumulator::PrintLDAVocabulary(const string& filename) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print LDA vocabulary");
  }
  vector<string> vocabulary(lda_vocabulary_.size());
  for (const auto& item : lda_vocabulary_) {
    vocabulary[item.second.index] = item.first;
  }
  for (const auto& word : vocabulary) {
    file << word << "\t" << lda_vocabulary_.at(word).frequency << "\tB\n";
  }
  file.close();
}
