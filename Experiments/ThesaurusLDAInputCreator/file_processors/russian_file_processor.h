// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_THESAURUSLDAINPUTCREATOR_FILE_PROCESSORS_RUSSIAN_FILE_PROCESSOR_H_
#define EXPERIMENTS_THESAURUSLDAINPUTCREATOR_FILE_PROCESSORS_RUSSIAN_FILE_PROCESSOR_H_

#include <string>
#include <unordered_set>
#include "../aho_corasick.h"
#include "./file_processor.h"

/**
  @brief Class for extracting Russian thesaurus terms from processing file

  This class should be used for extracting Russian thesaurus terms from Russian
  file
*/
class RussianFileProcessor : public FileProcessor {
 public:
  /**
    Explicitly creates object
    @param aho_corasick object for finding all occurrences of special terms in
    processing file
  */
  explicit RussianFileProcessor(const AhoCorasick* aho_corasick)
      : FileProcessor(aho_corasick) {
  }

  /**
    Processes given Russian file and extracts Russian thesaurus terms from it
    along with their frequencies
    @param filename file for processing and extracting thesaurus terms
    @throw std::runtime_error in case of any occurred error
  */
  void ProcessFile(const std::string& filename);

 private:
  /**
    Extracts same word forms from the given file
    @param[out] file pointer to file that should be processed
    @throw std::runtime_error in case of any occurred error
  */
  std::unordered_set<std::string> ExtractSameWordForms(std::ifstream* file);

  /**
    Converts given string from CP-1251 encoding to UTF-8
    @param string_cp1251 string in CP-1251 encoding
    @return string in UTF-8 encoding
  */
  std::string ConvertFromCP1251ToUTF8(const std::string& string_cp1251)
      const noexcept;
};

#endif  // EXPERIMENTS_THESAURUSLDAINPUTCREATOR_FILE_PROCESSORS_RUSSIAN_FILE_PROCESSOR_H_
