// Copyright 2014 Michael Nokel
#include <iconv.h>
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <functional>
#include <stdexcept>
#include <string>
#include <unordered_set>
#include <vector>
#include "../auxiliary.h"
#include "./russian_file_processor.h"

using boost::is_any_of;
using boost::split;
using boost::token_compress_on;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::string;
using std::unordered_set;
using std::vector;

void RussianFileProcessor::ProcessFile(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to process Russian file");
  }
  vector<int> current_vertices;
  string line;
  getline(file, line);
  while (!file.eof()) {
    unordered_set<string> words = ExtractSameWordForms(&file);
    if (words.size() == 1 && words.begin()->empty()) {
      continue;
    }
    unordered_set<string> new_terms;
    aho_corasick_->ReadWords(words, &current_vertices, &new_terms);
    if (!new_terms.empty()) {
      AddTerms(new_terms);
    }
  }
  file.close();
}

unordered_set<string> RussianFileProcessor::ExtractSameWordForms(
    ifstream* file) {
  unordered_set<string> same_word_forms;
  int first_letter = -1;
  do {
    string line;
    getline(*file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    line.erase(line.begin(),
               find_if(line.begin(),
                       line.end(),
                       not1(ptr_fun<int, int>(isspace))));
    if (line.empty()) {
      continue;
    }
    line = ConvertFromCP1251ToUTF8(line);
    vector<string> tokens;
    split(tokens, line, is_any_of(" \t"), token_compress_on);
    if (tokens.size() <= 4) {
      throw runtime_error("Failed to process Russian file");
    }
    string word = tokens[tokens.size() - 2];
    if (tokens[4] == "ЛЕ") {
      same_word_forms.insert(word);
    } else if (tokens[4] == "РЗД") {
      same_word_forms.insert("");
    }
    first_letter = file->get();
  } while (!file->eof() && isspace(first_letter));
  if (!file->eof()) {
    file->unget();
  }
  return same_word_forms;
}

string RussianFileProcessor::ConvertFromCP1251ToUTF8(
    const string& string_cp1251) const noexcept {
  iconv_t cd = iconv_open("utf-8", "cp1251");
  if (cd == (iconv_t) - 1) {
    return "";
  }
  size_t input_size = string_cp1251.length();
  size_t output_size = 2*input_size + 1;
  size_t total_output_size = output_size;
#ifdef _WIN32
  const char* input_string_pointer = string_cp1251.data();
#else
  char* input_string_pointer = const_cast<char*>(string_cp1251.data());
#endif
  char* output_string = new char[output_size];
  char* output_string_pointer = output_string;
  memset(output_string, 0, output_size);
  if ((iconv(cd, &input_string_pointer, &input_size, &output_string_pointer,
             &output_size)) == (size_t) - 1) {
    iconv_close(cd);
    delete[] output_string;
    return "";
  }
  iconv_close(cd);
  string string_utf8 = string(output_string, total_output_size - output_size);
  delete[] output_string;
  return string_utf8;
}