// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <cctype>
#include <fstream>
#include <functional>
#include <stdexcept>
#include <string>
#include <unordered_set>
#include <vector>
#include "./english_file_processor.h"

using boost::is_any_of;
using boost::split;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::string;
using std::unordered_set;
using std::vector;

void EnglishFileProcessor::ProcessFile(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to process English file");
  }
  vector<int> current_vertices;
  while (!file.eof()) {
    string line;
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    line.erase(line.begin(),
               find_if(line.begin(),
                       line.end(),
                       not1(ptr_fun<int, int>(isspace))));
    if (line.empty()) {
      continue;
    }
    vector<string> tokens;
    split(tokens, line, is_any_of("\t"));
    if (tokens.size() != 5) {
      throw runtime_error("Failed to process English file");
    }
    if (tokens[2] == "DT") {
      continue;
    }
    unordered_set<string> words;
    words.insert(tokens[1]);
    unordered_set<string> new_terms;
    aho_corasick_->ReadWords(words, &current_vertices, &new_terms);
    if (!new_terms.empty()) {
      AddTerms(new_terms);
    }
  }
  file.close();
}
