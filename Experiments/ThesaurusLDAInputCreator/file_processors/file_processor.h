// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_THESAURUSLDAINPUTCREATOR_FILE_PROCESSORS_FILE_PROCESSOR_H_
#define EXPERIMENTS_THESAURUSLDAINPUTCREATOR_FILE_PROCESSORS_FILE_PROCESSOR_H_

#include <string>
#include <unordered_map>
#include "../aho_corasick.h"

/**
  @brief Abstract class for processing files in separate threads

  This class should be derived by each class implementing processing files of
  any particular language
*/
class FileProcessor {
 public:
  /**
    Explicitly creates an object
    @param aho_corasick object that should be used for finding all occurrences
    of special terms in processing file
  */
  explicit FileProcessor(const AhoCorasick* aho_corasick)
      : aho_corasick_(aho_corasick) {
  }

  /**
    Processes given file and extracts all found there thesaurus terms with their
    frequencies
    @param filename file for processing and extracting thesaurus terms
  */
  virtual void ProcessFile(const std::string& filename) = 0;

 protected:
  /** Object for finding all occurrences of special terms in processing file */
  const AhoCorasick* aho_corasick_;

  /** Hash map containing all found thesaurus terms with their frequencies */
  std::unordered_map<std::string, size_t> terms_;

 public:
  /**
    Returns start iterator to the hash map containing extracted terms along with
    their frequencies
    @return start iterator to the hash map containing extracted terms along with
    their frequencies
  */
  virtual std::unordered_map<std::string, size_t>::const_iterator
      GetStartIterator() {
    return terms_.begin();
  }

  /**
    Returns end iterator to the hash map containing extracted terms along with
    their frequencies
    @return end iterator to the hash map containing extracted terms along with
    their frequencies
  */
  virtual std::unordered_map<std::string, size_t>::const_iterator
      GetEndIterator() {
    return terms_.end();
  }

 protected:
  /**
    Adds newly extracted terms to the storage
    @param terms hash set containing newly extracted terms to add
  */
  void AddTerms(const std::unordered_set<std::string>& new_terms) {
    for (const auto& term : new_terms) {
      auto insert_result = terms_.insert({term, 1});
      if (!insert_result.second) {
        ++insert_result.first->second;
      }
    }
  }
};

#endif  // EXPERIMENTS_THESAURUSLDAINPUTCREATOR_FILE_PROCESSORS_FILE_PROCESSOR_H_
