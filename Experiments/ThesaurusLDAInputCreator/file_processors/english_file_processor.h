// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_THESAURUSLDAINPUTCREATOR_FILE_PROCESSORS_ENGLISH_FILE_PROCESSOR_H_
#define EXPERIMENTS_THESAURUSLDAINPUTCREATOR_FILE_PROCESSORS_ENGLISH_FILE_PROCESSOR_H_

#include <string>
#include "../aho_corasick.h"
#include "./file_processor.h"

/**
  @brief Class for extracting English thesaurus terms during parsing file

  This class should be used for extracting English thesaurus terms from English
  file
*/
class EnglishFileProcessor : public FileProcessor {
 public:
  /**
    Explicitly creates object
    @param aho_corasick object used for finding all occurrences of special terms
    in a processing file
  */
  explicit EnglishFileProcessor(const AhoCorasick* aho_corasick)
      : FileProcessor(aho_corasick) {
  }

  /**
    Processes given English file and extracts all found there thesaurus terms
    along with their frequencies
    @param filename English file for processing and extracting thesaurus terms
    @throw std::rubtime_error in case of any occurred error
  */
  void ProcessFile(const std::string& filename) override;
};

#endif  // EXPERIMENTS_THESAURUSLDAINPUTCREATOR_FILE_PROCESSORS_ENGLISH_FILE_PROCESSOR_H_
