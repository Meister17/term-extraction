// Copyright 2014 Michael Nokel
#include <boost/filesystem.hpp>
#include <boost/threadpool.hpp>
#include <boost/thread/mutex.hpp>
#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>
#include "./auxiliary.h"
#include "./file_processors/english_file_processor.h"
#include "./file_processors/russian_file_processor.h"
#include "./terms_accumulator.h"
#include "./thesaurus_lda_input_creator.h"

using boost::filesystem::directory_iterator;
using boost::filesystem::is_regular_file;
using boost::threadpool::pool;
using std::cout;
using std::runtime_error;
using std::sort;
using std::string;
using std::vector;

void ThesaurusLDAInputCreator::ParseThesaurusFile(const string& filename) {
  cout << "Parsing file with thesaurus terms\n";
  aho_corasick_.ParseThesaurusFile(filename);
}

void ThesaurusLDAInputCreator::CreateLDAInput(
    const string& input_directory_name,
    const Language& language,
    size_t minimum_frequency) {
  cout << "Creating input for PLSA\n";
  vector<string> files;
  for (directory_iterator iterator(input_directory_name); iterator !=
       directory_iterator(); ++iterator) {
    if (is_regular_file(iterator->status())) {
      files.push_back(iterator->path().string());
    }
  }
  sort(files.begin(), files.end());
  ProcessFiles(language, files);
  terms_accumulator_.FilterLDAVocabulary(minimum_frequency);
}

void ThesaurusLDAInputCreator::PrintResults(
    const string& lda_vocabulary_filename,
    const string& lda_input_filename) {
  cout << "Printing results\n";
  pool thread_pool(kNumberOfThreads_);
  thread_pool.schedule(boost::bind(&TermsAccumulator::PrintLDAVocabulary,
                                   &terms_accumulator_,
                                   lda_vocabulary_filename));
  thread_pool.schedule(boost::bind(&TermsAccumulator::PrintLDAInput,
                                   &terms_accumulator_,
                                   lda_input_filename));
}

void ThesaurusLDAInputCreator::ProcessFiles(const Language& language,
                                            const vector<string>& files) {
  terms_accumulator_.ReserveForFiles(files.size());
  pool thread_pool(kNumberOfThreads_);
  switch (language) {
    case Language::RUSSIAN:
      for (size_t file_number = 0; file_number < files.size(); ++file_number) {
        thread_pool.schedule(boost::bind(
            &ThesaurusLDAInputCreator::ProcessRussianFile,
            this,
            files[file_number],
            file_number));
      }
      break;
    case Language::ENGLISH:
      for (size_t file_number = 0; file_number < files.size(); ++file_number) {
        thread_pool.schedule(boost::bind(
            &ThesaurusLDAInputCreator::ProcessEnglishFile,
            this,
            files[file_number],
            file_number));
      }
      break;
    case Language::NO_LANGUAGE: default:
      throw runtime_error("Wrong language of text corpus");
  }
}

void ThesaurusLDAInputCreator::ProcessRussianFile(const string& filename,
                                                  size_t file_number) {
  RussianFileProcessor russian_file_processor(&aho_corasick_);
  russian_file_processor.ProcessFile(filename);
  boost::mutex::scoped_lock lock(mutex_);
  terms_accumulator_.AccumulateTerms(russian_file_processor.GetStartIterator(),
                                     russian_file_processor.GetEndIterator(),
                                     file_number);
}

void ThesaurusLDAInputCreator::ProcessEnglishFile(const string& filename,
                                                  size_t file_number) {
  EnglishFileProcessor english_file_processor(&aho_corasick_);
  english_file_processor.ProcessFile(filename);
  boost::mutex::scoped_lock lock(mutex_);
  terms_accumulator_.AccumulateTerms(english_file_processor.GetStartIterator(),
                                     english_file_processor.GetEndIterator(),
                                     file_number);
}
