// Copyright 2014 Michael Nokel
#include <iostream>
#include <stdexcept>
#include "./program_options_parser.h"
#include "./thesaurus_lda_input_creator.h"

using std::cerr;
using std::exception;

/**
  Main function that starts the program
  @param argc number of command-line arguments
  @param argv command-line arguments
  #return exit code of success
*/
int main(int argc, char** argv) {
  try {
    ProgramOptionsParser program_options_parser;
    program_options_parser.Parse(argc, argv);
    if (program_options_parser.help_message_printed()) {
      return 0;
    }
    ThesaurusLDAInputCreator thesaurus_lda_input_creator;
    thesaurus_lda_input_creator.ParseThesaurusFile(
        program_options_parser.thesaurus_filename());
    thesaurus_lda_input_creator.CreateLDAInput(
        program_options_parser.input_directory_name(),
        program_options_parser.language(),
        program_options_parser.minimum_frequency());
    thesaurus_lda_input_creator.PrintResults(
        program_options_parser.lda_vocabulary_filename(),
        program_options_parser.lda_input_filename());
    return 0;
  } catch (const exception& except) {
    cerr << "Fatal error occurred: " << except.what() << "\n";
    return 1;
  }
}
