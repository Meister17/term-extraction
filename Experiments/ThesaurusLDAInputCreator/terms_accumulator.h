// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_THESAURUSLDAINPUTCREATOR_TERMS_ACCUMULATOR_H_
#define EXPERIMENTS_THESAURUSLDAINPUTCREATOR_TERMS_ACCUMULATOR_H_

#include <string>
#include <unordered_map>
#include <vector>
#include "./auxiliary.h"

/**
  @brief Class for accumulating and printing extracted thesaurus terms

  This class should be used for accumulating extracted thesaurus terms and
  printing resulted vocabulary and input data for LDA
*/
class TermsAccumulator {
 public:
  /**
    Reserves storage for further processing files
    @param number_files number of files in the whole collection for processing
  */
  void ReserveForFiles(size_t number_files) noexcept;

  /**
    Accumulates extracted thesaurus terms
    @param start_iterator iterator to the beginning of the hash map containing
    extracted terms along with their frequencies
    @param end_itterator iterator to the end of the hash map containing
    extracted terms along with their frequencies
    @param file_number number of processed file
  */
  void AccumulateTerms(
      std::unordered_map<std::string, size_t>::const_iterator start_iterator,
      std::unordered_map<std::string, size_t>::const_iterator end_iterator,
      size_t file_number) noexcept;

  /**
    Filters LDA vocabulary so that only terms with frequencies greater or equal
    to the given one remains in it
    @param minimum_frequency minimum frequency of the term to remain
    @throw std::runtime_error in case of any occurred errors
  */
  void FilterLDAVocabulary(size_t minimum_frequency);

  /**
    Prints input file for LDA
    @param filename file where input data for LDA should be printed
    @throw std::runtime_error in case of any occurred error
  */
  void PrintLDAInput(const std::string& filename) const;

  /**
    Prints vocabulary for LDA
    @param filename file where vocabulary for LDA should be printed
    @throw std::runtime_error in case of any occurred error
  */
  void PrintLDAVocabulary(const std::string& filename) const;

 private:
  /** Vector containing lda input for further processing */
  std::vector<std::unordered_map<size_t, size_t>> lda_input_;

  /** Hash map containing all extracted thesaurus terms with their indexes and
      frequencies */
  std::unordered_map<std::string, TermItem> lda_vocabulary_;
};

#endif  // EXPERIMENTS_THESAURUSLDAINPUTCREATOR_TERMS_ACCUMULATOR_H_
