// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_THESAURUSLDAINPUTCREATOR_AHO_CORASICK_H_
#define EXPERIMENTS_THESAURUSLDAINPUTCREATOR_AHO_CORASICK_H_

#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

/**
  @brief Class that implements Aho-Corasick algorithm

  This class implements Aho-Corasick algorithm based on individual words. It
  should be used for identifying thesaurus terms from the text collection
*/
class AhoCorasick {
 public:
  AhoCorasick();

  /**
    Parses file containing thesaurus terms
    @param filename file containing thesaurus terms
    @throw std::runtime_error in case of any occurred error
  */
  void ParseThesaurusFile(const std::string& filename);

 private:
  /** Empty transition in the trie */
  const int kEmptyTransition_ = -1;

  /** Vector containing thesaurus terms */
  std::vector<std::string> terms_;

  /** Vector containing transition functions */
  std::vector<std::unordered_map<std::string, int>> transition_functions_;

  /** Vector containing terminal vertexes */
  std::vector<std::vector<int>> terminal_vertices_;

  /** Vector containing fault functions */
  std::vector<int> fault_functions_;

  /** Vector containing compressed fault functions */
  std::vector<int> compressed_fault_functions_;

 public:
  /**
    Reads words and tries to find thesaurus terms among them
    @param words hash set containing new words
    @param[out] current_vertices vector containing current positions in trie
    @oaram[out] new_occurrences hash set with new found thesaurus terms
  */
  void ReadWords(const std::unordered_set<std::string>& words,
                 std::vector<int>* current_vertices,
                 std::unordered_set<std::string>* new_occurrences)
                 const noexcept;

 private:
  /**
    Adds new term to the trie
    @param term new term to add to the trie
  */
  void AddTerm(const std::string& term) noexcept;

  /**
    Initializes fault functions for Aho-Corasick algorithm
  */
  void InitializeFaultFunctions() noexcept;

  /**
    Compresses fault functions for Aho-Corasick algorithm
  */
  void CompressFaultFunctions() noexcept;
};


#endif  // EXPERIMENTS_THESAURUSLDAINPUTCREATOR_AHO_CORASICK_H_
