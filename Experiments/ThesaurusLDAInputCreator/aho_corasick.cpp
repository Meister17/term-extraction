// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <cctype>
#include <fstream>
#include <functional>
#include <queue>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "./aho_corasick.h"

using boost::is_any_of;
using boost::split;
using std::count;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::queue;
using std::runtime_error;
using std::sort;
using std::string;
using std::unique;
using std::unordered_map;
using std::unordered_set;
using std::vector;

AhoCorasick::AhoCorasick() {
  transition_functions_.push_back(unordered_map<string, int>());
  terminal_vertices_.push_back(vector<int>());
}

void AhoCorasick::ParseThesaurusFile(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse file containing thesaurus terms");
  }
  while (!file.eof()) {
    string line;
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (!line.empty() && count(line.begin(), line.end(), ' ') > 0) {
      AddTerm(line);
    }
  }
  file.close();
  InitializeFaultFunctions();
}

void AhoCorasick::ReadWords(const unordered_set<string>& words,
                            vector<int>* current_vertices,
                            unordered_set<string>* new_occurrences)
                            const noexcept {
  if (current_vertices->empty()) {
    current_vertices->push_back(0);
  }
  for (const auto& word : words) {
    for (size_t index = 0; index < current_vertices->size(); ++index) {
      while (transition_functions_[current_vertices->at(index)].find(word) ==
             transition_functions_[current_vertices->at(index)].end() &&
             current_vertices->at(index) != 0) {
        current_vertices->at(index) =
            fault_functions_[current_vertices->at(index)];
      }
      auto iterator = transition_functions_[current_vertices->at(index)].find(
          word);
      if (iterator !=
          transition_functions_[current_vertices->at(index)].end()) {
        current_vertices->at(index) =
            transition_functions_[current_vertices->at(index)].at(word);
      }
      for (auto term_index : terminal_vertices_[current_vertices->at(index)]) {
        new_occurrences->insert(terms_[term_index]);
      }
      int vertex = compressed_fault_functions_[current_vertices->at(index)];
      while (vertex != kEmptyTransition_) {
        for (auto index : terminal_vertices_[vertex]) {
          new_occurrences->insert(terms_[index]);
        }
        vertex = compressed_fault_functions_[vertex];
      }
    }
  }
  sort(current_vertices->begin(), current_vertices->end());
  current_vertices->resize(
      unique(current_vertices->begin(), current_vertices->end()) -
      current_vertices->begin());
}

void AhoCorasick::AddTerm(const string& term) noexcept {
  int term_index = terms_.size();
  terms_.push_back(term);
  int vertex = 0;
  vector<string> words;
  split(words, term, is_any_of(" "));
  for (const auto& word : words) {
    auto insert_result = transition_functions_[vertex].insert(
        {word, transition_functions_.size()});
    if (insert_result.second) {
      transition_functions_.push_back(unordered_map<string, int>());
      terminal_vertices_.push_back(vector<int>());
    }
    vertex = insert_result.first->second;
  }
  terminal_vertices_[vertex].push_back(term_index);
}

void AhoCorasick::InitializeFaultFunctions() noexcept {
  int vertex = 0;
  fault_functions_.resize(transition_functions_.size(), kEmptyTransition_);
  queue<int> vertices;
  vertices.push(vertex);
  while (!vertices.empty()) {
    vertex = vertices.front();
    vertices.pop();
    for (const auto& element : transition_functions_[vertex]) {
      if (element.second != kEmptyTransition_) {
        vertices.push(element.second);
        int fault_vertex = fault_functions_[vertex];
        bool found_fault = false;
        while (fault_vertex != kEmptyTransition_ && !found_fault) {
          if (transition_functions_[fault_vertex].find(element.first) ==
              transition_functions_[fault_vertex].end()) {
            fault_vertex = fault_functions_[fault_vertex];
          } else {
            found_fault = true;
          }
        }
        if (!found_fault) {
          fault_functions_[element.second] = 0;
        } else {
          fault_functions_[element.second] =
              transition_functions_[fault_vertex][element.first];
        }
      }
    }
  }
  CompressFaultFunctions();
}

void AhoCorasick::CompressFaultFunctions() noexcept {
  compressed_fault_functions_.resize(fault_functions_.size(),
                                     kEmptyTransition_);
  int vertex = 0;
  queue<int> vertices;
  vertices.push(vertex);
  while (!vertices.empty()) {
    vertex = vertices.front();
    vertices.pop();
    for (const auto& element : transition_functions_[vertex]) {
      if (element.second != kEmptyTransition_ &&
          fault_functions_[element.second] != kEmptyTransition_) {
        if (!terminal_vertices_[fault_functions_[element.second]].empty()) {
          compressed_fault_functions_[element.second] =
              fault_functions_[element.second];
        } else {
          compressed_fault_functions_[element.second] =
              compressed_fault_functions_[fault_functions_[element.second]];
        }
        vertices.push(element.second);
      }
    }
  }
}
