// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_THESAURUSLDAINPUTCREATOR_AUXILIARY_H_
#define EXPERIMENTS_THESAURUSLDAINPUTCREATOR_AUXILIARY_H_

#include <string>

/**
  @brief Enumerator containing all possible languages of the text collections

  This enumerator currently supports only Russian and English, while NO_LANGUGAE
  indicates an error
*/
enum class Language {
  NO_LANGUAGE,
  RUSSIAN,
  ENGLISH
};

/**
  @brief Structure containing index and frequency of the extracted thesaurus
  term

  It contains index of thesaurus term in resulting LDA vocabulary along with
  its frequency
*/
struct TermItem {
  /**
    Creates an object
    @param number index of thesaurus term in LDA vocabulary
    @param term_frequency frequency of the thesaurus term in LDA vocabulary
  */
  TermItem(size_t number, size_t term_frequency)
      : index(number),
        frequency(term_frequency) {
  }

  /** Index of thesaurus term in LDA vocabulary */
  size_t index;

  /** Frequency of thesaurus term in LDA vocabulary */
  size_t frequency;
};

#endif  // EXPERIMENTS_THESAURUSLDAINPUTCREATOR_AUXILIARY_H_
