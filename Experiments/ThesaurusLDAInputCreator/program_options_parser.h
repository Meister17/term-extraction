// Copyright 2014 Michael Nokel
#ifndef EXPERIMENTS_THESAURUSLDAINPUTCREATOR_PROGRAM_OPTIONS_PARSER_H_
#define EXPERIMENTS_THESAURUSLDAINPUTCREATOR_PROGRAM_OPTIONS_PARSER_H_

#include <string>
#include "./auxiliary.h"

/**
  @brief Class for parsing program options

  This class should be used for parsing program options from command line and/or
  from configuration file. After what one can access each necessary program
  option
*/
class ProgramOptionsParser {
 public:
  /**
    Parses program options from command line and/or from configuration file
    @param argc number of command line arguments (from main function)
    @param argv command line arguments (from main function)
    @throw std::exception in case of wrong program options
  */
  void Parse(int argc, char** argv);

 private:
  /** Flag indicating whether help message should be printed or not */
  bool help_message_printed_ = false;

  /** Language of collection of documents */
  Language language_ = Language::NO_LANGUAGE;

  /** Directory containing input files for processing */
  std::string input_directory_name_ = "";

  /** File containing terms from thesaurus */
  std::string thesaurus_filename_ = "";

  /** File where vocabulary of phrases will be written for LDA input */
  std::string lda_vocabulary_filename_ = "";

  /** File where input with phrases will be written for LDA input */
  std::string lda_input_filename_ = "";

  /** Minimum frequency of the thesaurus term to consider it */
  size_t minimum_frequency_ = 0;

 public:
  bool help_message_printed() const noexcept {
    return help_message_printed_;
  }

  Language language() const noexcept {
    return language_;
  }

  std::string input_directory_name() const noexcept {
    return input_directory_name_;
  }

  std::string thesaurus_filename() const noexcept {
    return thesaurus_filename_;
  }

  std::string lda_vocabulary_filename() const noexcept {
    return lda_vocabulary_filename_;
  }

  std::string lda_input_filename() const noexcept {
    return lda_input_filename_;
  }

  size_t minimum_frequency() const noexcept {
    return minimum_frequency_;
  }

 private:
  /**
    Checks existing file for correctness
    @param filename file for checking
    @throw std::runtime_error in case of wrong file
  */
  void CheckExistingFile(const std::string& filename) const;

  /**
    Checks existing directory for correctness
    @param directory_name directory for checking
    @throw std::exception in case of wrong directory
  */
  void CheckExistingDirectory(const std::string& directory_name) const;

  /**
    Checks output file for correctness
    @param filename file for checking
    @throw std::runtime_error in case of wrong file
  */
  void CheckOutputFile(const std::string& filename) const;

  /**
    Parses language from its string representation
    @param language_string string representation of the language
    @return parsed language
    @throw std::runtime_error in case of any occurred error
  */
  Language ParseLanguage(const std::string& language_string) const;
};


#endif  // EXPERIMENTS_THESAURUSLDAINPUTCREATOR_PROGRAM_OPTIONS_PARSER_H_
