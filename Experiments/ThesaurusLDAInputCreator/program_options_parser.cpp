// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include "./program_options_parser.h"

using boost::filesystem::exists;
using boost::filesystem::is_directory;
using boost::program_options::notify;
using boost::program_options::options_description;
using boost::program_options::parse_command_line;
using boost::program_options::parse_config_file;
using boost::program_options::store;
using boost::program_options::value;
using boost::program_options::variables_map;
using boost::to_lower;
using std::cout;
using std::ifstream;
using std::ofstream;
using std::runtime_error;
using std::string;

void ProgramOptionsParser::Parse(int argc, char** argv) {
  variables_map program_options_storage;
  string configuration_filename = "";
  string language_string = "";
  options_description program_options_description("Options for program:");
  program_options_description.add_options()
      ("help,h", value<bool>(&help_message_printed_)->implicit_value(true),
          "Print help message and exit")
      ("config_filename,c", value<string>(&configuration_filename),
          "Configuration file")
      ("language", value<string>(&language_string),
          "Language of the text collection")
      ("input_directory_name", value<string>(&input_directory_name_),
          "Directory containing input files")
      ("thesaurus_filename", value<string>(&thesaurus_filename_),
          "File containing terms from thesaurus")
      ("lda_vocabulary_filename", value<string>(&lda_vocabulary_filename_),
          "File where vocabulary for LDA will be printed")
      ("lda_input_filename", value<string>(&lda_input_filename_),
          "File where input for LDA will be printed")
      ("minimum_frequency", value<size_t>(&minimum_frequency_),
          "Minimum frequency of the thesaurus term");
  store(parse_command_line(argc, argv, program_options_description),
        program_options_storage);
  notify(program_options_storage);
  if (!configuration_filename.empty()) {
    store(parse_config_file<char>(configuration_filename.data(),
                                  program_options_description),
          program_options_storage);
    notify(program_options_storage);
  }
  if (help_message_printed_) {
    cout << program_options_description << "\n";
  } else {
    CheckExistingFile(thesaurus_filename_);
    CheckExistingDirectory(input_directory_name_);
    CheckOutputFile(lda_vocabulary_filename_);
    CheckOutputFile(lda_input_filename_);
    language_ = ParseLanguage(language_string);
  }
}

void ProgramOptionsParser::CheckExistingFile(const string& filename) const {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to open existing file");
  }
  file.close();
}

void ProgramOptionsParser::CheckExistingDirectory(const string& directory_name)
    const {
  if (!exists(directory_name) || !is_directory(directory_name)) {
    throw runtime_error("Wrong existing directory was specified");
  }
}

void ProgramOptionsParser::CheckOutputFile(const string& filename) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to open output file");
  }
  file.close();
}

Language ProgramOptionsParser::ParseLanguage(const string& language_string)
    const {
  string lang_string = language_string;
  to_lower(lang_string);
  if (lang_string == "russian") {
    return Language::RUSSIAN;
  } else if (lang_string == "english") {
    return Language::ENGLISH;
  } else {
    throw runtime_error("Wrong language has been specified");
  }
}
