/*
 * ProgramOptionsParser.java
 * 
 * Created on January 13, 2013, 11:38 PM
 * 
 * 
 * Copyright (c) 2013 Michael Nokel
 */
package stanfordanalyzer;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.SimpleJSAP;
import com.martiansoftware.jsap.defaultsources.PropertyDefaultSource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * @brief Class for parsing program options from command line and/or from
 * configuration file
 * 
 * After parsing program options should be checked for their correctness
 */
public class ProgramOptionsParser {
  public ProgramOptionsParser() {    
  }

  public void Parse(String[] args) throws JSAPException {
    // specify program options
    SimpleJSAP program_options_parser = new SimpleJSAP(
        "Program for performing morphological analysis on English texts",
        "Uses StanfordCoreNLP library to perform morphological analysis",
        new Parameter[] {
          new FlaggedOption("source_directory_name",
                            JSAP.STRING_PARSER,
                            JSAP.NO_DEFAULT,
                            JSAP.REQUIRED,
                            's',
                            "source_directory_name",
                            "Name of source directory to scan"),
          new FlaggedOption("destination_directory_name",
                            JSAP.STRING_PARSER,
                            JSAP.NO_DEFAULT,
                            JSAP.REQUIRED,
                            'd',
                            "destination_directory_name",
                            "Name of destination directory for processed files")
        }
    );
    // add configuration file for looking for program options
    boolean throwingExceptions = true;
    program_options_parser.registerDefaultSource(
        new PropertyDefaultSource("stanfordanalyzer.conf", throwingExceptions));
    // parse program options
    JSAPResult jsapResult = program_options_parser.parse(args);
    if (!jsapResult.success()) {
      // failed to parse program options -> throw error
      throw new JSAPException(
          jsapResult.getErrorMessageIterator().next().toString());
    }
    if (program_options_parser.messagePrinted()) {
      this.help_message_printed_ = true;
    }
    // get parsed program options
    ParseProgramOptions(jsapResult);
  }

  /**
   * Checks program options for their correctness
   * @throws JSAPException if some program options are not correct
   */
  public void CheckProgramOptions() throws JSAPException {
    boolean create = false;
    CheckDirectory(this.source_directory_name_, create);
    create = true;
    CheckDirectory(this.destination_directory_name_, create);
  }

  public boolean help_message_printed() {
    return this.help_message_printed_;
  }

  public String source_directory_name() {
    return this.source_directory_name_;
  }

  public String destination_directory_name() {
    return this.destination_directory_name_;
  }

  /**
   * Stores parsed program options in private fields, so one can get access to
   * any of them by public getters
   * @param jsapResult result of parsing program options in JSAP form
   */
  private void ParseProgramOptions(JSAPResult jsapResult) {
    this.source_directory_name_ = jsapResult.getString("source_directory_name");
    this.destination_directory_name_ = jsapResult.getString(
        "destination_directory_name");
  }

  /**
   * Checks directory for its correctness
   * @param directory_name name of directory to check
   * @param create flag indicating whether directory should be created (if it
   * doesn't exist)
   * @throws JSAPException if checking correcntess has failed
   */
  private void CheckDirectory(String directory_name, boolean create)
      throws JSAPException {
    File directory = new File(directory_name);
    if (create && !directory.exists()) {
      try {
        Files.createDirectories(directory.toPath());
      } catch (IOException exception) {
        throw new JSAPException(exception.getMessage());
      }
    }
    if (!directory.exists() || !directory.isDirectory()) {
      throw new JSAPException("Wrong directory");
    }
  }

  /** Flag indicating whether help message has been printed or not */
  private boolean help_message_printed_ = false;

  /** Name of source directory to scan */
  private String source_directory_name_ = "";

  /** Name of destination directory for processed text files */
  private String destination_directory_name_ = "";
}
