// Copyright 2012 Michael Nokel
#pragma once

#include "./auxiliary_macros.h"
#include <boost/thread/mutex.hpp>
#include <string>
#include <unordered_map>

/**
  @brief Class for accumulating and printing extracted term candidates

  This class should be used three times in the whole program: for single-word
  term candidates, for two-word term candidates and for multi-word term
  candidates.
*/
class CandidatesAccumulator {
 public:
  /**
    Accumulates previously extracted term candidates during parsing files
    @param start_iterator start iterator to the map containing extracted term
    candidates and their frequencies
    @param end_iterator end iterator to the map containing extracted term
    candidates and their frequencies
  */
  void Accumulate(
      std::unordered_map<std::string, unsigned int>::const_iterator
          start_iterator,
      std::unordered_map<std::string, unsigned int>::const_iterator
          end_iterator);

  /**
    Prints accumulated term candidates to the given file
    @param filename file where term candidates should be printed
    @throw std::runtime_error in cae of any occurred error
  */
  void PrintTermCandidates(const std::string& filename) const;

 private:
  /** Total number of extracted term candidates in the collection */
  unsigned int total_number_candidates_ = 0;

  /** Mutex for synchronization accumulating maps with term candidates obtained
      for each parsed file separately */
  boost::mutex mutex_;

  /** Hash map containing accumulated term candidates and their frequencies */
  std::unordered_map<std::string, unsigned int> term_candidates_map_;
};
