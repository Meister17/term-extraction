// Copyright 2012 Michael Nokel
#include "./candidates_accumulator.h"
#include <boost/thread/mutex.hpp>
#include <algorithm>
#include <fstream>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

using std::ofstream;
using std::runtime_error;
using std::sort;
using std::string;
using std::unordered_map;
using std::vector;

void CandidatesAccumulator::Accumulate(
    const unordered_map<string, unsigned int>::const_iterator start_iterator,
    const unordered_map<string, unsigned int>::const_iterator end_iterator) {
  if (start_iterator != end_iterator) {
    boost::mutex::scoped_lock lock(mutex_);
    for (auto iterator = start_iterator; iterator != end_iterator; ++iterator) {
      total_number_candidates_ += iterator->second;
      auto inserted_result = term_candidates_map_.insert(
          {iterator->first, iterator->second});
      if (!inserted_result.second) {
        inserted_result.first->second += iterator->second;
      }
    }
  }
}

void CandidatesAccumulator::PrintTermCandidates(const string& filename) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print term candidates");
  }
  file << total_number_candidates_ << "\n";
  for (const auto& candidate: term_candidates_map_) {
    file << candidate.first << "\t" << candidate.second << "\n";
  }
  file.close();
}
