// Copyright 2012 Michael Nokel
#pragma once

#include "./candidates_accumulator.h"
#include "./auxiliary_macros.h"
#include "./words_processor.h"
#include <boost/thread.hpp>
#include <fstream>
#include <string>
#include <vector>

class SingleWordTermsExtractor;
class TwoWordTermsExtractor;

/**
  @brief Main class for analyzing collection

  With the help of this class one can scan the given directory with text files
  and extract single-word, two-word and multi-word term candidates with their
  frequencies.
*/
class CollectionAnalyzer {
 public:
  /**
    Parses file containing prepositions and their cases
    @param prepositions_filename name of file containing prepositions and their
    cases
    @throw std::runtime_error in case of any occurred error
  */
  void ParsePrepositionsFile(const std::string& prepositions_filename) {
    words_processor_.ParsePrepositionsFile(prepositions_filename);
  }

  /**
    Scans source directory with text files and extracts all met term candidates
    @param directory_name name of directory containing text files for processing
    @throw std::runtime_error in case of any occurred error
  */
  void ExtractTermCandidates(const std::string& directory_name);

  /**
    Prints all extracted single-word, two-word and multi-word term candidates
    @param single_words_filename file for printing extracted single-word term
    candidates
    @param two_words_filename file for printing extracted two-word term
    candidates
    @throw std::runtime_error in case of any occurred error
  */
  void PrintTermCandidates(const std::string& single_words_filename,
                           const std::string& two_words_filename) const;

 private:
  /**
    Processes given text file line by line and extracts found term candidates
    @param filename file for processing
  */
  void ParseTextFile(const std::string& filename);

  /**
    Extracts words that have same forms from the current line in file
    @param number_of_words_in_file current number of extracted words in file
    @param[out] file file from where to extract words
    @param[out] word_extracted flag indicating whether word was extracted or not
    @param[out] was_punctuation_divider flag indicating whether there was a
    punctuation divider extracted
    @param[out] same_word_forms vector where extracted words with same forms
    will be stored
  */
  void ExtractSameWordForms(unsigned int number_of_words_in_file,
                            std::ifstream* file,
                            bool& word_extracted,
                            bool& was_punctuation_divider,
                            std::vector<WordProperties>* same_word_forms) const;

  /**
    Stores newly extracted term candidates
    @param words_to_store vector containing most recently extracted words
    @param[out] single_word_terms_extractor object used for extracting
    single-word term candidates
    @param[out] two_word_terms_extractor object used for extracting two-word
    term candidates
  */
  void StoreNewWords(
      const std::vector<WordProperties>& words_to_store,
      SingleWordTermsExtractor* single_word_terms_extractor,
      TwoWordTermsExtractor* two_word_terms_extractor) const;

  /**
    Accumulates results of parsing current text file
    @param single_word_terms_extractor object for extracting single-word term
    candidates
    @param two_word_terms_extractor object for extracting two-word term
    candidates
  */
  void AccumulateResults(
      const SingleWordTermsExtractor& single_word_terms_extractor,
      const TwoWordTermsExtractor& two_word_terms_extractor);

  /**
    Converts string from CP1251 encoding to UTF8 encoding
    @param string_cp1251 string in CP1251 encoding
    @return equivalent string in UTF8 encoding
  */
  std::string ConvertFromCP1251ToUTF8(const std::string& string_cp1251) const;

  /** Extension of files that will be processed */
  const std::string kProcessingFilesExtension_ = ".plm";

  /** Number of threads in a thread pool */
  const int kNumberOfThreads_ =
      static_cast<int>(boost::thread::hardware_concurrency());

  /** Object for extracting and processing words in files */
  WordsProcessor words_processor_;

  /** Object for accumulating single-word term candidates extracted from files
    */
  CandidatesAccumulator single_word_terms_accumulator_;

  /** Object for accumulating two-word term candidates extracted from files */
  CandidatesAccumulator two_word_terms_accumulator_;

  /** Object for accumulating multi-word term candidates extracted from files */
  CandidatesAccumulator multi_word_terms_accumulator_;
};
