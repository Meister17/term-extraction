// Copyright 2012 Michael Nokel
#include "./collection_analyzer.h"
#include "./program_options_parser.h"
#include <iostream>
#include <stdexcept>

using std::cout;
using std::exception;
using std::runtime_error;

/**
  Main function which starts the program
  @param argc number of program options
  @param argv array containing program options
  @return code of success
*/
int main(int argc, char** argv) {
  try {
    ProgramOptionsParser program_options_parser;
    program_options_parser.Parse(argc, argv);
    if (program_options_parser.help_message_printed()) {
      return 0;
    }
    CollectionAnalyzer collection_analyzer;
    collection_analyzer.ParsePrepositionsFile(
        program_options_parser.prepositions_filename());
    collection_analyzer.ExtractTermCandidates(
        program_options_parser.source_directory_name());
    collection_analyzer.PrintTermCandidates(
        program_options_parser.single_word_terms_filename(),
        program_options_parser.two_word_terms_filename());
  } catch (const exception& except) {
    cout << "Fatal error occurred: " << except.what() << "\n";
    return 1;
  }
  return 0;
}
