// Copyright 2013 Michael Nokel
#include "./two_word_terms_extractor.h"
#include "../auxiliary_macros.h"
#include <set>
#include <string>
#include <vector>

using std::set;
using std::string;
using std::vector;

void TwoWordTermsExtractor::AddNewTermCandidates(
    const vector<WordProperties>& new_term_candidates) {
  if (new_term_candidates.empty()) {
    return;
  } else if (previous_words_.empty()) {
    StoreNewTermCandidates(new_term_candidates);
  } else if (new_term_candidates.front().is_after_punctuation_divider ||
             new_term_candidates.front().word_number_in_file -
             previous_words_.front().word_number_in_file > 1) {
    previous_words_.clear();
    StoreNewTermCandidates(new_term_candidates);
  } else {
    for (const auto& candidate: new_term_candidates) {
      if (candidate.word_category == WordCategory::NOUN ||
          candidate.word_category == WordCategory::ADJECTIVE) {
        FormTwoWordTermCandidates(candidate);
      }
    }
    previous_words_.clear();
    StoreNewTermCandidates(new_term_candidates);
  }
}

void TwoWordTermsExtractor::StoreNewTermCandidates(
    const vector<WordProperties>& new_term_candidates) {
  for (const auto& candidate: new_term_candidates) {
    if (candidate.word_category == WordCategory::NOUN ||
        candidate.word_category == WordCategory::ADJECTIVE) {
      previous_words_.push_back(candidate);
    }
  }
}

void TwoWordTermsExtractor::FormTwoWordTermCandidates(
    const WordProperties& term_candidate) {
  if (term_candidate.word_category == WordCategory::NOUN &&
      !term_candidate.is_after_punctuation_divider) {
    bool is_in_genitive = IsInGenitive(term_candidate.changeable_word_parts);
    for (const auto& previous_word: previous_words_) {
      if ((previous_word.word_category == WordCategory::ADJECTIVE) ||
          (previous_word.word_category == WordCategory::NOUN &&
           is_in_genitive)) {
        string word = previous_word.word + " " + term_candidate.word;
        if (IsRussianWord(word)) {
          auto inserted_result = term_candidates_map_.insert({word, 1});
          if (!inserted_result.second) {
            ++inserted_result.first->second;
          }
        }
      }
    }
  }
}

bool TwoWordTermsExtractor::IsInGenitive(
    const set<ChangeableWordParts>& candidate_word_parts) const {
  for (const auto& candidate: candidate_word_parts) {
    if (candidate.word_case == WordCase::GENITIVE) {
      return true;
    }
  }
  return false;
}
