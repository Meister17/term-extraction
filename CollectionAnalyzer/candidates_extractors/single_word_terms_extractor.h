// Copyright 2012 Michael Nokel
#pragma once

#include "./candidates_extractor.h"
#include "../auxiliary_macros.h"
#include <string>
#include <unordered_map>
#include <vector>

/**
  @brief Class for extracting single-word term candidates

  For each thread that parses file its own instance of this class should be
  created. This instance should be used for accumulating single-word term
  candidates and their frequencies. Then accumulated information should be
  merged with previously extracted.
*/
class SingleWordTermsExtractor : public CandidatesExtractor {
 public:
  /**
    Adds new term candidates
    @param new_term_candidates vector containing word properties of new term
    candidates
  */
  void AddNewTermCandidates(
      const std::vector<WordProperties>& new_term_candidates);
};
