// Copyright 2012 Michael Nokel
#pragma once

#include "../auxiliary_macros.h"
#include <string>
#include <unordered_map>
#include <vector>

/**
  @brief Abstract class for extracting various term candidates

  This class should be derived by each class intended to extract various term
  candidates depending on their length. For each thread that parses file its own
  instance of this class should be created. This instance should be used for
  accumulating candidates and necessary information about them. Then accumulated
  information should be merged with previously extracted ones.
*/
class CandidatesExtractor {
 public:
  /**
    Adds new term candidates
    @param new_term_candidates vector containing word properties of new term
    candidates
  */
  virtual void AddNewTermCandidates(
      const std::vector<WordProperties>& new_term_candidates) = 0;

  /**
    Returns start iterator to the map containing all extracted candidates and
    their frequencies from parsed file
    @return start iterator to such map
  */
  virtual std::unordered_map<std::string, unsigned int>::const_iterator
      GetStartIterator() const {
    return term_candidates_map_.begin();
  }

  /**
    Returns end iterator to the map containing all extracted candidates and
    their frequencies from parsed file
    @return end iterator to such map
  */
  virtual std::unordered_map<std::string, unsigned int>::const_iterator
      GetEndIterator() const {
    return term_candidates_map_.end();
  }

  /**
    Virtual destructor for the purposes of proper deriving
  */
  virtual ~CandidatesExtractor() {
  }

 protected:
  /**
    Checks whether given word is Russian
    @param word word to check
    @return true if given word is Russian
  */
  bool IsRussianWord(const std::string& word) const {
    for (size_t index = 0; index < word.size(); index += 2) {
      if (kRussianLetters_.find(word.substr(index, 2)) == std::string::npos) {
        return false;
      }
    }
    return true;
  }

  /** All possible Russian letters */
  const std::string kRussianLetters_ =
      "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";

  /** hash map containing extracted term candidates and their frequencies */
  std::unordered_map<std::string, unsigned int> term_candidates_map_;
};
