// Copyright 2012 Michael Nokel
#pragma once

#include "./candidates_extractor.h"
#include "../auxiliary_macros.h"
#include <set>
#include <string>
#include <vector>

/**
  @brief Class for extracting two-word term candidates

  For each thread that parses file its own instance of this class should be
  created. This instance should be used for accumulating two-word term
  candidates and necessary information about them. Then accumulated information
  should be merged with previously extracted.
*/
class TwoWordTermsExtractor : public CandidatesExtractor {
 public:
  /**
    Adds new term candidates
    @param new_term_candidates vector containing word properties of new term
    candidates
  */
  void AddNewTermCandidates(
      const std::vector<WordProperties>& new_term_candidates);

 private:
  /**
    Stores new term candidates
    @param new_term_candidates vector containing first term candidates
  */
  void StoreNewTermCandidates(
      const std::vector<WordProperties>& new_term_candidates);

  /**
    Forms and inserts new two-word term candidates by adding given single-word
    term candidate
    @param term_candidate word properties of adding single-word term candidate
  */
  void FormTwoWordTermCandidates(const WordProperties& term_candidate);

  /**
    Checks whether given term candidate is in genitive case
    @param candidate_word_parts set containing changeable word parts
    @return true if term candidate is in genitive case
  */
  bool IsInGenitive(const std::set<ChangeableWordParts>& candidate_word_parts)
      const;

  /** Previous words for forming two-word term candidate */
  std::vector<WordProperties> previous_words_;
};
