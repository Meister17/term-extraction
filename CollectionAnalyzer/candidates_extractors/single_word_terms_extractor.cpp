// Copyright 2013 Michael Nokel
#include "./single_word_terms_extractor.h"
#include "../auxiliary_macros.h"
#include <vector>

using std::vector;

void SingleWordTermsExtractor::AddNewTermCandidates(
    const vector<WordProperties>& new_term_candidates) {
  for (const auto& candidate: new_term_candidates) {
    if ((candidate.word_category == WordCategory::NOUN ||
         candidate.word_category == WordCategory::ADJECTIVE) &&
         IsRussianWord(candidate.word)) {
      auto result = term_candidates_map_.insert({candidate.word, 1});
      if (!result.second) {
        ++result.first->second;
      }
    }
  }
}