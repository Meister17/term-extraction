// Copyright 2012 Michael Nokel
#pragma once

#include <string>

/**
  @brief Class for working with program options

  With the help of this class one can parse program options from command line
  and/or from configuration file, check their correctness and if everything is
  correct one can get the values of the necessary program options.
*/
class ProgramOptionsParser {
 public:
  /**
    Parses program options from command line and/or from configuration file
    @param argc number of program options (from main function)
    @param argv array containing program options (from main function)
    @throw std::runtime_error in case of any occurred errors
  */
  void Parse(int argc, char** argv);

  bool help_message_printed() const {
    return help_message_printed_;
  }

  std::string source_directory_name() const {
    return source_directory_name_;
  }

  std::string prepositions_filename() const {
    return prepositions_filename_;
  }

  std::string single_word_terms_filename() const {
    return single_word_terms_filename_;
  }

  std::string two_word_terms_filename() const {
    return two_word_terms_filename_;
  }

  std::string multi_word_terms_filename() const {
    return multi_word_terms_filename_;
  }

 private:
  /** Flag indicating whether help message should be printed */
  bool help_message_printed_ = false;

  /** Name of source directory with files from contrast collection */
  std::string source_directory_name_ = "";

  /** Name of file containing prepositions with their cases */
  std::string prepositions_filename_ = "";

  /** Name of file where single-word term candidates with their frequencies
      will be written */
  std::string single_word_terms_filename_ = "";

  /** Name of file where two-word term candidates with their frequencies will
      be written */
  std::string two_word_terms_filename_ = "";

  /** Name of file where multi-word term candidates with their frequencies will
      be written */
  std::string multi_word_terms_filename_ = "";
};
