// Copyright 2012 Michael Nokel
#pragma once

#include <set>
#include <string>

/**
  @brief All cases in Russian grammar.

  This enumeration contains nominative, genitive, dative, accusative,
  instrumental and prepositional cases (also no case is possible)
*/
enum class WordCase {
  NO_CASE,
  NOMINATIVE,
  GENITIVE,
  DATIVE,
  ACCUSATIVE,
  INSTRUMENTAL,
  PREPOSITIONAL
};

/**
  @brief All genders in Russian grammar.

  This enumeration contains masculine, feminine and neuter genders (also no
  gender and plural are possible)
*/
enum class WordGender {
  NO_GENDER,
  MASCULINE,
  FEMININE,
  NEUTER,
  PLURAL
};

/**
  @brief All types of words' categories that we consider.

  These types include nouns, adjectives, proper nouns (to exclude them from the
  consideration), agreement words (including pronouns, numerals and
  participles), prepositions and various dividers (no category is also possible)
*/
enum class WordCategory {
  NO_CATEGORY,
  NOUN,
  ADJECTIVE,
  PROPER_NOUN,
  AGREEMENT_WORD,
  PREPOSITION,
  DIVIDER
};

/**
  @brief Structure containing changeable parts of words

  This structure contains word's case and word's gender. For each word in the
  corresponding structure should be set or vector of such pairs to represent
  their structure
*/
struct ChangeableWordParts {
  ChangeableWordParts(WordCase extracted_word_case,
                      WordGender extracted_word_gender)
      : word_case(extracted_word_case),
        word_gender(extracted_word_gender) {
  }

  /**
    Compares given two structure containing gender and case and returns true if
    first structure is less than second one
    @param first_word_parts first sturcture to compare
    @param second_word_parts second structure to compare
    @return true if first structure is less than second one
  */
  friend bool operator<(const ChangeableWordParts& first_word_parts,
                        const ChangeableWordParts& second_word_parts) {
    return first_word_parts.word_case < second_word_parts.word_case &&
           first_word_parts.word_gender < second_word_parts.word_gender;
  }

  /** Case of word */
  WordCase word_case = WordCase::NO_CASE;

  /** Gender of word */
  WordGender word_gender = WordGender::NO_GENDER;
};

/**
  @brief Structure containing information about extracted word

  This information contains extracted word, its category, genders, numbers,
  possible cases and flag indicating whether this word is going after some
  punctuation divider
*/
struct WordProperties {
  WordProperties() {
  }

  WordProperties(const std::string& extracted_word,
                 const WordCategory& extracted_word_category,
                 bool is_after_punctuation_sign,
                 unsigned int word_number)
      : word(extracted_word),
        word_category(extracted_word_category),
        changeable_word_parts(),
        is_after_punctuation_divider(is_after_punctuation_sign),
        word_number_in_file(word_number) {
  }

  /**
    Compares two words' data only by words
    @param first_word first word to compare
    @param second_word second word to compare
    @return true if the words are equal
  */
  static bool CompareByWords(const WordProperties& first_word,
                             const WordProperties& second_word) {
    return first_word.word == second_word.word;
  }

  /** Extracted word */
  std::string word = "";

  /** Word's category */
  WordCategory word_category = WordCategory::NO_CATEGORY;

  /** Information about changeable word parts */
  std::set<ChangeableWordParts> changeable_word_parts;

  /** Flag indicating whether the word is going just after the punctuation
      divider */
  bool is_after_punctuation_divider = false;

  /** Number of word in file */
  unsigned int word_number_in_file = 0;
};
