// Copyright 2012 Michael Nokel
#include "./collection_analyzer.h"
#include "./candidates_extractors/single_word_terms_extractor.h"
#include "./candidates_extractors/two_word_terms_extractor.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/threadpool.hpp>
#include <iconv.h>
#include <algorithm>
#include <cctype>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

using boost::filesystem::directory_iterator;
using boost::filesystem::is_regular_file;
using boost::is_any_of;
using boost::split;
using boost::threadpool::pool;
using boost::token_compress_on;
using std::cout;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::string;
using std::vector;

void CollectionAnalyzer::ExtractTermCandidates(
    const string& directory_name) {
  cout << "Extracting term candidates\n";
  pool thread_pool(kNumberOfThreads_);
  for (directory_iterator iterator(directory_name); iterator !=
       directory_iterator(); ++iterator) {
    if (is_regular_file(iterator->status()) &&
        iterator->path().extension() == kProcessingFilesExtension_) {
      thread_pool.schedule(boost::bind(&CollectionAnalyzer::ParseTextFile,
                                       this,
                                       iterator->path().string()));
    }
  }
}

void CollectionAnalyzer::PrintTermCandidates(
    const string& single_word_terms_filename,
    const string& two_word_terms_filename) const {
  cout << "Printing extracted candidates\n";
  pool thread_pool(kNumberOfThreads_);
  thread_pool.schedule(boost::bind(
      &CandidatesAccumulator::PrintTermCandidates,
      &single_word_terms_accumulator_,
      single_word_terms_filename));
  thread_pool.schedule(boost::bind(
      &CandidatesAccumulator::PrintTermCandidates,
      &two_word_terms_accumulator_,
      two_word_terms_filename));
}

void CollectionAnalyzer::ParseTextFile(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse text file");
  }
  SingleWordTermsExtractor single_word_terms_extractor;
  TwoWordTermsExtractor two_word_terms_extractor;
  unsigned int number_of_words_in_file = 0;
  bool was_punctuation_divider = true;
  vector<WordProperties> previous_words;
  while (!file.eof()) {
    vector<WordProperties> same_word_forms;
    bool word_extracted = false;
    ExtractSameWordForms(number_of_words_in_file,
                         &file,
                         word_extracted,
                         was_punctuation_divider,
                         &same_word_forms);
    if (word_extracted) {
      ++number_of_words_in_file;
    }
    bool continue_processing;
    vector<WordProperties> words_to_store;
    words_processor_.AddNewWords(same_word_forms,
                                 continue_processing,
                                 &previous_words,
                                 &words_to_store);
    if (!continue_processing) {
      StoreNewWords(words_to_store,
                    &single_word_terms_extractor,
                    &two_word_terms_extractor);
    }
  }
  file.close();
  if (!previous_words.empty()) {
    StoreNewWords(previous_words,
                  &single_word_terms_extractor,
                  &two_word_terms_extractor);
  }
  AccumulateResults(single_word_terms_extractor,
                    two_word_terms_extractor);
}

void CollectionAnalyzer::ExtractSameWordForms(
    unsigned int number_of_words_in_file,
    ifstream* file,
    bool& word_extracted,
    bool& was_punctuation_divider,
    vector<WordProperties>* same_word_forms) const {
  char first_letter;
  word_extracted = false;
  bool punctuation_divider_found = false;
  bool was_punctuation_mark = was_punctuation_divider;
  do {
    string line;
    getline(*file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    line = ConvertFromCP1251ToUTF8(line);
    vector<string> tokens;
    split(tokens, line, is_any_of(" \t"), token_compress_on);
    WordProperties extracted_word_properties;
    bool is_word = false;
    words_processor_.ExtractWordFromLine(tokens,
                                         is_word,
                                         was_punctuation_mark,
                                         &extracted_word_properties);
    if (was_punctuation_mark) {
      punctuation_divider_found = true;
      was_punctuation_mark = was_punctuation_divider;
    }
    word_extracted |= is_word;
    extracted_word_properties.word_number_in_file = number_of_words_in_file;
    same_word_forms->push_back(extracted_word_properties);
    first_letter = file->get();
  } while (!file->eof() && isspace(first_letter));
  if (!file->eof()) {
    file->unget();
  }
  was_punctuation_divider = punctuation_divider_found;
}

void CollectionAnalyzer::StoreNewWords(
    const vector<WordProperties>& words_to_store,
    SingleWordTermsExtractor* single_word_terms_extractor,
    TwoWordTermsExtractor* two_word_terms_extractor) const {
  single_word_terms_extractor->AddNewTermCandidates(words_to_store);
  two_word_terms_extractor->AddNewTermCandidates(words_to_store);
}

void CollectionAnalyzer::AccumulateResults(
    const SingleWordTermsExtractor& single_word_terms_extractor,
    const TwoWordTermsExtractor& two_word_terms_extractor) {
  single_word_terms_accumulator_.Accumulate(
      single_word_terms_extractor.GetStartIterator(),
      single_word_terms_extractor.GetEndIterator());
  two_word_terms_accumulator_.Accumulate(
      two_word_terms_extractor.GetStartIterator(),
      two_word_terms_extractor.GetEndIterator());
}

string CollectionAnalyzer::ConvertFromCP1251ToUTF8(const string& string_cp1251)
    const {
  iconv_t cd = iconv_open("utf-8", "cp1251");
  if (cd == (iconv_t) - 1) {
    return "";
  }
  size_t input_size = string_cp1251.length();
  size_t output_size = 2*input_size + 1;
  size_t total_output_size = output_size;
#ifdef _WIN32
  const char* input_string_pointer = string_cp1251.data();
#else
  char* input_string_pointer = const_cast<char*>(string_cp1251.data());
#endif
  char* output_string = new char[output_size];
  char* output_string_pointer = output_string;
  memset(output_string, 0, output_size);
  if ((iconv(cd, &input_string_pointer, &input_size, &output_string_pointer,
             &output_size)) == (size_t) - 1) {
    iconv_close(cd);
    delete[] output_string;
    return "";
  }
  iconv_close(cd);
  string string_utf8 = string(output_string, total_output_size - output_size);
  delete[] output_string;
  return string_utf8;
}