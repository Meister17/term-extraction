// Copyright 2012 Michael Nokel
#pragma once

#include "../auxiliary_macros.h"
#include <string>

/**
  @brief Abstract class for processing words depending on their parts of speech

  This class should be used for extracting cases and genders of words. It should
  be overridden by classes representing those parts of speech that we are
  considering.
*/
class PartsOfSpeechProcessor {
 public:
  /**
    Main function which processes given token and stores cases and genders of
    extracted word
    @param token token for processing
    @param[out] word_properties structure where cases and genders of given word
    will be stored
    @see WordProperties
  */
  virtual void ProcessWord(
      const std::string& part_of_speech,
      WordProperties* word_properties) const = 0;

  virtual ~PartsOfSpeechProcessor() {
  }

 protected:
  /**
    Gets word's case depending on given its string representation
    @param case_sign string representation of word's case
    @return word's case
    @see WordCase
  */
  virtual WordCase GetWordCase(const std::string& case_sign) const = 0;

  /**
    Gets word's gender depending on given its string representation
    @param gender_sign string representation of word's gender
    @return word's gender
    @see WordGender
  */
  virtual WordGender GetWordGender(const std::string& gender_sign) const = 0;
};
