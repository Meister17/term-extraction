// Copyright 2012 Michael Nokel
#pragma once

#include "./parts_of_speech_processor.h"
#include "../auxiliary_macros.h"
#include <string>

/**
  @brief Class for processing adjectives

  This class is derived from base abstract class PartsOfSpeechProcessor and
  should be used for working with adjectives.
*/
class AdjectivesProcessor : public PartsOfSpeechProcessor {
 public:
  /**
    Processes adjective and stores its cases and genders
    @param token token for processing
    @param[out] word_properties structure where cases and genders of given
    adjective will be stored
    @see WordProperties
  */
  void ProcessWord(const std::string& token,
                   WordProperties* word_properties) const;

 private:
  /**
    Gets adjective's case depending on given its string representation
    @param case_sign string representation of adjective's case
    @return adjective's case
    @see WordCase
  */
  WordCase GetWordCase(const std::string& case_sign) const;

  /**
    Gets adjective's gender depending on given its string representation
    @param gender_sign string representation of adjective's gender
    @return adjective's gender
    @see WordGender
  */
  WordGender GetWordGender(const std::string& gender_sign) const;
};
