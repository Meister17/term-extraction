// Copyright 2012 Michael Nokel
#pragma once

#include "./parts_of_speech_processor.h"
#include "../auxiliary_macros.h"
#include <string>

/**
  @brief Class for processing nouns

  This class is derived from base abstract class PartsOfSpeechProcessor and
  should be used for working with nouns.
*/
class NounsProcessor : public PartsOfSpeechProcessor {
 public:
  /**
    Processes noun and stores its cases and genders
    @param token token for processing
    @param[out] word_properties structure where cases and genders of given noun
    will be stored
    @see WordProperties
  */
  void ProcessWord(const std::string& token,
                   WordProperties* word_properties) const;

 private:
  /**
    Gets noun's case depending on given its string representation
    @param case_sign string representation of noun's case
    @return noun's case
    @see WordCase
  */
  WordCase GetWordCase(const std::string& case_sign) const;

  /**
    Gets noun's gender depending on given its string representation
    @param gender_sign string representation of noun's gender
    @return noun's gender
    @see WordGender
  */
  WordGender GetWordGender(const std::string& gender_sign) const;
};
