// Copyright 2012 Michael Nokel
#include "./adjectives_processor.h"
#include "../auxiliary_macros.h"
#include <string>

using std::string;

void AdjectivesProcessor::ProcessWord(const string& token,
                                      WordProperties* word_properties) const {
  word_properties->word_category = WordCategory::ADJECTIVE;
  for (size_t index = 2; index < token.size(); index += 4) {
    string sign = token.substr(index, 2);
    ChangeableWordParts word_parts(GetWordCase(sign), GetWordGender(sign));
    word_properties->changeable_word_parts.insert(word_parts);
  }
}

WordCase AdjectivesProcessor::GetWordCase(const string& case_sign) const {
  if (string("ажмт").find(case_sign) != string::npos) {
    return WordCase::NOMINATIVE;
  } else if (string("бзну").find(case_sign) != string::npos) {
    return WordCase::GENITIVE;
  } else if (string("виоф").find(case_sign) != string::npos) {
    return WordCase::DATIVE;
  } else if (string("гйпх").find(case_sign) != string::npos) {
    return WordCase::ACCUSATIVE;
  } else if (string("дкрц").find(case_sign) != string::npos) {
    return WordCase::INSTRUMENTAL;
  } else if (string("елсч").find(case_sign) != string::npos) {
    return WordCase::PREPOSITIONAL;
  }
  return WordCase::NO_CASE;
}

WordGender AdjectivesProcessor::GetWordGender(const string& gender_sign) const {
  if (string("абвгдеш").find(gender_sign) != string::npos) {
    return WordGender::MASCULINE;
  } else if (string("жзийклщ").find(gender_sign) != string::npos) {
    return WordGender::FEMININE;
  } else if (string("мнопрсы").find(gender_sign) != string::npos) {
    return WordGender::NEUTER;
  } else if (string("туфхцч").find(gender_sign) != string::npos) {
    return WordGender::PLURAL;
  }
  return WordGender::NO_GENDER;
}
