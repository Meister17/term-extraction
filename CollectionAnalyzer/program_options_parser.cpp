// Copyright 2012 Michael Nokel
#include "./program_options_parser.h"
#include <boost/program_options.hpp>
#include <iostream>
#include <stdexcept>
#include <string>

using namespace boost::program_options;
using std::cout;
using std::exception;
using std::string;

void ProgramOptionsParser::Parse(int argc, char** argv) {
  variables_map program_options_storage;
  string configuration_filename;
  options_description program_options_description("Options for program:");
  program_options_description.add_options()
      ("help,h", value<bool>(&help_message_printed_)->implicit_value(true),
          "Print help message and exit")
      ("configuration_file,c", value<string>(&configuration_filename),
          "Name of configuration file of program")
      ("source_directory_name", value<string>(&source_directory_name_),
          "Name of source directory containing files for processing")
      ("prepositions_filename", value<string>(&prepositions_filename_),
          "Name of file with prepositions and their cases")
      ("single_word_terms_filename",
          value<string>(&single_word_terms_filename_),
          "Name of file where single-word term candidates will be written")
      ("two_word_terms_filename", value<string>(&two_word_terms_filename_),
          "Name of file where two-word term candidates will be written");
  store(parse_command_line(argc, argv, program_options_description),
        program_options_storage);
  notify(program_options_storage);
  if (!configuration_filename.empty()) {
    store(parse_config_file<char>(configuration_filename.data(),
                                  program_options_description),
          program_options_storage);
    notify(program_options_storage);
  }
  if (help_message_printed_) {
    cout << program_options_description << "\n";
  }
}
