// Copyright 2011 Michael Nokel
#pragma once

#include "parts_of_speech/adjectives_processor.h"
#include "parts_of_speech/nouns_processor.h"
#include "parts_of_speech/numerals_processor.h"
#include "parts_of_speech/participles_processor.h"
#include "parts_of_speech/prepositions_processor.h"
#include "parts_of_speech/pronouns_processor.h"
#include "./auxiliary_macros.h"
#include <list>
#include <memory>
#include <set>
#include <string>
#include <vector>

/**
  @brief Class for extracting and processing words from files previously
  processed by morphological analyzer

  This class extracts words from given files, verifies what are they: lemmas,
  punctuation signs or dividers. It should also be used for adding new words by
  finding agreements in cases and genders between them and previous ones and
  adding new phrases.
*/
class WordsProcessor {
 public:
  /**
    Initializes object by parsing file containing prepositions and their cases
    @param prepositions_filename name of file containing prepositions and their
    cases
    @throw std::runtime_error in case of any occurred errors
  */
  void ParsePrepositionsFile(const std::string& prepositions_filename) {
    prepositions_processor_.ParseFile(prepositions_filename);
  }

  /**
    Extracts word from current line from the file
    @param[out] tokens vector containing tokens on the processing line
    @param[out] is_word flag indicating whether extracted word is real word
    @paran[out] was_punctuation_divider flag indicating whether there was a
    punctuation divider
    @param[out] word_properties structure containing extracted word properties
    @throw std::runtime_error in case of any occurred errors
  */
  void ExtractWordFromLine(const std::vector<std::string>& tokens,
                           bool& is_word,
                           bool& was_punctuation_divider,
                           WordProperties* word_properties) const;

  /**
    Adds new words depending on parts of speech, their cases and genders
    @param same_word_forms words with the same forms that are adding
    @param[out] continue_processing flag indicating whether processing should
    be continued without any adding new words
    @param[out] previous_words vector containing previous words of the same form
    @param[out] words_to_store vector where words that should be stored
    will be placed
    @note If continue_processing == false after calling this function,
    previous_words will contain new words
  */
  void AddNewWords(const std::vector<WordProperties>& same_word_forms,
                   bool& continue_processing,
                   std::vector<WordProperties>* previous_words,
                   std::vector<WordProperties>* words_to_store) const;

 private:
  /**
    Processes number
    @param[out] was_punctuation_divider flag indicating whether there was a
    punctuation divider
    @param[out] word_properties properties of the processing data
  */
  void ProcessNumber(bool& was_punctuation_divider,
                     WordProperties* word_properties) const;

  /**
    Processes sign that is not a lemma. It tries to find whether this sign marks
    the end of the sentence or no
    @param tokens vector containing tokens from the processing line
    @param[out] was_punctuation_divider flag indicating whether there was a
    punctuation divider
    @param[out] word_properties structure containing extracted word properties
    @note In case of error file will not be closed
  */
  void ProcessSign(const std::vector<std::string>& tokens,
                   bool& was_punctuation_divider,
                   WordProperties* word_properties) const;

  /**
    Processes lemma
    @param tokens vector containing tokens from the processing line
    @param[out] was_punctuation_divider flag indicating whether there was a
    punctuation divider
    @param[out] is_word flag indicating whether word was read
    @param[out] word_properties structure containing extracted word properties
    @note In case of error file will not be closed
  */
  void ProcessLemma(const std::vector<std::string>& tokens,
                    bool& was_punctuation_divider,
                    bool& is_word,
                    WordProperties* word_properties) const;

  /**
    Tries to add new word. Word will be added only if it matches grammatical
    rules of Russian language (only cases and genders are taken into account)
    @param previous_words vector containing all previous words
    @param[out] word_properties new word to add (cases will be reduced for
    agreement only)
    @param[out] agreement_words vector where words that are in agreement with
    adding one will be placed
    @return true if new word can be added (in this case all agreement words will
    be placed into given parameter)
    @see WordProperties
  */
  bool TryToAddWord(const std::vector<WordProperties>& previous_words,
                    WordProperties* word_properties,
                    std::vector<WordProperties>* agreement_words) const;

  /**
    Checks whether there is agreement in genitive
    @param next_word adding word that should be in genitive
    @param previous_word previous word that should be in agreement
    @return true if there is an agreement
  */
  bool AgreementInGenitive(const ChangeableWordParts& next_word,
                           const WordProperties& previous_word) const {
    return (next_word.word_case == WordCase::GENITIVE ||
            next_word.word_case == WordCase::NO_CASE)
            && previous_word.word_category == WordCategory::NOUN;
  }

  /**
    Checks whether there is agreement in case
    @param next_word adding word for checking
    @param previous_word previous word for checking
    @return true if there is an agreement
  */
  bool AgreementInCase(const ChangeableWordParts& next_word,
                       const ChangeableWordParts& previous_word) const {
    return next_word.word_case == previous_word.word_case ||
           next_word.word_case == WordCase::NO_CASE ||
           previous_word.word_case == WordCase::NO_CASE;
  }

  /**
    Checks whether there is agreement in gender
    @param next_word adding word for checking
    @param previous_word previous word for checking
    @return true if there is an agreement
  */
  bool AgreementInGender(const ChangeableWordParts& next_word,
                         const ChangeableWordParts& previous_word) const {
    return next_word.word_gender == previous_word.word_gender ||
           next_word.word_gender == WordGender::NO_GENDER ||
           previous_word.word_gender == WordGender::NO_GENDER;
  }

  /**
    Saves changeable parts of word that are in agreement
    @param new_word_part part of the word that should be saved
    @param previous_word_parts previous parts of words
    @param[out] returning_word_parts pointer to set where agreement word parts
    will be placed
  */
  void SaveAgreementWordParts(
    const ChangeableWordParts& new_word_part,
    const std::set<ChangeableWordParts>& previous_word_parts,
    std::set<ChangeableWordParts>* returning_word_parts) const;

  /**
    Saves given agreement word in the given vector. If such word does not exist,
    it is added. Otherwise only new agreement parts of words are added
    @param word_properties properties of the adding word
    @param agreement_word_parts parts of words that are agreed
    @param[out] agreement_words vector containing previously found agreement
    words (may be probably modified after calling this function)
  */
  void SaveAgreementWord(
      const WordProperties& word_properties,
      const std::set<ChangeableWordParts>& agreement_word_parts,
      std::vector<WordProperties>* agreement_words) const;

  /** Object which processes adjectives found in files */
  AdjectivesProcessor adjectives_processor_;

  /** Object which processes nouns found in files */
  NounsProcessor nouns_processor_;

  /** Object which processes numerals found in files */
  NumeralsProcessor numerals_processor_;

  /** Object which processes participles found in files */
  ParticiplesProcessor participles_processor_;

  /** Object which processes prepositions */
  PrepositionsProcessor prepositions_processor_;

  /** Object which processes pronouns found in files */
  PronounsProcessor pronouns_processor_;
};
