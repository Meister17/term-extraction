// Copyright 2012 Michael Nokel
#pragma once

#include "./algorithms/clustering_algorithm.h"
#include "./candidates_extractor.h"
#include "./auxiliary_macros.h"
#include <boost/scoped_ptr.hpp>
#include <string>
#include <vector>

/**
  @brief Main class for maintaining various clustering algorithms

  Firstly, this class should be used to parse files with input data. After what,
  desired clustering algorithm is performed and results of clustering are
  printed in the given file.
*/
class Clusterer {
 public:
  Clusterer() : term_candidates_type_(NO_TERMS) {
  }

  /**
    Parses files with input data and vocabulary for various types of term
    candidates
    @param single_word_input_filename file with input data for single-word term
    candidates
    @param single_word_vocabulary_filename file with vocabulary for single-word
    term candidates
    @param two_word_input_filename file with input data for two-word term
    candidates
    @param two_word_vocabulary_filename file with vocabulary for two-word term
    candidates
    @throw std::runtime_error in case of any occurred error
  */
  void ParseInputFiles(
      const std::string& single_word_input_filename,
      const std::string& single_word_vocabulary_filename,
      const std::string& two_word_input_filename,
      const std::string& two_word_vocabulary_filename);

  /**
    Performs given clustering algorithm on previously parsed input data
    @param clustering_algorithm algorithm of clustering to be performed
    @param clustering_options vector containing options for clustering algorithm
    @param term_frequency term frequency to use
    @param document_frequency document frequency to use
    @param number_of_clusters desired number of clusters
    @param single_word_output_filename file where results for single-word term
    candidates should be written
    @param two_word_output_filename file where results for two-word term
    candidates should be written
    @throw std::runtime_error in case of any occurred errors
  */
  void Cluster(const std::string& clustering_algorithm,
               const std::vector<std::string>& clustering_options,
               const std::string& term_frequency,
               const std::string& document_frequency,
               int number_of_clusters,
               const std::string& single_word_output_filename,
               const std::string& two_word_output_filename);

 private:
  /** All possible types of term candidates to extract: single-word and two-word
      term candidates */
  enum TermCandidatesType {
    NO_TERMS,
    SINGLE_WORD_TERMS,
    TWO_WORD_TERMS,
    ALL_TERMS
  } term_candidates_type_;

  /**
    Applies specified term frequency to the matrix
    @param term_frequency specified term frequency to be applied to the matrix
    @param[out] matrix matrix to which term frequency should be applied
  */
  void ApplyTermFrequency(
      const std::string& term_frequency,
      std::vector<std::vector<SparseMatrixEntry> >* matrix) const;

  /**
    Applies specified document frequency to the matrix
    @param document_frequency specified document frequency to be applied to the
    matrix
    @param[out] matrix matrix to which document frequency should be applied
  */
  void ApplyDocumentFrequency(
      const std::string& document_frequency,
      std::vector<std::vector<SparseMatrixEntry> >* matrix) const;

  /** Pointer to particular realization of clustering algorithm */
  boost::scoped_ptr<ClusteringAlgorithm> clustering_algorithm_;

  /** Object for extracting single-word term candidates */
  CandidatesExtractor single_word_terms_extractor_;

  /** Object for extracting two-word term candidates */
  CandidatesExtractor two_word_terms_extractor_;
};