// Copyright 2012 Michael Nokel
#pragma once

#include "./auxiliary_macros.h"
#include <iostream>
#include <string>
#include <boost/unordered_map.hpp>
#include <vector>

/**
  @brief Class for extracting term candidates from files and printing results of
  clustering

  This class should be used for parsing input and vocabulary files for specified
  type of terms (single-word or two-word ones) and printing results of
  clustering for this type of terms.
*/
class CandidatesExtractor {
 public:
  /**
    Parses file with input data for particular type of terms
    @param filename file containing input data
    @throw std::runtime_error in case of any occurred error
  */
  void ParseInputFile(const std::string& filename);

  /**
    Parses file with vocabulary for particular type of terms
    @param filename file containing vocabulary
    @throw std::runtime_error in case of any occurred error
  */
  void ParseVocabularyFile(const std::string& filename);

  /**
    Sorts words in clusters in the descending order of their normalized term
    frequencies
    @param number_of_clusters number of clusters
    @param documents_assignments vector containing assignments of documents to
    clusters
    @param documents_numbers vector containing numbers of non-empty documents
    that were clustered
    @param[out] words_assignments vector where assignments of words to clusters
    will be stored
  */
  void SortWordsInClusters(
      int number_of_clusters,
      const std::vector<size_t>& documents_assignments,
      const std::vector<size_t>& documents_numbers,
      std::vector<boost::unordered_map<unsigned int, double> >*
          words_assignments) const;

  /**
    Prints results of clustering to the given file
    @param words_assignments assignments of words to clusters
    @param filename file where results of clustering should be printed
    @throw std::runtime_error in case of any occurred errors
  */
  void PrintResults(
      const std::vector<boost::unordered_map<unsigned int, double> >&
          words_assignments,
      const std::string& filename) const;

  std::vector<std::vector<SparseMatrixEntry> > matrix() const {
    return matrix_;
  }

  std::vector<size_t> non_empty_documents() const {
    return non_empty_documents_;
  }

 private:
  /** Matrix for clustering */
  std::vector<std::vector<SparseMatrixEntry> > matrix_;

  /** Vector containing all words in documents in alphabetical order */
  std::vector<std::string> vocabulary_;

  /** Vector containing numbers of non-empty documents */
  std::vector<size_t> non_empty_documents_;
};
