// Copyright 2012 Michael Nokel
#include "./clustering_algorithm.h"
#include "../auxiliary_macros.h"
#include <algorithm>
#include <string>
#include <vector>

using std::max;
using std::string;
using std::vector;

size_t ClusteringAlgorithm::GetColumnsNumber(
    const vector<vector<SparseMatrixEntry> >& matrix) const {
  size_t columns_number = 0;
  for (size_t index = 0; index < matrix.size(); ++index) {
    columns_number = max(columns_number, matrix[index].back().column_index);
  }
  return columns_number + 1;
}

double ClusteringAlgorithm::CalculateSquaredDistance(
    const vector<SparseMatrixEntry>& sparse_vector,
    const vector<double>& dense_vector) const {
  double distance = 0.0;
  size_t dense_index = 0;
  for (size_t sparse_index = 0; sparse_index < sparse_vector.size();
       ++sparse_index) {
    while (dense_index < sparse_vector[sparse_index].column_index) {
      distance += dense_vector[dense_index] * dense_vector[dense_index];
      ++dense_index;
    }
    distance += (dense_vector[dense_index] - sparse_vector[sparse_index].value)
              * (dense_vector[dense_index] - sparse_vector[sparse_index].value);
    ++dense_index;
  }
  while (dense_index < dense_vector.size()) {
    distance += dense_vector[dense_index] * dense_vector[dense_index];
    ++dense_index;
  }
  return distance;
}

double ClusteringAlgorithm::CalculateSquaredDistance(
    const vector<double>& first_vector,
    const vector<double>& second_vector) const {
  double distance = 0.0;
#pragma omp parallel for reduction(+:distance) schedule(static, 100)
  for (size_t index = 0; index < first_vector.size(); ++index) {
    distance += (first_vector[index] - second_vector[index]) *
                (first_vector[index] - second_vector[index]);
  }
  return distance;
}
