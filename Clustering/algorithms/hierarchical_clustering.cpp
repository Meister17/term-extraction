// Copyright 2012 Michael Nokel
#include "./hierarchical_clustering.h"
#include "../auxiliary_macros.h"
#include <omp.h>
#include <algorithm>
#include <cfloat>
#include <cmath>
#include <iostream>
#include <list>
#include <stdexcept>
#include <string>
#include <vector>

using std::cout;
using std::list;
using std::max;
using std::min;
using std::runtime_error;
using std::string;
using std::swap;
using std::vector;

void HierarchicalClustering::ParseClusteringAlgorithmOptions(
    const vector<string>& clustering_options) {
  if (clustering_options.size() != 1) {
    throw runtime_error("Wrong options for hierarchical clustering");
  }
  if (clustering_options.front() == "single-linkage") {
    linkage_ = SINGLE_LINKAGE;
  } else if (clustering_options.front() == "complete-linkage") {
    linkage_ = COMPLETE_LINKAGE;
  } else if (clustering_options.front() == "average-linkage") {
    linkage_ = AVERAGE_LINKAGE;
  } else {
    throw runtime_error("Wrong type of linkage for hierarchical clustering");
  }
}

void HierarchicalClustering::Cluster(
    const vector<vector<SparseMatrixEntry> >& matrix,
    int number_of_clusters,
    vector<size_t>* documents_assignments) {
  cout << "Starting hierarchical clustering algorithm\n";
  omp_set_num_threads(kNumberThreads_);
  list<list<size_t> > documents_in_clusters(matrix.size(), list<size_t>());
  InitializeDocumentsInClusters(&documents_in_clusters);
  vector<vector<double> > distances(matrix.size(), vector<double>());
  CalculateDistances(matrix, &distances);
  for (int current_clusters_number = static_cast<int>(matrix.size());
       current_clusters_number != number_of_clusters;
       --current_clusters_number) {
    cout << "Number of clusters: " << current_clusters_number << "\n";
    list<list<size_t> >::iterator first_cluster;
    list<list<size_t> >::iterator second_cluster;
    FindClosestDocuments(distances,
                         &documents_in_clusters,
                         first_cluster,
                         second_cluster);
    UnionClusters(first_cluster, second_cluster, &documents_in_clusters);
  }
  documents_assignments->resize(matrix.size(), 0);
  CalculateDocumentsAssignments(documents_in_clusters, documents_assignments);
}

void HierarchicalClustering::InitializeDocumentsInClusters(
    list<list<size_t> >* documents_in_clusters) const {
  size_t cluster_number = 0;
  for (list<list<size_t> >::iterator iterator = documents_in_clusters->begin();
       iterator != documents_in_clusters->end(); ++iterator) {
    iterator->push_back(cluster_number);
    ++cluster_number;
  }
}

void HierarchicalClustering::CalculateDistances(
    const vector<vector<SparseMatrixEntry> >& matrix,
    vector<vector<double> >* distances) const {
  cout << "Calculating distances\n";
#pragma omp parallel for schedule(static, 100)
  for (size_t first_index = 0; first_index < matrix.size(); ++first_index) {
    for (size_t second_index = first_index + 1; second_index < matrix.size();
         ++second_index) {
      distances->at(first_index).push_back(
          CalculateDistanceBetweenDocuments(matrix[first_index],
                                            matrix[second_index]));
    }
  }
}

double HierarchicalClustering::CalculateDistanceBetweenDocuments(
    const vector<SparseMatrixEntry>& first_vector,
    const vector<SparseMatrixEntry>& second_vector) const {
  double distance = 0.0;
  size_t first_index = 0;
  size_t second_index = 0;
  while (first_index < first_vector.size() &&
         second_index < second_vector.size()) {
    if (first_vector[first_index].column_index <
        second_vector[second_index].column_index) {
      distance += first_vector[first_index].value *
                  first_vector[first_index].value;
      ++first_index;
    } else if (first_vector[first_index].column_index >
               second_vector[second_index].column_index) {
      distance += second_vector[second_index].value *
                  second_vector[second_index].value;
      ++second_index;
    } else {
      distance += (first_vector[first_index].value -
                   second_vector[second_index].value) *
                  (first_vector[first_index].value -
                   second_vector[second_index].value);
      ++first_index;
      ++second_index;
    }
  }
  while (first_index < first_vector.size()) {
    distance += first_vector[first_index].value *
                first_vector[first_index].value;
    ++first_index;
  }
  while (second_index < second_vector.size()) {
    distance += second_vector[second_index].value *
                second_vector[second_index].value;
    ++second_index;
  }
  return sqrt(distance);
}

void HierarchicalClustering::FindClosestDocuments(
    const vector<vector<double> >& distances,
    list<list<size_t> >* documents_in_clusters,
    list<list<size_t> >::iterator& first_cluster,
    list<list<size_t> >::iterator& second_cluster) const {
  double minimal_distance = DBL_MAX;
  first_cluster = second_cluster = documents_in_clusters->begin();
  for (list<list<size_t> >::iterator first_iterator =
       documents_in_clusters->begin(); first_iterator !=
       documents_in_clusters->end(); ++first_iterator) {
    list<list<size_t> >::iterator second_iterator = first_iterator;
    for (++second_iterator; second_iterator != documents_in_clusters->end();
         ++second_iterator) {
      double new_distance = CalculateDistanceBetweenClusters(distances,
                                                             first_iterator,
                                                             second_iterator);
      if (new_distance < minimal_distance) {
        minimal_distance = new_distance;
        first_cluster = first_iterator;
        second_cluster = second_iterator;
      }
    }
  }
}

double HierarchicalClustering::CalculateDistanceBetweenClusters(
    const vector<vector<double> >& distances,
    list<list<size_t> >::iterator first_cluster,
    list<list<size_t> >::iterator second_cluster) const {
  vector<double> document_distances;
  for (list<size_t>::iterator first_iterator = first_cluster->begin();
       first_iterator != first_cluster->end(); ++first_iterator) {
    for (list<size_t>::iterator second_iterator = second_cluster->begin();
         second_iterator != second_cluster->end(); ++second_iterator) {
      size_t first_index = min(*first_iterator, *second_iterator);
      size_t second_index = abs(*second_iterator - *first_iterator) - 1;
      document_distances.push_back(distances[first_index][second_index]);
    }
  }
  double distance = 0.0;
  if (linkage_ == SINGLE_LINKAGE) {
    distance = DBL_MAX;
    for (size_t index = 0; index < document_distances.size(); ++index) {
      distance = min(distance, document_distances[index]);
    }
  } else if (linkage_ == COMPLETE_LINKAGE) {
    for (size_t index = 0; index < document_distances.size(); ++index) {
      distance = max(distance, document_distances[index]);
    }
  } else if (linkage_ == AVERAGE_LINKAGE) {
    for (size_t index = 0; index < document_distances.size(); ++index) {
      distance += document_distances[index];
    }
    distance /= static_cast<double>(document_distances.size());
  }
  return distance;
}

void HierarchicalClustering::UnionClusters(
    list<list<size_t> >::iterator first_cluster,
    list<list<size_t> >::iterator second_cluster,
    list<list<size_t> >* documents_in_clusters) const {
  first_cluster->splice(first_cluster->end(), *second_cluster);
  documents_in_clusters->erase(second_cluster);
}

void HierarchicalClustering::CalculateDocumentsAssignments(
    const list<list<size_t> >& documents_in_clusters,
    vector<size_t>* documents_assignments) const {
  size_t cluster_number = 0;
  for (list<list<size_t> >::const_iterator cluster_iterator =
       documents_in_clusters.begin(); cluster_iterator !=
       documents_in_clusters.end(); ++cluster_iterator) {
    for (list<size_t>::const_iterator document_iterator =
         cluster_iterator->begin(); document_iterator !=
         cluster_iterator->end(); ++document_iterator) {
      documents_assignments->at(*document_iterator) = cluster_number;
    }
    ++cluster_number;
  }
}
