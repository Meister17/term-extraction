// Copyright 2012 Michael Nokel
#pragma once

#include "./clustering_algorithm.h"
#include "../auxiliary_macros.h"
#include <cmath>
#include <string>
#include <vector>

class CandidatesExtractor;

/**
  @brief Class implementing K-Means algorithm

  This class implements K-Means clustering algorithm: regular or spherical.
*/
class KMeans : public ClusteringAlgorithm {
 public:
  KMeans(CandidatesExtractor* single_words_extractor,
         CandidatesExtractor* two_words_extractor)
      : ClusteringAlgorithm(single_words_extractor, two_words_extractor),
        k_means_type_(NO_KMEANS_TYPE),
        maximum_iterations_number_(0) {
  }

  /**
    Parses options for clustering algorithm
    @param clustering_options vector containing options for clustering algorithm
    @throw std::runtime_error in case of any occurred errors
  */
  void ParseClusteringAlgorithmOptions(
      const std::vector<std::string>& clustering_options);

  /**
    Clusters given matrix of data into given number of clusters
    @param matrix sparse matrix with input data for clustering
    @param number_of_clusters desired number of clusters
    @param[out] documents_assignments vector where assignments of documents to
    clusters will be stored
  */
  void Cluster(
      const std::vector<std::vector<SparseMatrixEntry> >& matrix,
      int number_of_clusters,
      std::vector<size_t>* documents_assignments);

 private:
  /**
    @brief Type of K-Means clustering algorithm

    It can be regular K-Means or spherical K-Means (no type is also possible)
  */
  enum KMeansType {
    NO_KMEANS_TYPE,
    REGULAR,
    SPHERICAL
  } k_means_type_;

  /**
    Normalizes rows in given matrix so that they are on the unit hypersphere.
    This should be done before performing spherical K-Means
    @param[out] matrix matrix to be normalized
  */
  void NormalizeMatrix(std::vector<std::vector<SparseMatrixEntry> >* matrix)
      const;

  /**
    Calculates norm of the given sparse vector
    @param element sparse vector to calculate norm for
    @return calculated norm of the given sparse vector
  */
  double CalculateNorm(const std::vector<SparseMatrixEntry>& element) const;

  /**
    Calculates norm of the given dense vector
    @param element dense vector to calculate norm for
    @return calculated norm of the given dense vector
  */
  double CalculateNorm(const std::vector<double>& element) const;

  /**
    Initialize centers to random points of sparse matrix
    @param matrix sparse matrix with input data for clustering
    @param[out] center vector containing centers of clusters
  */
  void InitializeCenters(
      const std::vector<std::vector<SparseMatrixEntry> >& matrix,
      std::vector<std::vector<double> >* centers) const;

  /**
    Copies sparse matrix entry to the given center of cluster
    @param matrix_entry sparse matrix entry that should be copied
    @param[out] center center of cluster where sparse matrix entry should be
    copied
  */
  void CopySparseMatrixEntry(
      const std::vector<SparseMatrixEntry>& matrix_entry,
      std::vector<double>* center) const;

  /**
    Assigns documents to clusters
    @param matrix sparse matrix with input data for clustering
    @param centers vector containing centers of clusters
    @param[out] documents_assignments vector containing assignments of words to
    clusters
    @return true if some changes have occurred
  */
  bool AssignDocumentsToClusters(
      const std::vector<std::vector<SparseMatrixEntry> >& matrix,
      const std::vector<std::vector<double> >& centers,
      std::vector<size_t>* documents_assignments) const;

  /**
    Assigns documents to clusters in regular K-Means clustering algorithm
    @param matrix sparse matrix with input data for clustering
    @param centers vector containing centers of clusters
    @param[out] documents_assignments vector containing assignments of words to
    clusters
    @return true if some changes have occurred
  */
  bool AssignDocumentsInRegularKMeans(
      const std::vector<std::vector<SparseMatrixEntry> >& matrix,
      const std::vector<std::vector<double> >& centers,
      std::vector<size_t>* documents_assignments) const;

  /**
    Assigns documents to clusters in spherical K-Means clustering algorithm
    @param matrix sparse matrix with input data for clustering
    @param centers vector containing centers of clusters
    @param[out] documents_assignments vector containing assignments of words to
    clusters
    @return true if some changes have occurred
  */
  bool AssignDocumentsInSphericalKMeans(
      const std::vector<std::vector<SparseMatrixEntry> >& matrix,
      const std::vector<std::vector<double> >& centers,
      std::vector<size_t>* documents_assignments) const;

  /**
    Calculates Cosine Similarity between two normalized vectors
    @param sparse_vector sparse vector to be used in calculations
    @param dense_vector dense vector to be used in calculations
    @return calculated Cosine Similarity between two given normalized vectors
  */
  double CalculateCosineSimilarity(
      const std::vector<SparseMatrixEntry>& sparse_vector,
      const std::vector<double>& dense_vector) const;

  /**
    Updates centers of clusters
    @param matrix sparse matrix with input data for clustering
    @param documents_assignments vector containing assignments of words to
    clusters
    @param[out] centers vector containing centers of clusters
  */
  void UpdateCenters(
      const std::vector<std::vector<SparseMatrixEntry> >& matrix,
      const std::vector<size_t>& documents_assignments,
      std::vector<std::vector<double> >* centers) const;

  /** Maximum number of iterations to perform */
  size_t maximum_iterations_number_;
};
