// Copyright 2012 Michael Nokel
#pragma once

#include "../auxiliary_macros.h"
#include <boost/thread.hpp>
#include <string>
#include <vector>

class CandidatesExtractor;

/**
  @brief Base abstract class for all clustering algorithms

  This class should be derived by all particular clustering algorithms.
*/
class ClusteringAlgorithm {
 public:
  ClusteringAlgorithm(CandidatesExtractor* single_words_extractor,
                      CandidatesExtractor* two_words_extractor)
      : kNumberThreads_(
            static_cast<int>(boost::thread::hardware_concurrency())),
        single_word_terms_extractor_(single_words_extractor),
        two_word_terms_extractor_(two_words_extractor) {
  }

  /**
    Parses options for clustering algorithm
    @param clustering_options vector containing options for clustering algorithm
    @throw std::runtime_error in case of any occurred error
  */
  virtual void ParseClusteringAlgorithmOptions(
      const std::vector<std::string>& clustering_options) = 0;

  /**
    Clusters given matrix containing input data into the given number of
    clusters
    @param matrix matrix containing input data
    @param number_of_clusters number of clusters
    @param[out] documents_assignments vector where assignments of documents to
    clusters will be stored
  */
  virtual void Cluster(
      const std::vector<std::vector<SparseMatrixEntry> >& matrix,
      int number_of_clusters,
      std::vector<size_t>* documents_assignments) = 0;

  /**
    Virtual destructor for the purposes of proper deriving
  */
  virtual ~ClusteringAlgorithm() {
  }

 protected:
  /**
    Returns number of columns in the sparse matrix
    @param matrix sparse matrix with input data for clustering
    @return number of columns in the sparse matrix
  */
  virtual size_t GetColumnsNumber(
      const std::vector<std::vector<SparseMatrixEntry> >& matrix) const;

  /**
    Calculates squared distance between given sparse and dense vectors
    @param sparse_vector given sparse vector to compute distance for
    @param dense_vector given dense vector to compute distance for
    @return squared distance between given vectors
  */
  virtual double CalculateSquaredDistance(
      const std::vector<SparseMatrixEntry>& sparse_vector,
      const std::vector<double>& dense_vector) const;

    /**
    Calculates squared distance between two given vectors
    @param first_vector first vector for calculating distance
    @param second_vector second vector for calculating distance
    @return calculated squared distance between vectors
  */
  virtual double CalculateSquaredDistance(
      const std::vector<double>& first_vector,
      const std::vector<double>& second_vector) const;

  /** Number of threads for OpenMP */
  const int kNumberThreads_;

  /** Pointer to object that works with single-word term candidates */
  CandidatesExtractor* single_word_terms_extractor_;

  /** Pointer to object that works with two-word term candidates */
  CandidatesExtractor* two_word_terms_extractor_;
};
