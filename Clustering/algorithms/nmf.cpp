// Copyright 2012 Michael Nokel
#include "./nmf.h"
#include "../auxiliary_macros.h"
#include <boost/random.hpp>
#include <boost/unordered_map.hpp>
#include <cstdlib>
#include <iostream>
#include <mkl.h>
#include <omp.h>
#include <stdexcept>
#include <string>
#include <vector>

using boost::random::mt19937;
using boost::random::uniform_real_distribution;
using boost::unordered_map;
using std::cout;
using std::runtime_error;
using std::string;
using std::vector;

void NMF::ParseClusteringAlgorithmOptions(
    const vector<string>& clustering_options) {
  if (clustering_options.size() != 3) {
    throw runtime_error("Wrong options for NMF");
  }
  if (clustering_options[0] == "euclidean") {
    divergence_metric_ = EUCLIDEAN;
  } else if (clustering_options[0] == "kullback-leibler") {
    divergence_metric_ = KULLBACK_LEIBLER;
  } else {
    throw runtime_error("Wrong divergence metric for NMF");
  }
  maximum_iterations_number_ = atoi(clustering_options[1].data());
  epsilon_ = atof(clustering_options[2].data());
  if (maximum_iterations_number_ <= 0 || epsilon_ <= 0) {
    throw runtime_error("Wrong options for NMF");
  }
}

void NMF::Cluster(const vector<vector<SparseMatrixEntry> >& matrix,
                  int number_of_clusters,
                  vector<size_t>* documents_assignments) {
  cout << "Starting NMF clustering algorithm\n";
  omp_set_num_threads(kNumberThreads_);
  CompressedSparseColumnMatrix A;
  CreateCompressedSparseColumnMatrix(matrix, &A);
  DenseMatrix H(number_of_clusters, A.columns_number);
  DenseMatrix W(A.rows_number, number_of_clusters);
  InitializeMatrix(&H);
  InitializeMatrix(&W);
  char matdescra[4];
  matdescra[0] = 'G';
  matdescra[3] = 'C';
  if (divergence_metric_ == EUCLIDEAN) {
    ClusterInEuclideanDivergence(matdescra, &A, &W, &H);
  } else if (divergence_metric_ == KULLBACK_LEIBLER) {
    ClusterInKullbackLeiblerDivergence(matdescra, &A, &W, &H);
  }
  NormalizeMatrices(&W, &H);
  documents_assignments->resize(matrix.size(), 0);
  CalculateDocumentsAssignments(H, documents_assignments);
  CalculateWordsAssignments(W, &words_assignments_);
}

void NMF::CreateCompressedSparseColumnMatrix(
    const vector<vector<SparseMatrixEntry> >& matrix,
    CompressedSparseColumnMatrix* sparse_matrix) const {
  sparse_matrix->rows_number = static_cast<int>(GetColumnsNumber(matrix));
  sparse_matrix->columns_number = static_cast<int>(matrix.size());
  sparse_matrix->columns_indexes = new int[sparse_matrix->columns_number + 1];
  int non_zero_numbers = 0;
  int non_zero_index = 0;
  double element = matrix.front().front().value;
  for (int document_index = 0;
       document_index < sparse_matrix->columns_number; ++document_index) {
    sparse_matrix->columns_indexes[document_index] = non_zero_numbers;
    non_zero_numbers += static_cast<int>(matrix[document_index].size());
    sparse_matrix->values = static_cast<double*>(
        realloc(sparse_matrix->values, non_zero_numbers * sizeof(element)));
    sparse_matrix->rows_indexes = static_cast<int*>(
        realloc(sparse_matrix->rows_indexes,
                non_zero_numbers * sizeof(non_zero_index)));
    for (size_t second_index = 0; second_index < matrix[document_index].size();
         ++second_index) {
      sparse_matrix->values[non_zero_index] =
          matrix[document_index][second_index].value;
      sparse_matrix->rows_indexes[non_zero_index] =
          matrix[document_index][second_index].column_index;
      ++non_zero_index;
    }
  }
  sparse_matrix->columns_indexes[sparse_matrix->columns_number] =
      non_zero_numbers;
  sparse_matrix->non_zero_numbers = non_zero_numbers;
}

void NMF::InitializeMatrix(DenseMatrix* matrix) const {
  mt19937 generator;
  uniform_real_distribution<double> distribution(epsilon_, 1 - epsilon_);
  int total_size = matrix->rows_number * matrix->columns_number;
  for (int index = 0; index < total_size; ++index) {
    matrix->values[index] = distribution(generator);
  }
}

void NMF::ClusterInEuclideanDivergence(char* matdescra,
                                       CompressedSparseColumnMatrix* A,
                                       DenseMatrix* W,
                                       DenseMatrix* H) const {
  // parameters for BLAS routines
  double alpha = 1.0;
  double beta = 0.0;
  char transpose_string[1];
  transpose_string[0] = 'T';
  char non_transpose_string[1];
  non_transpose_string[0] = 'N';
  // create and initialize auxiliary matrices for H update
  DenseMatrix AtW(A->columns_number, W->columns_number);
  DenseMatrix WtA(W->columns_number, A->columns_number);
  DenseMatrix WtW(W->columns_number, W->columns_number);
  DenseMatrix WtWH(W->columns_number, H->columns_number);
  // create and initialize auxiliary matrices for W update
  DenseMatrix Ht(H->columns_number, H->rows_number);
  DenseMatrix AHt(A->rows_number, H->rows_number);
  DenseMatrix HHt(H->rows_number, H->rows_number);
  DenseMatrix WHHt(W->rows_number, H->rows_number);
  // compute NMF in <max_iter> iterations
  for (size_t iteration = 1; iteration <= maximum_iterations_number_;
       ++iteration) {
    cout << "Iteration " << iteration << "\n";
    // compute AtW matrix
    mkl_dcscmm(transpose_string,
               &A->rows_number,
               &AtW.columns_number,
               &A->columns_number,
               &alpha,
               matdescra,
               A->values,
               A->rows_indexes,
               A->columns_indexes,
               A->columns_indexes + 1,
               W->values,
               &W->columns_number,
               &beta,
               AtW.values,
               &AtW.columns_number);
    // transpose matrix -> compute WtA
    TransposeMatrix(AtW, &WtA);
    // compute WtW matrix
    cblas_dgemm(CblasRowMajor,
                CblasTrans,
                CblasNoTrans,
                WtW.rows_number,
                WtW.columns_number,
                W->rows_number,
                alpha,
                W->values,
                W->columns_number,
                W->values,
                W->columns_number,
                beta,
                WtW.values,
                WtW.columns_number);
    // compute WtWH matrix
    cblas_dgemm(CblasRowMajor,
                CblasNoTrans,
                CblasNoTrans,
                WtW.rows_number,
                WtWH.columns_number,
                WtW.columns_number,
                alpha,
                WtW.values,
                WtW.columns_number,
                H->values,
                H->columns_number,
                beta,
                WtWH.values,
                WtWH.columns_number);
    // divide matrices -> update H matrix
    DivideMatrices(WtA, WtWH, H);
    // transpose H matrix
    TransposeMatrix(*H, &Ht);
    // compute AHt matrix
    mkl_dcscmm(non_transpose_string,
               &A->rows_number,
               &AHt.columns_number,
               &A->columns_number,
               &alpha,
               matdescra,
               A->values,
               A->rows_indexes,
               A->columns_indexes,
               A->columns_indexes + 1,
               Ht.values,
               &Ht.columns_number,
               &beta,
               AHt.values,
               &AHt.columns_number);
    // compute HHt matrix
    cblas_dgemm(CblasRowMajor,
                CblasNoTrans,
                CblasNoTrans,
                HHt.rows_number,
                HHt.columns_number,
                H->columns_number,
                alpha,
                H->values,
                H->columns_number,
                Ht.values,
                Ht.columns_number,
                beta,
                HHt.values,
                HHt.columns_number);
    // compute WHHt matrix
    cblas_dgemm(CblasRowMajor,
                CblasNoTrans,
                CblasNoTrans,
                WHHt.rows_number,
                WHHt.columns_number,
                W->columns_number,
                alpha,
                W->values,
                W->columns_number,
                HHt.values,
                HHt.columns_number,
                beta,
                WHHt.values,
                WHHt.columns_number);
    // divide matrices -> update W matrix
    DivideMatrices(AHt, WHHt, W);
  }
}

void NMF::ClusterInKullbackLeiblerDivergence(char* matdescra,
                                             CompressedSparseColumnMatrix* A,
                                             DenseMatrix* W,
                                             DenseMatrix* H) const {
  // parameters for BLAS routines
  double alpha = 1.0;
  double beta = 0.0;
  char transpose_string[1];
  transpose_string[0] = 'T';
  char non_transpose_string[1];
  non_transpose_string[0] = 'N';
  // auxiliary matrices for both updates
  CompressedSparseColumnMatrix V;
  V.columns_number = A->columns_number;
  V.columns_indexes = A->columns_indexes;
  V.rows_number = A->rows_number;
  V.rows_indexes = A->rows_indexes;
  V.values = new double[A->non_zero_numbers];
  // auxiliary matrix for H update
  DenseMatrix VtW(V.columns_number, W->columns_number);
  DenseMatrix WtV(W->columns_number, V.columns_number);
  vector<double> Wsum(W->columns_number, 0.0);
  // auxiliary matrix for W update
  DenseMatrix Ht(H->columns_number, H->rows_number);
  DenseMatrix VHt(A->rows_number, H->rows_number);
  vector<double> Htsum(Ht.columns_number, 0.0);
  // compute NMF in <max_iter> iterations
  for (size_t iteration = 1; iteration <= maximum_iterations_number_;
       ++iteration) {
    cout << "Iteration " << iteration << "\n";
    // compute V = A/(WH)
    CalculateSparseMatrixByDividing(*A, *W, *H, &V);
    // compute VtW
    mkl_dcscmm(transpose_string,
               &V.rows_number,
               &VtW.columns_number,
               &V.columns_number,
               &alpha,
               matdescra,
               V.values,
               V.rows_indexes,
               V.columns_indexes,
               V.columns_indexes + 1,
               W->values,
               &W->columns_number,
               &beta,
               VtW.values,
               &VtW.columns_number);
    // transpose matrix -> compute VWt
    TransposeMatrix(VtW, &WtV);
    // compute sum of rows for W
    CalculateRowsSum(*W, &Wsum);
    // divide matrices -> update H matrix
    DivideMatricesForH(WtV, Wsum, H);
    // compute Ht
    TransposeMatrix(*H, &Ht);
    // compute A/(WH) * Ht
    mkl_dcscmm(non_transpose_string,
               &V.rows_number,
               &VHt.columns_number,
               &V.columns_number,
               &alpha,
               matdescra,
               V.values,
               V.rows_indexes,
               V.columns_indexes,
               V.columns_indexes + 1,
               Ht.values,
               &Ht.columns_number,
               &beta,
               VHt.values,
               &VHt.columns_number);
    // calculate sum of rows in Ht
    CalculateRowsSum(Ht, &Htsum);
    // divide matrices -> update W matrix
    DivideMatricesForW(VHt, Htsum, W);
    NormalizeMatrices(W, H);
  }
  V.columns_indexes = NULL;
  V.rows_indexes = NULL;
}

void NMF::TransposeMatrix(const DenseMatrix& source_matrix,
                          DenseMatrix* destination_matrix) const {
  for (int first_index = 0; first_index < source_matrix.rows_number;
       ++first_index) {
    for (int second_index = 0; second_index < source_matrix.columns_number;
         ++second_index) {
      int source_index = first_index * source_matrix.columns_number +
          second_index;
      int destination_index =
          second_index * destination_matrix->columns_number + first_index;
      destination_matrix->values[destination_index] =
          source_matrix.values[source_index];
    }
  }
}

void NMF::DivideMatrices(const DenseMatrix& first_matrix,
                         const DenseMatrix& second_matrix,
                         DenseMatrix* destination_matrix) const {
  int total_size = first_matrix.rows_number * first_matrix.columns_number;
  for (int index = 0; index < total_size; ++index) {
    destination_matrix->values[index] *= first_matrix.values[index] /
        (second_matrix.values[index] + epsilon_);
  }
}

void NMF::CalculateSparseMatrixByDividing(
    const CompressedSparseColumnMatrix& sparse_matrix,
    const DenseMatrix& first_dense_matrix,
    const DenseMatrix& second_dense_matrix,
    CompressedSparseColumnMatrix* result_matrix) const {
  for (int column_index = sparse_matrix.columns_indexes[0];
       column_index < sparse_matrix.columns_number; ++column_index) {
    for (int nnz_index = sparse_matrix.columns_indexes[column_index];
         nnz_index < sparse_matrix.columns_indexes[column_index + 1];
         ++nnz_index) {
      int row_number = sparse_matrix.rows_indexes[nnz_index];
      double value = 0.0;
      for (int middle_index = 0;
           middle_index < first_dense_matrix.columns_number; ++middle_index) {
        int first_index = row_number * first_dense_matrix.columns_number +
            middle_index;
        int second_index = middle_index * second_dense_matrix.columns_number +
            column_index;
        value += first_dense_matrix.values[first_index] *
            second_dense_matrix.values[second_index];
      }
      result_matrix->values[nnz_index] = sparse_matrix.values[nnz_index] /
          (value + epsilon_);
    }
  }
}

void NMF::CalculateRowsSum(const DenseMatrix& source_matrix,
                           vector<double>* destination_vector) const {
  for (int first_index = 0; first_index < source_matrix.columns_number;
       ++first_index) {
    double row_sum = 0.0;
    for (int second_index = 0; second_index < source_matrix.rows_number;
         ++second_index) {
      int index = second_index * source_matrix.columns_number + first_index;
      row_sum += source_matrix.values[index];
    }
    destination_vector->at(first_index) = row_sum;
  }
}

void NMF::DivideMatricesForH(const DenseMatrix& source_matrix,
                             const vector<double>& source_vector,
                             DenseMatrix* destination_matrix) const {
  for (int first_index = 0; first_index < destination_matrix->rows_number;
       ++first_index) {
    for (int second_index = 0;
         second_index < destination_matrix->columns_number; ++second_index) {
      int index = first_index * destination_matrix->columns_number +
          second_index;
      destination_matrix->values[index] *= source_matrix.values[index] /
          (source_vector[first_index] + epsilon_);
    }
  }
}

void NMF::DivideMatricesForW(const DenseMatrix& source_matrix,
                             const std::vector<double>& source_vector,
                             DenseMatrix* destination_matrix) const {
  for (int first_index = 0; first_index < destination_matrix->rows_number;
       ++first_index) {
    for (int second_index = 0;
         second_index < destination_matrix->columns_number; ++second_index) {
      int index = first_index * destination_matrix->columns_number +
          second_index;
      destination_matrix->values[index] *= source_matrix.values[index] /
          (source_vector[second_index] + epsilon_);
    }
  }
}

void NMF::NormalizeMatrices(DenseMatrix* first_matrix,
                            DenseMatrix* second_matrix) const {
  for (int first_index = 0; first_index < first_matrix->columns_number;
       ++first_index) {
    // calculate normalizing coefficient
    double normalizing_coefficient = 0.0;
    for (int second_index = 0; second_index < first_matrix->rows_number;
         ++second_index) {
      int element_index = second_index * first_matrix->columns_number +
          first_index;
      normalizing_coefficient += first_matrix->values[element_index];
    }
    // normalize first matrix
    for (int second_index = 0; second_index < first_matrix->rows_number;
         ++second_index) {
      int element_index = second_index * first_matrix->columns_number +
          first_index;
      first_matrix->values[element_index] /= normalizing_coefficient;
    }
    // normalize second matrix
    for (int second_index = 0; second_index < second_matrix->columns_number;
         ++second_index) {
      int element_index = first_index * second_matrix->columns_number +
          second_index;
      second_matrix->values[element_index] *= normalizing_coefficient;
    }
  }
}

void NMF::CalculateDocumentsAssignments(const DenseMatrix& matrix,
                                        vector<size_t>* documents_assignments)
                                        const {
  for (int first_index = 0; first_index < matrix.columns_number;
       ++first_index) {
    int maximum_value_index = 0;
    double maximum_value = matrix.values[first_index];
    for (int second_index = 0; second_index < matrix.rows_number;
         ++second_index) {
      int element_index = second_index * matrix.columns_number + first_index;
      if (matrix.values[element_index] > maximum_value) {
        maximum_value_index = second_index;
        maximum_value = matrix.values[element_index];
      }
    }
    documents_assignments->at(first_index) = maximum_value_index;
  }
}

void NMF::CalculateWordsAssignments(
    const DenseMatrix& matrix,
    vector<unordered_map<unsigned int, double> >* terms_assignments) const {
  for (int first_index = 0; first_index < matrix.columns_number;
       ++first_index) {
    unordered_map<unsigned int, double> words_cluster_assignments;
    for (int second_index = 0; second_index < matrix.rows_number;
         ++second_index) {
      double value =
          matrix.values[second_index * matrix.columns_number + first_index];
      if (value > 0.0) {
        words_cluster_assignments[second_index] = value;
      }
    }
    terms_assignments->push_back(words_cluster_assignments);
  }
}
