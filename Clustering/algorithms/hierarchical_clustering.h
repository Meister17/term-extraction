// Copyright 2012 Michael Nokel
#pragma once

#include "./clustering_algorithm.h"
#include "../auxiliary_macros.h"
#include "../candidates_extractor.h"
#include <list>
#include <string>
#include <vector>

/**
  @brief Class implementing Hierarchical Clustering algorithm

  This class implements Hierarchical Clustering algorithm (agglomerative) with
  various options: single linking, complete linking and average linking.
*/
class HierarchicalClustering : public ClusteringAlgorithm {
 public:
  HierarchicalClustering(
      CandidatesExtractor* single_words_extractor,
      CandidatesExtractor* two_words_extractor)
      : ClusteringAlgorithm(single_words_extractor, two_words_extractor),
        linkage_(NO_LINKAGE) {
  }

  /**
    Parses options for clustering algorithm
    @param clustering_options vector containing options for clustering algorithm
    @throw std::runtime_error in case of any occurred errors
  */
  void ParseClusteringAlgorithmOptions(
      const std::vector<std::string>& clustering_options);

  /**
    Clusters given matrix of data into given number of clusters
    @param matrix matrix with input data for clustering
    @param number_of_clusters desired number of clusters
    @param[out] documents_assignments vector where assignments of documents to
    clusters will be stored
  */
  void Cluster(
      const std::vector<std::vector<SparseMatrixEntry> >& matrix,
      int number_of_clusters,
      std::vector<size_t>* documents_assignments);

 private:
  /**
    @brief All possible linkages for computing similarities between clusters

    This enumerator contains single-linkage, complete-linkage, average-linkage
    (no linkage indicates error)
  */
  enum Linkage {
    NO_LINKAGE,
    SINGLE_LINKAGE,
    COMPLETE_LINKAGE,
    AVERAGE_LINKAGE
  } linkage_;

  /**
    Assigns each document to a separate cluster
    @param[out] documents_assignments vector containing assignments of documents
    to clusters
    @param[out] documents_in_clusters vector containing list of documents
    assigned to each cluster
  */
  void InitializeDocumentsInClusters(
      std::list<std::list<size_t> >* documents_in_clusters) const;

  /**
    Calculates distances between documents and stores them in the given matrix
    @param matrix matrix with input data for clustering
    @param[out] distances matrix where distances between documents will be
    stored
  */
  void CalculateDistances(
      const std::vector<std::vector<SparseMatrixEntry> >& matrix,
      std::vector<std::vector<double> >* distances) const;

  /**
    Calculates distance between given two documents represented as sparse
    vectors of words
    @param first_vector first sparse vector to calculate distance
    @param second_vector second sparse vector to calculate distance
    @return calculated distance between given two sparse vectors
  */
  double CalculateDistanceBetweenDocuments(
      const std::vector<SparseMatrixEntry>& first_vector,
      const std::vector<SparseMatrixEntry>& second_vector) const;

  /**
    Finds closest documents in the given linkage
    @param distances matrix with distances between documents
    @param[out] documents_in_clusters list containing lists of documents
    assigned to each cluster
    @param[out] first_cluster iterator to the first cluster that should be
    joined
    @param[out] second_cluster iterator to the second cluster that should be
    joined
  */
  void FindClosestDocuments(
      const std::vector<std::vector<double> >& distances,
      std::list<std::list<size_t> >* documents_in_clusters,
      std::list<std::list<size_t> >::iterator& first_cluster,
      std::list<std::list<size_t> >::iterator& second_cluster) const;

  /**
    Calculates distance between given two clusters
    @param distances matrix containing distances between documents
    @param[out] first_cluster iterator to the first cluster
    @param[out] second_cluster iterator to the second cluster
    @return distance between given two clusters
  */
  double CalculateDistanceBetweenClusters(
      const std::vector<std::vector<double> >& distances,
      std::list<std::list<size_t> >::iterator first_cluster,
      std::list<std::list<size_t> >::iterator second_iterator) const;

  /**
    Unions given two clusters
    @param first_cluster iterator to the first cluster to union
    @param second_cluster iterator to the second cluster to union
    @param[out] documents_in_clusters list containing list of documents
    assigned to each cluster (will be modified after calling this function)
  */
  void UnionClusters(std::list<std::list<size_t> >::iterator first_cluster,
                     std::list<std::list<size_t> >::iterator second_cluster,
                     std::list<std::list<size_t> >* documents_in_clusters)
                     const;

  /**
    Calculates vector containing assignments of documents to clusters
    @param documents_in_clusters list containing list of documents assigned to
    each cluster
    @param[out] documents_assignments vector where assignments of documents to
    clusters will be stored
  */
  void CalculateDocumentsAssignments(
      const std::list<std::list<size_t> >& documents_in_clusters,
      std::vector<size_t>* documents_assignments) const;
};
