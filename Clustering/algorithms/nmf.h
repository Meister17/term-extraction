// Copyright 2012 Michael Nokel
#pragma once

#include "./clustering_algorithm.h"
#include "../auxiliary_macros.h"
#include <boost/unordered_map.hpp>
#include <string>
#include <vector>

class CandidatesExtractor;

/**
  @brief Class implementing NMF

  This class implements NMF clustering algorithm.
*/
class NMF : public ClusteringAlgorithm {
 public:
  NMF(CandidatesExtractor* single_words_extractor,
      CandidatesExtractor* two_words_extractor)
      : ClusteringAlgorithm(single_words_extractor, two_words_extractor),
        divergence_metric_(NO_METRIC),
        maximum_iterations_number_(0),
        epsilon_(0.0) {
  }

  /**
    Parses options for clustering algorithm
    @param clustering_options vector containing options for clustering algorithm
    @throw std::runtime_error in case of any occurred errors
  */
  void ParseClusteringAlgorithmOptions(
      const std::vector<std::string>& clustering_options);

  /**
    Clusters given matrix of data into given number of clusters
    @param matrix sparse matrix with input data for clustering
    @param number_of_clusters desired number of clusters
    @param[out] documents_assignments vector where assignments of documents to
    clusters will be stored
  */
  void Cluster(
      const std::vector<std::vector<SparseMatrixEntry> >& matrix,
      int number_of_clusters,
      std::vector<size_t>* documents_assignments);

  std::vector<boost::unordered_map<unsigned int, double> > words_assignments()
      const {
    return words_assignments_;
  }

 private:
  /**
    @brief Divergence metric to minimize in NMF

    This metrics can be Euclidean or Kullback-Leibler (no metric is possible)
  */
  enum DivergenceMetric {
    NO_METRIC,
    EUCLIDEAN,
    KULLBACK_LEIBLER
  } divergence_metric_;

  /**
    @brief Structure representing Compressed Sparse Column format of matrix

    This format is used in NMF clustering algorithm by sparse BLAS functions
    from Intel MKL library.
  */
  struct CompressedSparseColumnMatrix {
    CompressedSparseColumnMatrix()
        : rows_number(0),
          columns_number(0),
          non_zero_numbers(0),
          values(NULL),
          rows_indexes(NULL),
          columns_indexes(NULL) {
    }

    /**
      Destructor. Frees previously allocated memory
    */
    ~CompressedSparseColumnMatrix() {
      if (values != NULL) {
        free(values);
      }
      if (rows_indexes != NULL) {
        free(rows_indexes);
      }
      if (columns_indexes != NULL) {
        delete[] columns_indexes;
      }
    }

    /** Total number of rows in matrix */
    int rows_number;

    /** Total number of columns in matrix */
    int columns_number;

    /** Total number of non zero values */
    int non_zero_numbers;

    /** Array containing non zero values */
    double* values;

    /** Array containing row indexes of non zero values */
    int* rows_indexes;

    /** Array containing column indexes of non zero values */
    int* columns_indexes;
  };

  /**
    @brief Structure representing Dense format of matrix

    This format is used in NMF clustering algorithm by BLAS routines
  */
  struct DenseMatrix {
    DenseMatrix(int rows, int columns)
        : rows_number(rows),
          columns_number(columns),
          values(new double[rows * columns]) {
    }

    /** Total number of rows in matrix */
    int rows_number;

    /** Total number of columns in matrix */
    int columns_number;

    /** Array containing values of matrix */
    double* values;
  };

  /**
    Creates matrix in Compressed Sparse Column format from the given input one
    @param matrix matrix with input data for translating into another format
    @param[out] sparse_matrix structure where matrix in the CSC format will be
    stored
  */
  void CreateCompressedSparseColumnMatrix(
      const std::vector<std::vector<SparseMatrixEntry> >& matrix,
      CompressedSparseColumnMatrix* sparse_matrix) const;

  /**
    Initializes matrix by random numbers
    @param[out] matrix matrix for initialization
  */
  void InitializeMatrix(DenseMatrix* matrix) const;

  /**
    Runs NMF in Euclidean divergence
    @param matdescra parameter for Intel MKL for performing Sparse BLAS routines
    @param[out] A sparse matrix to cluster
    @param[out] W first dense matrix in NMF
    @param[out] H second dense matrix in NMF
  */
  void ClusterInEuclideanDivergence(char* matdescra,
                                    CompressedSparseColumnMatrix* A,
                                    DenseMatrix* W,
                                    DenseMatrix* H) const;

  /**
    Runs NMF in Kullback-Leibler divergence
    @param matdescra parameter for Intel MKL for performing Sparse BLAS routines
    @param[out] A sparse matrix to cluster
    @param[out] W first dense matrix in NMF
    @param[out] H second dense matrix in NMF
  */
  void ClusterInKullbackLeiblerDivergence(char* matdescra,
                                          CompressedSparseColumnMatrix* A,
                                          DenseMatrix* W,
                                          DenseMatrix* H) const;

  /**
    Transposes source matrix to form destination one
    @param source_matrix matrix to be transposed
    @param[out] destination_matrix matrix where transposition of the source
    matrix will be stored
  */
  void TransposeMatrix(const DenseMatrix& source_matrix,
                       DenseMatrix* destination_matrix) const;

  /**
    Updates destination matrix by multiplying its elements by division of
    corresponding elements of another two given matrices
    @param first_matrix first matrix that is in nominator for dividing
    @param second_matrix second matrix that is in denominator for dividing
    @param[out] destination_matrix matrix where result of updating will be
    stored
  */
  void DivideMatrices(const DenseMatrix& first_matrix,
                      const DenseMatrix& second_matrix,
                      DenseMatrix* destination_matrix) const;

  /**
    Computes sparse matrix by dividing given sparse matrix by the multiplication
    of the given two dense matrices
    @param sparse_matrix given sparse matrix for computing
    @param first_dense_matrix first dense matrix for computing
    @param second_dense_matrix second dense matrix for computing
    @param[out] result_matrix resulting sparse matrix
  */
  void CalculateSparseMatrixByDividing(
      const CompressedSparseColumnMatrix& sparse_matrix,
      const DenseMatrix& first_dense_matrix,
      const DenseMatrix& second_dense_matrix,
      CompressedSparseColumnMatrix* result_matrix) const;

  /**
    Calculates sum of rows of given source matrix
    @param source_matrix source matrix to calculate sum of rows for
    @param[out] destination_vector vector where sum of rows will be stored
  */
  void CalculateRowsSum(const DenseMatrix& source_matrix,
                        std::vector<double>* destination_vector) const;

  /**
    Divides matrix by vector and multiplies element-wise by destination matrix
    (for H update)
    @param source_matrix matrix that should be used for dividing
    @param source_vector vector that should be used for dividing
    @param[out] destination_matrix matrix that should be updated by calling this
    function
  */
  void DivideMatricesForH(const DenseMatrix& source_matrix,
                          const std::vector<double>& source_vector,
                          DenseMatrix* destination_matrix) const;

  /**
    Divides matrix by vector and multiplies element-wise by destination matrix
    (for W update)
    @param source_matrix matrix that should be used for dividing
    @param source_vector vector that should be used for dividing
    @param[out] destination_matrix matrix that should be updated by calling this
    function
  */
  void DivideMatricesForW(const DenseMatrix& source_matrix,
                          const std::vector<double>& source_vector,
                          DenseMatrix* destination_matrix) const;

  /**
    Normalizes two given matrices so that their multiplication will be unchanged
    @param[out] first_matrix first matrix that should be normalized
    @param[out] second_matrix second matrix that should be normalized
  */
  void NormalizeMatrices(DenseMatrix* first_matrix, DenseMatrix* second_matrix)
      const;

  /**
    Calculates assignments of documents to clusters based on the results of NMF
    @param matrix matrix containing results of NMF
    @param[out] documents_assignments vector where assignments of documents to
    clusters will be stored
  */
  void CalculateDocumentsAssignments(const DenseMatrix& matrix,
                                     std::vector<size_t>* documents_assignments)
                                     const;

  /**
    Calculates assignments of words to clusters based on the results of NMF
    @param matrix matrix containing results of NMF
    @param[out] terms_assignments vector where assignments of words to clusters
    will be stored
  */
  void CalculateWordsAssignments(
      const DenseMatrix& matrix,
      std::vector<boost::unordered_map<unsigned int, double> >*
          terms_assignments) const;

  /** Maximum number of iterations to perform */
  size_t maximum_iterations_number_;

  /** Epsilon for computing division of matrices */
  double epsilon_;

  /** Vector containing assignments of words to clusters */
  std::vector<boost::unordered_map<unsigned int, double> > words_assignments_;
};
