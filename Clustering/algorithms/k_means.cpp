// Copyright 2012 Michael Nokel
#include "./k_means.h"
#include "../auxiliary_macros.h"
#include <boost/random.hpp>
#include <omp.h>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

using boost::random::mt19937;
using boost::random::uniform_int_distribution;
using std::cout;
using std::runtime_error;
using std::string;
using std::vector;

void KMeans::ParseClusteringAlgorithmOptions(
    const vector<string>& clustering_options) {
  if (clustering_options.size() != 2) {
    throw runtime_error("Failed to parse options for K-Means");
  }
  if (clustering_options.front() == "regular") {
    k_means_type_ = REGULAR;
  } else if (clustering_options.front() == "spherical") {
    k_means_type_ = SPHERICAL;
  } else {
    throw runtime_error("Wrong type of K-Means algorithm");
  }
  maximum_iterations_number_ = atoi(clustering_options.back().data());
}

void KMeans::Cluster(const vector<vector<SparseMatrixEntry> >& matrix,
                     int number_of_clusters,
                     vector<size_t>* documents_assignments) {
  cout << "Starting K-Means clustering algorithm\n";
  documents_assignments->resize(matrix.size(), 0);
  vector<vector<double> > centers(number_of_clusters,
                                  vector<double>(GetColumnsNumber(matrix), 0));
  vector<vector<SparseMatrixEntry> > clustering_matrix = matrix;
  if (k_means_type_ == SPHERICAL) {
    NormalizeMatrix(&clustering_matrix);
  }
  InitializeCenters(clustering_matrix, &centers);
  bool change_occurred = true;
  omp_set_num_threads(kNumberThreads_);
  for (size_t iteration = 1; iteration <= maximum_iterations_number_;
       ++iteration) {
    cout << "Iteration " << iteration << "\n";
    change_occurred = AssignDocumentsToClusters(clustering_matrix,
                                                centers,
                                                documents_assignments);
    if (change_occurred) {
      UpdateCenters(clustering_matrix, *documents_assignments, &centers);
    }
  }
}

void KMeans::NormalizeMatrix(vector<vector<SparseMatrixEntry> >* matrix) const {
  for (size_t first_index = 0; first_index < matrix->size(); ++first_index) {
    double normalizing_coefficient = CalculateNorm(matrix->at(first_index));
    for (size_t second_index = 0; second_index < matrix->at(first_index).size();
         ++second_index) {
      matrix->at(first_index)[second_index].value /= normalizing_coefficient;
    }
  }
}

double KMeans::CalculateNorm(const vector<SparseMatrixEntry>& element)
    const {
  double element_norm = 0.0;
  for (size_t index = 0; index < element.size(); ++index) {
    element_norm += element[index].value * element[index].value;
  }
  return sqrt(element_norm);
}

double KMeans::CalculateNorm(const vector<double>& element) const {
  double element_norm = 0.0;
  for (size_t index = 0; index < element.size(); ++index) {
    element_norm += element[index] * element[index];
  }
  return sqrt(element_norm);
}

void KMeans::InitializeCenters(
  const vector<vector<SparseMatrixEntry> >& matrix,
  vector<vector<double> >* centers) const {
  mt19937 generator;
  uniform_int_distribution<int> distribution(0, matrix.size());
  for (size_t index = 0; index < centers->size(); ++index) {
    int document_index = distribution(generator);
    CopySparseMatrixEntry(matrix[document_index], &centers->at(index));
  }
}

void KMeans::CopySparseMatrixEntry(
    const vector<SparseMatrixEntry>& matrix_entry,
    vector<double>* center) const {
  for (size_t index = 0; index < matrix_entry.size(); ++index) {
    center->at(matrix_entry[index].column_index) = matrix_entry[index].value;
  }
}

bool KMeans::AssignDocumentsToClusters(
    const vector<vector<SparseMatrixEntry> >& matrix,
    const vector<vector<double> >& centers,
    vector<size_t>* documents_assignments) const {
  if (k_means_type_ == REGULAR) {
    return AssignDocumentsInRegularKMeans(matrix,
                                          centers,
                                          documents_assignments);
  }
  return AssignDocumentsInSphericalKMeans(matrix,
                                          centers,
                                          documents_assignments);
}

bool KMeans::AssignDocumentsInRegularKMeans(
    const vector<vector<SparseMatrixEntry> >& matrix,
    const vector<vector<double> >& centers,
    vector<size_t>* documents_assignments) const {
  int changes_occurred = 0;
  #pragma omp parallel for reduction(|:changes_occurred) schedule(static, 100)
  for (size_t first_index = 0; first_index < matrix.size(); ++first_index) {
    double minimal_distance = CalculateSquaredDistance(matrix[first_index],
                                                       centers[0]);
    size_t nearest_cluster = 0;
    for (size_t second_index = 0; second_index < centers.size();
         ++second_index) {
      double new_distance = CalculateSquaredDistance(matrix[first_index],
                                                     centers[second_index]);
      if (new_distance < minimal_distance) {
        minimal_distance = new_distance;
        nearest_cluster = second_index;
      }
    }
    if (documents_assignments->at(first_index) != nearest_cluster) {
      documents_assignments->at(first_index) = nearest_cluster;
      changes_occurred |= 1;
    }
  }
  return static_cast<bool>(changes_occurred);
}

bool KMeans::AssignDocumentsInSphericalKMeans(
    const vector<vector<SparseMatrixEntry> >& matrix,
    const vector<vector<double> >& centers,
    vector<size_t>* documents_assignments) const {
  int changes_occurred = 0;
  #pragma omp parallel for reduction(|:changes_occurred) schedule(static, 100)
  for (size_t first_index = 0; first_index < matrix.size(); ++first_index) {
    double maximal_similarity = CalculateCosineSimilarity(matrix[first_index],
                                                          centers[0]);
    size_t nearest_cluster = 0;
    for (size_t second_index = 0; second_index < centers.size();
         ++second_index) {
      double new_similarity = CalculateCosineSimilarity(matrix[first_index],
                                                        centers[second_index]);
      if (new_similarity > maximal_similarity) {
        maximal_similarity = new_similarity;
        nearest_cluster = second_index;
      }
    }
    if (documents_assignments->at(first_index) != nearest_cluster) {
      documents_assignments->at(first_index) = nearest_cluster;
      changes_occurred |= 1;
    }
  }
  return static_cast<bool>(changes_occurred);
}

double KMeans::CalculateCosineSimilarity(
    const vector<SparseMatrixEntry>& sparse_vector,
    const vector<double>& dense_vector) const {
  double similarity = 0.0;
  for (size_t index = 0; index < sparse_vector.size(); ++index) {
    size_t column_index = sparse_vector[index].column_index;
    similarity += dense_vector[column_index] * sparse_vector[index].value;
  }
  return similarity;
}

void KMeans::UpdateCenters(const vector<vector<SparseMatrixEntry> >& matrix,
                           const vector<size_t>& documents_assignments,
                           vector<vector<double> >* centers) const {
  vector<unsigned int> documents_in_cluster(centers->size(), 0);
  centers->assign(centers->size(), vector<double>(centers->front().size(), 0));
#pragma omp parallel for schedule(static, 100)
  for (size_t first_index = 0; first_index < matrix.size(); ++first_index) {
    size_t center_index = documents_assignments[first_index];
    for (size_t second_index = 0; second_index < matrix[first_index].size();
         ++second_index) {
      SparseMatrixEntry entry = matrix[first_index][second_index];
      centers->at(center_index)[entry.column_index] += entry.value;
      ++documents_in_cluster[center_index];
    }
  }
  mt19937 generator;
  uniform_int_distribution<int> distribution(0, matrix.size());
  for (size_t first_index = 0; first_index < centers->size(); ++first_index) {
    if (documents_in_cluster[first_index] == 0) {
      int document_index = distribution(generator);
      CopySparseMatrixEntry(matrix[document_index], &centers->at(first_index));
      size_t center_index = documents_assignments[document_index];
      for (size_t second_index = 0; second_index <
           matrix[document_index].size(); ++second_index) {
        SparseMatrixEntry entry = matrix[document_index][second_index];
        centers->at(center_index)[entry.column_index] -= entry.value;
      }
      --documents_in_cluster[center_index];
      ++documents_in_cluster[first_index];
    }
  }
  if (k_means_type_ == REGULAR) {
#pragma omp parallel for schedule(static, 100)
    for (size_t first_index = 0; first_index < centers->size(); ++first_index) {
      for (size_t second_index = 0; second_index <
           centers->at(first_index).size(); ++second_index) {
        centers->at(first_index)[second_index] /=
            static_cast<double>(documents_in_cluster[first_index]);
      }
    }
  } else {
#pragma omp parallel for schedule(static, 100)
    for (size_t first_index = 0; first_index < centers->size(); ++first_index) {
      double normalizing_coefficient = CalculateNorm(centers->at(first_index));
      for (size_t second_index = 0; second_index <
           centers->at(first_index).size(); ++second_index) {
        centers->at(first_index)[second_index] /= normalizing_coefficient;
      }
    }
  }
}
