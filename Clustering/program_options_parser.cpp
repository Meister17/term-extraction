// Copyright 2012 Michael Nokel
#include "./program_options_parser.h"
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <climits>
#include <string>

using namespace boost::program_options;
using boost::is_any_of;
using boost::split;
using std::cout;
using std::string;

ProgramOptionsParser::ProgramOptionsParser()
    : help_message_printed_(false),
      clustering_algorithm_(""),
      term_frequency_(""),
      document_frequency_(""),
      number_of_clusters_(0),
      single_word_input_filename_(""),
      single_word_vocabulary_filename_(""),
      single_word_output_filename_(""),
      two_word_input_filename_(""),
      two_word_vocabulary_filename_(""),
      two_word_output_filename_("") {
}

void ProgramOptionsParser::Parse(int argc, char** argv) {
  variables_map program_options_storage;
  string configuration_filename;
  string clustering_options;
  options_description program_options_description("Options for program:");
  program_options_description.add_options()
      ("help,h", value<bool>(&help_message_printed_)->implicit_value(true),
          "Print help message and exit")
      ("configuration_file,c", value<string>(&configuration_filename),
          "Configuration file of program")
      ("clustering_algorithm", value<string>(&clustering_algorithm_),
          "Name of clustering algorithm")
      ("clustering_algorithm_options", value<string>(&clustering_options),
          "Options for specified clustering algorithm")
      ("term_frequency", value<string>(&term_frequency_),
          "Used term frequency")
      ("document_frequency", value<string>(&document_frequency_),
          "Used document frequency")
      ("number_of_clusters", value<int>(&number_of_clusters_),
          "Desired number of clusters")
      ("single_word_input_filename",
          value<string>(&single_word_input_filename_),
          "Name of file containing input data for single-word term candidates")
      ("single_word_vocabulary_filename",
          value<string>(&single_word_vocabulary_filename_),
          "Name of file containing vocabulary for single-word term candidates")
      ("single_word_output_filename",
          value<string>(&single_word_output_filename_),
          "Name of file where results for single-word terms will be written")
      ("two_word_input_filename", value<string>(&two_word_input_filename_),
          "Name of file containing input data for two-word term candidates")
      ("two_word_vocabulary_filename",
          value<string>(&two_word_vocabulary_filename_),
          "Name of file containing vocabulary for two-word term candidates")
      ("two_word_output_filename", value<string>(&two_word_output_filename_),
          "Name of file where results for two-word terms will be written");
  store(parse_command_line(argc, argv, program_options_description),
        program_options_storage);
  notify(program_options_storage);
  if (!configuration_filename.empty()) {
    store(parse_config_file<char>(configuration_filename.data(),
                                  program_options_description),
          program_options_storage);
    notify(program_options_storage);
  }
  split(clustering_algorithm_options_, clustering_options, is_any_of(";"));
  if (help_message_printed_) {
    cout << program_options_description << "\n";
  }
}
