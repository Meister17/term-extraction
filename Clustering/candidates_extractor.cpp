// Copyright 2012 Michael Nokel
#include "./candidates_extractor.h"
#include "./auxiliary_macros.h"
#include <boost/algorithm/string.hpp>
#include <boost/unordered_map.hpp>
#include <algorithm>
#include <cctype>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include <stdexcept>
#include <string>
#include <vector>

using boost::is_any_of;
using boost::split;
using boost::unordered_map;
using std::cout;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::make_pair;
using std::map;
using std::ofstream;
using std::not1;
using std::pair;
using std::ptr_fun;
using std::runtime_error;
using std::string;
using std::vector;

void CandidatesExtractor::ParseInputFile(const string& filename) {
  cout << "Parsing file with input data\n";
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse file with input data");
  }
  for (size_t documents_number = 0; !file.eof(); ++documents_number) {
    string line;
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (line.empty()) {
      continue;
    }
    vector<string> tokens;
    split(tokens, line, is_any_of(" \t"));
    if (tokens.empty()) {
      throw runtime_error("Failed to parse file with input data");
    }
    int unique_terms_number = atoi(tokens[0].data());
    if (unique_terms_number > 0) {
      non_empty_documents_.push_back(documents_number);
      vector<SparseMatrixEntry> document;
      for (size_t index = 1; index < tokens.size(); ++index) {
        vector<string> elements;
        split(elements, tokens[index], is_any_of(":"));
        if (elements.size() != 2) {
          throw runtime_error("Failed to parse file with input data");
        }
        document.push_back(SparseMatrixEntry(atoi(elements.front().data()),
                                             atof(elements.back().data())));
      }
      matrix_.push_back(document);
    }
  }
  file.close();
}

void CandidatesExtractor::ParseVocabularyFile(const string& filename) {
  cout << "Parsing vocabulary file\n";
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse vocabulary file");
  }
  while (!file.eof()) {
    string line;
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    vocabulary_.push_back(line);
  }
  file.close();
}

void CandidatesExtractor::SortWordsInClusters(
    int number_of_clusters,
    const vector<size_t>& documents_assignments,
    const vector<size_t>& documents_numbers,
    vector<unordered_map<unsigned int, double> >* words_assignments) const {
  words_assignments->resize(number_of_clusters);
  vector<double> total_frequencies_in_clusters(number_of_clusters);
  size_t documents_index = 0;
  for (size_t first_index = 0; first_index < matrix_.size(); ++first_index) {
    while (documents_numbers[documents_index] !=
           non_empty_documents_[first_index]) {
      ++documents_index;
    }
    size_t cluster_number = documents_assignments[documents_index];
    for (size_t second_index = 0; second_index < matrix_[first_index].size();
         ++second_index) {
      total_frequencies_in_clusters[cluster_number] +=
          matrix_[first_index][second_index].value;
    }
  }
  for (size_t first_index = 0; first_index < matrix_.size(); ++first_index) {
    size_t cluster_number = documents_assignments[first_index];
    for (size_t second_index = 0; second_index < matrix_[first_index].size();
         ++second_index) {
      size_t word_index = matrix_[first_index][second_index].column_index;
      words_assignments->at(cluster_number)[word_index] =
          matrix_[first_index][second_index].value /
          total_frequencies_in_clusters[cluster_number];
    }
  }
}

void CandidatesExtractor::PrintResults(
    const vector<unordered_map<unsigned int, double> >& words_assignments,
    const string& filename) const {
  cout << "Printing results of clustering\n";
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print results of clustering");
  }
  for (size_t index = 0; index < words_assignments.size(); ++index) {
    file << "Topic " << index << ":\n";
    map<double, vector<unsigned int> > printing_map;
    for (unordered_map<unsigned int, double>::const_iterator iterator =
         words_assignments[index].begin(); iterator !=
         words_assignments[index].end(); ++iterator) {
      pair<map<double, vector<unsigned int> >::iterator, bool> inserted_result =
          printing_map.insert(make_pair(iterator->second,
                                        vector<unsigned int>()));
      inserted_result.first->second.push_back(iterator->first);
    }
    for (map<double, vector<unsigned int> >::const_reverse_iterator iterator =
         printing_map.rbegin(); iterator != printing_map.rend(); ++iterator) {
      for (size_t index = 0; index < iterator->second.size(); ++index) {
        file << "\t" << vocabulary_[iterator->second[index]] << "\t" <<
            iterator->first << "\n";
      }
    }
  }
  file.close();
}
