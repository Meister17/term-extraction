// Copyright 2012 Michael Nokel
#pragma once

#include <string>
#include <vector>

/**
  @brief Class for working with program options from command line and/or from
  configuration file

  This class parses program options from command line and/or from configuration
  file, check their correctness. After what one can use this class for getting
  values of the necessary program options.
*/
class ProgramOptionsParser {
 public:
  ProgramOptionsParser();

  /**
    Parses program options from command line and/or from configuration file
    @param argc number of command line options
    @param argv array containing command line options
    @throw std::runtime_error in case of any occurred errors
  */
  void Parse(int argc, char** argv);

  bool help_message_printed() const {
    return help_message_printed_;
  }

  std::string clustering_algorithm() const {
    return clustering_algorithm_;
  }

  std::vector<std::string> clustering_algorithm_options() const {
    return clustering_algorithm_options_;
  }

  std::string term_frequency() const {
    return term_frequency_;
  }

  std::string document_frequency() const {
    return document_frequency_;
  }

  unsigned int number_of_clusters() const {
    return number_of_clusters_;
  }

  std::string single_word_input_filename() const {
    return single_word_input_filename_;
  }

  std::string single_word_vocabulary_filename() const {
    return single_word_vocabulary_filename_;
  }

  std::string single_word_output_filename() const {
    return single_word_output_filename_;
  }

  std::string two_word_input_filename() const {
    return two_word_input_filename_;
  }

  std::string two_word_vocabulary_filename() const {
    return two_word_vocabulary_filename_;
  }

  std::string two_word_output_filename() const {
    return two_word_output_filename_;
  }

 private:
  /** Flag indicating whether help message has been printed */
  bool help_message_printed_;

  /** Name of clustering algorithm to be performed */
  std::string clustering_algorithm_;

  /** Vector containing options for specified clustering algorithm */
  std::vector<std::string> clustering_algorithm_options_;

  /** Used term frequency: tf, logarithm or augmented */
  std::string term_frequency_;

  /** Used document frequency: no, idf or prob_idf */
  std::string document_frequency_;

  /** Number of clusters */
  int number_of_clusters_;

  /** Name of file containing input data for single-word term candidates */
  std::string single_word_input_filename_;

  /** Name of file containing vocabulary for single-word term candidates */
  std::string single_word_vocabulary_filename_;

  /** Name of file where results for single-word terms will be written */
  std::string single_word_output_filename_;

  /** Name of file containing input data for two-word term candidates */
  std::string two_word_input_filename_;

  /** Name of file containing vocabulary for two-word term candidates */
  std::string two_word_vocabulary_filename_;

  /** Name of file where results for two-word terms will be written */
  std::string two_word_output_filename_;
};
