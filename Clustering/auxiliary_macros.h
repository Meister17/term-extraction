// Copyright 2012 Michael Nokel
#pragma once

#include <string>

/**
  @brief Structure for using in sparse matrices

  This structure contains column index and value stored in it
*/
struct SparseMatrixEntry {
  SparseMatrixEntry(size_t index, double weight)
      : column_index(index),
        value(weight) {
  }

  /** Index of the column */
  size_t column_index;

  /** Value stored in this entry */
  double value;
};
