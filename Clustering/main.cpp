// Copyright 2012 Michael Nokel
#include "./clusterer.h"
#include "./program_options_parser.h"
#include <iostream>
#include <stdexcept>

using std::cout;
using std::exception;

/**
  Main function which starts program
  @param argc number of program options
  @param argv vector containing program options
  @return exit code of program
*/
int main(int argc, char** argv) {
  try {
    ProgramOptionsParser program_options_parser;
    program_options_parser.Parse(argc, argv);
    if (program_options_parser.help_message_printed()) {
      return 0;
    }
    Clusterer clusterer;
    clusterer.ParseInputFiles(
        program_options_parser.single_word_input_filename(),
        program_options_parser.single_word_vocabulary_filename(),
        program_options_parser.two_word_input_filename(),
        program_options_parser.two_word_vocabulary_filename());
    clusterer.Cluster(
        program_options_parser.clustering_algorithm(),
        program_options_parser.clustering_algorithm_options(),
        program_options_parser.term_frequency(),
        program_options_parser.document_frequency(),
        program_options_parser.number_of_clusters(),
        program_options_parser.single_word_output_filename(),
        program_options_parser.two_word_output_filename());
    return 0;
  } catch (const exception& except) {
    cout << "Fatal error occurred: " << except.what() << "\n";
    return 1;
  }
}
