// Copyright 2012 Michael Nokel
#include "./clusterer.h"
#include "./auxiliary_macros.h"
#include "./algorithms/hierarchical_clustering.h"
#include "./algorithms/k_means.h"
#include "./algorithms/nmf.h"
#include <boost/unordered_map.hpp>
#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

using boost::unordered_map;
using std::cout;
using std::max;
using std::runtime_error;
using std::string;
using std::vector;

void Clusterer::ParseInputFiles(
    const string& single_word_input_filename,
    const string& single_word_vocabulary_filename,
    const string& two_word_input_filename,
    const string& two_word_vocabulary_filename) {
  if (!single_word_input_filename.empty() &&
      !single_word_vocabulary_filename.empty()) {
    term_candidates_type_ = SINGLE_WORD_TERMS;
    single_word_terms_extractor_.ParseInputFile(single_word_input_filename);
    single_word_terms_extractor_.ParseVocabularyFile(
        single_word_vocabulary_filename);
  }
  if (!two_word_input_filename.empty() &&
      !two_word_vocabulary_filename.empty()) {
    if (term_candidates_type_ == SINGLE_WORD_TERMS) {
      term_candidates_type_ = ALL_TERMS;
    } else {
      term_candidates_type_ = TWO_WORD_TERMS;
    }
    two_word_terms_extractor_.ParseInputFile(two_word_input_filename);
    two_word_terms_extractor_.ParseVocabularyFile(two_word_vocabulary_filename);
  }
  if (term_candidates_type_ == NO_TERMS) {
    throw runtime_error("Failed to parse files with input data");
  }
}

void Clusterer::Cluster(const string& clustering_algorithm,
                        const vector<string>& clustering_options,
                        const string& term_frequency,
                        const string& document_frequency,
                        int number_of_clusters,
                        const string& single_word_output_filename,
                        const string& two_word_output_filename) {
  if (clustering_algorithm == "hierarchical") {
    clustering_algorithm_.reset(new HierarchicalClustering(
        &single_word_terms_extractor_,
        &two_word_terms_extractor_));
  } else if (clustering_algorithm == "k-means") {
    clustering_algorithm_.reset(new KMeans(&single_word_terms_extractor_,
                                           &two_word_terms_extractor_));
  } else  if (clustering_algorithm == "nmf") {
    clustering_algorithm_.reset(new NMF(&single_word_terms_extractor_,
                                        &two_word_terms_extractor_));
  } else {
    throw runtime_error("Wrong algorithm for clustering");
  }
  clustering_algorithm_->ParseClusteringAlgorithmOptions(clustering_options);
  vector<vector<SparseMatrixEntry> > weighted_matrix =
      single_word_terms_extractor_.matrix();
  ApplyTermFrequency(term_frequency, &weighted_matrix);
  ApplyDocumentFrequency(document_frequency, &weighted_matrix);
  vector<size_t> documents_assignments;
  clustering_algorithm_->Cluster(weighted_matrix,
                                 number_of_clusters,
                                 &documents_assignments);
  vector<unordered_map<unsigned int, double> > words_assignments;
  if (clustering_algorithm != "nmf") {
    single_word_terms_extractor_.SortWordsInClusters(
        number_of_clusters,
        documents_assignments,
        single_word_terms_extractor_.non_empty_documents(),
        &words_assignments);
  } else {
    words_assignments =
        static_cast<NMF*>(clustering_algorithm_.get())->words_assignments();
  }
  single_word_terms_extractor_.PrintResults(words_assignments,
                                            single_word_output_filename);
  if (term_candidates_type_ == TWO_WORD_TERMS ||
      term_candidates_type_ == ALL_TERMS) {
    words_assignments.clear();
    two_word_terms_extractor_.SortWordsInClusters(
        number_of_clusters,
        documents_assignments,
        single_word_terms_extractor_.non_empty_documents(),
        &words_assignments);
    two_word_terms_extractor_.PrintResults(words_assignments,
                                           two_word_output_filename);
  }
}

void Clusterer::ApplyTermFrequency(
    const string& term_frequency,
    vector<vector<SparseMatrixEntry> >* matrix) const {
  if (term_frequency == "tf") {
    return;
  }
  if (term_frequency == "normalized") {
    cout << "Normalizing term frequency\n";
    for (size_t first_index = 0; first_index < matrix->size(); ++first_index) {
      double total_term_frequency = 0.0;
      for (size_t second_index = 0; second_index <
           matrix->at(first_index).size(); ++second_index) {
        total_term_frequency += matrix->at(first_index)[second_index].value;
      }
      for (size_t second_index = 0; second_index <
           matrix->at(first_index).size(); ++second_index) {
        matrix->at(first_index)[second_index].value /= total_term_frequency;
      }
    }
  }
  if (term_frequency == "logarithm") {
    cout << "Applying logarithmic term frequency\n";
    for (size_t first_index = 0; first_index < matrix->size(); ++first_index) {
      for (size_t second_index = 0; second_index <
           matrix->at(first_index).size(); ++second_index) {
        matrix->at(first_index)[second_index].value = 1.0 + log(
            static_cast<double>(matrix->at(first_index)[second_index].value));
      }
    }
  } else if (term_frequency == "augmented") {
    cout << "Applying augmented term frequency\n";
    for (size_t first_index = 0; first_index < matrix->size(); ++first_index) {
      double maximum_frequency = 0.0;
      for (size_t second_index = 0; second_index <
           matrix->at(first_index).size(); ++second_index) {
        maximum_frequency = max(maximum_frequency,
                                matrix->at(first_index)[second_index].value);
      }
      for (size_t second_index = 0; second_index <
           matrix->at(first_index).size(); ++second_index) {
        matrix->at(first_index)[second_index].value = 0.5 + 0.5 *
            static_cast<double>(matrix->at(first_index)[second_index].value) /
            maximum_frequency;
      }
    }
  } else {
    throw runtime_error("Wrong term frequency to apply");
  }
}

void Clusterer::ApplyDocumentFrequency(
    const string& document_frequency,
    vector<vector<SparseMatrixEntry> >* matrix) const {
  if (document_frequency == "no") {
    return;
  }
  unordered_map<unsigned int, unsigned int> document_frequencies;
  for (size_t first_index = 0; first_index < matrix->size(); ++first_index) {
    for (size_t second_index = 0; second_index < matrix->at(first_index).size();
         ++second_index) {
      unsigned int word_index =
          matrix->at(first_index)[second_index].column_index;
      ++document_frequencies[word_index];
    }
  }
  if (document_frequency == "idf") {
    cout << "Applying IDF\n";
    for (size_t first_index = 0; first_index < matrix->size(); ++first_index) {
      for (size_t second_index = 0; second_index <
           matrix->at(first_index).size(); ++second_index) {
        unsigned int word_index =
            matrix->at(first_index)[second_index].column_index;
        matrix->at(first_index)[second_index].value *=
            log(static_cast<double>(matrix->size()) /
                static_cast<double>(document_frequencies[word_index]));
      }
    }
  } else if (document_frequency == "prob_idf") {
    cout << "Applying probabilistic IDF\n";
    for (size_t first_index = 0; first_index < matrix->size(); ++first_index) {
      for (size_t second_index = 0; second_index <
           matrix->at(first_index).size(); ++second_index) {
        unsigned int word_index =
            matrix->at(first_index)[second_index].column_index;
        matrix->at(first_index)[second_index].value *=
            max(0.0, log(static_cast<double>(matrix->size() -
                         document_frequencies[word_index]) /
                     static_cast<double>(document_frequencies[word_index])));
      }
    }
  } else {
    throw runtime_error("Wrong document frequency to apply");
  }
}
