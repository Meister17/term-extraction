// Copyright 2013 Michael Nokel
#pragma once

#include "../auxiliary_macros.h"
#include <boost/thread/mutex.hpp>
#include <algorithm>
#include <string>
#include <unordered_map>
#include <vector>

/**
  @brief Class intended to be used for accumulating and printing noun phrases

  This class accumulates noun phrases and their term frequencies while parsing
  files in separate threads. After parsing all files, accumulated information is
  printed to the specified file.
*/
class NounPhrasesAccumulator {
 public:
  /**
    Accumulates previously extracted noun phrases
    @param start_iterator iterator to the beginning of the accumulating map
    @param end_iterator iterator to the end of the accumulating map
  */
  void Accumulate(
      std::unordered_map<std::string, NounPhraseData>::const_iterator
          start_iterator,
      std::unordered_map<std::string, NounPhraseData>::const_iterator
          end_iterator);

  /**
    Prints extracted noun phrases for single-word term candidates and their
    frequencies to the given files
    @param phrases_filename file for printing noun phrases for single-word term
    candidates
    @param minimum_term_frequency minimum frequency of phrases to be taken into
    account
    @throw std::runtime_error in case of any occurred error
  */
  void PrintSingleWordNounPhrases(const std::string& phrases_filename,
                                  unsigned int minimum_term_frequency) const;

  /**
    Prints extracted noun phrases for two-word term candidates and their
    frequencies to the given files
    @param phrases_filename file for printing noun phrases for two-word term
    candidates
    @param minimum_term_frequency minimum frequency of phrases to be taken into
    account
    @throw std::runtime_error in case of any occurred error
  */
  void PrintTwoWordNounPhrases(const std::string& phrases_filename,
                               unsigned int minimum_term_frequency) const;

 private:
  /**
    Inserts new noun phrase to the given map
    @param phrase new adding phrase
    @param term_frequency term frequency of the adding phrase
    @param[out] noun_phrases_map map where the noun phrase should be added
  */
  void InsertNewPhrase(
      const std::string& phrase,
      unsigned int term_frequency,
      std::unordered_map<std::string, unsigned int>* noun_phrases_map) const;

  /**
    Print given phrases and their frequencies to the given file
    @param filename file where phrases and their frequencies should be printed
    @param total_number_of_phrases total number of phrases to print
    @param phrases_map map containing phrases and their frequencies for printing
    @param minimum_term_frequency minimum frequency of phrases to be taken into
    account
    @throw std::runtime_error in case of any occurred error
  */
  void PrintPhrases(
      const std::string& filename,
      unsigned int total_number_of_phrases,
      const std::unordered_map<std::string, unsigned int>& phrases_map,
      unsigned int minimum_term_frequency) const;

  /**
    Sorts noun phrases by their term frequencies
    @param phrases_map map containing noun phrases and their frequencies
    @return vector of noun phrases sorted by their term frequencies
  */
  std::vector<std::pair<std::string, unsigned int> > SortByTermFrequencies(
      const std::unordered_map<std::string, unsigned int>& phrases_map) const;

  /** Length of noun phrases for single-word term candidates */
  const unsigned int kLengthForSingleWordTerms_ = 2;

  /** Length of noun phrases for two-word term candidates */
  const unsigned int kLengthForTwoWordTerms_ = 3;

  /** Total number of extracted noun phrases for single-word term candidates */
  unsigned int total_number_of_single_word_phrases_ = 0;

  /** Total number of extracted noun phrases for two-word term candidates */
  unsigned int total_number_of_two_word_phrases_ = 0;

  /** Mutex for synchronization accumulating from various threads */
  boost::mutex mutex_;

  /** Hash map containing found noun phrases for single-word term candidates and
      their frequencies */
  std::unordered_map<std::string, unsigned int> single_word_phrases_map_;

  /** Hash map containing found noun phrases for two-word term candidates and
      their frequencies */
  std::unordered_map<std::string, unsigned int> two_word_phrases_map_;
};
