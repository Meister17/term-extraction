// Copyright 2012 Michael Nokel
#pragma once

#include "../auxiliary_macros.h"
#include <boost/thread/mutex.hpp>
#include <fstream>
#include <string>
#include <unordered_map>
#include <vector>

/**
  @brief Abstract class for accumulating and printing term candidates

  This class should be derived by all classes that deals with accumulating and
  printing term candidates.
*/
class CandidatesAccumulator {
 public:
  /**
    Reserves space for given number of files
    @param files_number number of files that will be processed
  */
  virtual void ReserveForFiles(size_t files_number) {
    lda_container_.resize(files_number,
                          std::unordered_map<std::string, unsigned int>());
  }

  /**
    Accumulates previously extracted term candidates during parsing file
    @param start_iterator start iterator to the map containing extracted term
    candidates and necessary information about them
    @param end_iterator end iterator to the map containing extracted term
    candidates and necessary information about them
    @param file_numer number of processed file
  */
  virtual void Accumulate(
      std::unordered_map<std::string, TermCandidateData>::const_iterator
          start_iterator,
      std::unordered_map<std::string, TermCandidateData>::const_iterator
          end_iterator,
      size_t file_number);

  /**
    Prints extracted term candidates in the decreasing order of term frequencies
    @param filename name of file where candidates should be printed
    @param minimum_term_frequency minimum frequency of term candidates
    (candidates with frequencies below this threshold won't be printed)
    @throw std::runtime_error in case of any error
  */
  virtual void PrintTermCandidates(const std::string& filename,
                                   unsigned int minimum_term_frequency) const;


  /**
    Creates input and vocabulary files for LDA/clustering
    @param lda_input_filename name of file for input data for LDA/clustering
    @param lda_vocabulary_filename name of file for vocabulary for
    LDA/clustering
    @throw std::runtime_error in case of any occurred error
  */
  virtual void CreateLDAFiles(const std::string& lda_input_filename,
                              const std::string& lda_vocabulary_filename,
                              unsigned int minimum_term_frequency) const;

  virtual ~CandidatesAccumulator() {
  }

 protected:
  /**
    Updates stored common data for given term candidate
    @param term_candidate_data new data for term candidate that should be used
    for updating
    @param[out] updating_iterator iterator pointing to previously stored data
    for the given term candidate
  */
  virtual void UpdateStoredData(
      const TermCandidateData& term_candidate_data,
      std::unordered_map<std::string, TermCandidateData>::iterator
          updating_iterator) const;

  /**
    Sorts term candidates by their term frequencies
    @param candidates_map map containing term candidates and some data about
    them
    @return vector of term candidates sorted by term frequencies
  */
  virtual std::vector<std::pair<std::string, TermCandidateData>>
      SortByTermFrequencies(
          const std::unordered_map<std::string, TermCandidateData>&
              candidates_map) const;

  /**
    Returns char representing term's category: 'A' for Adjective, 'N' for Noun,
    and 'B' for both types
    @param term_category category of the term
    @return char representing term's category
    @throw std::runtime_error in case of wrong term category
  */
  char GetTermCategory(const TermCategory& term_category) const;

  /**
    Calculates probabilistic term frequencies and scores for the given term
    candidate
    @param term_frequencies vector containing frequencies of term candidate
    @param document_sizes vector containing sizes of documents in which
    term occurs at least once
    @param term_frequency total term frequency of the given candidate
    @return structure containing term frequencies and scores
  */
  virtual ProbabilisticData CalculateProbabilisticData(
      const std::vector<unsigned int>& term_frequencies,
      const std::vector<unsigned int>& document_sizes,
      unsigned int term_frequency) const;

  /**
    Returns words in texts with term frequencies greater or equal the given
    threshold
    @param minimum_term_frequency minimum term frequency that should be taken
    into account
    @return vector containing words in texts with such term frequencies
  */
  virtual std::vector<std::string> GetWordsAboveThreshold(
      unsigned int minimum_term_frequency) const;

  /**
    Prints LDA vocabulary to the given file
    @param filename file where LDA vocabulary should be printed
    @param words_above_threshold vector containing words with frequencies above
    minimum threshold
    @throw std::runtime_error in case of any occurred error
  */
  virtual void PrintLDAVocabulary(
      const std::string& filename,
      const std::vector<std::string>& words_above_threshold) const;

  /**
    Prints LDA input to the given file
    @param filename file where LDA input should be printed
    @param words_above_threshold vector containing words with frequencies above
    minimum threshold
    @throw std::runtime_error in case of any occurred error
  */
  virtual void PrintLDAInput(
      const std::string& filename,
      const std::vector<std::string>& words_above_threshold) const;

  /** Total number of candidates in the corpus */
  unsigned int total_number_of_candidates_ = 0;

  /** Total number of documents in the corpus */
  unsigned int total_number_of_documents_ = 0;

  /** Mutex for synchronizing accumulating maps with term candidates obtained
      for each parsed file separately */
  boost::mutex mutex_;

  /** Container for further application of LDA/clustering */
  std::vector<std::unordered_map<std::string, unsigned int> > lda_container_;

  /** Hash map containing term candidates and necessary information about them
      extracted from all parsed files */
  std::unordered_map<std::string, TermCandidateData> term_candidates_map_;
};
