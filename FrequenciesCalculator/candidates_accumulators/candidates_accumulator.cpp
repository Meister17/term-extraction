// Copyright 2013 Michael Nokel
#include "./candidates_accumulator.h"
#include <boost/thread/mutex.hpp>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

using std::max;
using std::ofstream;
using std::pair;
using std::runtime_error;
using std::sort;
using std::string;
using std::unordered_map;
using std::vector;

void CandidatesAccumulator::Accumulate(
    unordered_map<string, TermCandidateData>::const_iterator start_iterator,
    unordered_map<string, TermCandidateData>::const_iterator end_iterator,
    size_t file_number) {
  boost::mutex::scoped_lock lock(mutex_);
  if (start_iterator != end_iterator) {
    ++total_number_of_documents_;
    for (auto iterator = start_iterator; iterator != end_iterator; ++iterator) {
      total_number_of_candidates_ += iterator->second.term_frequency;
      auto inserted_result = term_candidates_map_.insert(*iterator);
      if (!inserted_result.second) {
        UpdateStoredData(iterator->second, inserted_result.first);
      }
      lda_container_[file_number][iterator->first] =
          iterator->second.term_frequency;
    }
  }
}

void CandidatesAccumulator::PrintTermCandidates(
    const string& filename,
    unsigned int minimum_term_frequency) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print term candidates");
  }
  auto term_candidates_vector = SortByTermFrequencies(term_candidates_map_);
  file << total_number_of_documents_ << "\t" << total_number_of_candidates_ <<
      "\n";
  for (const auto& term_candidate: term_candidates_vector) {
    if (term_candidate.second.term_frequency < minimum_term_frequency) {
      break;
    }
    char is_novel_candidate = '0';
    char is_ambiguous_candidate = '0';
    if (term_candidate.second.is_new_candidate) {
      is_novel_candidate = '1';
    }
    if (term_candidate.second.is_ambiguous_candidate) {
      is_ambiguous_candidate = '1';
    }
    char term_category = GetTermCategory(term_candidate.second.term_category);
    file << term_candidate.first << "\t" << term_category << "\t" <<
        is_novel_candidate << "\t" << is_ambiguous_candidate << "\t" <<
        static_cast<double>(term_candidate.second.first_occurrence_position) /
        static_cast<double>(term_candidate.second.document_frequency) << "\t" <<
        term_candidate.second.near_terms_frequency << "\t" <<
        term_candidate.second.term_frequency << "\t" <<
        term_candidate.second.term_frequency_as_capital_word << "\t" <<
        term_candidate.second.term_frequency_as_non_initial_word << "\t" <<
        term_candidate.second.term_frequency_as_subject << "\t" <<
        term_candidate.second.document_frequency << "\t" <<
        term_candidate.second.document_frequency_as_capital_word << "\t" <<
        term_candidate.second.document_frequency_as_non_initial_word << "\t" <<
        term_candidate.second.document_frequency_as_subject << "\t" <<
        term_candidate.second.domain_consensus << "\t" <<
        term_candidate.second.domain_consensus_as_capital_word << "\t" <<
        term_candidate.second.domain_consensus_as_non_initial_word << "\t" <<
        term_candidate.second.domain_consensus_as_subject << "\t";
    ProbabilisticData probabilistic_data = CalculateProbabilisticData(
        term_candidate.second.term_candidate_frequencies,
        term_candidate.second.document_sizes,
        term_candidate.second.term_frequency);
    file << probabilistic_data.term_frequency << "\t" <<
        probabilistic_data.maximum_term_frequency << "\t" <<
        probabilistic_data.term_score << "\t" <<
        probabilistic_data.maximum_term_score << "\t" <<
        probabilistic_data.term_contribution << "\t" <<
        probabilistic_data.term_variance << "\t" <<
        probabilistic_data.term_variance_quality << "\t" <<
        probabilistic_data.term_frequency_bm25 << "\n";
  }
  file.close();
}

void CandidatesAccumulator::CreateLDAFiles(
    const string& lda_input_filename,
    const string& lda_vocabulary_filename,
    unsigned int minimum_term_frequency) const {
  vector<string> words_above_threshold = GetWordsAboveThreshold(
      minimum_term_frequency);
  PrintLDAVocabulary(lda_vocabulary_filename, words_above_threshold);
  PrintLDAInput(lda_input_filename, words_above_threshold);
}

void CandidatesAccumulator::UpdateStoredData(
    const TermCandidateData& term_candidate_data,
    unordered_map<string, TermCandidateData>::iterator updating_iterator)
    const {
  if (updating_iterator->second.term_category !=
      term_candidate_data.term_category) {
    updating_iterator->second.term_category = TermCategory::ADJECTIVE_AND_NOUN;
  }
  updating_iterator->second.is_new_candidate |=
      term_candidate_data.is_new_candidate;
  updating_iterator->second.is_ambiguous_candidate |=
      term_candidate_data.is_ambiguous_candidate;
  updating_iterator->second.first_occurrence_position +=
      term_candidate_data.first_occurrence_position;
  updating_iterator->second.near_terms_frequency +=
      term_candidate_data.near_terms_frequency;
  updating_iterator->second.term_frequency +=
      term_candidate_data.term_frequency;
  updating_iterator->second.term_frequency_as_capital_word +=
      term_candidate_data.term_frequency_as_capital_word;
  updating_iterator->second.term_frequency_as_non_initial_word +=
      term_candidate_data.term_frequency_as_non_initial_word;
  updating_iterator->second.term_frequency_as_subject +=
      term_candidate_data.term_frequency_as_subject;
  updating_iterator->second.document_frequency +=
      term_candidate_data.document_frequency;
  updating_iterator->second.document_frequency_as_capital_word +=
      term_candidate_data.document_frequency_as_capital_word;
  updating_iterator->second.document_frequency_as_non_initial_word +=
      term_candidate_data.document_frequency_as_non_initial_word;
  updating_iterator->second.document_frequency_as_subject +=
      term_candidate_data.document_frequency_as_subject;
  updating_iterator->second.domain_consensus +=
      term_candidate_data.domain_consensus;
  updating_iterator->second.domain_consensus_as_capital_word +=
      term_candidate_data.domain_consensus_as_capital_word;
  updating_iterator->second.domain_consensus_as_non_initial_word +=
      term_candidate_data.domain_consensus_as_non_initial_word;
  updating_iterator->second.domain_consensus_as_subject +=
      term_candidate_data.domain_consensus_as_subject;
  updating_iterator->second.term_candidate_frequencies.push_back(
      term_candidate_data.term_candidate_frequencies.front());
  updating_iterator->second.document_sizes.push_back(
      term_candidate_data.document_sizes.front());
}

vector<pair<string, TermCandidateData> >
    CandidatesAccumulator::SortByTermFrequencies(
        const unordered_map<string, TermCandidateData>& candidates_map) const {
  vector<pair<string, TermCandidateData> > term_candidates_vector(
      candidates_map.begin(),
      candidates_map.end());
  sort(term_candidates_vector.begin(),
       term_candidates_vector.end(),
       [&](const pair<string, TermCandidateData>& first,
           const pair<string, TermCandidateData>& second) {
          return first.second.term_frequency > second.second.term_frequency;});
  return term_candidates_vector;
}

char CandidatesAccumulator::GetTermCategory(const TermCategory& term_category)
    const {
  switch (term_category) {
    case TermCategory::NOUN:
      return 'N';
    case TermCategory::ADJECTIVE:
      return 'A';
    case TermCategory::ADJECTIVE_AND_NOUN:
      return 'B';
    case TermCategory::NO_TERM_CATEGORY: default:
      throw runtime_error("Wrong category of the term candidate");
  }
}

ProbabilisticData CandidatesAccumulator::CalculateProbabilisticData(
    const vector<unsigned int>& term_frequencies,
    const vector<unsigned int>& document_sizes,
    unsigned int term_frequency) const {
  ProbabilisticData probabilistic_data;
  double term_score = 1.0;
  for (size_t index = 0; index < term_frequencies.size(); ++index) {
    double probability = static_cast<double>(term_frequencies[index]) /
        static_cast<double>(document_sizes[index]);
    probabilistic_data.term_frequency += probability;
    probabilistic_data.maximum_term_frequency = max(
        probabilistic_data.maximum_term_frequency,
        probability);
    term_score *= pow(probability,
                      1.0 / static_cast<double>(term_frequencies.size()));
  }
  for (size_t index = 0; index < term_frequencies.size(); ++index) {
    double probability =
        static_cast<double>(term_frequencies[index]) /
        static_cast<double>(document_sizes[index]);
    double current_term_score = probability * log(probability / term_score);
    probabilistic_data.term_score += current_term_score;
    probabilistic_data.maximum_term_score = max(
        probabilistic_data.maximum_term_score,
        current_term_score);
  }
  double average_frequency =
      term_frequency / static_cast<double>(term_frequencies.size());
  double average_document_length =
      static_cast<double>(total_number_of_documents_) /
      static_cast<double>(total_number_of_candidates_);
  for (size_t first_index = 0; first_index < term_frequencies.size();
       ++first_index) {
    probabilistic_data.term_variance_quality +=
        term_frequencies[first_index] * term_frequencies[first_index];
    for (size_t second_index = first_index + 1;
         second_index < term_frequencies.size(); ++second_index) {
      probabilistic_data.term_contribution += term_frequencies[first_index] *
          term_frequencies[second_index];
    }
    probabilistic_data.term_frequency_bm25 +=
        static_cast<double>(term_frequencies[first_index]) * 3.0 /
        (static_cast<double>(term_frequencies[first_index]) + 2.0 *
         (0.25 + 0.75 * document_sizes[first_index] / average_document_length));
  }
  double value = term_frequency * term_frequency /
      static_cast<double>(term_frequencies.size());
  probabilistic_data.term_variance_quality -= value;
  probabilistic_data.term_variance = probabilistic_data.term_variance_quality -
      value + average_frequency * average_frequency;
  return probabilistic_data;
}

vector<string> CandidatesAccumulator::GetWordsAboveThreshold(
    unsigned int minimum_term_frequency) const {
  vector<string> words_above_threshold;
  for (const auto& element: term_candidates_map_) {
    if (element.second.term_frequency >= minimum_term_frequency) {
      words_above_threshold.push_back(element.first);
    }
  }
  sort(words_above_threshold.begin(), words_above_threshold.end());
  return words_above_threshold;
}

void CandidatesAccumulator::PrintLDAVocabulary(
    const string& filename,
    const vector<string>& words_above_threshold) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to open file for printing LDA vocabulary");
  }
  for (const auto& word: words_above_threshold) {
    file << word << "\t" << term_candidates_map_.at(word).term_frequency;
    if (count(word.begin(), word.end(), ' ') > 0) {
      file << "\tB\t-\n";
    } else {
      switch (term_candidates_map_.at(word).term_category) {
        case TermCategory::NOUN:
          file << "\tN\t-\n";
          break;
        case TermCategory::ADJECTIVE:
          file << "\tA\t-\n";
          break;
        case TermCategory::ADJECTIVE_AND_NOUN:
          file << "\tA\tN\t-\n";
          break;
        case TermCategory::NO_TERM_CATEGORY: default:
          throw runtime_error("Failed to print LDA vocabulary");
      }
    }
  }
  file.close();
}

void CandidatesAccumulator::PrintLDAInput(
    const string& filename,
    const vector<string>& words_above_threshold) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to open file for printing LDA input data");
  }
  for (const auto& document: lda_container_) {
    unordered_map<unsigned int, unsigned int> terms_in_document;
    for (const auto& element: document) {
      const auto found_iterator = lower_bound(words_above_threshold.begin(),
                                              words_above_threshold.end(),
                                              element.first);
      if (found_iterator != words_above_threshold.end() &&
          *found_iterator == element.first) {
        terms_in_document.insert(
            {found_iterator - words_above_threshold.begin(), element.second});
      }
    }
    for (const auto& element: terms_in_document) {
      file << element.first << ":" << element.second << "\t";
    }
    file << "\n";
  }
  file.close();
}
