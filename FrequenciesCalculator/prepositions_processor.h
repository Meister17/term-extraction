// Copyright 2011 Michael Nokel
#pragma once

#include "./auxiliary_macros.h"
#include <string>
#include <set>
#include <unordered_map>
#include <vector>

/**
  @brief Class for processing prepositions extracted from files

  This class firstly parses file containing prepositions and their cases. Then
  it should be used for getting cases for extracted from file prepositions.
*/
class PrepositionsProcessor {
 public:
  /**
    Parses file containing prepositions for different cases
    @param filename name of file containing prepositions for different cases
    @throw std::runtime_error in case of any error
  */
  void ParseFile(const std::string& filename);

  /**
    Gets and store cases for extracted preposition
    @param preposition extracted preposition
    @param[out] new_word_parts set containing cases for extracted prepositions
    @see ChangeableWordParts
  */
  void StoreCases(const std::string& preposition,
                  std::set<ChangeableWordParts>* new_word_parts) const {
    auto iterator = prepositions_map_.find(preposition);
    if (iterator != prepositions_map_.end()) {
      for (const auto& element: iterator->second) {
        ChangeableWordParts word_parts(element, WordGender::NO_GENDER);
        new_word_parts->insert(word_parts);
      }
    }
  }

 private:
  /**
    Gets case from its string representation
    @param case_name name of case in string representation
    @return decoded case
  */
  WordCase GetCaseFromString(const std::string& case_name) const;

  /** Hash map containing prepositions for different cases */
  std::unordered_map<std::string, std::vector<WordCase> > prepositions_map_;
};
