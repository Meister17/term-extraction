// Copyright 2011 Michael Nokel
#include "./program_options_parser.h"
#include <boost/program_options.hpp>
#include <iostream>
#include <stdexcept>
#include <string>

using namespace boost::program_options;
using std::cout;
using std::runtime_error;
using std::string;

void ProgramOptionsParser::Parse(int argc, char** argv) {
  variables_map program_options_storage;
  string configuration_filename;
  options_description program_options_description("Options for program:");
  program_options_description.add_options()
      ("help,h", value<bool>(&help_message_printed_)->implicit_value(true),
          "Print help message and exit")
      ("configuration_file,c", value<string>(&configuration_filename),
          "Configuration file of program")
      ("source_directory_name", value<string>(&source_directory_name_),
          "Source directory containing files for processing")
      ("prepositions_filename", value<string>(&prepositions_filename_),
          "File containing prepositions for different cases")
      ("predefined_terms_filename", value<string>(&predefined_terms_filename_),
          "File containing several predefined real terms")
      ("single_word_phrases_filename",
          value<string>(&single_word_phrases_filename_),
          "File for printing extracted phrases for single-word candidates")
      ("single_word_terms_filename",
          value<string>(&single_word_terms_filename_),
          "File for printing single-word term candidates")
      ("single_word_lda_input_filename",
          value<string>(&single_word_lda_input_filename_),
          "File for printing file for further input for LDA")
      ("single_word_lda_vocabulary_filename",
          value<string>(&single_word_lda_vocabulary_filename_),
          "File for printing vocabulary for further LDA")
      ("two_word_phrases_filename", value<string>(&two_word_phrases_filename_),
          "File for printing extracted phrases for two-word candidates")
      ("two_word_terms_filename", value<string>(&two_word_terms_filename_),
          "File for printing two-word term candidates")
      ("two_word_lda_input_filename",
          value<string>(&two_word_lda_input_filename_),
          "File for printing LDA-like input data for two-word terms")
      ("two_word_lda_vocabulary_filename",
          value<string>(&two_word_lda_vocabulary_filename_),
          "File for printing LDA-like vocabulary for two-word terms")
      ("single_words_filename", value<string>(&single_words_filename_),
          "File for printing single-word term candidates and frequencies")
      ("minimum_term_frequency", value<unsigned int>(&minimum_term_frequency_),
          "Minimum term frequency to be taken into account")
      ("context_terms_window_size",
          value<unsigned int>(&context_terms_window_size_),
          "Size of context terms' window")
      ("maximum_noun_phrases_length",
          value<unsigned int>(&maximum_noun_phrases_length_),
          "Maximal length for noun phrases to extract")
      ("morphology_best_variants_filename",
          value<string>(&morphology_best_variants_filename_),
          "File containing best variants for Russian morphology");
  store(parse_command_line(argc, argv, program_options_description),
        program_options_storage);
  notify(program_options_storage);
  if (!configuration_filename.empty()) {
    store(parse_config_file<char>(configuration_filename.data(),
                                  program_options_description),
          program_options_storage);
    notify(program_options_storage);
  }
  if (help_message_printed_) {
    cout << program_options_description << "\n";
  }
  candidates_type_ = DetermineCandidatesType();
}

CandidatesType ProgramOptionsParser::DetermineCandidatesType() const {
  CandidatesType candidate_type;
  if (!single_word_terms_filename_.empty() &&
      !single_word_phrases_filename_.empty() &&
      !single_word_lda_vocabulary_filename_.empty() &&
      !single_word_lda_input_filename_.empty()) {
    candidate_type = CandidatesType::SINGLE_WORD_TERMS;
  }
  if (!two_word_terms_filename_.empty() &&
      !two_word_phrases_filename_.empty() &&
      !two_word_lda_vocabulary_filename_.empty() &&
      !two_word_lda_input_filename_.empty() &&
      !single_words_filename_.empty()) {
    if (candidate_type == CandidatesType::SINGLE_WORD_TERMS) {
      candidate_type = CandidatesType::ALL_TERMS;
    } else {
      candidate_type = CandidatesType::TWO_WORD_TERMS;
    }
  }
  if (candidate_type == CandidatesType::NO_CANDIDATES_TYPE) {
    throw runtime_error("Wrong candidates type was specified");
  }
  return candidate_type;
}
