// Copyright 2012 Michael Nokel
#pragma once

#include "./auxiliary_macros.h"
#include <string>
#include <unordered_set>

/**
  @brief Class intended to be used for finding several predefined terms in texts
  from collection

  This class parses file with several predefined terms. After what it should be
  used for checking whether given word is such predefined term.
*/
class PredefinedWordsFinder {
 public:
  /**
    Parses the file containing several predefined terms
    @param filename name of file containing several predefined terms
    @throw std::runtime_error in case of any error;
  */
  void ParseFile(const std::string& filename);

  /**
    Checks whether given word is predefined term or not
    @param word word to check
    @return true if given word is a predefined term
  */
  bool IsPredefinedTerm(const std::string& word) {
    return predefined_terms_set_.find(word) != predefined_terms_set_.end();
  }

 private:
  /** Hash set containing several predefined terms */
  std::unordered_set<std::string> predefined_terms_set_;
};
