// Copyright 2011 Michael Nokel
#include "./frequencies_calculator.h"
#include "./program_options_parser.h"
#include <iostream>
#include <stdexcept>

using std::cout;
using std::exception;

/**
  Main function of program. Starts program
  @param argc number of program command line parameters
  @param argv command line parameters
  @return code of exit
*/
int main(int argc, char** argv) {
  try {
    ProgramOptionsParser program_options_parser;
    program_options_parser.Parse(argc, argv);
    if (program_options_parser.help_message_printed()) {
      return 0;
    }
    FrequenciesCalculator frequencies_calculator(
        program_options_parser.minimum_term_frequency(),
        program_options_parser.candidates_type(),
        program_options_parser.context_terms_window_size(),
        program_options_parser.maximum_noun_phrases_length());
    frequencies_calculator.Initialize(
        program_options_parser.predefined_terms_filename(),
        program_options_parser.prepositions_filename(),
        program_options_parser.morphology_best_variants_filename());
    frequencies_calculator.CalculateFrequencies(
        program_options_parser.source_directory_name());
    frequencies_calculator.PrintTermCandidates(
        program_options_parser.single_word_phrases_filename(),
        program_options_parser.single_word_terms_filename(),
        program_options_parser.single_word_lda_input_filename(),
        program_options_parser.single_word_lda_vocabulary_filename(),
        program_options_parser.single_words_filename(),
        program_options_parser.two_word_phrases_filename(),
        program_options_parser.two_word_terms_filename(),
        program_options_parser.two_word_lda_input_filename(),
        program_options_parser.two_word_lda_vocabulary_filename());
  } catch (const exception& except) {
    cout << "Fatal error occurred: " << except.what() << "\n";
    return 1;
  }
  return 0;
}
