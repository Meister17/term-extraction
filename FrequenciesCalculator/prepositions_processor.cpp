// Copyright 2011 Michael Nokel
#include "./auxiliary_macros.h"
#include "./prepositions_processor.h"
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <cctype>
#include <fstream>
#include <stdexcept>
#include <string>
#include <vector>

using boost::is_any_of;
using boost::split;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::string;
using std::vector;

void PrepositionsProcessor::ParseFile(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse file with prepositions");
  }
  while (!file.eof()) {
    string line;
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (line.empty()) {
      continue;
    }
    vector<string> tokens;
    split(tokens, line, is_any_of(","));
    WordCase case_name = GetCaseFromString(tokens[0]);
    if (case_name == WordCase::NO_CASE) {
      file.close();
      throw runtime_error("Failed to parse file with prepositions");
    }
    for (size_t index = 1; index < tokens.size(); ++index) {
      auto insert_result =
          prepositions_map_.insert({tokens[index], vector<WordCase>()});
      insert_result.first->second.push_back(case_name);
    }
  }
  file.close();
}

WordCase PrepositionsProcessor::GetCaseFromString(const string& case_name)
    const {
  if (case_name == "Именительный") {
    return WordCase::NOMINATIVE;
  } else if (case_name == "Родительный") {
    return WordCase::GENITIVE;
  } else if (case_name == "Дательный") {
    return WordCase::DATIVE;
  } else if (case_name == "Винительный") {
    return WordCase::ACCUSATIVE;
  } else if (case_name == "Творительный") {
    return WordCase::INSTRUMENTAL;
  } else if (case_name == "Предложный") {
    return WordCase::PREPOSITIONAL;
  }
  return WordCase::NO_CASE;
}
