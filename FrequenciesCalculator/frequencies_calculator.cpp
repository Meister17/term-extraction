// Copyright 2011 Michael Nokel
#include "./frequencies_calculator.h"
#include "./candidates_extractors/noun_phrases_extractor.h"
#include "./candidates_extractors/single_words_extractor.h"
#include "./candidates_extractors/single_word_terms_extractor.h"
#include "./candidates_extractors/two_word_terms_extractor.h"
#include <iconv.h>
#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/threadpool.hpp>
#include <cctype>
#include <fstream>
#include <iostream>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>

using boost::is_any_of;
using boost::filesystem::directory_iterator;
using boost::filesystem::is_regular_file;
using boost::mutex;
using boost::split;
using boost::token_compress_on;
using boost::thread;
using boost::threadpool::pool;
using std::cout;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::set;
using std::sort;
using std::string;
using std::vector;

FrequenciesCalculator::FrequenciesCalculator(
    unsigned int minimum_term_frequency,
    CandidatesType candidates_type,
    unsigned int context_terms_window_size,
    unsigned int maximal_noun_phrases_length)
    : kMinimumTermFrequency_(minimum_term_frequency),
      kCandidatesType_(candidates_type),
      kContextTermsWindowSize_(context_terms_window_size),
      kMaximalNounPhrasesLength_(maximal_noun_phrases_length) {
}

void FrequenciesCalculator::Initialize(
    const string& predefined_terms_filename,
    const string& prepositions_filename,
    const string& morphology_best_variants_filename) {
  cout << "Initializing\n";
  predefined_words_finder_.ParseFile(predefined_terms_filename);
  words_processor_.ParseFile(prepositions_filename);
  morphology_variants_chooser_.ParseFile(morphology_best_variants_filename);
}

void FrequenciesCalculator::CalculateFrequencies(
    const string& source_directory_name) {
  cout << "Extracting term candidates\n";
  vector<string> files;
  for (directory_iterator iterator(source_directory_name);
       iterator != directory_iterator(); ++iterator) {
    if (is_regular_file(iterator->status()) &&
        iterator->path().extension() == kProcessingFileExtension_) {
      files.push_back(iterator->path().string());
    }
  }
  sort(files.begin(), files.end());
  cout << files.size() << " files found\n";
  if (kCandidatesType_ == CandidatesType::SINGLE_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    single_word_terms_accumulator_.ReserveForFiles(files.size());
  }
  if (kCandidatesType_ == CandidatesType::TWO_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    two_word_terms_accumulator_.ReserveForFiles(files.size());
  }
  pool thread_pool(kNumberOfThreads_);
  for (size_t file_number = 0; file_number < files.size(); ++file_number) {
    thread_pool.schedule(boost::bind(&FrequenciesCalculator::ParseCurrentFile,
                                     this,
                                     files[file_number],
                                     file_number));
  }
}

void FrequenciesCalculator::PrintTermCandidates(
    const string& single_word_phrases_filename,
    const string& single_word_terms_filename,
    const string& single_word_lda_input_filename,
    const string& single_word_lda_vocabulary_filename,
    const string& single_words_filename,
    const string& two_word_phrases_filename,
    const string& two_word_terms_filename,
    const string& two_word_lda_input_filename,
    const string& two_word_lda_vocabulary_filename) {
  cout << "Printing extracted term candidates\n";
  pool thread_pool(kNumberOfThreads_);
  if (kCandidatesType_ == CandidatesType::SINGLE_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    thread_pool.schedule(boost::bind(
        &CandidatesAccumulator::PrintTermCandidates,
        &single_word_terms_accumulator_,
        single_word_terms_filename,
        kMinimumTermFrequency_));
    thread_pool.schedule(boost::bind(&CandidatesAccumulator::CreateLDAFiles,
                                     &single_word_terms_accumulator_,
                                     single_word_lda_input_filename,
                                     single_word_lda_vocabulary_filename,
                                     kMinimumTermFrequency_));
    thread_pool.schedule(boost::bind(
        &NounPhrasesAccumulator::PrintSingleWordNounPhrases,
        &noun_phrases_accumulator_,
        single_word_phrases_filename,
        kMinimumTermFrequency_));
  }
  if (kCandidatesType_ == CandidatesType::TWO_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    thread_pool.schedule(boost::bind(
        &TwoWordTermsAccumulator::PrintTermCandidates,
        &two_word_terms_accumulator_,
        two_word_terms_filename,
        kMinimumTermFrequency_));
    thread_pool.schedule(boost::bind(&TwoWordTermsAccumulator::CreateLDAFiles,
                                     &two_word_terms_accumulator_,
                                     two_word_lda_input_filename,
                                     two_word_lda_vocabulary_filename,
                                     kMinimumTermFrequency_));
    thread_pool.schedule(boost::bind(&TwoWordTermsAccumulator::PrintSingleWords,
                                     &two_word_terms_accumulator_,
                                     single_words_filename,
                                     kMinimumTermFrequency_));
    thread_pool.schedule(boost::bind(
        &NounPhrasesAccumulator::PrintTwoWordNounPhrases,
        &noun_phrases_accumulator_,
        two_word_phrases_filename,
        kMinimumTermFrequency_));
  }
}

void FrequenciesCalculator::ParseCurrentFile(const string& filename,
                                             size_t file_number) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse file");
  }
  NounPhrasesExtractor noun_phrases_extractor(kMaximalNounPhrasesLength_);
  SingleWordsExtractor single_words_extractor;
  SingleWordTermsExtractor single_word_terms_extractor(
      kContextTermsWindowSize_,
      &predefined_words_finder_);
  TwoWordTermsExtractor two_word_terms_extractor(
      kContextTermsWindowSize_,
      &predefined_words_finder_);
  unsigned int number_of_words_in_file = 0;
  unsigned int number_of_tokens_in_file = 0;
  WordProcessingData word_processing_data;
  vector<WordProperties> previous_words;
  while (!file.eof()) {
    vector<WordProperties> same_word_forms;
    ExtractSameWordForms(number_of_words_in_file,
                         number_of_tokens_in_file,
                         &file,
                         &word_processing_data,
                         &same_word_forms);
    CorrectWordProcessingData(number_of_words_in_file,
                              number_of_tokens_in_file,
                              &word_processing_data);
    bool continue_processing;
    vector<WordProperties> words_to_store;
    words_processor_.AddNewWords(same_word_forms,
                                 continue_processing,
                                 &previous_words,
                                 &words_to_store);
    if (!continue_processing) {
      StoreNewWords(words_to_store,
                    &noun_phrases_extractor,
                    &single_words_extractor,
                    &single_word_terms_extractor,
                    &two_word_terms_extractor);
    }
  }
  file.close();
  if (!previous_words.empty()) {
    StoreNewWords(previous_words,
                  &noun_phrases_extractor,
                  &single_words_extractor,
                  &single_word_terms_extractor,
                  &two_word_terms_extractor);
  }
  ProcessLastTermCandidates(&single_word_terms_extractor,
                            &two_word_terms_extractor);
  AccumulateResults(noun_phrases_extractor,
                    single_words_extractor,
                    single_word_terms_extractor,
                    two_word_terms_extractor,
                    file_number);
}

void FrequenciesCalculator::ExtractSameWordForms(
    unsigned int number_of_words_in_file,
    unsigned int number_of_tokens_in_file,
    ifstream* file,
    WordProcessingData* word_processing_data,
    vector<WordProperties>* same_word_forms) const {
  vector<WordProperties> extracted_words;
  int first_letter = -1;
  do {
    string line;
    getline(*file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    line.erase(line.begin(),
               find_if(line.begin(),
                       line.end(),
                       not1(ptr_fun<int, int>(isspace))));
    line = ConvertFromCP1251ToUTF8(line);
    vector<string> tokens;
    split(tokens, line, is_any_of(" \t"), token_compress_on);
    WordProperties extracted_word_properties;
    bool is_token = false;
    bool is_word = false;
    words_processor_.ExtractWordFromLine(tokens,
                                         word_processing_data,
                                         is_token,
                                         is_word,
                                         &extracted_word_properties);
    if (is_word) {
      word_processing_data->word_extracted = true;
      if (extracted_word_properties.word_category != WordCategory::DIVIDER) {
        word_processing_data->candidate_extracted = true;
      }
    }
    if (is_token) {
      word_processing_data->token_extracted = true;
    }
    extracted_word_properties.word_number_in_file = number_of_words_in_file;
    extracted_word_properties.token_number_in_file = number_of_tokens_in_file;
    extracted_words.push_back(extracted_word_properties);
    first_letter = file->get();
  } while (!file->eof() && isspace(first_letter));
  if (!file->eof()) {
    file->unget();
  }
  CorrectExtractedSameWordForms(extracted_words, same_word_forms);
}

void FrequenciesCalculator::CorrectExtractedSameWordForms(
    const vector<WordProperties>& extracted_words,
    vector<WordProperties>* same_word_forms) const noexcept {
  bool has_known_word = false;
  bool has_auxiliary_word = false;
  for (const auto& word_properties: extracted_words) {
    if (!word_properties.is_new_word) {
      has_known_word = true;
    }
    if (word_properties.is_auxiliary_word) {
      has_auxiliary_word = true;
    }
  }
  vector<WordProperties> filtered_words;
  if (has_auxiliary_word) {
    for (const auto& word_properties: extracted_words) {
      if (word_properties.is_auxiliary_word) {
        filtered_words.push_back(word_properties);
      }
    }
  } else if (has_known_word) {
    for (const auto& word_properties: extracted_words) {
      if (!word_properties.is_new_word ||
          word_properties.word.find("-") != string::npos) {
        filtered_words.push_back(word_properties);
      }
    }
  } else {
    filtered_words.push_back(extracted_words[0]);
  }
  if (filtered_words.size() <= 1) {
    *same_word_forms = filtered_words;
  } else {
    bool has_nouns = false;
    bool has_adjectives = false;
    for (const auto& word_property: filtered_words) {
      if (word_property.word.find("-") == string::npos) {
        if (word_property.word_category == WordCategory::NOUN ||
            word_property.word_category == WordCategory::PROPER_NOUN) {
          has_nouns = true;
        } else if (word_property.word_category == WordCategory::ADJECTIVE) {
          has_adjectives = true;
        }
      }
    }
    if (has_adjectives && has_nouns) {
      *same_word_forms = filtered_words;
    } else {
      *same_word_forms = morphology_variants_chooser_.GetMostProbableWordForms(
          filtered_words);
    }
  }
}

void FrequenciesCalculator::CorrectWordProcessingData(
    unsigned int& number_of_words_in_file,
    unsigned int& number_of_tokens_in_file,
    WordProcessingData* word_processing_data) const {
  if (word_processing_data->word_extracted) {
    ++number_of_words_in_file;
    word_processing_data->waiting_for_first_word = false;
    if (word_processing_data->candidate_extracted) {
      word_processing_data->was_punctuation_divider = false;
    }
  }
  word_processing_data->word_extracted = false;
  word_processing_data->candidate_extracted = false;
  if (word_processing_data->token_extracted) {
    ++number_of_tokens_in_file;
  }
  word_processing_data->token_extracted = false;
}

void FrequenciesCalculator::StoreNewWords(
    const vector<WordProperties>& words_to_store,
    NounPhrasesExtractor* noun_phrases_extractor,
    SingleWordsExtractor* single_words_extractor,
    SingleWordTermsExtractor* single_word_terms_extractor,
    TwoWordTermsExtractor* two_word_terms_extractor) const {
  noun_phrases_extractor->AddNewTermCandidates(words_to_store);
  if (kCandidatesType_ == CandidatesType::SINGLE_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    single_word_terms_extractor->AddNewTermCandidates(words_to_store);
  }
  if (kCandidatesType_ == CandidatesType::TWO_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    single_words_extractor->AddNewTermCandidates(words_to_store);
    two_word_terms_extractor->AddNewTermCandidates(words_to_store);
  }
}

void FrequenciesCalculator::ProcessLastTermCandidates(
    SingleWordTermsExtractor* single_word_terms_extractor,
    TwoWordTermsExtractor* two_word_terms_extractor) {
  if (kCandidatesType_ == CandidatesType::SINGLE_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    single_word_terms_extractor->ProcessLastTermCandidates();
  }
  if (kCandidatesType_ == CandidatesType::TWO_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    two_word_terms_extractor->ProcessLastTermCandidates();
  }
}

void FrequenciesCalculator::AccumulateResults(
    const NounPhrasesExtractor& noun_phrases_extractor,
    const SingleWordsExtractor& single_words_extractor,
    const SingleWordTermsExtractor& single_word_terms_extractor,
    const TwoWordTermsExtractor& two_word_terms_extractor,
    size_t file_number) {
  noun_phrases_accumulator_.Accumulate(
      noun_phrases_extractor.GetStartIterator(),
      noun_phrases_extractor.GetEndIterator());
  if (kCandidatesType_ == CandidatesType::SINGLE_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    single_word_terms_accumulator_.Accumulate(
        single_word_terms_extractor.GetStartIterator(),
        single_word_terms_extractor.GetEndIterator(),
        file_number);
  }
  if (kCandidatesType_ == CandidatesType::TWO_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    two_word_terms_accumulator_.Accumulate(
        two_word_terms_extractor.GetStartIterator(),
        two_word_terms_extractor.GetEndIterator(),
        file_number);
    two_word_terms_accumulator_.AccumulateSingleWords(
        single_words_extractor.GetStartIterator(),
        single_words_extractor.GetEndIterator());
  }
}

string FrequenciesCalculator::ConvertFromCP1251ToUTF8(
    const string& string_cp1251) const {
  iconv_t cd = iconv_open("utf-8", "cp1251");
  if (cd == (iconv_t) - 1) {
    return "";
  }
  size_t input_size = string_cp1251.length();
  size_t output_size = 2*input_size + 1;
  size_t total_output_size = output_size;
#ifdef _WIN32
  const char* input_string_pointer = string_cp1251.data();
#else
  char* input_string_pointer = const_cast<char*>(string_cp1251.data());
#endif
  char* output_string = new char[output_size];
  char* output_string_pointer = output_string;
  memset(output_string, 0, output_size);
  if ((iconv(cd, &input_string_pointer, &input_size, &output_string_pointer,
             &output_size)) == (size_t) - 1) {
    iconv_close(cd);
    delete[] output_string;
    return "";
  }
  iconv_close(cd);
  string string_utf8 = string(output_string, total_output_size - output_size);
  delete[] output_string;
  return string_utf8;
}