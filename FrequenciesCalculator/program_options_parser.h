// Copyright 2011 Michael Nokel
#pragma once

#include "./auxiliary_macros.h"
#include <string>

/**
  @brief Class using to work with program options

  With the help of this class one can parse program options from command line
  and/or from configuration file, check their correctness and if everything is
  correct one can get the values of the necessary program options
*/
class ProgramOptionsParser {
 public:
  /**
    Parses program options from command line and/or from configuration file
    @param argc number of program options (from main function)
    @param argv array containing program options (from main function)
    @return error codes of parsing program options
  */
  void Parse(int argc, char** argv);

  bool help_message_printed() const {
    return help_message_printed_;
  }

  std::string source_directory_name() const {
    return source_directory_name_;
  }

  std::string prepositions_filename() const {
    return prepositions_filename_;
  }

  std::string predefined_terms_filename() const {
    return predefined_terms_filename_;
  }

  std::string single_word_phrases_filename() const {
    return single_word_phrases_filename_;
  }

  std::string single_word_terms_filename() const {
    return single_word_terms_filename_;
  }

  std::string single_word_lda_input_filename() const {
    return single_word_lda_input_filename_;
  }

  std::string single_word_lda_vocabulary_filename() const {
    return single_word_lda_vocabulary_filename_;
  }

  std::string two_word_phrases_filename() const {
    return two_word_phrases_filename_;
  }

  std::string two_word_terms_filename() const {
    return two_word_terms_filename_;
  }

  std::string two_word_lda_input_filename() const {
    return two_word_lda_input_filename_;
  }

  std::string two_word_lda_vocabulary_filename() const {
    return two_word_lda_vocabulary_filename_;
  }

  std::string single_words_filename() const {
    return single_words_filename_;
  }

  CandidatesType candidates_type() const {
    return candidates_type_;
  }

  unsigned int minimum_term_frequency() const {
    return minimum_term_frequency_;
  }

  unsigned int context_terms_window_size() const {
    return context_terms_window_size_;
  }

  unsigned int maximum_noun_phrases_length() const {
    return maximum_noun_phrases_length_;
  }

  std::string morphology_best_variants_filename() const {
    return morphology_best_variants_filename_;
  }

 private:
  /**
    Determines candidates type based on the given program options
    @return proper candidates type
    @see CandidatesType
  */
  CandidatesType DetermineCandidatesType() const;

  /** Flag indicating whether help message has been printed */
  bool help_message_printed_ = false;

  /** Source directory with files that should be processed */
  std::string source_directory_name_ = "";

  /** File containing real phrases from specified area */
  std::string phrases_filename_ = "";

  /** File containing prepositions for different cases */
  std::string prepositions_filename_ = "";

  /** File containing several predefined real terms */
  std::string predefined_terms_filename_ = "";

  /** File where extracted phrases for single-word terms will be written */
  std::string single_word_phrases_filename_ = "";

  /** File where extracted single-word term candidates will be written */
  std::string single_word_terms_filename_ = "";

  /** File where input data for LDA will be written */
  std::string single_word_lda_input_filename_ = "";

  /** File where vocabulary for LDA will be written */
  std::string single_word_lda_vocabulary_filename_ = "";

  /** File where extracted phrases for two-word term candidates will be
      written */
  std::string two_word_phrases_filename_ = "";

  /** File where extracted two-word term candidates will be written */
  std::string two_word_terms_filename_ = "";

  /** File where input data for LDA for two-word term candidates will be
      written */
  std::string two_word_lda_input_filename_ = "";

  /** File where vocabulary in LDA-like style for two-word term candidates will
      be written */
  std::string two_word_lda_vocabulary_filename_ = "";

  /** File for placing there single-word term candidates and some data about
      them */
  std::string single_words_filename_ = "";

  /** Types of term candidates to work with */
  CandidatesType candidates_type_ = CandidatesType::NO_CANDIDATES_TYPE;

  /** Minimum term frequency to be taken into account */
  unsigned int minimum_term_frequency_ = 0;

  /** Size of context terms' window (for counting terms in the window of
      predefined real terms) */
  unsigned int context_terms_window_size_ = 0;

  /** Maximum length of Noun phrases to extract */
  unsigned int maximum_noun_phrases_length_ = 0;

  /** File containing best variants for Russian morphology */
  std::string morphology_best_variants_filename_ = "";
};
