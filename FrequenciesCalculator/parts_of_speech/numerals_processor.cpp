// Copyright 2012 Michael Nokel
#include "./numerals_processor.h"
#include "../auxiliary_macros.h"
#include <string>

using std::string;

void NumeralsProcessor::ProcessWord(const string& token,
                                    WordProperties* word_properties) const {
  word_properties->word_category = WordCategory::AGREEMENT_WORD;
  for (size_t index = 1; index < token.size(); index += 4) {
    string sign = token.substr(index, 2);
    ChangeableWordParts word_parts(GetWordCase(sign), GetWordGender(sign));
    word_properties->changeable_word_parts.insert(word_parts);
  }
}

WordCase NumeralsProcessor::GetWordCase(const string& case_sign) const {
  if (string("ажмт").find(case_sign) != string::npos) {
    return WordCase::NOMINATIVE;
  } else if (string("бзнущ").find(case_sign) != string::npos) {
    return WordCase::GENITIVE;
  } else if (string("виоф").find(case_sign) != string::npos) {
    return WordCase::DATIVE;
  } else if (string("гйпх").find(case_sign) != string::npos) {
    return WordCase::ACCUSATIVE;
  } else if (string("дкрц").find(case_sign) != string::npos) {
    return WordCase::INSTRUMENTAL;
  } else if (string("елсч").find(case_sign) != string::npos) {
    return WordCase::PREPOSITIONAL;
  }
  return WordCase::NO_CASE;
}

WordGender NumeralsProcessor::GetWordGender(const string& gender_sign) const {
  if (string("абвгде").find(gender_sign) != string::npos) {
    return WordGender::MASCULINE;
  } else if (string("жзийкл").find(gender_sign) != string::npos) {
    return WordGender::FEMININE;
  } else if (string("мнопрс").find(gender_sign) != string::npos) {
    return WordGender::NEUTER;
  } else if (string("туфхцч").find(gender_sign) != string::npos) {
    return WordGender::PLURAL;
  }
  return WordGender::NO_GENDER;
}
