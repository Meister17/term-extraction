// Copyright 2012 Michael Nokel
#include "./nouns_processor.h"
#include "../auxiliary_macros.h"
#include <string>

using std::string;

void NounsProcessor::ProcessWord(const string& token,
                                 WordProperties* word_properties) const {
  if (word_properties->word_category != WordCategory::PROPER_NOUN) {
    word_properties->word_category = WordCategory::NOUN;
  }
  for (size_t index = 0; index < token.size(); index += 4) {
    string case_sign = token.substr(index + 2, 2);
    string gender_sign = token.substr(index, 2);
    ChangeableWordParts word_parts(GetWordCase(case_sign),
                                   WordGender::NO_GENDER);
    if (string("абвгдемн").find(case_sign) != string::npos) {
      if (gender_sign == "в") {
        word_parts.word_gender = WordGender::FEMININE;
        word_properties->changeable_word_parts.insert(word_parts);
        word_parts.word_gender = WordGender::MASCULINE;
      } else {
        word_parts.word_gender = GetWordGender(gender_sign);
      }
    } else {
      word_parts.word_gender = WordGender::PLURAL;
    }
    word_properties->changeable_word_parts.insert(word_parts);
  }
}

WordCase NounsProcessor::GetWordCase(const string& case_sign) const {
  if (string("аж").find(case_sign) != string::npos) {
    return WordCase::NOMINATIVE;
  } else if (string("бз").find(case_sign) != string::npos) {
    return WordCase::GENITIVE;
  } else if (string("ви").find(case_sign) != string::npos) {
    return WordCase::DATIVE;
  } else if (string("гй").find(case_sign) != string::npos) {
    return WordCase::ACCUSATIVE;
  } else if (string("дк").find(case_sign) != string::npos) {
    return WordCase::INSTRUMENTAL;
  } else if (string("ел").find(case_sign) != string::npos) {
    return WordCase::PREPOSITIONAL;
  }
  return WordCase::NO_CASE;
}

WordGender NounsProcessor::GetWordGender(const string& gender_sign) const {
  if (string("аб").find(gender_sign) != string::npos) {
    return WordGender::MASCULINE;
  } else if (string("гд").find(gender_sign) != string::npos) {
    return WordGender::FEMININE;
  } else if (string("еж").find(gender_sign) != string::npos) {
    return WordGender::NEUTER;
  } else if (gender_sign == "и") {
    return WordGender::PLURAL;
  }
  return WordGender::NO_GENDER;
}
