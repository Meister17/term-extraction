// Copyright 2012 Michael Nokel
#include "./participles_processor.h"
#include "../auxiliary_macros.h"
#include <string>

using std::string;

void ParticiplesProcessor::ProcessWord(const string& token,
                                       WordProperties* word_properties) const {
  word_properties->word_category = WordCategory::AGREEMENT_WORD;
  for (size_t index = 1; index < token.size(); index += 4) {
    string sign = token.substr(index, 2);
    ChangeableWordParts word_parts(GetWordCase(sign), GetWordGender(sign));
    word_properties->changeable_word_parts.insert(word_parts);
  }
}

WordCase ParticiplesProcessor::GetWordCase(const string& case_sign) const {
  if (string("азох").find(case_sign) != string::npos) {
    return WordCase::NOMINATIVE;
  } else if (string("бипц").find(case_sign) != string::npos) {
    return WordCase::GENITIVE;
  } else if (string("вйрч").find(case_sign) != string::npos) {
    return WordCase::DATIVE;
  } else if (string("гксш").find(case_sign) != string::npos) {
    return WordCase::ACCUSATIVE;
  } else if (string("длтщ").find(case_sign) != string::npos) {
    return WordCase::INSTRUMENTAL;
  } else if (string("емуы").find(case_sign) != string::npos) {
    return WordCase::PREPOSITIONAL;
  }
  return WordCase::NO_CASE;
}

WordGender ParticiplesProcessor::GetWordGender(const string& gender_sign)
    const {
  if (string("абвгдеж").find(gender_sign) != string::npos) {
    return WordGender::MASCULINE;
  } else if (string("зийклмн").find(gender_sign) != string::npos) {
    return WordGender::FEMININE;
  } else if (string("опрстуф").find(gender_sign) != string::npos) {
    return WordGender::NEUTER;
  } else if (string("хцчшщыэ").find(gender_sign) != string::npos) {
    return WordGender::PLURAL;
  }
  return WordGender::NO_GENDER;
}
