// Copyright 2012 Michael Nokel
#include "./predefined_words_finder.h"
#include <algorithm>
#include <cctype>
#include <fstream>
#include <stdexcept>
#include <string>

using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::string;

void PredefinedWordsFinder::ParseFile(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse file with predefined terms");
  }
  while (!file.eof()) {
    string line;
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    predefined_terms_set_.insert(line);
  }
  file.close();
}
