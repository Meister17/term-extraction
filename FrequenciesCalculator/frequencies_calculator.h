// Copyright 2011 Michael Nokel
#pragma once

#include "./auxiliary_macros.h"
#include "./predefined_words_finder.h"
#include "./morphology_variants_chooser.h"
#include "./words_processor.h"
#include "./candidates_accumulators/candidates_accumulator.h"
#include "./candidates_accumulators/noun_phrases_accumulator.h"
#include "./candidates_accumulators/two_word_terms_accumulator.h"
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <string>
#include <vector>

class NounPhrasesExtractor;
class SingleWordsExtractor;
class SingleWordTermsExtractor;
class TwoWordTermsExtractor;

/**
  @brief Class intended to use for calculating frequencies of phrases and terms
  found in collection

  This class should be used to identify possible term phrases, single terms
  found in given collection. For term phrases only TF (Term Frequency) is
  calculated. Please, note that all extracted terms are printed in the
  descending order of their term frequencies.
*/
class FrequenciesCalculator {
 public:
  /**
    Initializes object
    @param minimum_term_frequency minimum term frequency to be taken into
    account
    @param candidates_type type of candidates that are to be extracted
    @param context_terms_window_size size of context window expressed in terms
    @param maximum_noun_phrases_length maximum length of noun phrases to extract
  */
  FrequenciesCalculator(unsigned int minimum_term_frequency,
                        CandidatesType candidates_type,
                        unsigned int context_terms_window_size,
                        unsigned int maximum_noun_phrases_length);

  /**
    Initializes object by parsing files containing real phrases from the
    specified area, several predefined terms and prepositions with their cases
    @param predefined_terms_filename file containing several predefined terms
    @param prepositions_filename file containing prepositions with their cases
    @param morphology_best_variants_filename file containing best variants for
    Russian morphology disambiguation
    @throw std::runtime_error in case of any error
  */
  void Initialize(const std::string& predefined_terms_filename,
                  const std::string& prepositions_filename,
                  const std::string& morphology_best_variants_filename);

  /**
    Scans source directory and calculate frequencies by parsing each file in
    a thread pool
    @param source_directory_name source directory containing files that should
    be processed
    @throw std::runtime_error in case of any error
  */
  void CalculateFrequencies(const std::string& source_directory_name);

  /**
    Prints all extracted term candidates in the given files in a thread pool
    @param single_word_phrases_filename file where extracted phrases for
    single-word term candidates will be printed
    @param single_word_terms_filename file where single-word term candidates
    will be printed
    @param single_word_lda_input_filename file where input file for LDA will be
    created
    @param single_word_lda_vocabulary_filename file where vocabulary for LDA
    will be printed
    @param single_words_filename file where single-word term candidates and
    their frequencies will be printed
    @param two_word_phrases_filename file where extracted phrases for two-word
    term candidates will be printed
    @param two_word_terms_filename file where two-word term candidates will be
    printed
    @param two_word_lda_input_filename file where input data for LDA for
    two-word term candidates will be written
    @param two_word_lda_vocabulary_filename file where vocabulary for LDA for
    two-word term candidates will be written
  */
  void PrintTermCandidates(
      const std::string& single_word_phrases_filename,
      const std::string& single_word_terms_filename,
      const std::string& single_word_lda_input_filename,
      const std::string& single_word_lda_vocabulary_filename,
      const std::string& single_words_filename,
      const std::string& two_word_phrases_filename,
      const std::string& two_word_terms_filename,
      const std::string& two_word_lda_input_filename,
      const std::string& two_word_lda_vocabulary_filename);

 private:
  /**
    Parses current file line by line and calculates frequencies of found terms
    and phrases
    @param filename name of file to process
    @oaran file_number number of processing file
    @throw std::runtime_error in case of any error
  */
  void ParseCurrentFile(const std::string& filename,
                        size_t file_number);

  /**
    Extracts words that have same forms from the current line in file
    @param number_of_words_in_file current number of extracted words in file
    @param number_of_tokens_in_file current number of extracted tokens in file
    @param[out] file file from where to extract words
    @param[out] word_processing_data structure containing information about
    processing word
    @param[out] same_word_forms vector where extracted known words with the
    same forms will be stored
    @throw std::runtime_error in case of any error
  */
  void ExtractSameWordForms(unsigned int number_of_words_in_file,
                            unsigned int number_of_tokens_in_file,
                            std::ifstream* file,
                            WordProcessingData* word_processing_data,
                            std::vector<WordProperties>* same_word_forms) const;

  /**
    Corrects extracted same words
    @param extracted_words vector with extracted words with their forms
    @param[out] same_word_forms vector where extracted words with the same
    forms will e stored
  */
  void CorrectExtractedSameWordForms(
      const std::vector<WordProperties>& extracted_words,
      std::vector<WordProperties>* same_word_forms) const noexcept;

  /**
    Corrects data of the current processing word
    @param[out] number_of_words_in_file current number of extracted words in
    file (will be incremented if the extracted lemma is real word)
    @param[out] number_of_tokens_in_file current number of extracted tokens in
    file (will be incremented)
    @param[out] word_processing_data data of the current processing word
  */
  void CorrectWordProcessingData(unsigned int& number_of_words_in_file,
                                 unsigned int& number_of_tokens_in_file,
                                 WordProcessingData* word_processing_data)
                                 const;

  /**
    Stores new words
    @param words_to_store vector containing properties of currently extracted
    words
    @param[out] noun_phrases_extractor object used for extracting phrases
    @param[out] single_words_extractor object used for extracting single-word
    term candidates for two-word term candidates
    @param[out] single_word_terms_extractor object used for extracting
    single-word term candidates
    @param[out] two_word_terms_extractor object used for extracting two-word
    term candidates
  */
  void StoreNewWords(
      const std::vector<WordProperties>& words_to_store,
      NounPhrasesExtractor* noun_phrases_extractor,
      SingleWordsExtractor* single_words_extractor,
      SingleWordTermsExtractor* single_word_terms_extractor,
      TwoWordTermsExtractor* two_word_terms_extractor) const;

  /**
    Processes last term candidates
    @param[out] single_word_terms_extractor object used for extracting
    single-word term candidates
    @param[out] two_word_terms_extractor object used for extracting two-word
    term candidates
    @note This function should be called after successful parsing each file
  */
  void ProcessLastTermCandidates(
      SingleWordTermsExtractor* single_word_terms_extractor,
      TwoWordTermsExtractor* two_word_terms_extractor);

  /**
    Accumulates results of words extracted from parsed file
    @param phrases_extractor object used for extracting phrases
    @param single_words_extractor object used for extracting single-word term
    candidates for two-word term candidates
    @param single_word_terms_extractor object used for extracting single-word
    term candidates
    @param two_word_terms_extractor object used for extracting two-word term
    candidates
    @param file_number number of processed file
  */
  void AccumulateResults(
      const NounPhrasesExtractor& noun_phrases_extractor,
      const SingleWordsExtractor& single_words_extractor,
      const SingleWordTermsExtractor& single_word_terms_extractor,
      const TwoWordTermsExtractor& two_word_terms_extractor,
      size_t file_number);

  /**
    Converts string from UTF8 encoding to CP1251
    @param string_cp1251 string in CP1251 encoding
    @return equivalent string in UTF8 encoding
  */
  std::string ConvertFromCP1251ToUTF8(const std::string& string_cp1251) const;

  /** Number of threads in a thread pool */
  const int kNumberOfThreads_ =
      static_cast<int>(boost::thread::hardware_concurrency());

  /** Extension of files that should be processed */
  const std::string kProcessingFileExtension_ = ".txt";

  /** Minimum single frequency for term to make decision whether to take current
      current term into account or not */
  const unsigned int kMinimumTermFrequency_;

  /** Type of candidates that are to be extracted */
  const CandidatesType kCandidatesType_;

  /** Size of context window expressed in terms */
  const unsigned int kContextTermsWindowSize_;

  /** Maximal length of noun phrases to extract */
  const unsigned int kMaximalNounPhrasesLength_;

  /** Object for choosing best morphology variants */
  MorphologyVariantsChooser morphology_variants_chooser_;

  /** Object which extracts and processed words from collection */
  WordsProcessor words_processor_;

  /** Object for determining several predefined terms among term candidates */
  PredefinedWordsFinder predefined_words_finder_;

  /** Object for accumulating noun phrases extracted from files */
  NounPhrasesAccumulator noun_phrases_accumulator_;

  /** Object for accumulating single-word term candidates extracted from
      files */
  CandidatesAccumulator single_word_terms_accumulator_;

  /** Object for accumulating two-word term candidates extracted from files */
  TwoWordTermsAccumulator two_word_terms_accumulator_;
};
