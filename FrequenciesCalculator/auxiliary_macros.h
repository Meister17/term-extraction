// Copyright 2011 Michael Nokel
#pragma once

#include <set>
#include <string>
#include <vector>

/**
  @brief Enumerator containing all possible types of candidates for extracting

  The program can extract phrases, single-word and two-word terms
*/
enum class CandidatesType {
  NO_CANDIDATES_TYPE,
  SINGLE_WORD_TERMS,
  TWO_WORD_TERMS,
  ALL_TERMS
};

/**
  @brief All cases in Russian grammar.

  This enumeration contains nominative, genitive, dative, accusative,
  instrumental and prepositional cases (also no case is possible)
*/
enum class WordCase {
  NO_CASE,
  NOMINATIVE,
  GENITIVE,
  DATIVE,
  ACCUSATIVE,
  INSTRUMENTAL,
  PREPOSITIONAL
};

/**
  @brief All genders in Russian grammar.

  This enumeration contains masculine, feminine and neuter genders (also no
  gender and plural are possible)
*/
enum class WordGender {
  NO_GENDER,
  MASCULINE,
  FEMININE,
  NEUTER,
  PLURAL
};

/**
  @brief All types of words' categories that we consider.

  These types include nouns, adjectives, proper nouns (to exclude them from the
  consideration), agreement words (including pronouns, numerals and
  participles), prepositions and various dividers (no category is also possible)
*/
enum class WordCategory {
  NO_CATEGORY,
  NOUN,
  ADJECTIVE,
  PROPER_NOUN,
  AGREEMENT_WORD,
  PREPOSITION,
  DIVIDER
};

/**
  @brief All possible types of capitalized words

  These types include not_capitalized, capitalized and initial words
*/
enum class CapitalizedWordCategory {
  NOT_CAPITALIZED,
  CAPITALIZED_WORD,
  INITIAL_WORD
};

/**
  @brief Structure containing changeable parts of words

  This structure contains word's case and word's gender. For each word in the
  corresponding structure should be set or vector of such pairs to represent
  their structure
*/
struct ChangeableWordParts {
  ChangeableWordParts(WordCase extracted_word_case,
                      WordGender extracted_word_gender)
      : word_case(extracted_word_case),
        word_gender(extracted_word_gender) {
  }

  friend bool operator<(const ChangeableWordParts& first_word_parts,
                        const ChangeableWordParts& second_word_parts) {
    return first_word_parts.word_case < second_word_parts.word_case &&
           first_word_parts.word_gender < second_word_parts.word_gender;
  }

  /** Case of word */
  WordCase word_case = WordCase::NO_CASE;

  /** Gender of word */
  WordGender word_gender = WordGender::NO_GENDER;
};

/**
  @brief Structure containing information about extracted word

  This information contains extracted word, its category, capitalized category,
  genders, numbers, possible cases and flags indicating whether this word is
  known for morphological analyzer, is going after some punctuation divider, and
  word and token numbers in file
*/
struct WordProperties {
  WordProperties() {
  }

  WordProperties(const std::string& extracted_word,
                 const WordCategory& extracted_word_category,
                 const CapitalizedWordCategory& capitalized_category,
                 bool is_new,
                 bool is_after_punctuation_sign,
                 unsigned int word_number,
                 unsigned int token_number)
      : word(extracted_word),
        word_category(extracted_word_category),
        changeable_word_parts(),
        is_new_word(is_new),
        is_auxiliary_word(false),
        is_after_punctuation_divider(is_after_punctuation_sign),
        capitalized_word_category(capitalized_category),
        word_number_in_file(word_number),
        token_number_in_file(token_number) {
  }

  /**
    Compares two words' data only by words
    @param first_word first word to compare
    @param second_word second word to compare
    @return true if the words are equal
  */
  static bool CompareByWords(const WordProperties& first_word,
                             const WordProperties& second_word) {
    return first_word.word == second_word.word;
  }

  /** Extracted word */
  std::string word = "";

  /** Word's category */
  WordCategory word_category = WordCategory::NO_CATEGORY;

  /** Information about changeable word parts */
  std::set<ChangeableWordParts> changeable_word_parts;

  /** Flag indicating whether word is new for morphological analyzer */
  bool is_new_word = false;

  /** Flag indicating whether word is auxiliary */
  bool is_auxiliary_word = false;

  /** Flag indicating whether the word is going just after the punctuation
      divider */
  bool is_after_punctuation_divider = false;

  /** Capitalized word's category */
  CapitalizedWordCategory capitalized_word_category =
      CapitalizedWordCategory::NOT_CAPITALIZED;

  /** Number of word in file */
  unsigned int word_number_in_file = 0;

  /** Number of token in file */
  unsigned int token_number_in_file = 0;
};

/**
  @brief Structure containing data necessary for processing words extracted from
  files

  This structure contains information about whether we are waiting for first
  word, for punctuation divider and whether word, candidate or token was
  extracted
*/
struct WordProcessingData {
  /** Flag indicating whether we are waiting for first word in sentence to
      be extracted */
  bool waiting_for_first_word = true;

  /** Flag indicating whether punctuation divider was previously extracted */
  bool was_punctuation_divider = false;

  /** Flag indicating whether word was extracted */
  bool word_extracted = false;

  /** Flag indicating whether candidate was extracted */
  bool candidate_extracted = false;

  /** Flag indicating whether token was extracted */
  bool token_extracted = false;
};

/**
  @brief Enumerator containing all supported terms' categories: nouns,
  adjectives or both (no category is also possible). For two-word terms they
  are equivalent to Noun + (of)* + Noun and Adjective + Noun correspondingly.
*/
enum class TermCategory {
  NO_TERM_CATEGORY,
  NOUN,
  ADJECTIVE,
  ADJECTIVE_AND_NOUN
};

/**
  @brief Base structure containing all common data for all term candidates

  This structure contains first occurrence position, term frequency (as term,
  as capital word, as non-initial word, as subject), document frequency (as
  term, as capital word, as non-initial word, as subject), domain consensus (as
  term, as capital word, as non-initial word, as subject) and vector of
  probbilities in the documents. All structures containing special data for
  particular term candidates should be derived from this one.
*/
struct TermCandidateData {
  /**
    Initializes object
    @param word_category category of the determining word
    @param is_new_word flag indicating whether term candidate is known for
    morphological analyzer or not
    @param is_ambiguous_word flag indicating whether term candidate is ambiguous
    @param first_occurrence position of the first occurrence of term candidate
    in documents
    @param is_capital_word flag indicating whether term candidate is capital
    @param is_non_initial_word flag indicating whether term candidate is
    non-initial
    @param is_subject flag indicating whether term candidate is subject
  */
  TermCandidateData(const WordCategory& word_category,
                    bool is_new_word,
                    bool is_ambiguous_word,
                    unsigned int first_occurrence,
                    bool is_capital_word,
                    bool is_non_initial_word,
                    bool is_subject)
      : is_new_candidate(is_new_word),
        is_ambiguous_candidate(is_ambiguous_word),
        first_occurrence_position(first_occurrence) {
    if (is_capital_word) {
      term_frequency_as_capital_word = 1;
      document_frequency_as_capital_word = 1;
    }
    if (is_non_initial_word) {
      term_frequency_as_non_initial_word = 1;
      document_frequency_as_non_initial_word = 1;
    }
    if (is_subject) {
      term_frequency_as_subject = 1;
      document_frequency_as_subject = 1;
    }
    if (word_category == WordCategory::NOUN) {
      term_category = TermCategory::NOUN;
    } else if (word_category == WordCategory::ADJECTIVE) {
      term_category = TermCategory::ADJECTIVE;
    }
  }

  /** Term candidate's category */
  TermCategory term_category = TermCategory::NO_TERM_CATEGORY;

  /** Flag indicating whether term candidate is new for morphological
      analyzer */
  bool is_new_candidate = false;

  /** Flag indicating whether term candidate is ambiguous */
  bool is_ambiguous_candidate = false;

  /** Frequency of occurring near terms */
  unsigned int near_terms_frequency = 0;

  /** Position of the first occurrence */
  unsigned int first_occurrence_position = 0;

  /** Term frequency of single-word term candidate */
  unsigned int term_frequency = 1;

  /** Term frequency of single-word term candidate (only for capital words) */
  unsigned int term_frequency_as_capital_word = 0;

  /** Term frequency of single-word term candidate (only for non-initial words)
    */
  unsigned int term_frequency_as_non_initial_word = 0;

  /** Term frequency of single-word term candidate (only for subjects) */
  unsigned int term_frequency_as_subject = 0;

  /** Document frequency of single-word term candidate */
  unsigned int document_frequency = 1;

  /** Document frequency of single-word term candidate (only for capital words)
    */
  unsigned int document_frequency_as_capital_word = 0;

  /** Document frequency of single-word term candidate (only for non-initial
      words) */
  unsigned int document_frequency_as_non_initial_word = 0;

  /** Document frequency of single-word term candidate (only for subjects) */
  unsigned int document_frequency_as_subject = 0;

  /** Domain consensus of single-word term candidate */
  double domain_consensus = 0.0;

  /** Domain consensus of capital word */
  double domain_consensus_as_capital_word = 0.0;

  /** Domain consensus of non-initial word */
  double domain_consensus_as_non_initial_word = 0.0;

  /** Domain consensus of subject */
  double domain_consensus_as_subject= 0.0;

  /** Vector containing frequencies of single-word terms in documents */
  std::vector<unsigned int> term_candidate_frequencies;

  /** Vector containing sizes of documents where term candidate occurs */
  std::vector<unsigned int> document_sizes;
};

/**
  @brief Structure containing necessary data for calculating NearTermsFreq

  This structure contains word and its frequency counted in the context window
*/
struct NearTermsFrequency {
  NearTermsFrequency() {
  }

  NearTermsFrequency(const std::string& candidate, unsigned int frequency)
      : term_candidate(candidate),
        near_terms_frequency(frequency) {
  }

  /** Word */
  std::string term_candidate = "";

  /** Frequency in the context window */
  unsigned int near_terms_frequency = 0;
};

/**
  @brief Structure containing probabilistic data for term candidates

  This structure contains term frequency, domain consensus, maximum term
  frequency, term score and maximum term score for further calculating base
  lines for term scores. Also it contains term contribution, term variance,
  term variance quality and term frequency as is in BM-25
*/
struct ProbabilisticData {
  /** Term frequency */
  double term_frequency = 0.0;

  /** Domain consensus */
  double domain_consensus = 0.0;

  /** Maximum Term Frequency */
  double maximum_term_frequency = 0.0;

  /** Term Score */
  double term_score = 0.0;

  /** Maximum Term Score */
  double maximum_term_score = 0.0;

  /** Term Contribution weight */
  double term_contribution = 0.0;

  /** Term Variance */
  double term_variance = 0.0;

  /** Term Variance Quality */
  double term_variance_quality = 0.0;

  /** Term Frequency as is in BM-25 */
  double term_frequency_bm25 = 0.0;
};

/**
  @brief Structure containing necessary data about extracted noun phrases

  This structure contains frequency and length (in terms of Nouns and
  Adjectives) of the extracted noun phrase
*/
struct NounPhraseData {
  /**
    Initializes object
    @param phrase_length length of the phrase in terms of Nouns and Adjectives
  */
  explicit NounPhraseData(unsigned int phrase_length): length(phrase_length) {
  }

  unsigned int term_frequency = 1;

  /** Length of the noun phrase */
  unsigned int length;
};
