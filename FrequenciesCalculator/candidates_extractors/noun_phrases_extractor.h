// Copyright 2012 Michael Nokel
#pragma once

#include "../auxiliary_macros.h"
#include <boost/algorithm/string.hpp>
#include <string>
#include <unordered_map>
#include <vector>

/**
  @brief Class for extracting phrases from the collection

  For each thread that parses file its own instance of this class should be
  created. This instance should be used for accumulating phrases and their
  frequencies. Then accumulated information should be merged with previously
  extracted.
*/
class NounPhrasesExtractor {
 public:
  /**
    Initializes object
    @param maximum_noun_phrases_length maximum length of noun phrases to extract
  */
  NounPhrasesExtractor(unsigned int maximum_noun_phrases_length)
      : kMaximumNounPhrasesLength_(maximum_noun_phrases_length) {
  }

  /**
    Adds new term candidates and store all formed phrases
    @param new_term_candidates vector containing word properties of new
    candidates
  */
  void AddNewTermCandidates(
    const std::vector<WordProperties>& new_term_candidates);

  /**
    Returns start iterator to the map containing all extracted phrases for
    parsing file
    @return start iterator to such map
  */
  std::unordered_map<std::string, NounPhraseData>::const_iterator
      GetStartIterator() const {
    return phrases_map_.begin();
  }

  /**
    Returns end iterator to the map containing all extracted phrases for parsing
    file
    @return end iterator to such map
  */
  std::unordered_map<std::string, NounPhraseData>::const_iterator
      GetEndIterator() const {
    return phrases_map_.end();
  }

 private:
  /**
    @brief Structure containing all necessary data for noun phrases extraction

    This structure contains building noun phrase, its category and length
  */
  struct PhraseCandidateData {
    /**
      Initializes object
      @param word first word of the building phrase
    */
    PhraseCandidateData(const std::string& word)
        : noun_phrase(word) {
    }

    /** Building noun phrase */
    std::string noun_phrase = "";

    /** Length of the building noun phrase */
    unsigned int length = 1;
  };

  /**
    Tries to extend candidate for noun phrases by new word
    @param previous_candidate_data data of the previous extending noun phrase
    @param word_properties properties of the new adding word
    @param[out] new_noun_phrases vector where new formed candidates should be
    placed
    @return true if noun phrase was extended
  */
  bool TryToExtendNounPhrase(
      const PhraseCandidateData& previous_candidate_data,
      const WordProperties& word_properties,
      std::vector<PhraseCandidateData>* new_noun_phrases) const;

  /**
    Checks whether given phrase is Russian
    @param phrase phrase to check
    @return true if given phrase is Russian
  */
  bool IsRussianPhrase(const std::string& phrase) const {
    std::vector<std::string> tokens;
    boost::split(tokens, phrase, boost::is_any_of(" -"));
    for (const auto& word: tokens) {
      for (size_t index = 0; index < word.size(); index += 2) {
        if (kRussianLetters_.find(word.substr(index, 2)) == std::string::npos) {
          return false;
        }
      }
    }
    return true;
  }

  /** All possible Russian letters */
  const std::string kRussianLetters_ =
      "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";

  /** Maximum length of noun phrases for extraction */
  const unsigned int kMaximumNounPhrasesLength_;

  /** Vector containing candidates for noun phrases */
  std::vector<PhraseCandidateData> noun_phrases_;

  /** Hash map containing extracted phrases and some data about them */
  std::unordered_map<std::string, NounPhraseData> phrases_map_;
};
