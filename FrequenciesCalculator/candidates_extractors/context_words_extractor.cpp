// Copyright 2012 Michael Nokel
#include "./context_words_extractor.h"
#include "../auxiliary_macros.h"
#include "../predefined_words_finder.h"
#include <queue>
#include <string>
#include <vector>

using std::queue;
using std::string;
using std::vector;

vector<NearTermsFrequency> ContextWordsExtractor::StoreNewTermCandidates(
    const vector<string>& new_term_candidates) {
  vector<NearTermsFrequency> retrieved_term_candidates;
  if (context_terms_window_.size() > kContextTermsWindowSize_) {
    retrieved_term_candidates = EraseOldTerms();
  }
  vector<NearTermsData> new_candidates;
  bool predefined_term_inserted = false;
  for (const auto& term_candidate: new_term_candidates) {
    bool is_predefined_term =
        predefined_words_finder_->IsPredefinedTerm(term_candidate);
    new_candidates.push_back(NearTermsData(term_candidate,
                                           is_predefined_term));
    predefined_term_inserted |= is_predefined_term;
  }
  context_terms_window_.push(new_candidates);
  if (predefined_term_inserted) {
    ++predefined_terms_number_in_window_;
  }
  return retrieved_term_candidates;
}

vector<NearTermsFrequency> ContextWordsExtractor::RetrieveAllTermCandidates() {
  vector<NearTermsFrequency> retrieved_term_candidates;
  while (!context_terms_window_.empty()) {
    vector<NearTermsFrequency> new_term_candidates = EraseOldTerms();
    retrieved_term_candidates.insert(retrieved_term_candidates.end(),
                                     new_term_candidates.begin(),
                                     new_term_candidates.end());
  }
  return retrieved_term_candidates;
}

vector<NearTermsFrequency> ContextWordsExtractor::EraseOldTerms() {
  vector<NearTermsFrequency> retrieved_term_candidates;
  vector<NearTermsData> near_terms_data = context_terms_window_.front();
  context_terms_window_.pop();
  unsigned int near_term_frequency = predefined_terms_number_in_window_ +
      predefined_terms_number_in_previous_window_;
  for (const auto& term_data: near_terms_data) {
    retrieved_term_candidates.push_back(
        NearTermsFrequency(term_data.term_candidate, near_term_frequency));
  }
  if (previous_context_terms_window_.size() >= kContextTermsWindowSize_) {
    vector<NearTermsData> removing_near_terms_data =
        previous_context_terms_window_.front();
    previous_context_terms_window_.pop();
    if (HavePredefinedTerm(removing_near_terms_data)) {
      --predefined_terms_number_in_previous_window_;
    }
  }
  previous_context_terms_window_.push(near_terms_data);
  if (HavePredefinedTerm(near_terms_data)) {
    --predefined_terms_number_in_window_;
    ++predefined_terms_number_in_previous_window_;
  }
  return retrieved_term_candidates;
}

bool ContextWordsExtractor::HavePredefinedTerm(
    const vector<NearTermsData>& near_terms_data) const {
  for (const auto& term_data: near_terms_data) {
    if (term_data.is_predefined_term) {
      return true;
    }
  }
  return false;
}