// Copyright 2012 Michael Nokel
#pragma once

#include "../auxiliary_macros.h"
#include <queue>
#include <string>
#include <vector>

class PredefinedWordsFinder;

/**
  @brief Class for extracting context words for single-word term candidates

  This class implements context window for single-word term candidates. It adds
  new term candidates and retrieves term candidates that are out of the context
  window after adding terem candidates.
*/
class ContextWordsExtractor {
 public:
  /**
    Initializes object
    @param context_terms_window_size size of context window expressed in number
    of term candidates there
    @param predefined_words_finder pointer to object used for determining
    predefined terms among term candidates found in parsed texts
  */
  ContextWordsExtractor(
      unsigned int context_terms_window_size,
      PredefinedWordsFinder* predefined_words_finder)
      : kContextTermsWindowSize_(context_terms_window_size),
        predefined_words_finder_(predefined_words_finder) {
  }

  /**
    Stores new term candidates and retrieves term candidates that are out of
    context window after adding given term candidates
    @param new_term_candidates vector containing term candidates that should be
    added to the context window
    @return vector containing term candidates that were removed from the context
    window after adding given term candidates
  */
  std::vector<NearTermsFrequency> StoreNewTermCandidates(
      const std::vector<std::string>& new_term_candidates);

  /**
    Last function to call after processing file has ended. This function
    retrieves all remaining term candidates in the context window
    @return vector containing term candidates that will be removed from the
    context window after calling this function
  */
  std::vector<NearTermsFrequency> RetrieveAllTermCandidates();

 private:
  /**
    @brief Structure containing information about context words probably near
    several predefined terms

    This structure contains word, its NearTermsFreq in the context window, and
    flag indicating whether current word is the predefined term
  */
  struct NearTermsData {
    NearTermsData(const std::string& word, bool is_predefined)
        : term_candidate(word),
          is_predefined_term(is_predefined) {
    }

    /** Term candidate */
    std::string term_candidate = "";

    /** Flag indicating whether word is the predefined term */
    bool is_predefined_term = false;
  };

  /**
    Erases old term candidates from the context window
    @param predefined_term_inserted flag indicating whether predefined term was
    just inserted
  */
  std::vector<NearTermsFrequency> EraseOldTerms();

  /**
    Checks whether given vector contains at least one predefined term
    @param near_terms_data vector for checking
    @return true if there is at least one predefined term in the given vector
  */
  bool HavePredefinedTerm(const std::vector<NearTermsData>& near_terms_data)
      const;

  /** Size of context window expressed in terms */
  const unsigned int kContextTermsWindowSize_;

  /** Object used for determining predefined terms among term candidates */
  PredefinedWordsFinder* predefined_words_finder_;

  /** Queue containing term candidates in the current context window */
  std::queue<std::vector<NearTermsData> > context_terms_window_;

  /** Queue containing term candidate in the previous context window */
  std::queue<std::vector<NearTermsData> > previous_context_terms_window_;

  /** Number of predefined terms in the current context window */
  unsigned int predefined_terms_number_in_window_ = 0;

  /** Number of predefined terms in the previous context window */
  unsigned int predefined_terms_number_in_previous_window_ = 0;
};
