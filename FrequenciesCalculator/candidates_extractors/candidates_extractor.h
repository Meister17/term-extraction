// Copyright 2012 Michael Nokel
#pragma once

#include "./context_words_extractor.h"
#include "../auxiliary_macros.h"
#include <boost/algorithm/string.hpp>
#include <string>
#include <unordered_map>
#include <vector>

/**
  @brief Abstract class for extracting various term candidates

  This class should be derived by each class intended to extract various term
  candidates depending on their length. For each thread that parses file its own
  instance of this class should be created. This instance should be used for
  accumulating candidates and necessary information about them. Then accumulated
  information should be merged with previously extracted ones.
*/
class CandidatesExtractor {
 public:
  /**
    Initializes object
    @param context_terms_window_size size of context terms window
    @param[out] predefined_words_finder pointer to the object for finding
    predefined terms among words in texts
  */
  CandidatesExtractor(
      unsigned int context_terms_window_size,
      PredefinedWordsFinder* predefined_words_finder)
      : context_words_extractor_(context_terms_window_size,
                                 predefined_words_finder) {
  }

  /**
    Adds new term candidates
    @param new_term_candidates vector containing word properties of new term
    candidates
  */
  virtual void AddNewTermCandidates(
      const std::vector<WordProperties>& new_term_candidates) = 0;

  /**
    Processes last term candidates and calculates various domain consensuses for
    them
  */
  virtual void ProcessLastTermCandidates();

  /**
    Returns start iterator to the map containing all extracted candidates from
    parsing file
    @return start iterator to such map
  */
  virtual std::unordered_map<std::string, TermCandidateData>::const_iterator
      GetStartIterator() const {
    return term_candidates_map_.begin();
  }

  /**
    Returns end iterator to the map containing all extracted candidates from
    parsing file
    @return end iterator to such map
  */
  virtual std::unordered_map<std::string, TermCandidateData>::const_iterator
      GetEndIterator() const {
    return term_candidates_map_.end();
  }

  /**
    Virtual destructor for the purposes of proper deriving
  */
  virtual ~CandidatesExtractor() {
  }

 protected:
  /**
    Inserts new term candidate and necessary data about it
    @param term_candidate term candidate to insert
    @param new_term_data new data for term candidate
  */
  virtual void InsertNewTermCandidate(const std::string& term_candidate,
                                      const TermCandidateData& new_term_data);

  /**
    Calculates part of the domain consensus for term
    @param number_of_words_in_file total number of words in file
    @param term_frequency term frequency in file for given term
    @return part of the domain consensus for given term
  */
  virtual double CalculateDomainConsensusPart(
      unsigned int number_of_words_in_file,
      unsigned int term_frequency) const;

  /**
    Adds given term candidates to the context window
    @param term_candidates term candidates for adding
  */
  virtual void AddToContextWindow(
      const std::vector<std::string>& term_candidates);

  /**
    Checks whether given phrase is Russian or not
    @param phrase phrase to check
    @return true if phrase is Russian
  */
  virtual bool IsRussianPhrase(const std::string& phrase) const {
    std::vector<std::string> tokens;
    boost::split(tokens, phrase, boost::is_any_of("- "));
    for (const auto& word: tokens) {
      for (size_t index = 0; index < word.size(); index += 2) {
        if (kRussianLetters_.find(word.substr(index, 2)) == std::string::npos) {
          return false;
        }
      }
    }
    return true;
  }

  /** All possible Russian letters */
  const std::string kRussianLetters_ =
      "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";

  /** Object used for calculating NearTermsFreq values */
  ContextWordsExtractor context_words_extractor_;

  /** Hash map containing term candidates and some data about them */
  std::unordered_map<std::string, TermCandidateData> term_candidates_map_;
};
