// Copyright 2012 Michael Nokel
#include "./two_word_terms_extractor.h"
#include "../auxiliary_macros.h"
#include <set>
#include <string>
#include <vector>

using std::set;
using std::string;
using std::vector;

void TwoWordTermsExtractor::AddNewTermCandidates(
    const vector<WordProperties>& new_term_candidates) {
  vector<WordProperties> new_words;
  vector<string> context_words;
  for (const auto& new_candidate: new_term_candidates) {
    if (new_candidate.word_category == WordCategory::NOUN) {
      new_words.push_back(new_candidate);
      if (!new_candidate.is_after_punctuation_divider &&
          !previous_words_.empty() && new_candidate.token_number_in_file -
          previous_words_.front().token_number_in_file == 1) {
        bool is_in_genitive = IsInCase(new_candidate.changeable_word_parts,
                                       WordCase::GENITIVE);
        for (const auto& previous_word: previous_words_) {
          if ((previous_word.word_category == WordCategory::ADJECTIVE) ||
              (previous_word.word_category == WordCategory::NOUN &&
               is_in_genitive)) {
            string term_candidate = previous_word.word + " " +
                new_candidate.word;
            if (IsRussianPhrase(term_candidate)) {
              bool is_new_candidate =
                  previous_word.is_new_word || new_candidate.is_new_word;
              WordProperties main_noun;
              if (previous_word.word_category == WordCategory::ADJECTIVE) {
                main_noun = new_candidate;
              } else {
                main_noun = previous_word;
              }
              bool is_capital_word =
                  previous_word.capitalized_word_category ==
                      CapitalizedWordCategory::CAPITALIZED_WORD ||
                  new_candidate.capitalized_word_category ==
                      CapitalizedWordCategory::CAPITALIZED_WORD ||
                  previous_word.capitalized_word_category ==
                      CapitalizedWordCategory::INITIAL_WORD ||
                  previous_word.capitalized_word_category ==
                      CapitalizedWordCategory::INITIAL_WORD;
              bool is_non_initial_word =
                  previous_word.capitalized_word_category ==
                      CapitalizedWordCategory::INITIAL_WORD ||
                  previous_word.capitalized_word_category ==
                      CapitalizedWordCategory::INITIAL_WORD;
              bool is_subject = IsInCase(main_noun.changeable_word_parts,
                                         WordCase::NOMINATIVE);
              TermCandidateData term_candidate_data(
                  previous_word.word_category,
                  is_new_candidate,
                  was_ambiguous_word_,
                  previous_word.word_number_in_file,
                  is_capital_word,
                  is_non_initial_word,
                  is_subject);
              InsertNewTermCandidate(term_candidate, term_candidate_data);
              context_words.push_back(term_candidate);
            }
          }
        }
      }
    } else if (new_candidate.word_category == WordCategory::ADJECTIVE) {
      new_words.push_back(new_candidate);
    }
  }
  if (!context_words.empty()) {
    AddToContextWindow(context_words);
  }
  previous_words_ = new_words;
  was_ambiguous_word_ = new_term_candidates.size() > 1;
}

bool TwoWordTermsExtractor::IsInCase(const set<ChangeableWordParts>& word_parts,
                                     const WordCase& word_case) const {
  for (const auto& word_part: word_parts) {
    if (word_part.word_case == word_case) {
      return true;
    }
  }
  return false;
}