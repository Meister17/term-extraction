// Copyright 2012 Michael Nokel
#pragma once

#include "./candidates_extractor.h"
#include "../auxiliary_macros.h"
#include <set>
#include <vector>

class PredefinedWordsFinder;

/**
  @brief Class for extracting two-word term candidates

  For each thread that parses file its own instance of this class should be
  created. This instance should be used for accumulating two-word term
  candidates and necessary information about them. Then accumulated information
  should be merged with previously extracted.
*/
class TwoWordTermsExtractor : public CandidatesExtractor {
 public:
  /**
    Initializes object
    @param context_terms_window_size size of context terms window
    @param[out] predefined_words_finder pointer to the object for finding
    predefined terms among words in texts
  */
  TwoWordTermsExtractor(
      unsigned int context_terms_window_size,
      PredefinedWordsFinder* predefined_words_finder)
      : CandidatesExtractor(context_terms_window_size,
                            predefined_words_finder) {
  }

  /**
    Adds new term candidates
    @param new_term_candidates vector containing word properties of new term
    candidates
  */
  void AddNewTermCandidates(
      const std::vector<WordProperties>& new_term_candidates);

 private:
  /**
    Checks whether given word is in the given case
    @param word_parts changeable word parts of the word to check
    @param word_case word's case to check
    @return true if given word is in the given case
  */
  bool IsInCase(const std::set<ChangeableWordParts>& word_parts,
                const WordCase& word_case) const;

  /** Flag indicating whether previous word was ambiguous */
  bool was_ambiguous_word_ = false;

  /** Previous words for forming two-word term candidate */
  std::vector<WordProperties> previous_words_;
};
