// Copyright 2012 Michael Nokel
#include "./noun_phrases_extractor.h"
#include "../auxiliary_macros.h"
#include <string>
#include <vector>

using std::string;
using std::vector;

void NounPhrasesExtractor::AddNewTermCandidates(
    const vector<WordProperties>& new_term_candidates) {
  vector<PhraseCandidateData> new_phrases;
  for (const auto& new_candidate: new_term_candidates) {
    if (new_candidate.word_category == WordCategory::ADJECTIVE) {
      if (!new_candidate.is_after_punctuation_divider) {
        for (const auto& phrase: noun_phrases_) {
          if (phrase.length < kMaximumNounPhrasesLength_) {
            TryToExtendNounPhrase(phrase, new_candidate, &new_phrases);
          }
        }
      }
      new_phrases.push_back(PhraseCandidateData(new_candidate.word));
    } else if (new_candidate.word_category == WordCategory::NOUN) {
      if (!new_candidate.is_after_punctuation_divider) {
        for (const auto& phrase: noun_phrases_) {
          if (phrase.length < kMaximumNounPhrasesLength_ &&
              TryToExtendNounPhrase(phrase, new_candidate, &new_phrases)) {
            auto inserted_result = phrases_map_.insert({
                new_phrases.back().noun_phrase,
                NounPhraseData(new_phrases.back().length)});
            if (!inserted_result.second) {
              ++inserted_result.first->second.term_frequency;
            }
          }
        }
      }
      new_phrases.push_back(PhraseCandidateData(new_candidate.word));
    }
  }
  noun_phrases_ = new_phrases;
}

bool NounPhrasesExtractor::TryToExtendNounPhrase(
    const PhraseCandidateData& previous_candidate_data,
    const WordProperties& word_properties,
    vector<PhraseCandidateData>* new_noun_phrases) const {
  if (IsRussianPhrase(previous_candidate_data.noun_phrase + " " +
                      word_properties.word)) {
    new_noun_phrases->push_back(previous_candidate_data);
    new_noun_phrases->back().noun_phrase += " " + word_properties.word;
    ++new_noun_phrases->back().length;
    return true;
  }
  return false;
}
