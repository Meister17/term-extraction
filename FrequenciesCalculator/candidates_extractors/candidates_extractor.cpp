// Copyright 2013 Michael Nokel
#include "./candidates_extractor.h"
#include <cmath>
#include <string>
#include <vector>

using std::string;
using std::vector;

void CandidatesExtractor::ProcessLastTermCandidates() {
  vector<NearTermsFrequency> retrieved_term_candidates =
      context_words_extractor_.RetrieveAllTermCandidates();
  for (const auto& near_terms_frequency: retrieved_term_candidates) {
    string term_candidate = near_terms_frequency.term_candidate;
    term_candidates_map_.find(term_candidate)->second.near_terms_frequency +=
        near_terms_frequency.near_terms_frequency;
  }
  unsigned int number_of_words_in_file = 0;
  for (const auto& element: term_candidates_map_) {
    number_of_words_in_file += element.second.term_frequency;
  }
  for (auto& element: term_candidates_map_) {
    element.second.term_candidate_frequencies.push_back(
        element.second.term_frequency);
    element.second.document_sizes.push_back(number_of_words_in_file);
    element.second.domain_consensus += CalculateDomainConsensusPart(
        element.second.term_frequency,
        number_of_words_in_file);
    element.second.domain_consensus_as_capital_word +=
        CalculateDomainConsensusPart(
            element.second.term_frequency_as_capital_word,
            number_of_words_in_file);
    element.second.domain_consensus_as_non_initial_word +=
        CalculateDomainConsensusPart(
            element.second.term_frequency_as_non_initial_word,
            number_of_words_in_file);
    element.second.domain_consensus_as_subject +=
        CalculateDomainConsensusPart(
            element.second.term_frequency_as_subject,
            number_of_words_in_file);
  }
}

void CandidatesExtractor::InsertNewTermCandidate(
    const string& term_candidate,
    const TermCandidateData& term_candidate_data) {
  auto inserted_result = term_candidates_map_.insert({term_candidate,
                                                      term_candidate_data});
  if (!inserted_result.second) {
    if (inserted_result.first->second.term_category !=
        term_candidate_data.term_category) {
      inserted_result.first->second.term_category =
          TermCategory::ADJECTIVE_AND_NOUN;
    }
    inserted_result.first->second.is_new_candidate |=
        term_candidate_data.is_new_candidate;
    inserted_result.first->second.is_ambiguous_candidate |=
        term_candidate_data.is_ambiguous_candidate;
    inserted_result.first->second.term_frequency +=
        term_candidate_data.term_frequency;
    inserted_result.first->second.term_frequency_as_capital_word +=
        term_candidate_data.term_frequency_as_capital_word;
    inserted_result.first->second.term_frequency_as_non_initial_word +=
        term_candidate_data.term_frequency_as_non_initial_word;
    inserted_result.first->second.term_frequency_as_subject +=
        term_candidate_data.term_frequency_as_subject;
    inserted_result.first->second.document_frequency |=
        term_candidate_data.document_frequency;
    inserted_result.first->second.document_frequency_as_capital_word |=
        term_candidate_data.document_frequency_as_capital_word;
    inserted_result.first->second.document_frequency_as_non_initial_word |=
        term_candidate_data.document_frequency_as_non_initial_word;
    inserted_result.first->second.document_frequency_as_subject |=
        term_candidate_data.document_frequency_as_subject;
  }
}

double CandidatesExtractor::CalculateDomainConsensusPart(
    unsigned int term_frequency,
    unsigned int number_of_words_in_file) const {
  if (term_frequency > 0) {
    double normalized_term_frequency = static_cast<double>(term_frequency) /
        static_cast<double>(number_of_words_in_file);
    return -normalized_term_frequency * log(normalized_term_frequency);
  }
  return 0.0;
}

void CandidatesExtractor::AddToContextWindow(
    const vector<string>& term_candidates) {
  vector<NearTermsFrequency> near_terms_frequencies =
      context_words_extractor_.StoreNewTermCandidates(term_candidates);
  for (const auto& near_terms_frequency: near_terms_frequencies) {
    string word = near_terms_frequency.term_candidate;
    term_candidates_map_.find(word)->second.near_terms_frequency +=
        near_terms_frequency.near_terms_frequency;
  }
}
