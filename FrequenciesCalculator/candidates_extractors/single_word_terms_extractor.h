// Copyright 2012 Michael Nokel
#pragma once

#include "./candidates_extractor.h"
#include "../auxiliary_macros.h"
#include <vector>

class PredefinedWordsFinder;

/**
  @brief Class for extracting single-word term candidates

  For each thread that parses file its own instance of this class should be
  created. This instance should be used for accumulating single-word term
  candidates and necessary information about them. Then accumulated information
  should be merged with previously extracted.
*/
class SingleWordTermsExtractor : public CandidatesExtractor {
 public:
  /**
    Initializes object
    @param context_terms_window_size size of context window expressed in terms
    @param[out] predefined_words_finder pointer to object that should be used
    for determining predefined terms among all term candidates
  */
  SingleWordTermsExtractor(
      unsigned int context_terms_window_size,
      PredefinedWordsFinder* predefined_words_finder)
      : CandidatesExtractor(context_terms_window_size,
                            predefined_words_finder) {
  }

  /**
    Adds new term candidates
    @param new_term_candidates vector containing word properties of new term
    candidates
  */
  void AddNewTermCandidates(
      const std::vector<WordProperties>& new_term_candidates);
};
