// Copyright 2012 Michael Nokel
#include "./words_processor.h"
#include "./auxiliary_macros.h"
#include <set>
#include <stdexcept>
#include <string>
#include <vector>

using std::runtime_error;
using std::set;
using std::string;
using std::vector;

void WordsProcessor::ExtractWordFromLine(
    const vector<string>& tokens,
    WordProcessingData* word_processing_data,
    bool& is_token,
    bool& is_word,
    WordProperties* word_properties) const {
  if (tokens.size() < 4) {
    throw runtime_error("Failed to parse file");
  }
  if (tokens.size() == 4) {
    return;
  }
  is_word = false;
  is_token = true;
  if (tokens[4] == "ЦК") {
    ProcessNumber(word_processing_data, word_properties);
  } else if (tokens[4] == "ЗПР" || tokens[4] == "РЗД" || tokens[4] == "КОД" ||
             tokens[4] == "НЧТ") {
    is_token = false;
    ProcessSign(tokens, word_processing_data, word_properties);
  } else if (tokens[4] == "ЛЕ") {
    ProcessLemma(tokens, is_word, word_processing_data, word_properties);
  } else {
    word_properties->word_category = WordCategory::NO_CATEGORY;
  }
}

void WordsProcessor::AddNewWords(const vector<WordProperties>& same_word_forms,
                                 bool& continue_processing,
                                 vector<WordProperties>* previous_words,
                                 vector<WordProperties>* words_to_store) const {
  if (same_word_forms.size() == 1 &&
      same_word_forms.front().word_category == WordCategory::DIVIDER) {
    continue_processing = true;
    return;
  }
  continue_processing = false;
  WordProperties cases_word_properties;
  cases_word_properties.word_category = WordCategory::PREPOSITION;
  vector<WordProperties> nouns_adjectives_words, other_words;
  vector<WordProperties> nouns_adjectives_to_add, other_words_to_add;
  vector<WordProperties> words_without_agreement;
  bool agreement = false;
  bool store_everything_to_map = false;
  for (const auto& word_form: same_word_forms) {
    if (word_form.word_category == WordCategory::DIVIDER) {
      continue;
    } else if (word_form.word_category == WordCategory::NO_CATEGORY) {
      store_everything_to_map = true;
    } else if (word_form.word_category == WordCategory::PREPOSITION) {
      store_everything_to_map = true;
      cases_word_properties.word = word_form.word;
      cases_word_properties.word_number_in_file = word_form.word_number_in_file;
      cases_word_properties.token_number_in_file =
          word_form.token_number_in_file;
      prepositions_processor_.StoreCases(
          word_form.word,
          &cases_word_properties.changeable_word_parts);
    } else {
      vector<WordProperties>* vector_for_storing;
      vector<WordProperties>* vector_to_add;
      WordProperties current_word_properties = word_form;
      if (current_word_properties.word_category == WordCategory::NOUN ||
          current_word_properties.word_category == WordCategory::ADJECTIVE) {
        vector_for_storing = &nouns_adjectives_words;
        vector_to_add = &nouns_adjectives_to_add;
      } else {
        vector_for_storing = &other_words;
        vector_to_add = &other_words_to_add;
        agreement = true;
        words_without_agreement.push_back(current_word_properties);
      }
      if (TryToAddWord(*previous_words,
                       &current_word_properties,
                       vector_for_storing)) {
        agreement = true;
        vector_to_add->push_back(current_word_properties);
      }
    }
  }
  if (store_everything_to_map) {
    *words_to_store = *previous_words;
    previous_words->clear();
    if (!cases_word_properties.changeable_word_parts.empty()) {
      previous_words->push_back(cases_word_properties);
    }
  } else if (agreement) {
    if (!other_words_to_add.empty()) {
      *words_to_store = other_words;
      previous_words->clear();
      *previous_words = other_words_to_add;
    } else if (!words_without_agreement.empty()) {
      *words_to_store = *previous_words;
      previous_words->clear();
      *previous_words = words_without_agreement;
    } else {
      *words_to_store = nouns_adjectives_words;
      previous_words->clear();
      *previous_words = nouns_adjectives_to_add;
    }
  } else {
    *words_to_store = *previous_words;
    previous_words->clear();
    *previous_words = same_word_forms;
  }
}

void WordsProcessor::ProcessNumber(WordProcessingData* word_processing_data,
                                   WordProperties* word_properties) const {
  word_processing_data->was_punctuation_divider = false;
  word_processing_data->waiting_for_first_word = false;
  word_properties->word_category = WordCategory::DIVIDER;
}

void WordsProcessor::ProcessSign(const vector<string>& tokens,
                                 WordProcessingData* word_processing_data,
                                 WordProperties* word_properties) const {
  word_properties->word = tokens[0];
  bool end_of_sentence_found = false;
  bool punctuation_divider_found = false;
  bool hyphen_found = false;
  for (size_t index = 5; index < tokens.size(); ++index) {
    if (tokens[index] == "КПР") {
      end_of_sentence_found = true;
    } else if (tokens[index] == "ЗПР") {
      punctuation_divider_found = true;
    } else if (tokens[index] == "ДЕФ") {
      hyphen_found = true;
    }
  }
  if (hyphen_found) {
    word_properties->word_category = WordCategory::DIVIDER;
    word_processing_data->was_punctuation_divider = true;
  } else if (tokens[4] == "ЗПР" && tokens[0] == ",") {
    word_properties->word_category = WordCategory::DIVIDER;
    word_processing_data->was_punctuation_divider = true;
    word_processing_data->waiting_for_first_word = false;
  } else if (tokens[4] == "ЗПР" || end_of_sentence_found) {
    word_properties->word_category = WordCategory::NO_CATEGORY;
    word_processing_data->was_punctuation_divider = true;
    word_processing_data->waiting_for_first_word = true;
  } else {
    word_properties->word_category = WordCategory::DIVIDER;
    if (punctuation_divider_found) {
      word_processing_data->was_punctuation_divider = true;
    }
  }
}

void WordsProcessor::ProcessLemma(const vector<string>& tokens,
                                  bool& is_word,
                                  WordProcessingData* word_processing_data,
                                  WordProperties* word_properties) const {
  word_properties->word_category = WordCategory::NO_CATEGORY;
  is_word = true;
  if (word_processing_data->waiting_for_first_word) {
    word_properties->capitalized_word_category =
        CapitalizedWordCategory::INITIAL_WORD;
  }
  word_properties->is_after_punctuation_divider =
      word_processing_data->was_punctuation_divider;
  if (tokens.size() == 5) {
    return;
  }
  if (tokens[5] != "бб") {
    if (word_properties->capitalized_word_category ==
        CapitalizedWordCategory::NOT_CAPITALIZED) {
      word_properties->capitalized_word_category =
          CapitalizedWordCategory::CAPITALIZED_WORD;
    }
  }
  size_t position;
  for (position = 5; tokens[position] != "+" && tokens[position] != "-" &&
       tokens[position] != "="; ++position) {
    if (tokens[position] == "n") {
      word_properties->word_category = WordCategory::PROPER_NOUN;
    }
  }
  word_properties->is_new_word = tokens[position] == "-";
  word_properties->is_auxiliary_word = tokens[position] == "=";
  word_properties->word = tokens[position + 1];
  if (position + 2 < tokens.size()) {
    string morph_token = tokens[position + 2];
    string part_of_speech = morph_token.substr(0, 2);
    if (string("абвгдежзи").find(part_of_speech) != string::npos) {
      nouns_processor_.ProcessWord(morph_token, word_properties);
    } else if (part_of_speech == "й") {
      adjectives_processor_.ProcessWord(morph_token, word_properties);
    } else if (string("чшщы").find(part_of_speech) != string::npos) {
      pronouns_processor_.ProcessWord(morph_token, word_properties);
    } else if (string("эю").find(part_of_speech) != string::npos) {
      numerals_processor_.ProcessWord(morph_token, word_properties);
    } else if (string("лмопрсухц").find(part_of_speech) != string::npos) {
      participles_processor_.ProcessWord(morph_token, word_properties);
    } else if (part_of_speech == "я") {
      part_of_speech = morph_token.substr(2, 2);
      if (part_of_speech == "в") {
        word_properties->word_category = WordCategory::PREPOSITION;
      } else {
        word_properties->word_category = WordCategory::DIVIDER;
      }
    }
  }
  if (word_properties->is_auxiliary_word &&
      (word_properties->word_category == WordCategory::NOUN ||
       word_properties->word_category == WordCategory::PROPER_NOUN ||
       word_properties->word_category == WordCategory::ADJECTIVE)) {
    word_properties->word_category = WordCategory::NO_CATEGORY;
  }
}

bool WordsProcessor::TryToAddWord(
    const vector<WordProperties>& previous_words,
    WordProperties* word_properties,
    vector<WordProperties>* agreement_words) const {
  if (previous_words.empty()) {
    return true;
  }
  WordProperties returning_word(word_properties->word,
                                word_properties->word_category,
                                word_properties->capitalized_word_category,
                                word_properties->is_new_word,
                                word_properties->is_after_punctuation_divider,
                                word_properties->word_number_in_file,
                                word_properties->token_number_in_file);
  for (const auto& previous_word: previous_words) {
    set<ChangeableWordParts> agreement_word_parts;
    for (const auto& new_word_part: word_properties->changeable_word_parts) {
      if (AgreementInGenitive(new_word_part, previous_word)) {
        SaveAgreementWordParts(new_word_part,
                               previous_word.changeable_word_parts,
                               &returning_word.changeable_word_parts);
        SaveAgreementWord(previous_word,
                          previous_word.changeable_word_parts,
                          agreement_words);
      } else if (previous_word.word_category != WordCategory::NOUN ||
                 word_properties->word_category != WordCategory::NOUN) {
        for (const auto& previous_word_part:
             previous_word.changeable_word_parts) {
          if (AgreementInCase(new_word_part, previous_word_part) &&
              AgreementInGender(new_word_part, previous_word_part)) {
            ChangeableWordParts word_parts(new_word_part.word_case,
                                           new_word_part.word_gender);
            if (word_parts.word_case == WordCase::NO_CASE) {
              word_parts.word_case = previous_word_part.word_case;
            }
            if (word_parts.word_gender == WordGender::NO_GENDER) {
              word_parts.word_gender = previous_word_part.word_gender;
            }
            returning_word.changeable_word_parts.insert(word_parts);
            agreement_word_parts.insert(word_parts);
          }
        }
      }
    }
    if (!agreement_word_parts.empty()) {
      SaveAgreementWord(previous_word, agreement_word_parts, agreement_words);
    }
  }
  if (!returning_word.changeable_word_parts.empty()) {
    *word_properties = returning_word;
    return true;
  }
  return false;
}

void WordsProcessor::SaveAgreementWordParts(
    const ChangeableWordParts& new_word_part,
    const set<ChangeableWordParts>& previous_word_parts,
    set<ChangeableWordParts>* returning_word_parts) const {
  ChangeableWordParts word_parts(new_word_part.word_case,
                                 new_word_part.word_gender);
  if (word_parts.word_case != WordCase::NO_CASE &&
      word_parts.word_gender != WordGender::NO_GENDER) {
    returning_word_parts->insert(word_parts);
  }
  for (const auto& previous_word_part: previous_word_parts) {
    if (word_parts.word_case == WordCase::NO_CASE &&
        previous_word_part.word_case != WordCase::NO_CASE) {
      word_parts.word_case = previous_word_part.word_case;
      if (word_parts.word_gender != WordGender::NO_GENDER) {
        returning_word_parts->insert(word_parts);
      }
    }
    if (word_parts.word_gender == WordGender::NO_GENDER &&
        previous_word_part.word_gender != WordGender::NO_GENDER) {
      word_parts.word_gender = previous_word_part.word_gender;
      if (word_parts.word_case != WordCase::NO_CASE) {
        returning_word_parts->insert(word_parts);
      }
    }
  }
}

void WordsProcessor::SaveAgreementWord(
    const WordProperties& word_properties,
    const set<ChangeableWordParts>& agreement_word_parts,
    vector<WordProperties>* agreement_words) const {
  size_t index = 0;
  while (index < agreement_words->size()) {
    if (agreement_words->at(index).word == word_properties.word &&
        agreement_words->at(index).word_category ==
        word_properties.word_category) {
      break;
    }
    ++index;
  }
  if (index == agreement_words->size()) {
    WordProperties agreement_word_properties(
        word_properties.word,
        word_properties.word_category,
        word_properties.capitalized_word_category,
        word_properties.is_new_word,
        word_properties.is_after_punctuation_divider,
        word_properties.word_number_in_file,
        word_properties.token_number_in_file);
    agreement_word_properties.changeable_word_parts = agreement_word_parts;
    agreement_words->push_back(agreement_word_properties);
  } else {
    for (const auto& word_part: agreement_word_parts) {
      agreement_words->at(index).changeable_word_parts.insert(word_part);
    }
  }
}
