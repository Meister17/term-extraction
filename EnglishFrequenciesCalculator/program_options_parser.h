// Copyright 2013 Michael Nokel
#pragma once

#include "./auxiliary_macros.h"
#include <string>

/**
  @brief Class for parsing and working with program options obtained from
  command line and/or from configuration file

  This class should be used for parsing program options from command line and/or
  from configuration file, checking their correctness, after what one can access
  any of the presented program options.
*/
class ProgramOptionsParser {
 public:
  /**
    Parses program options from command line and/or from configuration file
    @param argc number of program options (from main function)
    @param argv array containing program options (from main function)
    @throw std::runtime_error in case of any occurred errors
  */
  void Parse(int argc, char** argv);

  CandidatesType candidates_type() const {
    return candidates_type_;
  }

  bool help_message_printed() const {
    return help_message_printed_;
  }

  unsigned int minimum_term_frequency() const {
    return minimum_term_frequency_;
  }

  unsigned int context_terms_window_size() const {
    return context_terms_window_size_;
  }

  std::string predefined_terms_filename() const {
    return predefined_terms_filename_;
  }

  std::string stop_words_filename() const {
    return stop_words_filename_;
  }

  std::string source_directory_name() const {
    return source_directory_name_;
  }

  std::string single_words_filename() const {
    return single_words_filename_;
  }

  std::string single_word_terms_filename() const {
    return single_word_terms_filename_;
  }

  std::string single_word_terms_lda_input_filename() const {
    return single_word_terms_lda_input_filename_;
  }

  std::string single_word_terms_lda_vocabulary_filename() const {
    return single_word_terms_lda_vocabulary_filename_;
  }

  std::string two_word_terms_filename() const {
    return two_word_terms_filename_;
  }

  std::string two_word_terms_lda_input_filename() const {
    return two_word_terms_lda_input_filename_;
  }

  std::string two_word_terms_lda_vocabulary_filename() const {
    return two_word_terms_lda_vocabulary_filename_;
  }

  unsigned int maximum_noun_phrases_length() const {
    return maximum_noun_phrases_length_;
  }

  std::string single_word_phrases_filename() const {
    return single_word_phrases_filename_;
  }

  std::string two_word_phrases_filename() const {
    return two_word_phrases_filename_;
  }

 private:
  /**
    Determines candidates type based on the given program options
    @return proper candidates type
    @see CandidatesType
  */
  CandidatesType DetermineCandidatesType() const;

  /** Types of candidates to extract */
  CandidatesType candidates_type_ = CandidatesType::NO_CANDIDATES_TYPE;

  /** Flag indicating whether help message was printed */
  bool help_message_printed_ = false;

  /** Minimum frequency of term candidates to be taken into account */
  unsigned int minimum_term_frequency_ = 0;

  /** Size of context terms' window (for counting terms in the window of
      predefined terms) */
  unsigned int context_terms_window_size_ = 0;

  /** File containing several predefined terms */
  std::string predefined_terms_filename_ = "";

  /** File containing stop words that should be excluded from the extracting
      words */
  std::string stop_words_filename_ = "";

  /** Source directory for scanning */
  std::string source_directory_name_ = "";

  /** File for placing there single words and their term frequencies for
      two-word term candidates */
  std::string single_words_filename_ = "";

  /** File for placing there single-word term candidates and some data about
      them */
  std::string single_word_terms_filename_ = "";

  /** File for placing there input data for LDA/clustering for single-word term
      candidates */
  std::string single_word_terms_lda_input_filename_ = "";

  /** File for LDA vocabulary for single-word term candidates */
  std::string single_word_terms_lda_vocabulary_filename_ = "";

  /** File for placing there two-word term candidates and some data about
      them */
  std::string two_word_terms_filename_ = "";

  /** File for placing there input file for LDA/clustering for two-word term
      candidates */
  std::string two_word_terms_lda_input_filename_ = "";

  /** File for LDA vocabulary for two-word term candidates */
  std::string two_word_terms_lda_vocabulary_filename_ = "";

  /** Maximum length of noun phrases for extraction */
  unsigned int maximum_noun_phrases_length_ = 3;

  /** File for found noun phrases for single-word term candidates */
  std::string single_word_phrases_filename_ = "";

  /** File for found noun phrases for two-word term candidates */
  std::string two_word_phrases_filename_ = "";
};
