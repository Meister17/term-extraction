// Copyright 2013 Michael Nokel
#include "./frequencies_calculator.h"
#include "./auxiliary_macros.h"
#include "./candidates_extractors/noun_phrases_extractor.h"
#include "./candidates_extractors/single_words_extractor.h"
#include "./candidates_extractors/single_word_terms_extractor.h"
#include "./candidates_extractors/two_word_terms_extractor.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/threadpool.hpp>
#include <algorithm>
#include <cctype>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

using boost::filesystem::directory_iterator;
using boost::filesystem::is_regular_file;
using boost::is_any_of;
using boost::split;
using boost::threadpool::pool;
using std::cout;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::string;
using std::vector;

void FrequenciesCalculator::Initialize(const string& predefined_terms_filename,
                                       const string& stop_words_filename) {
  cout << "Initializing\n";
  predefined_terms_finder_.ParseFile(predefined_terms_filename);
  stop_words_finder_.ParseFile(stop_words_filename);
}

void FrequenciesCalculator::CalculateFrequencies(
    const string& source_directory_name) {
  cout << "Extracting term candidates\n";
  vector<string> files;
  for (directory_iterator iterator(source_directory_name); iterator !=
       directory_iterator(); ++iterator) {
    if (is_regular_file(iterator->status())) {
      files.push_back(iterator->path().string());
    }
  }
  sort(files.begin(), files.end());
  cout << files.size() << " found\n";
  if (kCandidatesType_ == CandidatesType::SINGLE_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    single_word_terms_accumulator_.ReserveForFiles(files.size());
  }
  if (kCandidatesType_ == CandidatesType::TWO_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    two_word_terms_accumulator_.ReserveForFiles(files.size());
  }
  pool thread_pool(kNumberOfThreads_);
  for (size_t file_number = 0; file_number < files.size(); ++file_number) {
    thread_pool.schedule(boost::bind(&FrequenciesCalculator::ParseFile,
                                     this,
                                     files[file_number],
                                     file_number));
  }
}

void FrequenciesCalculator::PrintTermCandidates(
    const string& single_word_terms_filename,
    const string& single_word_terms_lda_input_filename,
    const string& single_word_terms_lda_vocabulary_filename,
    const string& single_word_phrases_filename,
    const string& single_words_filename,
    const string& two_word_terms_filename,
    const string& two_word_terms_lda_input_filename,
    const string& two_word_terms_lda_vocabulary_filename,
    const string& two_word_phrases_filename) {
  cout << "Printing extracted term candidates\n";
  pool thread_pool(kNumberOfThreads_);
  if (kCandidatesType_ == CandidatesType::SINGLE_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    thread_pool.schedule(boost::bind(
        &CandidatesAccumulator::PrintTermCandidates,
        &single_word_terms_accumulator_,
        single_word_terms_filename,
        kMinimumTermFrequency_));
    thread_pool.schedule(boost::bind(
        &CandidatesAccumulator::CreateLDAFiles,
        &single_word_terms_accumulator_,
        single_word_terms_lda_input_filename,
        single_word_terms_lda_vocabulary_filename,
        kMinimumTermFrequency_));
    thread_pool.schedule(boost::bind(
        &NounPhrasesAccumulator::PrintSingleWordNounPhrases,
        &noun_phrases_accumulator_,
        single_word_phrases_filename,
        kMinimumTermFrequency_));
  }
  if (kCandidatesType_ == CandidatesType::TWO_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    thread_pool.schedule(boost::bind(
        &TwoWordTermsAccumulator::PrintTermCandidates,
        &two_word_terms_accumulator_,
        two_word_terms_filename,
        kMinimumTermFrequency_));
    thread_pool.schedule(boost::bind(
        &TwoWordTermsAccumulator::CreateLDAFiles,
        &two_word_terms_accumulator_,
        two_word_terms_lda_input_filename,
        two_word_terms_lda_vocabulary_filename,
        kMinimumTermFrequency_));
    thread_pool.schedule(boost::bind(
        &TwoWordTermsAccumulator::PrintSingleWords,
        &two_word_terms_accumulator_,
        single_words_filename,
        kMinimumTermFrequency_));
    thread_pool.schedule(boost::bind(
        &NounPhrasesAccumulator::PrintTwoWordNounPhrases,
        &noun_phrases_accumulator_,
        two_word_phrases_filename,
        kMinimumTermFrequency_));
  }
}

void FrequenciesCalculator::ParseFile(const string& filename,
                                      size_t file_number) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse file");
  }
  SingleWordTermsExtractor single_word_terms_extractor(
      kContextTermsWindowSize_,
      &predefined_terms_finder_);
  TwoWordTermsExtractor two_word_terms_extractor(
      kContextTermsWindowSize_,
      &predefined_terms_finder_);
  SingleWordsExtractor single_words_extractor;
  NounPhrasesExtractor noun_phrases_extractor(kMaximumNounPhrasesLength_);
  WordProcessingData word_processing_data;
  unsigned int token_number_in_file = 0;
  unsigned int word_number_in_file = 0;
  while (!file.eof()) {
    string line;
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (line.empty()) {
      continue;
    }
    vector<string> tokens;
    split(tokens, line, is_any_of("\t"));
    if (tokens.size() < 3) {
      continue;
    }
    WordProperties word_properties;
    word_properties.token_number_in_file = token_number_in_file;
    word_properties.word_number_in_file = word_number_in_file;
    if (isupper(tokens[0][0]) || word_processing_data.was_end_of_sentence) {
      word_properties.is_capital_word = true;
      word_properties.is_non_initial_word =
          !word_processing_data.was_end_of_sentence;
    }
    word_processing_data.word_extracted = true;
    for (const auto letter: tokens[1]) {
      if (!isalpha(letter)) {
        word_processing_data.word_extracted = false;
        break;
      }
      word_properties.word.push_back(tolower(letter));
    }
    if (word_processing_data.word_extracted &&
        word_properties.word.size() <= 1) {
      word_processing_data.word_extracted = false;
    }
    string term_category = tokens[2];
    DetermineTermCategory(word_properties.word,
                          term_category,
                          word_properties.word_category,
                          &word_processing_data);
    if (word_processing_data.word_extracted) {
      ++word_number_in_file;
      ++token_number_in_file;
    } else if (word_processing_data.token_extracted) {
      ++token_number_in_file;
    }
    word_properties.is_after_punctuation_divider =
        word_processing_data.was_punctuation_divider;
    word_properties.is_after_preposition = word_processing_data.was_preposition;
    char is_novel, is_ambiguous;
		if (tokens.size() < 4) {
			is_novel = true;
		} else {
      is_novel = tokens[3][0];
		}
		if (tokens.size() < 5) {
			is_ambiguous = true;
		} else {
			is_ambiguous = tokens[4][0];
		}
    word_properties.is_novel_word = is_novel == '1';
    word_properties.is_ambiguous_word = is_ambiguous == '1';
    if (kCandidatesType_ == CandidatesType::SINGLE_WORD_TERMS ||
        kCandidatesType_ == CandidatesType::ALL_TERMS) {
      single_word_terms_extractor.AddNewWord(word_properties);
    }
    if (kCandidatesType_ == CandidatesType::TWO_WORD_TERMS ||
        kCandidatesType_ == CandidatesType::ALL_TERMS) {
      two_word_terms_extractor.AddNewWord(word_properties);
      single_words_extractor.AddNewWord(word_properties);
    }
    noun_phrases_extractor.AddNewWord(word_properties);
  }
  file.close();
  if (kCandidatesType_ == CandidatesType::SINGLE_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    single_word_terms_extractor.ProcessLastWords();
  }
  if (kCandidatesType_== CandidatesType::TWO_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    two_word_terms_extractor.ProcessLastWords();
  }
  AccumulateDataFromFile(single_words_extractor,
                         single_word_terms_extractor,
                         two_word_terms_extractor,
                         noun_phrases_extractor,
                         file_number);
}

void FrequenciesCalculator::DetermineTermCategory(
    const string& word,
    const string& term_category,
    WordCategory& word_category,
    WordProcessingData* word_processing_data) const {
  word_processing_data->was_preposition =
      word_processing_data->was_punctuation_divider =
      word_processing_data->was_end_of_sentence = false;
  word_processing_data->token_extracted = true;
  if (word_processing_data->word_extracted &&
      (term_category == "NN" || term_category == "NNS" ||
       term_category == "NNP" || term_category == "NNPS") &&
      (word == "eu" || (word.size() > 2 &&
                        !stop_words_finder_.IsPredefined(word)))) {
    word_category = WordCategory::NOUN;
  } else if (word_processing_data->word_extracted && term_category == "JJ" &&
             (word == "eu" ||
              (word.size() > 2 && !stop_words_finder_.IsPredefined(word)))) {
    word_category = WordCategory::ADJECTIVE;
  } else if (word_processing_data->word_extracted && term_category == "IN") {
    if (word == "of") {
      word_processing_data->word_extracted =
          word_processing_data->token_extracted = false;
    }
    word_category = WordCategory::PREPOSITION;
    word_processing_data->was_preposition = true;
  } else if (word_processing_data->word_extracted &&
             (term_category == "MD" || term_category == "VB" ||
              term_category == "VBD" || term_category == "VBG" ||
              term_category == "VBN" || term_category == "VBP" ||
              term_category == "VBZ")) {
    word_category = WordCategory::VERB;
  } else if (term_category == "DT" &&
             ((!word_processing_data->word_extracted && word == "a") ||
              (word_processing_data->word_extracted && (word == "an" ||
                                                        word == "the")))) {
    word_processing_data->word_extracted =
        word_processing_data->token_extracted = false;
    word_category = WordCategory::DETERMINER;
  } else if (term_category == "." || term_category == "-LRB-" ||
             term_category == "-RRB-" || term_category == "``" ||
             term_category == "''" || term_category == "-LCB-" ||
             term_category == "-RCB-") {
    word_category = WordCategory::END_OF_SENTENCE;
    word_processing_data->token_extracted = false;
    word_processing_data->was_end_of_sentence =
        word_processing_data->was_punctuation_divider = true;
  } else if (term_category == "," || term_category == ":" ||
             term_category == "$" || term_category == "#") {
    word_category = WordCategory::OTHER_CATEGORY;
    word_processing_data->token_extracted = false;
    word_processing_data->was_punctuation_divider = true;
  } else {
    word_category = WordCategory::OTHER_CATEGORY;
  }
}

void FrequenciesCalculator::AccumulateDataFromFile(
    const SingleWordsExtractor& single_words_extractor,
    const SingleWordTermsExtractor& single_word_terms_extractor,
    const TwoWordTermsExtractor& two_word_terms_extractor,
    const NounPhrasesExtractor& noun_phrases_extractor,
    size_t file_number) {
  if (kCandidatesType_ == CandidatesType::SINGLE_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    single_word_terms_accumulator_.Accumulate(
        single_word_terms_extractor.GetStartIterator(),
        single_word_terms_extractor.GetEndIterator(),
        file_number);
  }
  if (kCandidatesType_ == CandidatesType::TWO_WORD_TERMS ||
      kCandidatesType_ == CandidatesType::ALL_TERMS) {
    two_word_terms_accumulator_.Accumulate(
        two_word_terms_extractor.GetStartIterator(),
        two_word_terms_extractor.GetEndIterator(),
        file_number);
    two_word_terms_accumulator_.AccumulateSingleWords(
        single_words_extractor.GetStartIterator(),
        single_words_extractor.GetEndIterator());
  }
  noun_phrases_accumulator_.Accumulate(
      noun_phrases_extractor.GetStartIterator(),
      noun_phrases_extractor.GetEndIterator());
}
