// Copyright 2013 Michael Nokel
#pragma once

#include <string>
#include <unordered_set>

/**
  @brief Class intended to be used for finding several predefined words in texts
  from collection

  This class parses file with several predefined words. AFter what is should be
  used for checking whether the given word is among such ones.
*/
class PredefinedWordsFinder {
 public:
  /**
    Parses the file containing several predefined words
    @param filename name of file containing several predefined words
    @throw std::runtime_error in case of any occurred error
  */
  void ParseFile(const std::string& filename);

  /**
    Checks whether the given word is among predefined ones or not
    @param word word to check
    @return true if the given word is a predefined word
  */
  bool IsPredefined(const std::string& word) const {
    return predefined_words_set_.find(word) != predefined_words_set_.end();
  }

 private:
  /** Hash set containing several predefined words */
  std::unordered_set<std::string> predefined_words_set_;
};
