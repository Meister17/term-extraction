// Copyright 2013 Michael Nokel
#pragma once

#include <string>
#include <vector>

/**
  @brief Enumerator containing all possible types of candidates to extract

  This enumerator contains single-word and two-word term candidates
*/
enum class CandidatesType {
  NO_CANDIDATES_TYPE,
  SINGLE_WORD_TERMS,
  TWO_WORD_TERMS,
  ALL_TERMS
};

/**
  @brief Enumerator containing all possible word's categories

  This enumerator contains no word's category, noun, adjective, preposition,
  determiner, verb and marker of end of sentence and other categories
*/
enum class WordCategory {
  NO_WORD_CATEGORY,
  NOUN,
  ADJECTIVE,
  PREPOSITION,
  DETERMINER,
  VERB,
  END_OF_SENTENCE,
  OTHER_CATEGORY
};

/**
  @brief Enumerator containing all supported terms' categories

  This enumerator contains no word's category, adjective, noun and both
*/
enum class TermCategory {
  NO_TERM_CATEGORY,
  ADJECTIVE,
  NOUN,
  ADJECTIVE_AND_NOUN
};

/**
  @brief Structure containing data necessary for processing words extracted from
  files

  This structure contains information about whether end of sentence, punctuation
  divider, preposition was met, and whether word and token was extracted
*/
struct WordProcessingData {
  /** Flag indicating whether the end of sentence was met */
  bool was_end_of_sentence = true;

  /** Flag indicating whether the punctuation divider was met */
  bool was_punctuation_divider = false;

  /** Flag indicating whether the preposition was met */
  bool was_preposition = false;

  /** Flag indicating whether the word was extracted */
  bool word_extracted = false;

  /** Flag indicating whether the token was extracted */
  bool token_extracted = false;
};

/**
  @brief Base structure containing all common data for all term candidates

  This structure contains flags indicating whether word is novel or not, whether
  it is ambiguous or not, first occurrence position, term frequency (as term,
  as capital word, as non-initial word, as subject), document frequency (as
  term, as capital word, as non-initial word, as subject), domain consensus (as
  term, as capital word, as non-initial word, as subject) and terms'
  probabilities
*/
struct TermCandidateData {
  /**
    Initializes structure
    @param word_category category of the determining word
    @param first_occurrence position of the first occurrence
    @param is_capital_word flag indicating whether the word is capital
    @param is_non_initial_word flag indicating whether the word is non-initial
    @param is_novel_word flag indicating whether the word is novel
    @param is_ambiguous_word flag indicating whether the word is ambiguous
  */
  TermCandidateData(const WordCategory& word_category,
                    unsigned int first_occurrence,
                    bool is_capital_word,
                    bool is_non_initial_word,
                    bool is_novel_word,
                    bool is_ambiguous_word)
      : is_novel_candidate(is_novel_word),
        is_ambiguous_candidate(is_ambiguous_word),
        first_occurrence_position(first_occurrence) {
    if (is_capital_word) {
      ++term_frequency_as_capital_word;
      ++document_frequency_as_capital_word;
    }
    if (is_non_initial_word) {
      ++term_frequency_as_non_initial_word;
      ++document_frequency_as_non_initial_word;
    }
    if (word_category == WordCategory::NOUN) {
      term_category = TermCategory::NOUN;
    } else if (word_category == WordCategory::ADJECTIVE) {
      term_category = TermCategory::ADJECTIVE;
    }
  }

  /** Category of the term candidate */
  TermCategory term_category = TermCategory::NO_TERM_CATEGORY;

  /** Flag indicating whether candidate is novel or not */
  bool is_novel_candidate = false;

  /** Flag indicating whether candidate is ambiguous or not */
  bool is_ambiguous_candidate = false;

  /** Position of first occurrence of the term candidate */
  unsigned int first_occurrence_position = 0;

  /** NearTermsFreq value */
  unsigned int near_terms_frequency = 0;

  /** Term frequency of the term candidate */
  unsigned int term_frequency = 1;

  /** Term frequency of the term candidate (as capital word) */
  unsigned int term_frequency_as_capital_word = 0;

  /** Term frequency of the term candidate (as non-initial word) */
  unsigned int term_frequency_as_non_initial_word = 0;

  /** Term frequency of the term candidate (as subject) */
  unsigned int term_frequency_as_subject = 0;

  /** Document frequency of the term candidate */
  unsigned int document_frequency = 1;

  /** Document frequency of the term candidate (as capital word) */
  unsigned int document_frequency_as_capital_word = 0;

  /** Document frequency of the term candidate (as non-initial word) */
  unsigned int document_frequency_as_non_initial_word = 0;

  /** Document frequency of the term candidate (as subject) */
  unsigned int document_frequency_as_subject = 0;

  /** Domain consensus of the term candidate */
  double domain_consensus = 0.0;

  /** Domain consensus of the term candidate (as capital word) */
  double domain_consensus_as_capital_word = 0.0;

  /** Domain consensus of the term candidate (as non-initial word) */
  double domain_consensus_as_non_initial_word = 0.0;

  /** Domain consensus of the term candidate (as subject) */
  double domain_consensus_as_subject = 0.0;

  /** Vector containing term candidates' frequencies */
  std::vector<unsigned int> term_candidate_frequencies;

  /** Vector containing sizes of documents where term candidate occurs */
  std::vector<unsigned int> document_sizes;
};

/**
  @brief Structure containing probabilistic data for term candidates

  This structure contains probabilistic term frequency, maximum term frequency,
  term score and maximum term score, term contribution, term variation, term
  variation quality and term frequency as is in BM-25
*/
struct ProbabilisticData {
  /** Probabilistic term frequency */
  double term_frequency = 0.0;

  /** Maximum probabilistic term frequency */
  double maximum_term_frequency = 0.0;

  /** Probabilistic term score */
  double term_score = 0.0;

  /** Maximum probabilistic term score */
  double maximum_term_score = 0.0;

  /** Term Contribution weight */
  double term_contribution = 0.0;

  /** Term Variance */
  double term_variance = 0.0;

  /** Term Variance Quality */
  double term_variance_quality = 0.0;

  /** Term Frequency as is in BM-25 */
  double term_frequency_bm25 = 0.0;
};

/**
  @brief Structure containing properties of parsed word

  This structure contains extracted word, its category, its number in file and
  flags indicating whether it is a capital word or not, whether it is a
  non-initial word or not, whether is is going right after preposition or
  punctuation divider or not, whether word is novel or not, whether word is
  ambiguous or not
*/
struct WordProperties {
  /** Extracted word */
  std::string word = "";

  /** Category of extracted word */
  WordCategory word_category = WordCategory::NO_WORD_CATEGORY;

  /** Number of token in parsing file */
  unsigned int token_number_in_file = 0;

  /** Number of word in parsing file */
  unsigned int word_number_in_file = 0;

  /** Flag indicating whether word is capital */
  bool is_capital_word = false;

  /** Flag indicating whether word is non-initial */
  bool is_non_initial_word = false;

  /** Flag indicating whether word is going right after punctuation divider */
  bool is_after_punctuation_divider = false;

  /** Flag indicating whether word is going right after preposition */
  bool is_after_preposition = false;

  /** Flag indicating whether word is novel or not */
  bool is_novel_word = false;

  /** Flag indicating whether word is ambiguous or not */
  bool is_ambiguous_word = false;
};

/**
  @brief Structure containing necessary data for calculating NearTermsFreq
  feature

  This structure contains term candidate and its frequency counted in the
  context window
*/
struct NearTermsFrequency {
  /** Term candidate */
  std::string term_candidate = "";

  /** NearTermsFreq value of the term candidate */
  unsigned int near_terms_frequency = 0;
};

/**
  @brief Structure containing necessary data about extracted noun phrases

  This structure contains frequency and length (in terms of Nouns and
  Adjectives) of the extracted noun phrase
*/
struct NounPhraseData {
  /**
    Initializes object
    @param phrase_length length of the phrase in terms of Nouns and Adjectives
  */
  explicit NounPhraseData(unsigned int phrase_length): length(phrase_length) {
  }

  unsigned int term_frequency = 1;

  /** Length of the noun phrase */
  unsigned int length;
};

/**
  @brief Structure containing context data about words and bigrams

  This structure contains word, its number in file.
*/
struct ContextData {
  ContextData(const std::string& extracted_word,
              size_t token_number)
      : word(extracted_word),
        token_number_in_file(token_number) {
  }

  /** Extracted word */
  std::string word;

  /** Token's number in file */
  size_t token_number_in_file;
};