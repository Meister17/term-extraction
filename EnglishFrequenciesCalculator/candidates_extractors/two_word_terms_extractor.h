// Copyright 2013 Michael Nokel
#pragma once

#include "./candidates_extractor.h"
#include "../auxiliary_macros.h"

class PredefinedWordsFinder;

/**
  @brief Class for extracting two-word term candidates from the parsing file

  This class is derived from the base abstract class and should be used for
  extracting two-word term candidates from the currently parsing file. For each
  thread that parses file its own instance of this class should be created. This
  instance should be used for accumulating two-word term candidates and
  necessary information about them. Then accumulated information should be
  merged with the previously extracted one from other threads and files.
*/
class TwoWordTermsExtractor : public CandidatesExtractor {
 public:
  /**
    Initializes object
    @param context_terms_window_size size of context terms window
    @param[out] predefined_terms_finder pointer to the object for finding
    predefined terms among words in texts
  */
  TwoWordTermsExtractor(
      unsigned int context_terms_window_size,
      PredefinedWordsFinder* predefined_terms_finder)
      : CandidatesExtractor(context_terms_window_size,
                            predefined_terms_finder) {
  }

  /**
    Adds new extracted word
    @param word_properties properties of the new extracted word
  */
  void AddNewWord(const WordProperties& word_properties);

 private:
  /** Flag indicating whether there was a preposition before subject */
  bool was_preposition_before_subject_ = false;

  /** Previous word properties (for creating two-word term candidates) */
  WordProperties previous_word_properties_;
};
