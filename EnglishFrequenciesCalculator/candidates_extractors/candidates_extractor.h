// Copyright 2013 Michael Nokel
#pragma once

#include "./context_words_extractor.h"
#include "../auxiliary_macros.h"
#include <unordered_map>
#include <string>

class PredefinedTermsExtractor;

/**
  @brief Abstract class for extracting various term candidates

  This class should be derived by each class intended to extract various term
  candidates depending on their length. For each thread that parses file its own
  instance of this class should be created. This instance should be used for
  accumulating term candidates and necessary information about them. Then
  accumulated information should be merged with previously extracted ones.
*/
class CandidatesExtractor {
 public:
  /**
    Initializes object
    @param context_terms_window_size size of context terms window
    @param[out] predefined_terms_finder pointer to the object for finding
    predefined terms among words in texts
  */
  CandidatesExtractor(
      unsigned int context_terms_window_size,
      PredefinedWordsFinder* predefined_terms_finder)
      : context_words_extractor_(context_terms_window_size,
                                 predefined_terms_finder) {
  }

  /**
    Adds new extracted word
    @param word_properties properties of the extracted word
  */
  virtual void AddNewWord(const WordProperties& word_properties) = 0;

  /**
    Processes last words and calculates various domain consensuses
  */
  virtual void ProcessLastWords();

  /**
    Returns iterator to the beginning of the map containing all extracted term
    candidates from parsing file
    @return iterator to the beginning of such map
  */
  virtual std::unordered_map<std::string, TermCandidateData>::const_iterator
      GetStartIterator() const {
    return term_candidates_map_.begin();
  }

  /**
    Returns iterator to the end of the map containing all extracted term
    candidates from parsing file
    @return iterator to the end of such map
  */
  virtual std::unordered_map<std::string, TermCandidateData>::const_iterator
      GetEndIterator() const {
    return term_candidates_map_.end();
  }

  /**
    Virtual destructor for the purposes of proper deriving
  */
  virtual ~CandidatesExtractor() {
  }

 protected:
  /**
    Inserts new term candidate and necessary information about it
    @param term_candidate term candidate to insert
    @param term_candidate_data some data about inserting term candidate
  */
  virtual void InsertNewTermCandidate(
      const std::string& term_candidate,
      const TermCandidateData& term_candidate_data);

  /**
    Calculates part of domain consensus for the given term candidate for the
    parsed file
    @param term_frequency term frequency of the given term candidate
    @param number_of_words_in_file total number of words in the parsed file
    @return calculated part of domain consensus for the given term candidate for
    the parsed file
  */
  virtual double CalculateDomainConsensusPart(
      unsigned int term_frequency,
      unsigned int number_of_words_in_file) const;

  /**
    Adds given term candidate to the context window
    @param term_candidate term candidate for adding
  */
  virtual void AddToContextWindow(const std::string& term_candidate);

  /**
    Stores new subject in a map of term candidates (increases term and document
    frequencies of the given word as subject)
    @param term_candidate term candidate considered as new subject
  */
  virtual void StoreNewSubject(const std::string& term_candidate);

  /** Object used for calculating NearTermsFreq values */
  ContextWordsExtractor context_words_extractor_;

  /** Candidate for being subject (necessary for finding subjects) */
  std::string subject_candidate_;

  /** Hash map containing extracted term candidates and some data about them */
  std::unordered_map<std::string, TermCandidateData> term_candidates_map_;
};
