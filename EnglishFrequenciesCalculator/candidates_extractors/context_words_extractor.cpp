// Copyright 2013 Michael Nokel
#include "./context_words_extractor.h"
#include "../auxiliary_macros.h"
#include "../predefined_words_finder.h"
#include <queue>
#include <iostream>
#include <string>

using std::queue;
using std::string;

NearTermsFrequency ContextWordsExtractor::StoreNewTermCandidate(
    const string& term_candidate) {
  NearTermsFrequency near_terms_frequency;
  NearTermsData new_near_terms_data(
      term_candidate,
      predefined_terms_finder_->IsPredefined(term_candidate));
  if (context_terms_window_.size() > kContextTermsWindowSize_) {
    near_terms_frequency = EraseOldTermInWindow();
  }
  context_terms_window_.push(new_near_terms_data);
  if (new_near_terms_data.is_predefined_term) {
    ++predefined_terms_number_in_window_;
  }
  return near_terms_frequency;
}

queue<NearTermsFrequency> ContextWordsExtractor::RetrieveAllTermCandidates() {
  queue<NearTermsFrequency> retrieved_term_candidates;
  while (!context_terms_window_.empty()) {
    retrieved_term_candidates.push(EraseOldTermInWindow());
  }
  return retrieved_term_candidates;
}

NearTermsFrequency ContextWordsExtractor::EraseOldTermInWindow() {
  NearTermsFrequency near_terms_frequency;
  NearTermsData near_terms_data = context_terms_window_.front();
  context_terms_window_.pop();
  near_terms_frequency.term_candidate = near_terms_data.term_candidate;
  near_terms_frequency.near_terms_frequency =
      predefined_terms_number_in_window_ +
      predefined_terms_number_in_previous_window_;
  if (previous_context_terms_window_.size() >= kContextTermsWindowSize_) {
    NearTermsData removing_near_terms_data =
        previous_context_terms_window_.front();
    previous_context_terms_window_.pop();
    if (removing_near_terms_data.is_predefined_term) {
      --predefined_terms_number_in_previous_window_;
    }
  }
  previous_context_terms_window_.push(near_terms_data);
  if (near_terms_data.is_predefined_term) {
    --predefined_terms_number_in_window_;
    ++predefined_terms_number_in_previous_window_;
  }
  return near_terms_frequency;
}


