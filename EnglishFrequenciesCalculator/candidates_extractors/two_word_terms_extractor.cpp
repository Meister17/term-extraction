// Copyright 2013 Michael Nokel
#include "./two_word_terms_extractor.h"
#include "../auxiliary_macros.h"
#include <string>

using std::string;

void TwoWordTermsExtractor::AddNewWord(const WordProperties& word_properties) {
  bool new_candidate_stored = false;
  if (word_properties.word_category == WordCategory::NOUN) {
    if (!previous_word_properties_.word.empty() &&
        !word_properties.is_after_punctuation_divider &&
        word_properties.token_number_in_file -
        previous_word_properties_.token_number_in_file == 1) {
      string term_candidate = previous_word_properties_.word + " " +
          word_properties.word;
      WordCategory main_word_category = previous_word_properties_.word_category;
      if (main_word_category == WordCategory::PREPOSITION) {
        main_word_category = WordCategory::NOUN;
      }
      TermCandidateData term_candidate_data(
          main_word_category,
          previous_word_properties_.word_number_in_file,
          previous_word_properties_.is_capital_word ||
              word_properties.is_capital_word,
          previous_word_properties_.is_non_initial_word ||
              word_properties.is_non_initial_word,
          previous_word_properties_.is_novel_word ||
              word_properties.is_novel_word,
          previous_word_properties_.is_ambiguous_word ||
              word_properties.is_ambiguous_word);
      InsertNewTermCandidate(term_candidate, term_candidate_data);
      AddToContextWindow(term_candidate);
      if (!was_preposition_before_subject_) {
        subject_candidate_ = term_candidate;
      }
      new_candidate_stored = true;
    } else {
      was_preposition_before_subject_ = word_properties.is_after_preposition;
    }
    previous_word_properties_ = word_properties;
  } else if (word_properties.word_category == WordCategory::ADJECTIVE) {
    was_preposition_before_subject_ = word_properties.is_after_preposition;
    previous_word_properties_ = word_properties;
  } else if (word_properties.word_category == WordCategory::PREPOSITION &&
             word_properties.word == "of") {
    if (!previous_word_properties_.word.empty() &&
        previous_word_properties_.word_category == WordCategory::NOUN &&
        !word_properties.is_after_punctuation_divider &&
        word_properties.token_number_in_file -
        previous_word_properties_.token_number_in_file == 1) {
      previous_word_properties_.word += " " + word_properties.word;
      previous_word_properties_.word_category = word_properties.word_category;
    } else {
      was_preposition_before_subject_ = word_properties.is_after_preposition;
      previous_word_properties_ = WordProperties();
    }
  } else if (word_properties.word_category == WordCategory::VERB &&
             !subject_candidate_.empty()) {
    previous_word_properties_ = WordProperties();
    StoreNewSubject(subject_candidate_);
  } else if (!previous_word_properties_.word.empty() &&
             (word_properties.word_category != WordCategory::DETERMINER ||
              previous_word_properties_.word_category !=
              WordCategory::PREPOSITION ||
              word_properties.is_after_punctuation_divider ||
              word_properties.token_number_in_file -
              previous_word_properties_.token_number_in_file > 1)) {
    previous_word_properties_ = WordProperties();
  }
  if (!new_candidate_stored && !subject_candidate_.empty()) {
    subject_candidate_.clear();
  }
}
