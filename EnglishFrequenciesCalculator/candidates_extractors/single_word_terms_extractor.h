// Copyright 2013 Michael Nokel
#pragma once

#include "./candidates_extractor.h"
#include "../auxiliary_macros.h"

class PredefinedWordsFinder;

/**
  @brief Class for extracting single-word term candidates from the parsing file

  This class is derived from the base abstract class and should be used for
  extracting single-word term candidates from the currently parsing file. For
  each thread that parses file its own instance of this class should be created.
  This instance should be used for accumulating single-word term candidates and
  necessary information about them. Then accumulated information should be
  merged with the previously extracted one from other threads and files.
*/
class SingleWordTermsExtractor : public CandidatesExtractor {
 public:
  /**
    Initializes object
    @param context_terms_window_size size of context terms window
    @param[out] predefined_terms_finder pointer to the object for finding
    predefined terms among words in texts
  */
  SingleWordTermsExtractor(
      unsigned int context_terms_window_size,
      PredefinedWordsFinder* predefined_terms_finder)
      : CandidatesExtractor(context_terms_window_size,
                            predefined_terms_finder) {
  }

  /**
    Adds new extracted word
    @param word_properties properties of new extracted word
  */
  void AddNewWord(const WordProperties& word_properties);
};
