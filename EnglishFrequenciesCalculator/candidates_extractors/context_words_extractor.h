// Copyright 2013 Michael Nokel
#pragma once

#include "../auxiliary_macros.h"
#include <queue>
#include <string>

class PredefinedWordsFinder;

/**
  @brief Class for extracting context words for term candidates

  This class implements context window for various term candidates. It adds new
  term candidates and retrieves term candidates that are out of the context
  window after adding new term candidates.
*/
class ContextWordsExtractor {
 public:
  /**
    Initializes object
    @param context_terms_window_size size of the context terms' window
    @param[out] predefined_terms_finder pointer to the object for finding
    predefined terms among words in texts
  */
  ContextWordsExtractor(
      unsigned int context_terms_window_size,
      PredefinedWordsFinder* predefined_terms_finder)
      : kContextTermsWindowSize_(context_terms_window_size),
        predefined_terms_finder_(predefined_terms_finder) {
  }

  /**
    Stores new term candidate in the context window and returns the word that is
    out of the context window (if any)
    @param term_candidate new term candidate to store in the context window
    @return term candidate that is out of the context window and its
    NearTermsFreq feature value
  */
  NearTermsFrequency StoreNewTermCandidate(const std::string& term_candidate);

  /**
    Retrieves queue with all term candidates that are in the context window for
    the moment. This function should be called only when the whole file has been
    successfully parsed
    @return queue containing term candidates and their NearTermsFrequency value
  */
  std::queue<NearTermsFrequency> RetrieveAllTermCandidates();

 private:
  /**
    @brief Structure necessary for calculating NearTermsFreq values of term
    candidates

    This structure contains term candidate and flag indicating whether the term
    candidate is among predefined terms or not
  */
  struct NearTermsData {
    NearTermsData(const std::string& word, bool is_predefined)
        : term_candidate(word),
          is_predefined_term(is_predefined) {
    }

    /** Term candidate */
    std::string term_candidate = "";

    /** Flag indicating whether term candidate is predefined term or not */
    bool is_predefined_term = false;
  };

  /**
    Erases old term candidate in the context window
    @return NearTermsFreq value of the erased term candidate
  */
  NearTermsFrequency EraseOldTermInWindow();

  /** Size of context terms' window */
  const unsigned int kContextTermsWindowSize_;

  /** Pointer to the object for finding predefined terms among words in texts */
  PredefinedWordsFinder* predefined_terms_finder_;

  /** Queue containing term candidates in the context window */
  std::queue<NearTermsData> context_terms_window_;

  /** Queue containing term candidates in the previous context window */
  std::queue<NearTermsData> previous_context_terms_window_;

  /** Number of predefined terms in the context window */
  unsigned int predefined_terms_number_in_window_ = 0;

  /** Number of predefined terms in the previous context window */
  unsigned int predefined_terms_number_in_previous_window_ = 0;
};
