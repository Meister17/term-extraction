// Copyright 2013 Michael Nokel
#include "./noun_phrases_extractor.h"
#include "../auxiliary_macros.h"
#include <string>
#include <unordered_map>
#include <vector>

using std::string;
using std::unordered_map;
using std::vector;

void NounPhrasesExtractor::AddNewWord(const WordProperties& word_properties) {
  vector<PhraseCandidateData> new_phrases;
  if (word_properties.word_category == WordCategory::ADJECTIVE) {
    for (const auto& phrase: noun_phrases_) {
      if (phrase.length < kMaximumNounPhrasesLength_ &&
          (phrase.word_category == WordCategory::ADJECTIVE ||
           phrase.word_category == WordCategory::NOUN ||
           phrase.word_category == WordCategory::PREPOSITION)) {
        ExtendNounPhrases(phrase, word_properties, &new_phrases);
      }
    }
    new_phrases.push_back(PhraseCandidateData(word_properties.word,
                                              word_properties.word_category));
  } else if (word_properties.word_category == WordCategory::NOUN) {
    for (const auto& phrase: noun_phrases_) {
      if (phrase.length < kMaximumNounPhrasesLength_) {
        ExtendNounPhrases(phrase, word_properties, &new_phrases);
        // insert new found noun phrase to the map
        auto inserted_result = noun_phrases_map_.insert(
            {new_phrases.back().noun_phrase,
             NounPhraseData(new_phrases.back().length)});
        if (!inserted_result.second) {
          ++inserted_result.first->second.term_frequency;
        }
      }
    }
    new_phrases.push_back(PhraseCandidateData(word_properties.word,
                                              word_properties.word_category));
  } else if (word_properties.word_category == WordCategory::PREPOSITION &&
             word_properties.word == "of") {
    for (const auto& phrase: noun_phrases_) {
      if (phrase.length < kMaximumNounPhrasesLength_ &&
          (phrase.word_category == WordCategory::ADJECTIVE ||
           phrase.word_category == WordCategory::NOUN)) {
        ExtendNounPhrases(phrase, word_properties, &new_phrases);
      }
    }
  } else if (word_properties.word_category == WordCategory::DETERMINER) {
    for (const auto& phrase: noun_phrases_) {
      if (phrase.length < kMaximumNounPhrasesLength_ &&
          (phrase.word_category == WordCategory::PREPOSITION)) {
        new_phrases.push_back(phrase);
      }
    }
  }
  noun_phrases_ = new_phrases;
}

void NounPhrasesExtractor::ExtendNounPhrases(
    const PhraseCandidateData& previous_candidate_data,
    const WordProperties& word_properties,
    vector<PhraseCandidateData>* new_noun_phrases) const {
  new_noun_phrases->push_back(previous_candidate_data);
  new_noun_phrases->back().noun_phrase += " " + word_properties.word;
  new_noun_phrases->back().word_category = word_properties.word_category;
  if (word_properties.word_category == WordCategory::NOUN ||
      word_properties.word_category == WordCategory::ADJECTIVE) {
    ++new_noun_phrases->back().length;
  }
}
