// Copyright 2013 Michael Nokel
#pragma once

#include "../auxiliary_macros.h"
#include <string>
#include <unordered_map>
#include <vector>

/**
  @brief Class for extracting multi-word noun phrases from texts

  This class should be used for extracting multi-word noun phrases from parsing
  texts. It accepts only those candidates that satisfy the following pattern:
  ((Adj|Noun)+((Noun-Prep|DT|to|Conj)*(Adj|Noun)*)*Noun and have length of more
  than one word.
*/
class NounPhrasesExtractor {
 public:
  /**
    Initializes object
    @param maximum_noun_phrases_length maximum length of noun phrases for
    extraction
  */
  explicit NounPhrasesExtractor(unsigned int maximum_noun_phrases_length)
      : kMaximumNounPhrasesLength_(maximum_noun_phrases_length) {
  }

  /**
    Adds new word to the set of noun phrases candidates and tries to form all
    possible noun phrases
    @param word_properties properties of the adding word
  */
  void AddNewWord(const WordProperties& word_properties);

  /**
    Returns iterator to the beginning of the map containing extracted noun
    phrases and some data about them
    @return iterator to the beginning of such map
  */
  std::unordered_map<std::string, NounPhraseData>::const_iterator
      GetStartIterator() const {
    return noun_phrases_map_.begin();
  }

  /**
    Returns iterator to the end of the map containing extracted noun phrases and
    some data about them
    @return iterator to the end of such map
  */
  std::unordered_map<std::string, NounPhraseData>::const_iterator
      GetEndIterator() const {
    return noun_phrases_map_.end();
  }

 private:
  /**
    @brief Structure containing all necessary data for noun phrases extraction

    This structure contains building noun phrase, its category and length
  */
  struct PhraseCandidateData {
    /**
      Initializes object
      @param word first word of the building phrase
      @param category category of the first word in the building phrase
    */
    PhraseCandidateData(const std::string& word,
                        const WordCategory& category)
        : noun_phrase(word),
          word_category(category) {
    }

    /** Building noun phrase */
    std::string noun_phrase;

    /** Category of the building noun phrase */
    WordCategory word_category;

    /** Length of the building noun phrase */
    unsigned int length = 1;
  };

  /**
    Extends candidate for noun phrases by new word
    @param previous_candidate_data data of the previous extending noun phrase
    @param word_properties properties of the new adding word
    @return new_noun_phrases vector where new formed candidates should be placed
  */
  void ExtendNounPhrases(
      const PhraseCandidateData& previous_candidate_data,
      const WordProperties& word_properties,
      std::vector<PhraseCandidateData>* new_noun_phrases) const;

  /** Maximum length of noun phrases for extraction */
  const unsigned int kMaximumNounPhrasesLength_;

  /** Vector containing candidates for noun phrases */
  std::vector<PhraseCandidateData> noun_phrases_;

  /** Hash map containing noun phrases and some data about them in the parsing
      file */
  std::unordered_map<std::string, NounPhraseData> noun_phrases_map_;
};
