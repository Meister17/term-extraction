// Copyright 2013 Michael Nokel
#pragma once

#include "../auxiliary_macros.h"
#include <string>
#include <unordered_map>

/**
  @brief Class for extracting single words from the parsing file

  This class should be used for extracting single words from the currently
  parsing file. For each thread that parses file its own instance of this class
  should be created. This instance should be used for accumulating single words
  and necessary information about them. Then accumulated information should be
  mereged with the previously extracted one from other threads and files.
*/
class SingleWordsExtractor {
 public:
  /**
    Returns iterator to the beginning of the map containing single words along
    with their frequencies
    @return iterator to the beginning of such map
  */
  std::unordered_map<std::string, unsigned int>::const_iterator
      GetStartIterator() const {
    return single_words_map_.begin();
  }

  /**
    Returns iterator to the end of the map containing single words along with
    their frequencies
    @return iterator to the end of such map
  */
  std::unordered_map<std::string, unsigned int>::const_iterator GetEndIterator()
      const {
    return single_words_map_.end();
  }

  /**
    Adds new word to the set of single words
    @param word_properties properties of the adding word
  */
  void AddNewWord(const WordProperties& word_properties) {
    if (word_properties.word_category == WordCategory::NOUN ||
        word_properties.word_category == WordCategory::ADJECTIVE) {
      auto inserted_result = single_words_map_.insert(
          {word_properties.word, kDefaultFrequency_});
      if (!inserted_result.second) {
        ++inserted_result.first->second;
      }
    }
  }

 private:
  /** Default frequency of the new words (equals to one) */
  const unsigned int kDefaultFrequency_ = 1;

  /** Hash map containing single words and their term frequencies */
  std::unordered_map<std::string, unsigned int> single_words_map_;
};
