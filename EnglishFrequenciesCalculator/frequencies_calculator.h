// Copyright 2013 Michael Nokel
#pragma once

#include "./auxiliary_macros.h"
#include "./candidates_accumulators/candidates_accumulator.h"
#include "./candidates_accumulators/noun_phrases_accumulator.h"
#include "./candidates_accumulators/two_word_terms_accumulator.h"
#include "./predefined_words_finder.h"
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <string>

class NounPhrasesExtractor;
class SingleWordsExtractor;
class SingleWordTermsExtractor;
class TwoWordTermsExtractor;

/**
  @brief Main class for calculating frequencies of various term candidates

  This class should be used for extracting various term candidates and some data
  about them.
*/
class FrequenciesCalculator {
 public:
  /**
    Initializes object
    @param minimum_term_frequency minimum term frequency to be taken into
    account
    @param context_terms_window_size size of context terms' window (for counting
    terms in the window of predefined ones)
    @param maximum_noun_phrases_length maximum length of noun phrases for
    extraction
    @param candidates_type candidates type for extracting
  */
  FrequenciesCalculator(unsigned int minimum_term_frequency,
                        unsigned int context_terms_window_size,
                        unsigned int maximum_noun_phrases_length,
                        CandidatesType candidates_type)
      : kMinimumTermFrequency_(minimum_term_frequency),
        kContextTermsWindowSize_(context_terms_window_size),
        kMaximumNounPhrasesLength_(maximum_noun_phrases_length),
        kCandidatesType_(candidates_type) {
  }

  /**
    Initializes object by parsing file with several predefined terms and
    stopwords
    @param predefined_terms_filename file with several predefined terms
    @param stop_words_filename file with several stop words
    @throw std::runtime_error in case of any occurred error
  */
  void Initialize(const std::string& predefined_terms_filename,
                  const std::string& stop_words_filename);

  /**
    Starts the process of calculating frequencies for various term candidates
    @param source_directory_name source directory containing files that should
    be processed
    @throw std::runtime_error in case of any occurred errors
  */
  void CalculateFrequencies(const std::string& source_directory_name);

  /**
    Prints all extracted term candidates in the given files
    @param single_word_terms_filename file where single-word term candidates
    should be printed
    @param single_word_terms_lda_input_filename file where input data for
    LDA/clustering for single-word term candidates will be printed
    @param single_word_terms_lda_vocabulary_filename file where vocabulary for
    LDA/clusterign for single-word term candidates will be printed
    @param single_word_phrases_filename file where found noun phrases for
    single-word term candidates will be printed
    @param single_words_filename file where single words and their frequencies
    will be printed
    @param two_word_terms_filename file where two-word term candidates should be
    printed
    @param two_word_terms_lda_input_filename file where input data for
    LDA/clustering for two-word term candidates will be printed
    @param two_word_terms_lda_vocabulary_filename file where vocabulary for
    LDA/clustering for two-word term candidates will be printed
    @param two_word_phrases_filename file where found noun phrases for two-word
    term candidates will be printed
    @throw std::runtime_error in case of any occurred errors
  */
  void PrintTermCandidates(
      const std::string& single_word_terms_filename,
      const std::string& single_word_terms_lda_input_filename,
      const std::string& single_word_terms_lda_vocabulary_filename,
      const std::string& single_word_phrases_filename,
      const std::string& single_words_filename,
      const std::string& two_word_terms_filename,
      const std::string& two_word_terms_lda_input_filename,
      const std::string& two_word_terms_lda_vocabulary_filename,
      const std::string& two_word_phrases_filename);

 private:
  /**
    Parses given text file
    @param filename name of file for parsing
    @param file_number number of processing file
    @throw std::runtime_error in case of any occurred errors
  */
  void ParseFile(const std::string& filename, size_t file_number);

  /**
    Determines category of extracted word
    @param word extracted word
    @param term_category string representation of the category of the extracted
    word
    @param[out] word_category category of the processed word
    @param[out] word_processing_data pointer o the structure containing all
    necessary information about processing word
  */
  void DetermineTermCategory(const std::string& word,
                             const std::string& term_category,
                             WordCategory& word_category,
                             WordProcessingData* word_processing_data) const;

  /**
    Accumulates data extracted from the parsed file
    @param single_words_extractor object used for extracting single words
    @param single_word_terms_extractor object used for extracting single-word
    term candidates
    @param two_word_terms_extractor object used for extracting two-word term
    candidates
    @param noun_phrases_extractor object used for extracting noun phrases
    @param file_number number of processing file
  */
  void AccumulateDataFromFile(
      const SingleWordsExtractor& single_words_extractor,
      const SingleWordTermsExtractor& single_word_terms_extractor,
      const TwoWordTermsExtractor& two_word_terms_extractor,
      const NounPhrasesExtractor& noun_phrases_extractor,
      size_t file_number);

  /** Number of threads to use in pool */
  const int kNumberOfThreads_ =
      static_cast<int>(boost::thread::hardware_concurrency());

  /** Minimum frequency of term candidate to make decision whether to take
      current term into account or not */
  const unsigned int kMinimumTermFrequency_;

  /** Size of context terms' window (for counting terms in the window of
      predefined ones) */
  const unsigned int kContextTermsWindowSize_;

  /** Maximum length of noun phrases for extraction */
  const unsigned int kMaximumNounPhrasesLength_;

  /** Types of candidates to extract */
  const CandidatesType kCandidatesType_;

  /** Object for finding several predefined terms among words in files */
  PredefinedWordsFinder predefined_terms_finder_;

  /** Object for finding stop words among words in files */
  PredefinedWordsFinder stop_words_finder_;

  /** Object for accumulating single-word term candidates */
  CandidatesAccumulator single_word_terms_accumulator_;

  /** Object for accumulating two-word term candidates */
  TwoWordTermsAccumulator two_word_terms_accumulator_;

  /** Object for accumulating noun phrases */
  NounPhrasesAccumulator noun_phrases_accumulator_;
};
