// Copyright 2013 Michael Nokel
#include "./program_options_parser.h"
#include <boost/program_options.hpp>
#include <iostream>
#include <stdexcept>
#include <string>

using namespace boost::program_options;
using std::cout;
using std::runtime_error;
using std::string;

void ProgramOptionsParser::Parse(int argc, char** argv) {
  variables_map program_options_storage;
  string configuration_filename;
  options_description program_options_description("Options for program:");
  program_options_description.add_options()
      ("help,h", value<bool>(&help_message_printed_)->implicit_value(true),
          "Print help message and exit")
      ("configuration_filename,c", value<string>(&configuration_filename),
          "Name of configuration file")
      ("minimum_term_frequency,f",
          value<unsigned int>(&minimum_term_frequency_),
          "Minimum term frequency to be taken into account")
      ("context_terms_window_size,w",
          value<unsigned int>(&context_terms_window_size_),
          "Size of context terms window")
      ("predefined_terms_filename,p",
          value<string>(&predefined_terms_filename_),
          "File containing several predefined terms")
      ("stop_words_filename", value<string>(&stop_words_filename_),
          "File containing several stop words")
      ("source_directory_name,d", value<string>(&source_directory_name_),
          "Source directory for scanning")
      ("single_words_filename", value<string>(&single_words_filename_),
          "File where single words and frequencies will be printed")
      ("single_word_terms_filename,s",
          value<string>(&single_word_terms_filename_),
          "File where single-word term candidates will be printed")
      ("single_word_terms_lda_input_filename",
          value<string>(&single_word_terms_lda_input_filename_),
          "File where single-word candidates data for LDA will be printed")
      ("single_word_terms_lda_vocabulary_filename",
          value<string>(&single_word_terms_lda_vocabulary_filename_),
          "File where single-word candidate vocabulary for LDA will be printed")
      ("two_word_terms_filename,t", value<string>(&two_word_terms_filename_),
          "Name of file where two-word term candidates will be printed")
      ("two_word_terms_lda_input_filename",
          value<string>(&two_word_terms_lda_input_filename_),
          "File where two-word candidates data for LDA will be printed")
      ("two_word_terms_lda_vocabulary_filename",
          value<string>(&two_word_terms_lda_vocabulary_filename_),
          "File where two-word candidate vocabulary for LDA will be printed")
      ("maximum_noun_phrases_length,m",
          value<unsigned int>(&maximum_noun_phrases_length_),
          "Maximum length of noun phrases for extraction")
      ("single_word_phrases_filename",
          value<string>(&single_word_phrases_filename_),
          "File where found noun phrases for single-word terms will be printed")
      ("two_word_phrases_filename",
          value<string>(&two_word_phrases_filename_),
          "File where found noun phrases for two-word terms will be printed");
  store(parse_command_line(argc, argv, program_options_description),
        program_options_storage);
  notify(program_options_storage);
  if (!configuration_filename.empty()) {
    store(parse_config_file<char>(configuration_filename.data(),
                                  program_options_description),
          program_options_storage);
    notify(program_options_storage);
  }
  if (help_message_printed_) {
    cout << program_options_description << "\n";
  }
  candidates_type_ = DetermineCandidatesType();
}

CandidatesType ProgramOptionsParser::DetermineCandidatesType() const {
  CandidatesType candidate_type;
  if (!single_word_terms_filename_.empty() &&
      !single_word_phrases_filename_.empty() &&
      !single_word_terms_lda_vocabulary_filename_.empty() &&
      !single_word_terms_lda_input_filename_.empty()) {
    candidate_type = CandidatesType::SINGLE_WORD_TERMS;
  }
  if (!two_word_terms_filename_.empty() &&
      !two_word_phrases_filename_.empty() &&
      !two_word_terms_lda_vocabulary_filename_.empty() &&
      !two_word_terms_lda_input_filename_.empty() &&
      !single_words_filename_.empty()) {
    if (candidate_type == CandidatesType::SINGLE_WORD_TERMS) {
      candidate_type = CandidatesType::ALL_TERMS;
    } else {
      candidate_type = CandidatesType::TWO_WORD_TERMS;
    }
  }
  if (candidate_type == CandidatesType::NO_CANDIDATES_TYPE) {
    throw runtime_error("Wrong type for candidates was specified");
  }
  return candidate_type;
}
