// Copyright 2013 Michael Nokel
#include "./two_word_terms_accumulator.h"
#include <boost/thread/mutex.hpp>
#include <fstream>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

using std::ofstream;
using std::runtime_error;
using std::string;
using std::unordered_map;
using std::vector;

void TwoWordTermsAccumulator::AccumulateSingleWords(
    unordered_map<string, unsigned int>::const_iterator start_iterator,
    unordered_map<string, unsigned int>::const_iterator end_iterator) {
  if (start_iterator != end_iterator) {
    boost::mutex::scoped_lock lock(single_words_mutex_);
    for (auto iterator = start_iterator; iterator != end_iterator; ++iterator) {
      total_number_of_single_words_ += iterator->second;
      auto inserted_result = single_words_map_.insert(*iterator);
      if (!inserted_result.second) {
        inserted_result.first->second += iterator->second;
      }
    }
  }
}

void TwoWordTermsAccumulator::PrintSingleWords(
    const string& filename,
    unsigned int minimum_term_frequency) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print single-word term candidates");
  }
  file << total_number_of_single_words_ << "\n";
  for (const auto& element: single_words_map_) {
    if (element.second >= minimum_term_frequency) {
      file << element.first << "\t" << element.second << "\n";
    }
  }
  file.close();
}
