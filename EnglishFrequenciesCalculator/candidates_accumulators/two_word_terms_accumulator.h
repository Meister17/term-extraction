// Copyright 2013 Michael Nokel
#pragma once

#include "./candidates_accumulator.h"
#include <boost/thread/mutex.hpp>
#include <string>
#include <unordered_map>
#include <vector>

/**
  @brief Class intended to be used for accumulating and printing two-word term
  candidates

  This class accumulates two-word term candidates and necessary information
  about them while parsing files in separate threads. After parsing all files,
  accumulated information is printed to the specified file.
*/
class TwoWordTermsAccumulator : public CandidatesAccumulator {
 public:
  /**
    Accumulates previously extracted single words and their frequencies
    @param start_iterator iterator to the beginning of the accumulating map
    @param end_iterator iterator to the end of the accumulating map
  */
  void AccumulateSingleWords(
      std::unordered_map<std::string, unsigned int>::const_iterator
          start_iterator,
      std::unordered_map<std::string, unsigned int>::const_iterator
          end_iterator);

  /**
    Prints extracted single words along with their frequencies in the given file
    @param filename file where extracted single words should be printed
    @param minimum_term_frequency minimum sum frequency to be taken into account
    @throw std::runtime_error in case of any occurred errors
  */
  void PrintSingleWords(const std::string& filename,
                        unsigned int minimum_term_frequency) const;

 private:
  /** Mutex for synchronization accumulating map with single words and their
      frequencies */
  boost::mutex single_words_mutex_;

  /** Total number of extracted single words */
  unsigned int total_number_of_single_words_ = 0;

  /** Hash map containing single words along with their term frequencies */
  std::unordered_map<std::string, unsigned int> single_words_map_;
};