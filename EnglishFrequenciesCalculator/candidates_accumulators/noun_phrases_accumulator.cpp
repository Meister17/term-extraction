// Copyright 2013 Michael Nokel
#include "./noun_phrases_accumulator.h"
#include <boost/thread/mutex.hpp>
#include <fstream>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

using std::ofstream;
using std::pair;
using std::runtime_error;
using std::string;
using std::unordered_map;
using std::vector;

void NounPhrasesAccumulator::Accumulate(
    unordered_map<string, NounPhraseData>::const_iterator start_iterator,
    unordered_map<string, NounPhraseData>::const_iterator end_iterator) {
  if (start_iterator != end_iterator) {
    boost::mutex::scoped_lock lock(mutex_);
    for (auto iterator = start_iterator; iterator != end_iterator; ++iterator) {
      if (iterator->second.length == kLengthForSingleWordTerms_) {
        total_number_of_single_word_phrases_ += iterator->second.term_frequency;
        InsertNewPhrase(iterator->first,
                        iterator->second.term_frequency,
                        &single_word_phrases_map_);
      } else if (iterator->second.length == kLengthForTwoWordTerms_) {
        total_number_of_two_word_phrases_ += iterator->second.term_frequency;
        InsertNewPhrase(iterator->first,
                        iterator->second.term_frequency,
                        &two_word_phrases_map_);
      } else {
        throw runtime_error("Found noun phrase of the big length");
      }
    }
  }
}

void NounPhrasesAccumulator::PrintSingleWordNounPhrases(
    const string& phrases_filename,
    unsigned int minimum_term_frequency) const {
  PrintPhrases(phrases_filename,
               total_number_of_single_word_phrases_,
               single_word_phrases_map_,
               minimum_term_frequency);
}

void NounPhrasesAccumulator::PrintTwoWordNounPhrases(
    const string& phrases_filename,
    unsigned int minimum_term_frequency) const {
  PrintPhrases(phrases_filename,
               total_number_of_two_word_phrases_,
               two_word_phrases_map_,
               minimum_term_frequency);
}

void NounPhrasesAccumulator::InsertNewPhrase(
    const std::string& phrase,
    unsigned int term_frequency,
    unordered_map<string, unsigned int>* noun_phrases_map) const {
  auto inserted_result = noun_phrases_map->insert({phrase, term_frequency});
  if (!inserted_result.second) {
    inserted_result.first->second += term_frequency;
  }
}

void NounPhrasesAccumulator::PrintPhrases(
    const string& filename,
    unsigned int total_number_of_phrases,
    const unordered_map<string, unsigned int>& phrases_map,
    unsigned int minimum_term_frequency) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print extracted phrases");
  }
  auto noun_phrases_vector = SortByTermFrequencies(phrases_map);
  file << total_number_of_phrases << "\n";
  for (const auto& element: noun_phrases_vector) {
    if (element.second < minimum_term_frequency) {
      break;
    }
    file << element.first << "\t" << element.second << "\n";
  }
  file.close();
}

vector<pair<string, unsigned int> >
    NounPhrasesAccumulator::SortByTermFrequencies(
        const unordered_map<string, unsigned int>& phrases_map) const {
  vector<pair<string, unsigned int> > phrases_vector(phrases_map.begin(),
                                                     phrases_map.end());
  sort(phrases_vector.begin(),
       phrases_vector.end(),
       [&](const pair<string, unsigned int>& first,
           const pair<string, unsigned int>& second)
           {return first.second > second.second;});
  return phrases_vector;
}
